#!/usr/bin/env bash

docker run --rm \
  --network=${YLITSE_CONTAINER_STACK:-ylitse-local}_default \
  -v $(pwd)/ylitse:/app/ylitse \
  -v $(pwd)/ylitse_conf:/app/ylitse_conf \
  -v $(pwd)/tests:/app/tests \
  -v $(pwd)/scripts:/app/scripts \
  -v $(pwd)/.pycodestyle:/app/.pycodestyle \
  -v $(pwd)/.pylintrc:/app/.pylintrc \
  -v $(pwd)/.yamllintrc:/app/.yamllintrc \
  -v $(pwd)/alembic:/app/alembic \
  ylitse $@
