#!/usr/bin/env bash

if [[ "$*" == *"run-gunicorn"* ]]; then
    echo "run-gunicorn: checking /tmp/ylitse.pid"
    pkill -F /tmp/ylitse.pid && rm /tmp/ylitse.pid;
fi

. env/bin/activate
exec "$@"
