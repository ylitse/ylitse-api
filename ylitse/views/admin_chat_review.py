# pylint: disable=too-many-locals,line-too-long
"""messages module.

Contains `/messages` endpoint implementation.
"""

import datetime
import json
import os
import time
import jsonschema

import jwt
from aiohttp import web

from ylitse.store_abstraction import StoreView
from ylitse.utils import fst
from ylitse.data_definitions import data_definitions

from ..crud import crud_message, crud_contact, crud_chat_review_log

service_account = (
    "firebase-adminsdk-m87ag@ylitse-notifications.iam.gserviceaccount.com"  # noqa
)
default_path = (
    "/etc/ylitse/ylitse-notifications-firebase-adminsdk-m87ag-99e29d5c6d.json"  # noqa
)
file_path = os.getenv("FIREBASE_CONF", default_path)


def auth_token():
    scopes = [
        "https://www.googleapis.com/auth/firebase.messaging",
        "https://www.googleapis.com/auth/cloud-platform",
    ]
    iat = int(time.time())

    payload = {
        "iss": service_account,
        "scope": " ".join(scopes),
        "aud": "https://www.googleapis.com/oauth2/v4/token",
        "exp": iat + (60 * 30),
        "iat": iat,
    }

    with open(file_path, "r", encoding="utf-8") as f:
        read_json = json.load(f)
        private_key_str = read_json["private_key"]
        private_key_bytes = private_key_str.encode("utf-8")

        return jwt.encode(payload, private_key_bytes, algorithm="RS256")


class AdminMessagesView:
    def __init__(self, store):
        self.store = store
        self.data_type = "MESSAGE"
        self.match = "message_id"
        self.users = StoreView(store, "USER")
        self.quota = os.getenv("REVIEW_QUOTA", "10")
        self.accounts = StoreView(store, "ACCOUNT")

        self.refresh_token = None

        try:
            self.refresh_token = auth_token()
        except FileNotFoundError:
            print("Warn: Firebase config was not found but continuing...")

    async def get_json(self, data):
        """Get some good json."""
        data_type = "CHAT_REVIEW_LOG"
        schema = data_definitions[data_type]["schema"]

        try:
            jsonschema.validate(data, schema)
        except jsonschema.exceptions.ValidationError as e:
            print(data)
            print(schema)
            print()
            print(data_type)
            print()
            print(e)
            print(data_definitions)
            raise web.HTTPBadRequest

        raise_if_extra_json_properties(data_type=data_type, schema=schema, data=data)

        return data

    async def get(self, request):
        reviewer_id = request["token"]["scopes"]["user_id"]
        user_id = request.match_info["user_id"]
        contact_id = request.query.get("contact_user_ids")
        await self.check_review_user_and_quota(reviewer_id, user_id, contact_id)

        contacts = await crud_contact.get_mine(
            self.store, user_id=user_id, ignore_deleted=False
        )

        from_message = await self.parse_message_id_and_get_message(
            user_id=user_id, message_id=request.query.get("from_message_id")
        )
        contact_user_ids = await self.parse_contact_user_ids(
            request.query.get("contact_user_ids"), all_contacts=contacts
        )
        is_desc = self.parse_boolean_from_string(request.query.get("desc"))

        # "desc" is required if given "from_*" filters or contact_user_ids
        if contact_user_ids and is_desc is None:
            raise web.HTTPBadRequest
        if from_message and is_desc is None:
            raise web.HTTPBadRequest

        max_messages = self.parse_positive_nonzero_int_from_string(
            request.query.get("max")
        )

        messages = await crud_message.get_mine(
            self.store,
            user_id=user_id,
            all_contacts=contacts,
            contact_user_ids=contact_user_ids,
            from_message=from_message,
            is_desc=is_desc,
            max_messages=max_messages,
        )

        data_object = {
            "chat_user_id_1": user_id,
            "chat_user_id_2": contact_user_ids[0],
            "reviewer_user_id": reviewer_id,
        }

        data = await self.get_json(data_object)

        put_log_response = await crud_chat_review_log.post(self.store, data)
        if not put_log_response:
            raise web.HTTPBadRequest

        return web.json_response(
            {
                "resources": self.patch_timezone(messages),
                "contacts": crud_contact.ignore_deleted(contacts),
            }
        )

    async def get_contacts(self, request):
        user_id = request.match_info["user_id"]
        contacts = await crud_contact.get_mine(
            self.store, user_id=user_id, ignore_deleted=False
        )
        active_contacts = crud_contact.ignore_deleted(contacts)

        contacts_with_user = []

        for contact in active_contacts:
            contacts_with_user.insert(1, await self.add_user_to_contact(contact))

        return web.json_response({"resources": contacts_with_user})

    async def check_review_user_and_quota(self, reviewer_id, user_id, contact_id):
        reviewer_list = await self.users.read(id=reviewer_id)
        reviewer = fst(reviewer_list)
        if reviewer["role"] != "admin":
            raise web.HTTPBadRequest
        all_logs = await crud_chat_review_log.get_all_logs(self.store)
        entry_user_names = []
        for log in all_logs:
            if log["reviewer_user_id"] == reviewer_id:
                difference = datetime.datetime.now() - datetime.datetime.strptime(
                    log["created"], "%Y-%m-%dT%H:%M:%S.%f"
                )
                if (
                    difference.days == 0
                    and [log["chat_user_id_1"], log["chat_user_id_2"]]
                    not in entry_user_names
                ):
                    entry_user_names.append(
                        [log["chat_user_id_1"], log["chat_user_id_2"]]
                    )
        if (
            len(entry_user_names) >= int(self.quota)
            and [user_id, contact_id] not in entry_user_names
            and [contact_id, user_id] not in entry_user_names
        ):
            raise web.HTTPBadRequest(reason="quota exceeded")

    async def add_user_to_contact(self, contact):
        contact["user"] = fst(await self.users.read(account_id=contact["account_id"]))
        contact["account"] = fst(await self.accounts.read(id=contact["account_id"]))
        return contact

    async def parse_message_id_and_get_message(self, *, user_id, message_id):
        """Return message or raise web.HTTPBadRequest"""
        if not message_id:
            return None

        msg = await crud_message.get_by_id(
            self.store, user_id=user_id, message_id=message_id
        )
        if not msg:
            raise web.HTTPBadRequest

        return msg

    async def parse_contact_user_ids(self, contact_user_ids, all_contacts):
        """Return list of user_ids if they exists
        or raise web.HTTPBadRequest"""
        if not contact_user_ids:
            return []

        user_ids = contact_user_ids.split(",")

        validated_user_ids = []
        for user_id in user_ids:
            contact = [c for c in all_contacts if c["id"] == user_id]
            if contact:
                validated_user_ids.append(user_id)

        # If cannot find any valid user_ids
        if len(validated_user_ids) == 0:
            raise web.HTTPBadRequest

        return validated_user_ids

    def parse_boolean_from_string(self, boolean_string):
        """Return True, False, None or raise web.HTTPBadRequest"""
        if boolean_string in [None, ""]:
            return None

        try:
            return self.str_to_bool(boolean_string)
        except ValueError:
            raise web.HTTPBadRequest

    def str_to_bool(self, value):
        """
        Return whether the provided string
        represents true/false. Otherwise raise ValueError.
        """
        trues = ("y", "yes", "t", "true", "on", "1")
        falses = ("n", "no", "f", "false", "off", "0")

        if str(value).lower() in trues:
            return True

        if str(value).lower() in falses:
            return False

        raise ValueError

    def parse_positive_nonzero_int_from_string(self, int_string):
        """Return None, integer or raise web.HTTPBadRequest"""
        if not int_string:
            return None

        try:
            num = int(int_string)
            if num <= 0:
                raise ValueError
            return num
        except ValueError:
            raise web.HTTPBadRequest

    def patch_timezone(self, resources):
        """
        Add UTC timezone to created and updated.
        Delete this function when timezones are added to data someday.
        """
        resources_with_timezone = []
        for r in resources:
            r["updated"] = f"{r['updated']}+00:00"
            r["created"] = f"{r['created']}+00:00"
            resources_with_timezone.append(r)

        return resources_with_timezone


def raise_if_extra_json_properties(*, data_type, schema, data):
    """
    Fail the request if there are properties
    not defined in data_definitions
    """
    expected_properties = set(schema["properties"].keys())
    unexpected_properties = [x for x in data.keys() if x not in expected_properties]
    if any(unexpected_properties):
        raise web.HTTPUnprocessableEntity
