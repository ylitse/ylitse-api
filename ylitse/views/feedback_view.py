"""feedback module.

Contains `/feedback` endpoint implementation.
"""

from datetime import datetime

from aiohttp import web
from ylitse.utils import fst, get_date
from ylitse.store_abstraction import StoreView
from ..crud import crud_feedback_answer, crud_feedback_question
from ..schema import (
    one_via_schema,
    FeedbackAnswerCreate,
    FeedbackQuestionCreate,
)

# pylint: disable=missing-docstring


class FeedBackView:
    def __init__(self, store):
        self.store = store
        self.feedback_answers = StoreView(store, "FEEDBACK_ANSWER")

    async def post_question(self, request):
        client_json = one_via_schema(FeedbackQuestionCreate, await request.json())
        await crud_feedback_question.create(self.store, client_json)
        return web.Response(status=201)

    async def get_needs_answers(self, request):
        user_id = request["token"]["scopes"]["user_id"]

        return web.json_response(
            {
                "resources": await crud_feedback_question.fetch_all_needing_answers(
                    self.store, user_id=user_id
                )
            }
        )

    async def post_answer(self, request):
        user_id = request["token"]["scopes"]["user_id"]

        answer = one_via_schema(FeedbackAnswerCreate, await request.json())

        old_answer = fst(await self.feedback_answers.read(id=answer["answer_id"]))
        if not old_answer:
            raise web.HTTPNotFound

        await crud_feedback_answer.answer(
            self.store,
            user_id=user_id,
            feedback_answer_id=answer["answer_id"],
            value=answer["value"],
        )
        return web.Response(status=201)

    async def get_questions(self, request):
        return web.json_response(
            {
                "resources": await crud_feedback_question.list(
                    self.store,
                )
            }
        )

    async def get_answers(self, request):
        date_format = "%Y-%m-%d"
        parsed_start = get_date(request.query.get("start"), "1970-1-2", date_format)
        parsed_end = get_date(
            request.query.get("end"),
            datetime.utcnow().strftime(date_format),
            date_format,
        )

        if not parsed_start[0] or not parsed_end[0]:
            return web.Response(status=400)

        answers = await crud_feedback_answer.get_answers(
            self.store, parsed_start[1], parsed_end[1]
        )
        csv = crud_feedback_answer.to_csv(answers)

        return web.Response(text=csv, content_type="text/csv")

    async def delete_question(self, request):
        feedback_questions = StoreView(self.store, "FEEDBACK_QUESTION")
        question_id = request.match_info["id"]

        question = fst(await feedback_questions.read(id=question_id))
        if not question:
            raise web.HTTPNotFound
        await crud_feedback_question.delete(self.store, question_id=question_id)

        return web.Response(status=204)
