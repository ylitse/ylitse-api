from aiohttp import web
from ylitse.utils import fst
from ylitse.store_abstraction import StoreView
from ylitse.json_validation import get_json
from ..crud import crud_report
from ..crud import crud_message


class Reports:
    def __init__(self, store):
        self.store = store
        self.data_type = "REPORTED_USERS"
        self.match = "report_id"
        self.reports = StoreView(store, self.data_type)
        self.mentors = StoreView(store, "MENTOR")
        self.messages = StoreView(store, "MESSAGE")

    async def get_json(self, request):
        return await get_json(
            request, store=self.store, data_type=self.data_type, match_info=self.match
        )

    async def get_reports(self, request):
        response = await crud_report.get_all_reports(self.store)
        return web.json_response({"resources": response})

    async def post_report(self, request):
        try:
            client_json = await self.get_json(request)
            client_json["status"] = "received"
            client_json["comment"] = ""

            reported_id = client_json["reported_user_id"]
            reported_list = await self.mentors.read(user_id=reported_id)
            reported = fst(reported_list)
            if not reported:
                raise web.HTTPBadRequest

            # checked_values=await self.check_message_id_user_ids(client_json)
            # if checked_values:
            stored_json = await crud_report.post(self.store, data=client_json)
            return web.json_response(stored_json, status=201)

        except Exception:
            raise web.HTTPBadRequest

    async def delete_report(self, request):
        report_id = request.match_info["report_id"]
        if not report_id:
            raise web.HTTPBadRequest

        response = await crud_report.delete(
            self.store,
            report_id=report_id,
        )

        return web.json_response(response, status=204)

    async def patch_report(self, request):
        report_id = request.match_info["report_id"]
        if not report_id:
            raise web.HTTPBadRequest

        json = await request.json()
        if not json or not crud_report.is_allowed_status(json.get("status")):
            raise web.HTTPBadRequest

        await crud_report.update_report(
            self.store,
            report_id=report_id,
            status=json["status"],
            comment=json["comment"],
        )

        response = await crud_report.get_report_by_id(self.store, report_id=report_id)
        if not response:
            raise web.HTTPBadRequest
        return web.json_response({"resources": response})

    async def get_by_id(self, request):
        report_id = request.match_info["report_id"]
        if not report_id:
            raise web.HTTPBadRequest

        response = await crud_report.get_report_by_id(self.store, report_id=report_id)
        if not response:
            raise web.HTTPBadRequest
        return web.json_response({"resources": response})

    def parse_boolean_from_string(self, boolean_string):
        boolean_map = {
            "true": True,
            "false": False,
        }
        return boolean_map.get(boolean_string.lower())

    async def check_message_id_user_ids(self, client_json):
        message = await crud_message.get_by_id(
            self.store,
            user_id=client_json["reporter_user_id"],
            message_id=client_json["reported_message_id"],
        )
        if (
            message
            and message["id"] == client_json["reported_message_id"]
            and message["sender_id"] == client_json["reported_user_id"]
            and message["recipient_id"] == client_json["reporter_user_id"]
        ):
            return True
        return False
