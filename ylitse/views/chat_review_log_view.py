from aiohttp import web
from ylitse.store_abstraction import StoreView
from ylitse.json_validation import get_json
from ..crud import crud_chat_review_log


class ChatReviewLog:
    def __init__(self, store):
        self.store = store
        self.match = "chat_review_log_id"
        self.data_type = "CHAT_REVIEW_LOG"
        self.logs = StoreView(store, self.data_type)
        self.mentors = StoreView(store, "MENTOR")
        self.messages = StoreView(store, "MESSAGE")

    async def get_json(self, request):
        return await get_json(
            request, store=self.store, data_type=self.data_type, match_info=self.match
        )

    async def get(self, request):
        response = await crud_chat_review_log.get_all_logs(self.store)
        return web.json_response({"resources": response})

    async def post(self, request):
        try:
            client_json = await self.get_json(request)

            stored_json = await crud_chat_review_log.post(self.store, data=client_json)
            return web.json_response(stored_json, status=201)

        except Exception:
            raise web.HTTPBadRequest
