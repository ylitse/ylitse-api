# pylint: disable=too-many-locals,too-many-lines
"""
Generate anonymized stats about the service.
"""
import io
import csv
from datetime import datetime, timedelta
import calendar

from pydantic import BaseModel

from pytz import timezone as pytz_timezone

from dateutil.relativedelta import relativedelta

timezone_helsinki = pytz_timezone("Europe/Helsinki")


class ChatForStat(BaseModel):
    anon_mentee_user_id: str
    anon_mentor_user_id: str
    first_created: int = 0
    response_created: int = 0
    message_events: list[int] = []
    chat_length: int = 0


def new_chats_for_period(*, chats, start, end):
    return list(
        filter(
            lambda x: x.first_created >= start and x.first_created <= end,
            chats,
        )
    )


def new_chat_count(*, chats, start, end):
    return len(new_chats_for_period(chats=chats, start=start, end=end))


def active_chat_count(*, chats, start, end):
    active_chats = active_chats_for_period(chats=chats, start=start, end=end)
    return len(active_chats)


def active_chats_for_period(*, chats, start, end):
    all_chats = new_chats_for_period(chats=chats, start=0, end=end)
    return list(
        filter(lambda x: any(events_in_period(x.message_events, start, end)), all_chats)
    )


def events_in_period(events, start, end):
    return list(filter(lambda x: start <= x <= end, events))


def average_chat_message_count(*, chats, start, end):
    active_chats = active_chats_for_period(chats=chats, start=start, end=end)
    if len(active_chats) == 0:
        return 0

    message_counts = list(
        map(lambda x: len(events_in_period(x.message_events, start, end)), active_chats)
    )
    return round(sum(message_counts) / len(active_chats))


def answered(*, chats, start, end):
    return list(
        filter(
            lambda x: x.response_created > 0,
            new_chats_for_period(chats=chats, start=start, end=end),
        )
    )


def unanswered(*, chats, start, end):
    return list(
        filter(
            lambda x: x.response_created == 0,
            new_chats_for_period(chats=chats, start=start, end=end),
        )
    )


def unanswered_count(*, chats, start, end):
    return len(unanswered(chats=chats, start=start, end=end))


def average_response_time(*, chats, start, end):
    c = answered(chats=chats, start=start, end=end)
    if len(c) == 0:
        return ""

    response_times = list(map(lambda x: x.response_created - x.first_created, c))
    return str(timedelta(seconds=round(sum(response_times) / len(c))))


def daterange_inclusive(start_date, end_date):
    for n in range(int((end_date - start_date).days + 1)):
        yield start_date + timedelta(n)


def monthrange_inclusive(start_date, end_date):
    currentDate = start_date
    while currentDate < end_date:
        yield currentDate
        currentDate += relativedelta(months=+1)


def weekrange_inclusive(start_date, end_date):
    currentDate = start_date
    while currentDate < end_date + relativedelta(weeks=+1):
        yield currentDate
        currentDate += relativedelta(weeks=+1)


async def calc_joined(database, start, end):
    stats = {
        "account": 0,
        "admin": 0,
        "mentor": 0,
        "mentee": 0,
    }

    query = """ SELECT role, count(*) as count
            FROM stat_accounts
            WHERE created BETWEEN :start AND :end
            GROUP BY role """
    values = {"start": start, "end": end}
    counts = await database.fetch_all(query=query, values=values)
    for c in counts:
        stats[c["role"]] = c["count"]
        stats["account"] += c["count"]

    return stats


async def calc_deleted_accounts(database, start, end):
    stats = {
        "account": 0,
        "admin": 0,
        "mentor": 0,
        "mentee": 0,
    }

    query = """ SELECT role, count(*) as count
            FROM stat_accounts
            WHERE deleted BETWEEN :start AND :end AND
            deleted IS NOT NULL GROUP BY role """
    values = {
        "start": start,
        "end": end,
    }
    counts = await database.fetch_all(query=query, values=values)

    for c in counts:
        stats[c["role"]] = c["count"]
        stats["account"] += c["count"]

    return stats


async def calc_messages(database, start, end):
    stats = {
        "account": 0,
        "admin": 0,
        "mentor": 0,
        "mentee": 0,
    }

    query = """ SELECT from_role as role, count(*) as count
            FROM stat_messages
            WHERE created BETWEEN :start AND :end
            GROUP BY from_role """
    values = {"start": start, "end": end}
    counts = await database.fetch_all(query=query, values=values)

    for c in counts:
        stats[c["role"]] = c["count"]
        stats["account"] += c["count"]

    return stats


async def calc_mentors(database, end):
    """
    How many mentors are in the system.
    Meaning that accounts are there but dont need to be active.

    In practice, count mentors from the last day of the given period.
    """

    query = """
            SELECT COUNT(*) AS count
            FROM stat_mentors
            WHERE created <= :end
            AND (deleted IS NULL OR deleted >= :end)
            """
    values = {"end": end}
    res = await database.fetch_one(query=query, values=values)

    return res["count"]


async def calc_active_accounts(database, start, end):
    stats = {
        "account": 0,
        "admin": 0,
        "mentor": 0,
        "mentee": 0,
    }

    query = """ SELECT from_role AS role, COUNT(DISTINCT sender_id) AS count
            FROM stat_messages
            WHERE created BETWEEN :start AND :end
            GROUP BY from_role """
    values = {"start": start, "end": end}
    counts = await database.fetch_all(query=query, values=values)

    for c in counts:
        stats[c["role"]] = c["count"]
        stats["account"] += c["count"]

    return stats


async def calc_active_chats(database, start, end):
    """
    How many chats are active during a period.

    Active means that mentor has responsed once,
    but after that any message is counted towards activity
    """

    # Find all the messages that mentors have sent.
    # Make up unique chat_id (recipient_id + sender_id)
    # and use that to group messages.
    # intersect with other query that selects any message in the time period
    query = """
        SELECT COUNT(*) AS count FROM (
            SELECT concat(sender_id, recipient_id) as chat_id
            FROM stat_messages
            WHERE created <= :end
            AND from_role = 'mentor'
            GROUP BY chat_id

            INTERSECT

            SELECT CASE
                WHEN from_role = 'mentee'
                    THEN concat(recipient_id, sender_id)
                ELSE
                    concat(sender_id, recipient_id)
                END as chat_id
            FROM stat_messages
            WHERE created BETWEEN :start and :end
            GROUP BY chat_id
            ) I
            """
    values = {"start": start, "end": end}
    result = await database.fetch_one(query=query, values=values)

    return result["count"]


async def calc_time_of_day_for_messages(database, start, end):
    time_of_days = []

    query = """ SELECT time_of_day_fi, count(*) as count FROM stat_messages
            WHERE created BETWEEN :start and :end
            GROUP by time_of_day_fi
            ORDER BY count DESC, time_of_day_fi ASC """
    values = {"start": start, "end": end}
    results = await database.fetch_all(query=query, values=values)

    for r in results:
        time_of_days.append({"hour": r["time_of_day_fi"], "count": r["count"]})

    return time_of_days


async def calc_top_skills(database, start, end, *, limit):
    stats = []

    query = """
        WITH skills_counter
        AS (
            SELECT jsonb_array_elements_text(props->'skills') AS skill
            FROM stat_events
            WHERE
             name = :name
            AND
             created BETWEEN :start AND :end
        )
        SELECT skill, COUNT(skill) AS times_referenced
        FROM skills_counter
        GROUP BY skill
        ORDER BY
         times_referenced DESC,
         skill ASC
        LIMIT :limit;
        """
    values = {"name": "filter_skills", "start": start, "end": end, "limit": limit}
    counts = await database.fetch_all(query=query, values=values)

    for c in counts:
        stats.append(f'{c["skill"]} {c["times_referenced"]}')

    return "; ".join(stats)


async def calc_chat_stats(database):
    chats = {}
    # find all the unique sender_id, recipient_id pairs
    query = """
        SELECT sender_id, recipient_id
        FROM stat_messages
        WHERE from_role='mentee'
        GROUP BY sender_id, recipient_id;
        """
    res = await database.fetch_all(query=query)

    for r in res:
        chat_id = r["sender_id"] + r["recipient_id"]
        chats[chat_id] = ChatForStat(
            anon_mentee_user_id=r["sender_id"],
            anon_mentor_user_id=r["recipient_id"],
        )

    # get all messages in descending order, so first message and response are last
    for c in chats:  # pylint: disable=consider-using-dict-items
        mentee_id = chats[c].anon_mentee_user_id
        mentor_id = chats[c].anon_mentor_user_id
        query = """
            (SELECT sender_id, recipient_id, created
            FROM stat_messages
            WHERE
             sender_id=:mentee_id
             AND
             recipient_id=:mentor_id
            ORDER BY created DESC
            )

            UNION ALL

            (SELECT sender_id, recipient_id, created
            FROM stat_messages
            WHERE
             sender_id=:mentor_id
             AND
             recipient_id=:mentee_id
            ORDER BY created DESC
            )
            """
        values = {
            "mentee_id": mentee_id,
            "mentor_id": mentor_id,
        }
        res = await database.fetch_all(query=query, values=values)
        chat_id = mentee_id + mentor_id
        chats[chat_id].chat_length = len(res)
        events = list(map(lambda x: x["created"], res))
        chats[chat_id].message_events = events

        for r in res:
            # the last items will be the first items
            if r["sender_id"] == mentee_id:
                chats[chat_id].first_created = r["created"]
            elif r["sender_id"] == mentor_id:
                chats[chat_id].response_created = r["created"]
            else:
                raise RuntimeError("Dont know what to do")

    return chats


def csv_header(first_column):
    return [
        first_column,
        "Accounts",
        "Admins",
        "Mentors",
        "Mentees",
        "Accounts joined",
        "Admins joined",
        "Mentors joined",
        "Mentees joined",
        "Accounts deleted",
        "Admins deleted",
        "Mentors deleted",
        "Mentees deleted",
        "Mentors in system",
        "Active Mentors",
        "Active Mentees",
        "Messages from mentees",
        "Messages from mentors",
        "Messages from mentees cumulative",
        "Messages from mentors cumulative",
        "Most active hour fi",
        "Top skills",
        "Active chats",
        "New chats",
        "Cumulative chats",
        "Avg msg count / chat / period",
        "Avg msg count / chat / total",
        "Avg first response time / period",
        "Avg first response time / total",
        "Unanswered / period",
        "Unanswered / total",
    ]


class StatsReader:
    def __init__(self):
        self.skills_limit = 5

    async def daily_csv(self, store, period_start_date, period_end_date):
        today = datetime.utcnow().date()

        output = io.StringIO()
        writer = csv.writer(
            output, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        writer.writerow(csv_header("Date"))

        chats = (await calc_chat_stats(store.database)).values()

        for d in daterange_inclusive(period_start_date, period_end_date):
            # cannot generate stats for the current date or beyond
            if today == d:
                break

            day_start_timestamp = round(
                timezone_helsinki.localize(
                    datetime(d.year, d.month, d.day, hour=0, minute=0, second=0)
                ).timestamp()
            )

            day_end_timestamp = round(
                timezone_helsinki.localize(
                    datetime(d.year, d.month, d.day, hour=23, minute=59, second=59)
                ).timestamp()
            )

            joined = await calc_joined(
                store.database, day_start_timestamp, day_end_timestamp
            )
            joined_cumulative = await calc_joined(store.database, 0, day_end_timestamp)

            accounts_deleted = await calc_deleted_accounts(
                store.database, day_start_timestamp, day_end_timestamp
            )

            active_accounts = await calc_active_accounts(
                store.database, day_start_timestamp, day_end_timestamp
            )

            messages = await calc_messages(
                store.database, day_start_timestamp, day_end_timestamp
            )
            messages_cumulative = await calc_messages(
                store.database, 0, day_end_timestamp
            )

            active_hours = await calc_time_of_day_for_messages(
                store.database, day_start_timestamp, day_end_timestamp
            )
            most_active_hour_fi = ""
            if len(active_hours) > 0:
                most_active_hour_fi = active_hours[0]["hour"]

            top_skills = await calc_top_skills(
                store.database,
                day_start_timestamp,
                day_end_timestamp,
                limit=self.skills_limit,
            )

            writer.writerow(
                [
                    f"{d.strftime('%Y-%m-%d')}",
                    joined_cumulative["account"],
                    joined_cumulative["admin"],
                    joined_cumulative["mentor"],
                    joined_cumulative["mentee"],
                    joined["account"],
                    joined["admin"],
                    joined["mentor"],
                    joined["mentee"],
                    accounts_deleted["account"],
                    accounts_deleted["admin"],
                    accounts_deleted["mentor"],
                    accounts_deleted["mentee"],
                    await calc_mentors(store.database, day_end_timestamp),
                    active_accounts["mentor"],
                    active_accounts["mentee"],
                    messages["mentee"],
                    messages["mentor"],
                    messages_cumulative["mentee"],
                    messages_cumulative["mentor"],
                    most_active_hour_fi,
                    top_skills,
                    await calc_active_chats(
                        store.database, day_start_timestamp, day_end_timestamp
                    ),
                    new_chat_count(
                        chats=chats, start=day_start_timestamp, end=day_end_timestamp
                    ),
                    new_chat_count(chats=chats, start=0, end=day_end_timestamp),
                    average_chat_message_count(
                        chats=chats, start=day_start_timestamp, end=day_end_timestamp
                    ),
                    average_chat_message_count(
                        chats=chats, start=0, end=day_end_timestamp
                    ),
                    average_response_time(
                        chats=chats, start=day_start_timestamp, end=day_end_timestamp
                    ),
                    average_response_time(chats=chats, start=0, end=day_end_timestamp),
                    unanswered_count(
                        chats=chats, start=day_start_timestamp, end=day_end_timestamp
                    ),
                    unanswered_count(chats=chats, start=0, end=day_end_timestamp),
                ]
            )

        return output.getvalue()

    async def monthly_csv(self, store, period_start_date, period_end_date):
        today = datetime.utcnow().date()

        output = io.StringIO()
        writer = csv.writer(
            output, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        writer.writerow(csv_header("Month"))

        chats = (await calc_chat_stats(store.database)).values()

        for d in monthrange_inclusive(period_start_date, period_end_date):
            # cannot generate stats for the current date or beyond
            if today == d:
                break

            first_of_month_timestamp = round(
                timezone_helsinki.localize(
                    datetime(d.year, d.month, 1, hour=0, minute=0, second=0)
                ).timestamp()
            )

            last_of_month_timestamp = round(
                timezone_helsinki.localize(
                    datetime(
                        d.year,
                        d.month,
                        calendar.monthrange(d.year, d.month)[1],
                        hour=23,
                        minute=59,
                        second=59,
                    )
                ).timestamp()
            )

            joined = await calc_joined(
                store.database, first_of_month_timestamp, last_of_month_timestamp
            )
            joined_cumulative = await calc_joined(
                store.database, 0, last_of_month_timestamp
            )

            accounts_deleted = await calc_deleted_accounts(
                store.database, first_of_month_timestamp, last_of_month_timestamp
            )

            active_accounts = await calc_active_accounts(
                store.database, first_of_month_timestamp, last_of_month_timestamp
            )

            messages = await calc_messages(
                store.database, first_of_month_timestamp, last_of_month_timestamp
            )
            messages_cumulative = await calc_messages(
                store.database, 0, last_of_month_timestamp
            )

            active_hours = await calc_time_of_day_for_messages(
                store.database, first_of_month_timestamp, last_of_month_timestamp
            )
            most_active_hour_fi = ""
            if len(active_hours) > 0:
                most_active_hour_fi = active_hours[0]["hour"]

            top_skills = await calc_top_skills(
                store.database,
                first_of_month_timestamp,
                last_of_month_timestamp,
                limit=self.skills_limit,
            )

            writer.writerow(
                [
                    f"{d.strftime('%Y-%m')}",
                    joined_cumulative["account"],
                    joined_cumulative["admin"],
                    joined_cumulative["mentor"],
                    joined_cumulative["mentee"],
                    joined["account"],
                    joined["admin"],
                    joined["mentor"],
                    joined["mentee"],
                    accounts_deleted["account"],
                    accounts_deleted["admin"],
                    accounts_deleted["mentor"],
                    accounts_deleted["mentee"],
                    await calc_mentors(store.database, last_of_month_timestamp),
                    active_accounts["mentor"],
                    active_accounts["mentee"],
                    messages["mentee"],
                    messages["mentor"],
                    messages_cumulative["mentee"],
                    messages_cumulative["mentor"],
                    most_active_hour_fi,
                    top_skills,
                    await calc_active_chats(
                        store.database,
                        first_of_month_timestamp,
                        last_of_month_timestamp,
                    ),
                    new_chat_count(
                        chats=chats,
                        start=first_of_month_timestamp,
                        end=last_of_month_timestamp,
                    ),
                    new_chat_count(chats=chats, start=0, end=last_of_month_timestamp),
                    average_chat_message_count(
                        chats=chats,
                        start=first_of_month_timestamp,
                        end=last_of_month_timestamp,
                    ),
                    average_chat_message_count(
                        chats=chats, start=0, end=last_of_month_timestamp
                    ),
                    average_response_time(
                        chats=chats,
                        start=first_of_month_timestamp,
                        end=last_of_month_timestamp,
                    ),
                    average_response_time(
                        chats=chats, start=0, end=last_of_month_timestamp
                    ),
                    unanswered_count(
                        chats=chats,
                        start=first_of_month_timestamp,
                        end=last_of_month_timestamp,
                    ),
                    unanswered_count(chats=chats, start=0, end=last_of_month_timestamp),
                ]
            )

        return output.getvalue()

    async def weekly_csv(self, store, period_start_date, period_end_date):
        today = datetime.utcnow().date()

        output = io.StringIO()
        writer = csv.writer(
            output, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        writer.writerow(csv_header("Week"))

        chats = (await calc_chat_stats(store.database)).values()

        for d in weekrange_inclusive(period_start_date, period_end_date):
            # cannot generate stats for the current date or beyond
            if today == d:
                break

            week_start = d - timedelta(days=d.weekday())
            week_end = week_start + timedelta(days=6)

            first_of_week_timestamp = round(
                timezone_helsinki.localize(
                    datetime(
                        week_start.year,
                        week_start.month,
                        week_start.day,
                        hour=0,
                        minute=0,
                        second=0,
                    )
                ).timestamp()
            )

            last_of_week_timestamp = round(
                timezone_helsinki.localize(
                    datetime(
                        week_end.year,
                        week_end.month,
                        week_end.day,
                        hour=23,
                        minute=59,
                        second=59,
                    )
                ).timestamp()
            )

            joined = await calc_joined(
                store.database, first_of_week_timestamp, last_of_week_timestamp
            )
            joined_cumulative = await calc_joined(
                store.database, 0, last_of_week_timestamp
            )

            accounts_deleted = await calc_deleted_accounts(
                store.database, first_of_week_timestamp, last_of_week_timestamp
            )

            active_accounts = await calc_active_accounts(
                store.database, first_of_week_timestamp, last_of_week_timestamp
            )

            messages = await calc_messages(
                store.database, first_of_week_timestamp, last_of_week_timestamp
            )
            messages_cumulative = await calc_messages(
                store.database, 0, last_of_week_timestamp
            )

            active_hours = await calc_time_of_day_for_messages(
                store.database, first_of_week_timestamp, last_of_week_timestamp
            )
            most_active_hour_fi = ""
            if len(active_hours) > 0:
                most_active_hour_fi = active_hours[0]["hour"]

            top_skills = await calc_top_skills(
                store.database,
                first_of_week_timestamp,
                last_of_week_timestamp,
                limit=self.skills_limit,
            )

            writer.writerow(
                [
                    f"{week_start.strftime('%G-%V')}",
                    joined_cumulative["account"],
                    joined_cumulative["admin"],
                    joined_cumulative["mentor"],
                    joined_cumulative["mentee"],
                    joined["account"],
                    joined["admin"],
                    joined["mentor"],
                    joined["mentee"],
                    accounts_deleted["account"],
                    accounts_deleted["admin"],
                    accounts_deleted["mentor"],
                    accounts_deleted["mentee"],
                    await calc_mentors(store.database, last_of_week_timestamp),
                    active_accounts["mentor"],
                    active_accounts["mentee"],
                    messages["mentee"],
                    messages["mentor"],
                    messages_cumulative["mentee"],
                    messages_cumulative["mentor"],
                    most_active_hour_fi,
                    top_skills,
                    await calc_active_chats(
                        store.database, first_of_week_timestamp, last_of_week_timestamp
                    ),
                    new_chat_count(
                        chats=chats,
                        start=first_of_week_timestamp,
                        end=last_of_week_timestamp,
                    ),
                    new_chat_count(chats=chats, start=0, end=last_of_week_timestamp),
                    average_chat_message_count(
                        chats=chats,
                        start=first_of_week_timestamp,
                        end=last_of_week_timestamp,
                    ),
                    average_chat_message_count(
                        chats=chats, start=0, end=last_of_week_timestamp
                    ),
                    average_response_time(
                        chats=chats,
                        start=first_of_week_timestamp,
                        end=last_of_week_timestamp,
                    ),
                    average_response_time(
                        chats=chats, start=0, end=last_of_week_timestamp
                    ),
                    unanswered_count(
                        chats=chats,
                        start=first_of_week_timestamp,
                        end=last_of_week_timestamp,
                    ),
                    unanswered_count(chats=chats, start=0, end=last_of_week_timestamp),
                ]
            )

        return output.getvalue()

    async def period_csv(self, store, period_start_date, period_end_date):
        output = io.StringIO()
        writer = csv.writer(
            output, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        writer.writerow(csv_header("Period"))

        chats = (await calc_chat_stats(store.database)).values()

        first_of_period_timestamp = round(
            timezone_helsinki.localize(
                datetime(
                    period_start_date.year,
                    period_start_date.month,
                    period_start_date.day,
                    hour=0,
                    minute=0,
                    second=0,
                )
            ).timestamp()
        )

        last_of_period_timestamp = round(
            timezone_helsinki.localize(
                datetime(
                    period_end_date.year,
                    period_end_date.month,
                    period_end_date.day,
                    hour=23,
                    minute=59,
                    second=59,
                )
            ).timestamp()
        )

        joined = await calc_joined(
            store.database, first_of_period_timestamp, last_of_period_timestamp
        )
        joined_cumulative = await calc_joined(
            store.database, 0, last_of_period_timestamp
        )

        accounts_deleted = await calc_deleted_accounts(
            store.database, first_of_period_timestamp, last_of_period_timestamp
        )

        active_accounts = await calc_active_accounts(
            store.database, first_of_period_timestamp, last_of_period_timestamp
        )

        messages = await calc_messages(
            store.database, first_of_period_timestamp, last_of_period_timestamp
        )
        messages_cumulative = await calc_messages(
            store.database, 0, last_of_period_timestamp
        )

        active_hours = await calc_time_of_day_for_messages(
            store.database, first_of_period_timestamp, last_of_period_timestamp
        )
        most_active_hour_fi = ""
        if len(active_hours) > 0:
            most_active_hour_fi = active_hours[0]["hour"]

        top_skills = await calc_top_skills(
            store.database,
            first_of_period_timestamp,
            last_of_period_timestamp,
            limit=self.skills_limit,
        )

        writer.writerow(
            [
                f"{period_start_date.strftime('%Y-%m-%d')}"
                + " "
                + f"{period_end_date.strftime('%Y-%m-%d')}",
                joined_cumulative["account"],
                joined_cumulative["admin"],
                joined_cumulative["mentor"],
                joined_cumulative["mentee"],
                joined["account"],
                joined["admin"],
                joined["mentor"],
                joined["mentee"],
                accounts_deleted["account"],
                accounts_deleted["admin"],
                accounts_deleted["mentor"],
                accounts_deleted["mentee"],
                await calc_mentors(store.database, last_of_period_timestamp),
                active_accounts["mentor"],
                active_accounts["mentee"],
                messages["mentee"],
                messages["mentor"],
                messages_cumulative["mentee"],
                messages_cumulative["mentor"],
                most_active_hour_fi,
                top_skills,
                await calc_active_chats(
                    store.database, first_of_period_timestamp, last_of_period_timestamp
                ),
                new_chat_count(
                    chats=chats,
                    start=first_of_period_timestamp,
                    end=last_of_period_timestamp,
                ),
                new_chat_count(chats=chats, start=0, end=last_of_period_timestamp),
                average_chat_message_count(
                    chats=chats,
                    start=first_of_period_timestamp,
                    end=last_of_period_timestamp,
                ),
                average_chat_message_count(
                    chats=chats, start=0, end=last_of_period_timestamp
                ),
                average_response_time(
                    chats=chats,
                    start=first_of_period_timestamp,
                    end=last_of_period_timestamp,
                ),
                average_response_time(
                    chats=chats, start=0, end=last_of_period_timestamp
                ),
                unanswered_count(
                    chats=chats,
                    start=first_of_period_timestamp,
                    end=last_of_period_timestamp,
                ),
                unanswered_count(chats=chats, start=0, end=last_of_period_timestamp),
            ]
        )

        return output.getvalue()
