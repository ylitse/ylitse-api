"""Middleware module.

Coroutines for customizing request handlers.
"""

import json

from aiohttp import web


def json_error(status, error, message):
    """Create a JSON error response."""
    content_type = "application/json"
    body = {
        "error": error,
        "message": message,
    }
    body = json.dumps(body).encode("utf-8")

    return web.Response(status=status, content_type=content_type, body=body)


async def handle_401(request, response):
    """Override status 401 response."""
    message = "User not authenticated, provide correct credentials!"
    return json_error(response.status, response.reason, message)


async def handle_403(request, response):
    """Override status 403 response."""
    message = f"User not authorized to access {request.path}."
    return json_error(response.status, response.reason, message)


async def handle_404(request, response):
    """Override status 404 response."""
    message = f"Could not find {request.path}."
    return json_error(response.status, response.reason, message)


async def handle_other(request, response):
    """Override all other status responses."""
    message = f"{response.reason.lower().capitalize()}."
    return json_error(response.status, response.reason, message)


ERROR_HANDLERS = {
    401: handle_401,
    403: handle_403,
    404: handle_404,
}


@web.middleware
async def error_middleware(request, handler):
    """Error middleware.

    Return JSON response with a reasonable message.
    """
    try:
        return await handler(request)
    except web.HTTPException as e:
        h = ERROR_HANDLERS.get(e.status, handle_other)

        return await h(request, e)


def setup_middleware(app):
    """Add middleware to the app."""
    app.middlewares.append(error_middleware)
