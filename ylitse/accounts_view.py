"""user account creation module.

Contains `/mentor` endpoint implementation.
Contains `/mentee` endpoint implementation.
Contains `/admin` endpoint implementation.
"""
import pyotp

from aiohttp import web
from jsonschema import validate
from ylitse.data_definitions import data_definitions

# pylint: disable=missing-docstring


class AccountsView:
    def __init__(self, user_account_manager, JwtFunctions):
        self.jwt = JwtFunctions
        self.manager = user_account_manager

    async def post(self, request):
        try:
            client_json = await request.json()
        except Exception:
            raise web.HTTPBadRequest

        mfa_secret = client_json.pop("mfa_secret", None)
        if {*client_json.keys()} != {"password", "account"}:
            raise web.HTTPBadRequest

        if mfa_secret is not None:
            try:
                totp = pyotp.TOTP(mfa_secret)
                # Generate a token
                otp_token = totp.now()

                # Validate a token
                is_valid = totp.verify(otp_token)

                if not is_valid:
                    raise web.HTTPBadRequest
            except Exception:
                raise web.HTTPBadRequest

        try:
            validate(client_json["account"], data_definitions["ACCOUNT"]["schema"])
        except Exception:
            raise web.HTTPBadRequest

        try:
            validate(client_json["password"], data_definitions["PASSWORD"]["schema"])
        except Exception:
            raise web.HTTPBadRequest

        role = client_json["account"]["role"]

        if role != "mentee":
            token = self.jwt.extract_token(request, "access")
            if token["role"] != "admin":
                raise web.HTTPUnauthorized

        json = {
            **client_json,
            "user": {
                "display_name": "janteri",
                "role": client_json["account"]["role"],
            },
            "mfa_secret": mfa_secret,
        }

        created_account = None
        if role == "mentor":
            json["mentor"] = {
                "birth_year": 1990,
                "communication_channels": [],
                "display_name": "janteri",
                "gender": "other",
                "languages": [],
                "region": "HKI",
                "skills": [],
                "story": "",
            }
            created_account = await self.manager.create_mentor(json)
        else:
            created_account = await self.manager.create_non_mentor(json)

        return web.json_response(created_account, status=201)

    async def delete(self, request):
        account_id = request.match_info["account_id"]
        await self.manager.delete_user_account(account_id)
        return web.Response(status=204)

    async def change_password(self, request):
        try:
            json = await request.json()
            validate(json, data_definitions["CHANGE_PASSWORD"]["schema"])
            await self.manager.identity_manager.change_password(
                request["token"]["sub"],
                json["current_password"],
                json["new_password"],
            )
        except Exception:  # noqa, pylint: disable=broad-except
            try:
                validate(json, data_definitions["SET_PASSWORD"]["schema"])

                target_user = json["login_name"]
                logged_in_user = request["token"]["sub"]

                if request["token"]["role"] != "admin":
                    if target_user != logged_in_user:
                        raise web.HTTPUnauthorized

                await self.manager.identity_manager.set_password(
                    json["login_name"],
                    json["password"],
                )
            except Exception:  # noqa, pylint: disable=broad-except
                raise web.HTTPBadRequest

        return web.json_response(
            {"message": "Password update succesful"},
            status=200,
        )
