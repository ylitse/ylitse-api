from secrets import token_urlsafe
import time

from aiohttp import web
import jwt
from .config import config


class JwtFunctions:
    """Functions to handle jwt things."""

    def __init__(self, secret):
        self.secret = secret

    def encode(self, some_dict):
        """Create a jwt."""
        return jwt.encode(some_dict, self.secret, algorithm="HS256")

    def access_token(self, identity, session, client="mobile"):
        """Build an access token."""
        timestamp = int(time.time())
        payload = {
            "iat": timestamp,
            "exp": timestamp + config["access_token_ttl"],
            "iss": "ylitse-api",
            "client": client,
            "nonce": token_urlsafe(16),
            "sub": identity["login_name"],
            "role": identity["role"],
            "scopes": identity["scopes"],
            "session_id": session["id"],
        }
        return self.encode(payload)

    def refresh_token(self, identity, session, client):
        """Build a refresh token."""
        timestamp = int(time.time())
        payload = {
            "iat": timestamp,
            **(
                {"exp": timestamp + config["web_refresh_token_ttl"]}
                if client == "web"
                else {}
            ),
            "iss": "ylitse-api",
            "client": client,
            "nonce": token_urlsafe(16),
            "sub": identity["login_name"],
            "role": identity["role"],
            "session_id": session["id"],
        }
        return self.encode(payload)

    def get_token(self, request):
        """Get token."""
        authz_header = request.headers.get("Authorization")
        if authz_header:
            return authz_header.lstrip("Bearer").strip(" ").encode("utf-8")

        token = self.get_cookie_token(request, 'YLITSE_ACCESS="')
        if not token:
            return None

        return token

    def get_cookie_token(self, request, token_prefix):
        """Get token from cookie"""
        cookie = request.headers.get("cookie")
        if not cookie:
            return None

        token = cookie[cookie.find(token_prefix) + len(token_prefix) :].split('"')[0]
        if not token:
            return None

        return token

    def decode(self, encoded_jwt, verify_expiration=True):
        """Check that token is okay."""
        try:
            token = jwt.decode(
                encoded_jwt,
                self.secret,
                algorithms=["HS256"],
                options={"verify_exp": verify_expiration},
            )
            return token
        except jwt.ExpiredSignatureError:
            raise web.HTTPUnauthorized(reason="Token expired.")
        except Exception:
            raise web.HTTPUnauthorized(reason="Invalid token.")

    def extract_token(self, request, token_type="access"):
        """Extract token."""
        try:
            if token_type == "webrefresh":
                payload = self.decode(
                    self.get_cookie_token(request, 'YLITSE_REFRESH="')
                )
            else:
                payload = self.decode(self.get_token(request))

            if "role" not in payload:
                raise web.HTTPUnauthorized
            if "exp" not in payload:
                raise web.HTTPUnauthorized
            if payload["exp"] < int(time.time()):
                raise web.HTTPUnauthorized
        except Exception:
            raise web.HTTPUnauthorized

        request["token"] = payload
        return payload
