from ..store_abstraction import StoreView
from .abstract import CRUDAbstract


class CRUDCommon(CRUDAbstract):
    """
    CRUD object with default methods to
    Create, Read, Update, Delete (CRUD).
    """

    def store_view(self, store):
        return StoreView(store, self.data_type)
