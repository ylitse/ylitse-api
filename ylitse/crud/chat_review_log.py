from .common import CRUDCommon


class CRUDChatReviewLog(CRUDCommon):
    async def get_all_logs(self, store):
        query = self.table.select()
        return await self.fetch_all(store, query)

    async def post(self, store, data):
        async with store.database.connection() as connection:
            async with connection.transaction():
                posted = await self.create(store, data)
        return posted


crud_chat_review_log = CRUDChatReviewLog(data_type="CHAT_REVIEW_LOG")
