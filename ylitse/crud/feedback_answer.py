from datetime import datetime, timedelta

import io
import csv
from sqlalchemy import desc, func

from ylitse.db.postgres_exception_handler import HandlePostgresExceptions

from ..utils import fst, is_now_over_date_fraction
from .common import CRUDCommon

from .message import crud_message


class CRUDFeedbackAnswer(CRUDCommon):
    async def answer(self, store, *, user_id, feedback_answer_id, value):
        now_utc = datetime.utcnow()

        update_with_returning = (
            self.table.update()
            .returning(self.table)
            .where(self.table.c.id == feedback_answer_id)
            .where(self.table.c.user_id == user_id)
            .values(value=value, updated=now_utc)
        )
        return fst(await self.fetch_all(store, query=update_with_returning))

    async def count_answers_for_question(self, store, *, user_id, feedback_question_id):
        query = (
            self.table.select()
            .with_only_columns([func.count()])
            .where(self.table.c.user_id == user_id)
            .where(self.table.c.feedback_question_id == feedback_question_id)
        )
        count = await store.database.execute(query)

        return count

    def csv_header(self):
        return [
            "ID",
            "QUESTION_ID",
            "USER_ID",
            "ASKED_AT",
            "RETRIES",
            "MAX_RETRIES",
            "VALUE",
            "ACTIVE",
            "UPDATED",
            "CREATED",
        ]

    def to_csv(self, result):
        delimiter = ","
        output = io.StringIO()
        writer = csv.writer(
            output, delimiter=delimiter, quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        writer.writerow(self.csv_header())

        for row in result:
            values = list(map(str, row.values()))
            delimiter.join(values)
            writer.writerow(values)

        return output.getvalue().strip()

    async def get_answers(self, store, start, end):
        values = {"start": start - timedelta(days=1), "end": end + timedelta(days=1)}

        query = """
        SELECT * from feedback_answers
        WHERE created between :start AND :end
        """

        answers = await store.database.fetch_all(query=query, values=values)

        return answers

    async def create_for_answering(
        self, store, *, user_id, feedback_question_id, max_retries
    ):
        now_utc = datetime.utcnow()

        to_create = {
            "user_id": user_id,
            "feedback_question_id": feedback_question_id,
            "asked_at": now_utc,
            "max_retries": max_retries,
        }
        return await self.create(store, to_create)

    async def get_latest_for_question(self, store, *, user_id, feedback_question_id):
        query = (
            self.table.select()
            .where(
                self.table.c.user_id == user_id,
            )
            .where(
                self.table.c.feedback_question_id == feedback_question_id,
            )
            .order_by(desc(self.table.c.created))
            .limit(1)
        )

        return fst(await self.fetch_all(store, query=query))

    async def has_fetched_question_before(
        self, store, *, user_id, feedback_question_id
    ):
        query = (
            self.table.select()
            .with_only_columns([func.count()])
            .where(self.table.c.user_id == user_id)
            .where(self.table.c.feedback_question_id == feedback_question_id)
        )
        count = await store.database.execute(query)

        return count > 0

    async def has_answered_exactly(
        self, store, *, user_id, feedback_question_id, value
    ):
        query = (
            self.table.select()
            .with_only_columns([func.count()])
            .where(self.table.c.user_id == user_id)
            .where(self.table.c.feedback_question_id == feedback_question_id)
            .where(self.table.c.value == value)
        )
        count = await store.database.execute(query)

        return count > 0

    async def retry_question(self, store, *, user_id, feedback_question):
        now_utc = datetime.utcnow()
        remind_seconds = round(
            feedback_question["rules"]["schedule"]["remind_interval_days"] * 24 * 3600
        )
        ask_again_utc = now_utc - timedelta(seconds=remind_seconds)
        query = (
            self.table.select()
            .where(self.table.c.user_id == user_id)
            .where(self.table.c.feedback_question_id == feedback_question["id"])
            .where(self.table.c.value.is_(None))
            .where(self.table.c.max_retries > self.table.c.retries)
            .where(self.table.c.asked_at <= ask_again_utc)
        )

        # FODO: maybe defaultdict would help here
        # if mentee skip first answer,
        # then opens app next time after few  months,
        # we should generate new question and not retry old one
        days_since_prev_answer = feedback_question["rules"]["schedule"]["repetitions"][
            "days_since_previous_answer"
        ]
        query = query.where(
            self.table.c.asked_at
            >= now_utc - timedelta(days=round(days_since_prev_answer))  # long retries
        )
        query = query.order_by(desc(self.table.c.created)).limit(1)
        answer = fst(await self.fetch_all(store, query))
        if not answer:
            return None

        return await self.update(
            store,
            {
                "id": answer["id"],
                "retries": answer["retries"] + 1,
                "asked_at": now_utc,
            },
        )

    async def repeat_question(self, store, *, user_id, feedback_question):
        question_id = feedback_question["id"]
        question_repetitions = feedback_question["rules"]["schedule"]["repetitions"]
        answers_count = await self.count_answers_for_question(
            store,
            user_id=user_id,
            feedback_question_id=question_id,
        )
        if answers_count == 0:
            return None
        last_answer = await self.get_latest_for_question(
            store,
            user_id=user_id,
            feedback_question_id=feedback_question["id"],
        )
        if not last_answer:
            return None

        if question_repetitions["type"] == "predefined":
            # does it have enough answers already
            if answers_count <= question_repetitions["times"]:
                # time since last or sent messages since last,
                # which ever comes first
                sent_messages_since_last_answer_count = (
                    await crud_message.count_sent_by_user(
                        store,
                        user_id=user_id,
                        from_datetime=datetime.fromisoformat(last_answer["created"]),
                    )
                )
                has_been_active_enough = (
                    sent_messages_since_last_answer_count
                    >= question_repetitions["messages_since_previous_answer"]
                )
                is_over_ask_earliest = is_now_over_date_fraction(
                    last_answer["created"],
                    question_repetitions["days_since_previous_answer"],
                    True,
                )
                min_time_passed = is_now_over_date_fraction(
                    last_answer["created"],
                    question_repetitions["min_days_since_previous_answer"],
                    True,
                )
                # needs to be active enough and minimum time passed
                # OR enough time passed
                if (has_been_active_enough and min_time_passed) or is_over_ask_earliest:
                    return await self.create_for_answering(
                        store,
                        user_id=user_id,
                        feedback_question_id=question_id,
                        max_retries=feedback_question["rules"]["schedule"][
                            "remind_times_when_skipped"
                        ],
                    )

        if question_repetitions["type"] == "until_value":
            # if user answered exactly like needed, no need to continue
            if await self.has_answered_exactly(
                store,
                user_id=user_id,
                feedback_question_id=question_id,
                value=(question_repetitions["value"]),
            ):
                return None

            is_over_ask_earliest = is_now_over_date_fraction(
                last_answer["created"],
                question_repetitions["days_since_previous_answer"],
                True,
            )

            # enough time passed
            if is_over_ask_earliest:
                return await self.create_for_answering(
                    store,
                    user_id=user_id,
                    feedback_question_id=question_id,
                    max_retries=feedback_question["rules"]["schedule"][
                        "remind_times_when_skipped"
                    ],
                )

        return None

    @HandlePostgresExceptions
    async def delete_for_question(self, store, *, question_id):
        query = self.table.delete().where(
            self.table.c.feedback_question_id == question_id
        )
        await self.execute(store, query)


crud_feedback_answer = CRUDFeedbackAnswer(data_type="FEEDBACK_ANSWER")
