from ..utils import fst
from ..db.postgres_helpers import now_utc
from .common import CRUDCommon


class CRUDMentor(CRUDCommon):
    async def get_by_id(self, store, *, mentor_id):
        query = self.table.select().where(
            self.table.c.id == mentor_id,
        )

        return fst(await self.fetch_all(store, query=query))

    async def update_last_seen(self, store, *, mentor_id):
        """
        Updates current utc datetime to last_seen
        """

        to_update = {"id": mentor_id, "last_seen": now_utc()}
        await self.update(store, to_update)

    async def update_vacation_status(
        self,
        store,
        *,
        mentor_id,
        is_vacationing,
        status_message,
    ):
        """
        Updates mentor's is_vacationing and status_message.
        """

        to_update = {
            "id": mentor_id,
            "is_vacationing": is_vacationing,
            "status_message": status_message,
        }

        await self.update(store, to_update)


crud_mentor = CRUDMentor(data_type="MENTOR")
