from ..config import config
from ..encrypted_store_view import EncryptedStoreView
from ..symmetric_crypto_functions import SymmetricCryptoFunctions
from .abstract import CRUDAbstract


class CRUDEncryptedCommon(CRUDAbstract):
    """
    CRUD object with default methods to
    Create, Read, Update, Delete (CRUD).

    Encrypts and decrypts given items.
    """

    def __init__(
        self,
        *,
        data_type,
        enable_encryption_for,
    ):
        self.crypto = SymmetricCryptoFunctions(config["encryption_key_file"])
        self.enable_encryption_for = enable_encryption_for

        super().__init__(
            data_type=data_type,
        )

    def store_view(self, store):
        return EncryptedStoreView(
            store=store,
            crypto=self.crypto,
            data_type=self.data_type,
            keys=self.enable_encryption_for,
        )
