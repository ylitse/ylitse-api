from ylitse.utils import fst
from .common import CRUDCommon
from ..enum_with_list import EnumWithList


class ReportStatus(EnumWithList):
    RECEIVED = "received"
    HANDLED = "handled"


class CRUDReport(CRUDCommon):
    async def get_all_reports(self, store):
        query = self.table.select()
        return await self.fetch_all(store, query)

    async def post(self, store, data):
        async with store.database.connection() as connection:
            async with connection.transaction():
                posted = await self.create(store, data)
        return posted

    async def get_report_by_id(self, store, report_id):
        query = self.table.select().where(self.table.c.id == report_id)
        response = await self.fetch_all(store, query)
        return fst(response)

    async def delete(self, store, report_id):
        async with store.database.connection() as connection:
            async with connection.transaction():
                query = self.table.delete().where(self.table.c.id == report_id)
                return await self.execute(store, query)

    async def update_report(self, store, report_id, status, comment):
        old_one = await self.get_report_by_id(store, report_id=report_id)
        if old_one:
            old_one["status"] = status
            old_one["comment"] = comment
            return await self.update(store, old_one)

    def is_allowed_status(self, status):
        return status in ReportStatus.list()


crud_report = CRUDReport(data_type="REPORTED_USERS")
