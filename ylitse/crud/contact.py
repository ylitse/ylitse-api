from sqlalchemy import and_, asc, or_, select
from ..utils import fst
from ..enum_with_list import EnumWithList
from ..postgres_tables import table_messages, table_users
from .common import CRUDCommon


class ContactStatus(EnumWithList):
    OK = "ok"
    BANNED = "banned"
    DELETED = "deleted"
    ARCHIVED = "archived"


class CRUDContact(CRUDCommon):
    async def get_mine(self, store, *, user_id, ignore_deleted):
        recipients_subquery = (
            table_messages.select()
            .with_only_columns([table_messages.c.recipient_id])
            .where(table_messages.c.sender_id == user_id)
            .group_by(table_messages.c.recipient_id)
        )
        senders_subquery = (
            table_messages.select()
            .with_only_columns([table_messages.c.sender_id])
            .where(table_messages.c.recipient_id == user_id)
            .group_by(table_messages.c.sender_id)
        )

        query = (
            select([table_users, self.table.c.status])
            .select_from(self.__join_status_query(user_id))
            .where(
                or_(
                    table_users.c.id.in_(recipients_subquery),
                    table_users.c.id.in_(senders_subquery),
                )
            )
            .order_by(asc(table_users.c.created))
        )
        contacts = await self.fetch_all(store, query)

        if ignore_deleted:
            return self.ignore_deleted(contacts)

        return contacts

    async def get_by_contact_user_id(self, store, *, user_id, contact_user_id):
        query = (
            select([table_users, self.table.c.status])
            .select_from(self.__join_status_query(user_id))
            .where(table_users.c.id == contact_user_id)
            .order_by(asc(table_users.c.created))
        )
        return fst(await self.fetch_all(store, query))

    async def get_status_by_contact_user_id(self, store, *, user_id, contact_user_id):
        query = (
            self.table.select()
            .where(self.table.c.user_id == user_id)
            .where(self.table.c.contact_user_id == contact_user_id)
            .order_by(asc(self.table.c.created))
        )
        return fst(await self.fetch_all(store, query))

    async def update_status(self, store, *, user_id, contact_user_id, status):
        """
        Handles saving contact status.

        status == "ok"
        Revert a ban.
        By default no one is banned so when changing ban status to "ok"
        means that we can delete the ban status information.

        status == "banned"
        Save the status.

        status == "deleted"
        Save the status.

        status == "archived"
        Save the status.
        """

        if status == ContactStatus.OK.value:  # noqa, pylint: disable=no-member
            return await self.delete_status(
                store, user_id=user_id, contact_user_id=contact_user_id
            )

        old_one = await self.get_status_by_contact_user_id(
            store, user_id=user_id, contact_user_id=contact_user_id
        )
        if old_one:
            old_one["status"] = status
            return await self.update(store, old_one)

        return await self.create(
            store,
            {
                "user_id": user_id,
                "contact_user_id": contact_user_id,
                "status": status,
            },
        )

    async def delete_status(self, store, *, user_id, contact_user_id):
        query = (
            self.table.delete()
            .where(self.table.c.user_id == user_id)
            .where(self.table.c.contact_user_id == contact_user_id)
        )
        await self.execute(store, query)

    def ignore_deleted(self, contacts):
        return list(
            filter(
                lambda d: d.get("status", "")
                != ContactStatus.DELETED.value,  # noqa, pylint: disable=no-member
                contacts,
            )
        )

    def get_deleted_contact_user_ids(self, contacts):
        return [
            d["id"]
            for d in contacts
            if d.get("status", "")
            == ContactStatus.DELETED.value  # noqa, pylint: disable=no-member
        ]

    def is_allowed_status(self, status):
        return status in ContactStatus.list()

    def __join_status_query(self, user_id):
        join_status_query = table_users.join(
            self.table,
            and_(
                table_users.c.id == self.table.c.contact_user_id,
                self.table.c.user_id == user_id,
            ),
            isouter=True,
        )

        return join_status_query


crud_contact = CRUDContact(data_type="CONTACT")
