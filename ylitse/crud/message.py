# pylint: disable=too-many-arguments

from datetime import datetime

import pytz
import sqlalchemy
from aiohttp import web
from sqlalchemy import and_, or_, func


from ..utils import fst
from .. import stats_collect
from .encrypted_common import CRUDEncryptedCommon
from .contact import crud_contact
from ..db import HandlePostgresExceptions


class CRUDMessage(CRUDEncryptedCommon):
    async def get_by_id(self, store, *, user_id, message_id):
        query = (
            self.table.select()
            .where(self.table.c.id == message_id)
            .where(
                or_(
                    self.table.c["sender_id"] == user_id,
                    self.table.c["recipient_id"] == user_id,
                )
            )
            .order_by(sqlalchemy.asc(self.table.c.created))
        )
        return fst(await self.fetch_all(store, query))

    async def count_sent_by_user(self, store, *, user_id, from_datetime=None):
        query = (
            self.table.select()
            .with_only_columns([func.count()])
            .where(self.table.c.sender_id == user_id)
        )
        if from_datetime:
            query = query.where(self.table.c.created >= from_datetime)

        count = await store.database.execute(query)

        return count

    async def get_mine(
        self,
        store,
        *,
        user_id,
        all_contacts,
        contact_user_ids,
        from_message=None,
        is_desc=False,
        max_messages=None,
    ):
        query = self.table.select()

        # do we have something we can filter `created` with
        created_filter_naive = None
        if from_message:
            created_filter_naive = self.__aware_datetime_to_naive(
                datetime.fromisoformat(from_message["created"])
            )

        # Apply `created` filter if it came from `from_message`.
        # `created` is not unique so use <=, >= to compare.
        # If there are duplicate datetimes then at least
        # return them all. Other option would be to use  <, >
        # but that would skip over duplicate datetimes in msgs.
        #
        # but if you see duplicate msgs in the app this is propably why.
        if from_message and is_desc:
            query = query.where(self.table.c.created <= created_filter_naive).where(
                self.table.c.id != from_message["id"]
            )
        if from_message and not is_desc:
            query = query.where(self.table.c.created >= created_filter_naive).where(
                self.table.c.id != from_message["id"]
            )

        # `max_messages` limit can be applied to the main query
        # if we are fetching for all contacts or just one.
        if max_messages and len(contact_user_ids) <= 1:
            query = query.limit(max_messages)

        # When fetching msgs for multiple contacts
        # max_messages is for each contact.
        # Subquery gets msg ids that the main query can then fetch.
        if len(contact_user_ids) > 1 and not created_filter_naive:
            subqueries = []
            for contact_user_id in contact_user_ids:
                subquery = (
                    self.table.select()
                    .with_only_columns([self.table.c.id])
                    .where(
                        or_(
                            and_(
                                self.table.c["sender_id"] == contact_user_id,
                                self.table.c["recipient_id"] == user_id,
                            ),
                            and_(
                                self.table.c["recipient_id"] == contact_user_id,
                                self.table.c["sender_id"] == user_id,
                            ),
                        )
                    )
                    .limit(max_messages)
                )
                if is_desc:
                    subquery = subquery.order_by(sqlalchemy.desc(self.table.c.created))
                else:
                    subquery = subquery.order_by(sqlalchemy.asc(self.table.c.created))
                subqueries.append(self.table.c.id.in_(subquery))
            query = query.where(or_(*subqueries))
        elif len(contact_user_ids) > 1 and created_filter_naive:
            raise NotImplementedError(
                "Filtering with multiple contact_user_ids and "
                "having a from_message or from_created filter "
                "is not supported."
            )

        # Fetching for 1 contact
        if len(contact_user_ids) == 1:
            contact_user_id = contact_user_ids[0]
            query = query.where(
                or_(
                    and_(
                        self.table.c["sender_id"] == user_id,
                        self.table.c["recipient_id"] == contact_user_id,
                    ),
                    and_(
                        self.table.c["sender_id"] == contact_user_id,
                        self.table.c["recipient_id"] == user_id,
                    ),
                )
            )

        # final protection. make sure to get only current users msgs.
        query = query.where(
            or_(
                self.table.c["sender_id"] == user_id,
                self.table.c["recipient_id"] == user_id,
            )
        )

        # ignore messages with deleted contacts
        deleted_contact_user_ids = crud_contact.get_deleted_contact_user_ids(
            all_contacts
        )
        if deleted_contact_user_ids:
            query = query.where(
                and_(
                    self.table.c["sender_id"].notin_(deleted_contact_user_ids),
                    self.table.c["recipient_id"].notin_(deleted_contact_user_ids),
                )
            )

        # When fetching older msgs reverse results so they would still be in
        # correct order.
        if is_desc:
            query = query.order_by(sqlalchemy.desc(self.table.c.created))
            msgs = await self.fetch_all(store, query)
            msgs.reverse()
            return msgs

        query = query.order_by(sqlalchemy.asc(self.table.c.created))
        return await self.fetch_all(store, query)

    async def mark_as_read(self, store, *, user_id, message_id):
        msg = await self.get_by_id(store, user_id=user_id, message_id=message_id)
        if not msg:
            raise web.HTTPNotFound

        msg["opened"] = True

        return await self.update(store, msg)

    @HandlePostgresExceptions
    async def delete(self, store, *, user_id, contact_user_id):
        async with store.database.connection() as connection:
            async with connection.transaction():
                query = self.table.delete().where(
                    or_(
                        and_(
                            self.table.c.sender_id == user_id,
                            self.table.c.recipient_id == contact_user_id,
                        ),
                        and_(
                            self.table.c.sender_id == contact_user_id,
                            self.table.c.recipient_id == user_id,
                        ),
                    )
                )
                await self.execute(store, query)

                await stats_collect.delete_one_chat(
                    store.database,
                    user_id=user_id,
                    contact_user_id=contact_user_id,
                )

    @HandlePostgresExceptions
    async def post(self, store, *, data, sender_role):
        async with store.database.connection() as connection:
            async with connection.transaction():
                posted = await self.create(store, data)
                await stats_collect.insert_message(
                    store.database, message=posted, from_role=sender_role
                )
                return posted

    def __aware_datetime_to_naive(self, dt):
        if not dt.tzinfo:  # naive already
            return dt

        return dt.astimezone(pytz.timezone("UTC")).replace(tzinfo=None)


crud_message = CRUDMessage(
    data_type="MESSAGE",
    enable_encryption_for=[
        {
            "decrypted": "content",
            "encrypted": "content_enc",
        }
    ],
)
