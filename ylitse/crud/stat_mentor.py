from datetime import datetime, timezone
from dateutil.relativedelta import relativedelta

from ylitse.utils import fst
from ..stats_collect import anonymize_id, from_iso_format_to_utc_timestamp
from .common import CRUDCommon
from .mentor import crud_mentor


def years_ago_unix_utc_timestamp(*, years):
    """
    Integer utc unix timestamp from number of years ago.
    """
    return round((datetime.now(timezone.utc) - relativedelta(years=years)).timestamp())


class CRUDStatMentor(CRUDCommon):
    async def get_by_id(self, store, *, mentor_id):
        query = self.table.select().where(
            self.table.c.id == anonymize_id(mentor_id),
        )

        return fst(await self.fetch_all(store, query=query))

    async def anonymize_old_data(self, store):
        """
        Anonymize mentor data from deleted accounts.

        Mentor stats show display_name even if the mentor account
        has been deleted.

        Plan to deal with this is to forget if mentor account has been
        deleted for more than 1 year.
        """

        query = """
        UPDATE stat_mentors
        SET display_name = ''
        WHERE deleted <= :year_ago
        """

        values = {"year_ago": years_ago_unix_utc_timestamp(years=1)}

        return await store.database.execute(query=query, values=values)

    async def refresh(self, store, *, mentor_id):
        """
        Refresh stat version of mentor's data.
        Can be used after mentor has been created or updated.
        """
        mentor = await crud_mentor.get_by_id(store, mentor_id=mentor_id)
        if not mentor:
            return

        query = """
        INSERT INTO stat_mentors
               ( id, user_id, display_name, last_seen, created)
        VALUES (:id,:user_id,:display_name,:last_seen,:created)
                ON CONFLICT (id) DO UPDATE SET
                display_name=EXCLUDED.display_name,
                last_seen=EXCLUDED.last_seen; """

        values = {
            "id": anonymize_id(mentor_id),
            "user_id": anonymize_id(mentor["user_id"]),
            "display_name": mentor["display_name"],
            "last_seen": (
                from_iso_format_to_utc_timestamp(mentor["last_seen"])
                if mentor.get("last_seen", None) is not None
                else None
            ),
            "created": from_iso_format_to_utc_timestamp(mentor["created"]),
        }
        return await store.database.execute(query=query, values=values)


crud_stat_mentor = CRUDStatMentor(data_type="STAT_MENTOR")
