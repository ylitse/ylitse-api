from sqlalchemy import asc

from ..utils import fst, is_now_over_date_fraction
from .common import CRUDCommon

from .feedback_answer import crud_feedback_answer
from .message import crud_message
from ..store_abstraction import StoreView


class CRUDFeedbackQuestion(CRUDCommon):
    async def list(self, store):
        query = self.table.select().order_by(asc(self.table.c.created))
        return await self.fetch_all(store, query)

    async def delete(self, store, *, question_id):
        await crud_feedback_answer.delete_for_question(store, question_id=question_id)

        await self.store_view(store).delete(id=question_id)

    async def fetch_all_active(self, store):
        query = (
            self.table.select()
            .where(self.table.c.active.is_(True))
            .order_by(asc(self.table.c.created))
        )
        return await self.fetch_all(store, query)

    def questions_for_recipient(self, questions, user):
        return list(
            filter(
                lambda q: self.is_for_recipient(q["rules"]["recipients"], user),
                questions,
            )
        )

    def is_for_recipient(self, recipients, user):
        return (
            len(
                list(
                    filter(
                        lambda r: r in (user["id"], user["role"]),
                        recipients,
                    )
                )
            )
            > 0
        )

    async def fetch_all_needing_answers(self, store, *, user_id):
        response = []

        users_store_view = StoreView(store, "USER")
        user = fst(await users_store_view.read(id=user_id))
        if not user:
            return []

        questions = self.questions_for_recipient(
            await self.fetch_all_active(store), user
        )

        sent_messages_count = await crud_message.count_sent_by_user(
            store, user_id=user_id
        )

        for q in questions:

            # figure out if needs answer because skipped last time
            answer = await crud_feedback_answer.retry_question(
                store, feedback_question=q, user_id=user_id
            )
            if answer:
                response.append(
                    {
                        "id": q["id"],
                        "answer_id": answer["id"],
                        "titles": q["rules"]["titles"],
                        "answer": q["rules"]["answer"],
                    }
                )

                continue  # move to next question

            has_fetched_before = await crud_feedback_answer.has_fetched_question_before(
                store, user_id=user["id"], feedback_question_id=q["id"]
            )
            if not has_fetched_before:
                # first timer
                is_over_ask_earliest = is_now_over_date_fraction(
                    user["created"],
                    q["rules"]["schedule"]["first"]["days_since_registration"],
                    True,
                )
                is_under_max_old = is_now_over_date_fraction(
                    user["created"],
                    q["rules"]["schedule"]["first"]["max_old_account_in_days"],
                    False,
                )
                has_been_active_enough = (
                    q["rules"]["schedule"]["first"]["sent_messages_threshold"]
                    <= sent_messages_count
                )

                # first time question, and these conditions must pass to get it
                if is_over_ask_earliest and is_under_max_old and has_been_active_enough:
                    answer = await crud_feedback_answer.create_for_answering(
                        store,
                        user_id=user_id,
                        feedback_question_id=q["id"],
                        max_retries=q["rules"]["schedule"]["remind_times_when_skipped"],
                    )

                    # append the question to the results
                    response.append(
                        {
                            "id": q["id"],
                            "answer_id": answer["id"],
                            "titles": q["rules"]["titles"],
                            "answer": q["rules"]["answer"],
                        }
                    )

                continue  # move to next question

            # figure out if questions needs repetition
            answer = await crud_feedback_answer.repeat_question(
                store, feedback_question=q, user_id=user_id
            )
            if answer:
                response.append(
                    {
                        "id": q["id"],
                        "answer_id": answer["id"],
                        "titles": q["rules"]["titles"],
                        "answer": q["rules"]["answer"],
                    }
                )

                continue  # move to next question

        return response


crud_feedback_question = CRUDFeedbackQuestion(data_type="FEEDBACK_QUESTION")
