from copy import deepcopy

from ..utils import fst
from ..db.postgres_helpers import records_to_dict, generate_utc_now_timestamp
from ..db.postgres_exception_handler import HandlePostgresExceptions
from .common import CRUDCommon


class CRUDStatEvent(CRUDCommon):
    async def create_all(self, store, *, events):
        return [await self.create(store, e) for e in events]

    @HandlePostgresExceptions
    async def create(self, store, some_data):
        to_insert = deepcopy(some_data)
        to_insert["created"] = generate_utc_now_timestamp()

        query = self.table.insert().values(to_insert).returning(self.table)
        created = await store.database.fetch_all(query=query)
        return fst(records_to_dict(created))


crud_stat_event = CRUDStatEvent(data_type="STAT_EVENT")
