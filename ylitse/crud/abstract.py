from abc import ABC, abstractmethod

from ..db.postgres_exception_handler import HandlePostgresExceptions
from ..postgres_tables import data_type_to_postgres_table


class CRUDAbstract(ABC):
    """
    CRUD object with default methods to
    Create, Read, Update, Delete (CRUD).
    """

    def __init__(
        self,
        *,
        data_type,
    ):
        self.data_type = data_type
        self.table = data_type_to_postgres_table(data_type)

    async def create(self, store, some_data):
        store_view = self.store_view(store)
        return await store_view.create(some_data)

    @HandlePostgresExceptions
    async def execute(self, store, query):
        return await store.database.execute(query=query)

    async def fetch_all(self, store, query=None):
        return await self.store_view(store).read(query=query)

    async def update(self, store, some_data):
        return await self.store_view(store).update(some_data)

    @abstractmethod
    def store_view(self, store):
        pass
