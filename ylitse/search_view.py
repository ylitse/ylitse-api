from aiohttp import web

from ylitse.store_abstraction import StoreView
from ylitse.utils import fst


class SearchView:
    def __init__(self, store):
        self.accounts = StoreView(store, "ACCOUNT")

    async def head(self, request):
        login_name = request.query.get("login_name")
        if not login_name:
            return web.Response(status=400)
        matching = fst(await self.accounts.read(login_name=login_name))
        if matching:
            return web.Response(status=200)
        return web.Response(status=204)
