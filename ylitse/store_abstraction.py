"""Store abstraction module.

Contains definitions of store interface.
"""

from abc import ABC, abstractmethod
import datetime
import secrets

from aiohttp import web

from ylitse.data_definitions import data_definitions


def timestamp():
    """Time stamp in some ISO format."""
    return datetime.datetime.utcnow().isoformat()


def new_ylitse_id():
    """Somebody more clever could redo this one."""
    # 64 ** 32 possibilities. Does not collide. 16 would be enough.
    return secrets.token_urlsafe(32)


class StoreAbstraction(ABC):
    """Defines store interface."""

    @abstractmethod
    async def _read(self, data_type, **kwargs):
        """Read some."""

    async def read(self, data_type, **kwargs):
        """Return active data elements."""
        all_data = await self._read(data_type, **kwargs)
        # for x in all_data:
        #     del x['active']

        return all_data

    @abstractmethod
    async def delete(self, data_type, **kwargs):
        """Delete some."""

    @abstractmethod
    async def _update(self, data_type, some_data):
        """Update some."""

    async def update(self, data_type, some_data):
        """Update some."""
        some_data["updated"] = timestamp()

        return await self._update(data_type, some_data)

    @abstractmethod
    async def _create(self, data_type, some_data):
        """Create some."""

    async def create(self, data_type, data):
        """Create some. Check that no duplicates are created."""
        data["id"] = new_ylitse_id()
        data["created"] = timestamp()
        data["updated"] = timestamp()

        uniques = data_definitions[data_type]["uniques"]

        if uniques:
            data = {**data, "active": False}
            data = await self._create(data_type, data)

            for u in uniques:
                if u not in data.keys():
                    continue

                similar = await self._read(data_type, **{u: data[u]})
                if len(similar) > 1:
                    await self.delete(data_type, id=data["id"])
                    print(f"REASON OF 409: {u}")
                    raise web.HTTPConflict(text=f"{u} already exists")

            data["active"] = True
            data = await self.update(data_type, data)
        else:
            data["active"] = True
            data = await self._create(data_type, data)

        return data

    @abstractmethod
    async def on_startup(self, app=None):
        """Method to call on app startup"""

    @abstractmethod
    async def on_shutdown(self, app=None):
        """Method to call on app shutdown"""


class StoreView:
    """Nice helper class to get one type access to db."""

    def __init__(self, store, data_type):
        """Type and Store."""
        self.store = store
        self.data_type = data_type

    async def read(self, **kwargs):
        """Read some."""
        return await self.store.read(self.data_type, **kwargs)

    async def delete(self, **kwargs):
        """Delete some."""
        await self.store.delete(self.data_type, **kwargs)

    async def create(self, some_json):
        """Create some."""
        return await self.store.create(self.data_type, some_json)

    async def update(self, some_json):
        """Update some."""
        return await self.store.update(self.data_type, some_json)
