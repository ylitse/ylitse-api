"""Account device module.

Contains /account/{account_id}/device endpoint implementation."
"""

from aiohttp import web
import jsonschema

from ylitse.data_definitions import data_definitions
from ylitse.json_validation import raise_if_extra_json_properties
from ylitse.store_abstraction import StoreView
from ylitse.utils import fst

# pylint: disable=missing-docstring


class DeviceView:
    def __init__(self, store):
        self.devices = StoreView(store, "DEVICE")

    async def get(self, request):
        user_id = request.match_info["user_id"]
        device_info = fst(await self.devices.read(user_id=user_id))
        if device_info:
            return web.json_response(device_info)
        return web.json_response({"token": "none", "platform": "none"})

    async def put(self, request):
        user_id = request.match_info["user_id"]

        schema = data_definitions["DEVICE"]["schema"]

        print("USER ID:")
        print(user_id)

        try:
            json = await request.json()
            jsonschema.validate(json, schema)
        except Exception:
            raise web.HTTPBadRequest

        raise_if_extra_json_properties(data_type="DEVICE", schema=schema, json=json)

        old_info = await self.devices.read(user_id=user_id)
        old_info = fst(old_info)

        if old_info:
            new_info = {**json, "user_id": user_id, "id": old_info["id"]}
            new_info = await self.devices.update(new_info)
            return web.json_response(new_info)

        new_info = await self.devices.create({**json, "user_id": user_id})
        return web.json_response(new_info)
