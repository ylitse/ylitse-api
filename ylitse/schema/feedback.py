# pylint: disable=unused-import

from typing import Union, List
from typing_extensions import Literal

from pydantic import (
    BaseModel,
    Extra,
    NonNegativeInt,
    NonNegativeFloat,
    PositiveFloat,
    PositiveInt,
    constr,
    validator,
)


class FeedbackQuestionTitle(constr(min_length=2, max_length=200)):
    """
    String validated with ylitse FeedbackQuestionTitle format rules
    """

    def __repr__(self):
        return f"FeedbackQuestionTitle({super().__repr__()})"


class FeedbackQuestionAnswerLabel(constr(min_length=2, max_length=50)):
    """
    String validated with ylitse FeedbackQuestionAnswerLabel format rules
    """

    def __repr__(self):
        return f"FeedbackQuestionAnswerLabel({super().__repr__()})"


class FeedbackAnswerOption(NonNegativeInt):
    """
    Int validated with ylitse FeedbackAnswerOption rules
    """

    def __repr__(self):
        return f"FeedbackAnswerOption({super().__repr__()})"


class FeedbackQuestionTitles(BaseModel, extra=Extra.forbid):
    fi: FeedbackQuestionTitle
    en: FeedbackQuestionTitle


class FeedbackQuestionAnswerLabels(BaseModel, extra=Extra.forbid):
    fi: FeedbackQuestionAnswerLabel
    en: FeedbackQuestionAnswerLabel


class FeedbackRangeAnswerEndValue(BaseModel, extra=Extra.forbid):
    value: NonNegativeInt
    labels: FeedbackQuestionAnswerLabels


class FeedbackYesNoAnswerOption(BaseModel, extra=Extra.forbid):
    value: FeedbackAnswerOption
    labels: FeedbackQuestionAnswerLabels


class FeedbackYesNoAnswer(BaseModel, extra=Extra.forbid):
    type: Literal["yes-no"]
    yes: FeedbackYesNoAnswerOption
    no: FeedbackYesNoAnswerOption


class FeedbackRangeAnswer(BaseModel, extra=Extra.forbid):
    type: Literal["range"]
    step: PositiveInt
    min: FeedbackRangeAnswerEndValue
    max: FeedbackRangeAnswerEndValue

    @validator("max")
    def max_gt_min(cls, v, values, **kwargs):  # noqa, pylint: disable=no-self-argument
        assert v.value > values["min"].value, "must be greater than min"
        return v


class FeedbackScheduleFirst(BaseModel, extra=Extra.forbid):
    days_since_registration: NonNegativeFloat
    sent_messages_threshold: NonNegativeInt
    max_old_account_in_days: PositiveFloat


class FeedbackSchedulePredefinedRepetition(BaseModel, extra=Extra.forbid):
    type: Literal["predefined"]
    times: NonNegativeInt
    days_since_previous_answer: PositiveFloat
    min_days_since_previous_answer: PositiveFloat
    messages_since_previous_answer: NonNegativeInt


class FeedbackScheduleUntilValueRepetition(BaseModel, extra=Extra.forbid):
    type: Literal["until_value"]
    comparison: Literal["exact"]
    value: FeedbackAnswerOption
    days_since_previous_answer: PositiveFloat


class FeedbackSchedule(BaseModel, extra=Extra.forbid):
    remind_times_when_skipped: NonNegativeInt
    remind_interval_days: PositiveFloat
    first: FeedbackScheduleFirst
    repetitions: Union[
        FeedbackSchedulePredefinedRepetition,
        FeedbackScheduleUntilValueRepetition,
    ]


# Shared properties
class FeedbackQuestionRules(BaseModel, extra=Extra.forbid):
    titles: FeedbackQuestionTitles
    recipients: List[constr(min_length=2, max_length=50)]
    answer: Union[FeedbackRangeAnswer, FeedbackYesNoAnswer]
    schedule: FeedbackSchedule


class FeedbackQuestionBase(BaseModel, extra=Extra.forbid):
    rules: FeedbackQuestionRules


# # Properties to receive on item creation
class FeedbackQuestionCreate(FeedbackQuestionBase):
    pass


class FeedbackAnswerValue(NonNegativeInt):
    """
    Int validated with ylitse FeedbackAnswer rules
    """

    def __repr__(self):
        return f"FeedbackAnswerValue({super().__repr__()})"


class FeedbackAnswerId(constr(min_length=2, max_length=200)):
    """
    String validated with ylitse FeedbackAnswerId format rules
    """

    def __repr__(self):
        return f"FeedbackAnswerId({super().__repr__()})"


class FeedbackAnswerBase(BaseModel, extra=Extra.forbid):
    value: FeedbackAnswerValue
    answer_id: FeedbackAnswerId


# Properties to receive on item creation
class FeedbackAnswerCreate(FeedbackAnswerBase):
    pass
