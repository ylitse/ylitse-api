from typing import Union
from typing_extensions import Literal
from pydantic import BaseModel, conlist
from pydantic import Extra  # pylint: disable=unused-import

from .types import (
    MentorID,
    SkillName,
)


class SkillFilterEventProps(BaseModel, extra=Extra.forbid):
    skills: conlist(SkillName, min_items=1, max_items=100)


class SkillFilterEvent(BaseModel, extra=Extra.forbid):
    name: Literal["filter_skills"]
    props: SkillFilterEventProps


class MentorProfileViewEventProps(BaseModel, extra=Extra.forbid):
    mentor_id: MentorID


class MentorProfileViewEvent(BaseModel, extra=Extra.forbid):
    name: Literal["open_mentor_profile"]
    props: MentorProfileViewEventProps


# Properties to receive on item creation
class StatEventsCreate(BaseModel, extra=Extra.forbid):
    __root__: conlist(Union[SkillFilterEvent, MentorProfileViewEvent], max_items=100)
