import sys
import traceback

from pydantic import ValidationError
from aiohttp import web


def one_via_schema(schema, some_json):
    try:
        return schema.parse_obj(some_json).dict(exclude_unset=True)
    except ValidationError:
        traceback.print_exc(file=sys.stderr)
        raise web.HTTPUnprocessableEntity()
    # except ValidationError as e:
    #     print(some_json)
    #     print(e)
    #     raise e


def one_via_schema_as_list(schema, a_list):
    return one_via_schema(schema, a_list)["__root__"]
