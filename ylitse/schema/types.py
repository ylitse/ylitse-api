from pydantic import constr


class MentorID(constr(min_length=2, max_length=200)):
    """
    String validated with ylitse MentorID format rules
    """

    def __repr__(self):
        return f"MentorID({super().__repr__()})"


class DisplayName(constr(min_length=1, max_length=30)):
    """
    String validated with ylitse DisplayName format rules
    """

    def __repr__(self):
        return f"DisplayName({super().__repr__()})"


class SkillName(constr(min_length=2, max_length=40)):
    """
    String validated with ylitse SkillName format rules
    """

    def __repr__(self):
        return f"SkillName({super().__repr__()})"
