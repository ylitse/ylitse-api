"""Main module.

A web server application that can be passed to gunicorn. Also works as
standalone program (run from project root):

    PYTHONPATH=. python ylitse/main.py --host 127.0.0.1 --port 8080
"""

import argparse
import logging

import aiohttp

from ylitse.app import create_app

DEFAULT_HOST = "127.0.0.1"
DEFAULT_PORT = 8080

app = create_app()


def main():
    """Run a standalone development server."""
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument("--host", default=None)
    parser.add_argument("--port", type=int, default=0)

    args = parser.parse_args()
    host = args.host or DEFAULT_HOST
    port = args.port or DEFAULT_PORT

    aiohttp.web.run_app(app, host=host, port=port)


if __name__ == "__main__":
    main()
