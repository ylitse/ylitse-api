"""Version module.

This module contains only project version info. It is used by setup.py:

    python3.10 setup.py --version

And version endpoint among others:

    curl 127.0.0.1:8080/version

Remember to update NEWS file too when bumping a version.
"""

__version__ = "0.9.1"
