from enum import Enum


class EnumWithList(Enum):
    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))
