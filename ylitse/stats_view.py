"""stats module.

Contains `/stats` endpoint implementation.
"""

from aiohttp import web

from ylitse.store_abstraction import StoreView


def stats_and_totals(user_list, contact_statuses, sent, received):
    stats = {}
    totals = {
        "mentees": {
            "sent": 0,
            "received": 0,
            "banned": 0,
        },
        "mentors": {
            "sent": 0,
            "received": 0,
            "banned": 0,
        },
        "admins": {
            "sent": 0,
            "received": 0,
            "banned": 0,
        },
    }
    for user in user_list:
        user_id = user["id"]
        sent_messages = sent.get(user_id) or 0
        received_messages = received.get(user_id) or 0
        banned = 0

        for contact_status in contact_statuses:
            if contact_status["contact_user_id"] == user_id and contact_status[
                "status"
            ] in ["banned", "deleted"]:
                banned += 1

        account_id = user["account_id"]
        stats[account_id] = {
            "sent": sent_messages,
            "received": received_messages,
            "banned": banned,
        }

        role = user["role"]
        totals[f"{role}s"]["sent"] += sent_messages
        totals[f"{role}s"]["received"] += received_messages
        totals[f"{role}s"]["banned"] += banned
    return stats, totals


class StatsView:
    def __init__(self, store):
        self.messages = StoreView(store, "MESSAGE")
        self.users = StoreView(store, "USER")
        self.contact_statuses = StoreView(store, "CONTACT")

    async def get(self, request):
        all_messages = await self.messages.read()

        received = {}
        for message in all_messages:
            user_id = message["recipient_id"]
            count = received.setdefault(user_id, 0)
            received[user_id] = count + 1

        sent = {}
        for message in all_messages:
            user_id = message["sender_id"]
            count = sent.setdefault(user_id, 0)
            sent[user_id] = count + 1

        user_list = await self.users.read()
        contact_statuses = await self.contact_statuses.read()

        stats, totals = stats_and_totals(user_list, contact_statuses, sent, received)

        return web.json_response(
            {
                "totals": totals,
                "stats": stats,
            }
        )
