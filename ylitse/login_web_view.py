"""Login web module.

Contains `/weblogin` and `/webrefresh` endpoint implementation.
"""

from aiohttp import web
from multidict import MultiDict
from .config import config


class WebLoginView:
    """Endpoint methods."""

    def __init__(self, session_manager, jwt_functions):
        self.session_manager = session_manager
        self.jwt = jwt_functions

    def create_cookie_headers(self, access_token, refresh_token):
        """Generate headers with cookies for access and refresh tokens."""
        return MultiDict(
            [
                (
                    "Set-Cookie",
                    f'YLITSE_ACCESS="{access_token}"; '
                    "HttpOnly; "
                    "Path=/; "
                    "Secure; "
                    f'Max-Age={config["access_token_ttl"]}; '
                    "SameSite=Strict",
                ),
                (
                    "Set-Cookie",
                    f'YLITSE_REFRESH="{refresh_token}"; '
                    "HttpOnly; "
                    "Path=/; "
                    "Secure; "
                    f'Max-Age={config["web_refresh_token_ttl"]}; '
                    "SameSite=Strict",
                ),
            ]
        )

    async def login(self, request):
        """Process login request."""
        try:
            form = await request.post()
            username = form.get("username")
            password = form.get("password")
            mfa_token = form.get("mfa_token")

            tokens = (
                await self.session_manager.login(
                    login_name=username,
                    password=password,
                    mfa_token=mfa_token,
                    client="web",
                )
            )["tokens"]

            access_token = tokens["access_token"]
            refresh_token = tokens["refresh_token"]

            return web.json_response(
                {"message": "You were logged in."},
                headers=self.create_cookie_headers(access_token, refresh_token),
            )

        except (AssertionError, ValueError):
            raise web.HTTPBadRequest

    async def refresh(self, request):
        """Process refresh request."""
        try:
            refresh_token = self.jwt.extract_token(request, "webrefresh")
            if refresh_token["client"] != "web":
                raise web.HTTPUnauthorized

            access_token = await self.session_manager.refresh(refresh_token)

            return web.json_response(
                {"message": "Access token was refreshed."},
                headers=self.create_cookie_headers(
                    access_token, self.jwt.encode(refresh_token)
                ),
            )

        except web.HTTPUnauthorized:
            refresh_token = self.jwt.decode(
                self.jwt.get_cookie_token(request, 'YLITSE_REFRESH="'), False
            )
            await self.session_manager.forget(refresh_token["session_id"])
            return web.json_response(
                {"message": "Related refresh token invalidated."},
                status=401,
                headers=MultiDict(
                    [
                        (
                            "Set-Cookie",
                            'YLITSE_ACCESS="expired"; '
                            "HttpOnly; "
                            "Path=/; "
                            "Secure; "
                            "Max-Age=0; "
                            "SameSite=Strict",
                        ),
                        (
                            "Set-Cookie",
                            'YLITSE_REFRESH="expired"; '
                            "HttpOnly; "
                            "Path=/; "
                            "Secure; "
                            "Max-Age=0; "
                            "SameSite=Strict",
                        ),
                    ]
                ),
            )

        except Exception:
            raise web.HTTPUnauthorized
