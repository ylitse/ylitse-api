"""stats module.

Contains `/stats/period` endpoint implementation.
"""
from datetime import datetime

from aiohttp import web


def parse_period(start, end):
    return (
        datetime.strptime(start, "%Y-%m-%d").date(),
        datetime.strptime(end, "%Y-%m-%d").date(),
    )


class StatsPeriodView:
    def __init__(self, store, reader):
        self.store = store
        self.reader = reader

    async def get_daily(self, request):
        try:
            start_date, end_date = parse_period(
                request.match_info["start"], request.match_info["end"]
            )
        except ValueError:
            return web.Response(status=400)
        else:
            csv = await self.reader.daily_csv(self.store, start_date, end_date)
            return web.Response(text=csv, content_type="text/csv")

    async def get_monthly(self, request):
        try:
            start_date, end_date = parse_period(
                request.match_info["start"], request.match_info["end"]
            )
        except ValueError:
            return web.Response(status=400)
        else:
            csv = await self.reader.monthly_csv(self.store, start_date, end_date)
            return web.Response(text=csv, content_type="text/csv")

    async def get_weekly(self, request):
        try:
            start_date, end_date = parse_period(
                request.match_info["start"], request.match_info["end"]
            )
        except ValueError:
            return web.Response(status=400)
        else:
            csv = await self.reader.weekly_csv(self.store, start_date, end_date)
            return web.Response(text=csv, content_type="text/csv")

    async def get_period(self, request):
        try:
            start_date, end_date = parse_period(
                request.match_info["start"], request.match_info["end"]
            )
        except ValueError:
            return web.Response(status=400)
        else:
            csv = await self.reader.period_csv(self.store, start_date, end_date)
            return web.Response(text=csv, content_type="text/csv")
