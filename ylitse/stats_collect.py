#!/usr/bin/env python3.10
import hashlib

from datetime import datetime
from pytz import timezone as pytz_timezone

from .db.postgres_helpers import generate_utc_now_timestamp

timezone_helsinki = pytz_timezone("Europe/Helsinki")


def anonymize_id(string_id):
    return hashlib.sha3_224(string_id.encode()).hexdigest()


def from_iso_format_to_utc_timestamp(datetime_string):
    """Convert datetime from ylitse db to utc timestamp
    and round to nearest second"""
    # datetimes in ylitse db are missing timezone information
    # but they should be saved as utc so we force utc here when reading
    # eg: "created": "2020-11-26T14:36:36.894263" comes from
    # datetime.datetime.utcnow().isoformat()
    return round(datetime.timestamp(datetime.fromisoformat(datetime_string + "+00:00")))


async def insert_account(database_write, *, account, deleted=None):
    """Insert anonymized account to account table"""

    try:
        created = from_iso_format_to_utc_timestamp(account["created"])
        updated = from_iso_format_to_utc_timestamp(account["updated"])

        account_id = anonymize_id(account["id"])

        query = """ INSERT INTO stat_accounts
                        (id, role, updated, created, active, deleted)
                VALUES (:id,:role,:updated,:created,:active, :deleted)
                ON CONFLICT (id) DO UPDATE SET
                updated=EXCLUDED.updated,
                deleted=EXCLUDED.deleted; """

        values = {
            "id": account_id,
            "role": account["role"],
            "updated": updated,
            "created": created,
            "active": account["active"],
            "deleted": deleted,
        }
        return await database_write.execute(query=query, values=values)
    except KeyError:
        return None


async def delete_account(database_write, *, account, user_id):
    """Delete anonymized account"""

    now = generate_utc_now_timestamp()
    await insert_account(database_write, account=account, deleted=now)
    await delete_all_messages(database_write, user_id=user_id, deleted=now)
    await delete_mentor(database_write, user_id=user_id, deleted=now)


async def insert_message(database_write, *, message, from_role):
    """Insert anonymized message to messages table"""

    created = from_iso_format_to_utc_timestamp(message["created"])
    updated = from_iso_format_to_utc_timestamp(message["updated"])
    created_dt = datetime.fromisoformat(message["created"] + "+00:00")

    created_fi_dt = created_dt.astimezone(timezone_helsinki)
    time_of_day_utc = created_dt.hour
    time_of_day_fi = created_fi_dt.hour

    message_id = anonymize_id(message["id"])
    sender_id = anonymize_id(message["sender_id"])
    recipient_id = anonymize_id(message["recipient_id"])

    query = """ INSERT INTO stat_messages
                (id,from_role,sender_id,recipient_id,opened,updated,
            created,active,time_of_day_fi,time_of_day_utc)
            VALUES (:id,:from_role,:sender_id,:recipient_id,:opened,:updated,
            :created,:active,:time_of_day_fi,:time_of_day_utc)
            ON CONFLICT (id) DO NOTHING"""

    values = {
        "id": message_id,
        "from_role": from_role,
        "sender_id": sender_id,
        "recipient_id": recipient_id,
        "opened": message["opened"],
        "updated": updated,
        "created": created,
        "active": message["active"],
        "time_of_day_fi": time_of_day_fi,
        "time_of_day_utc": time_of_day_utc,
    }
    return await database_write.execute(query=query, values=values)


async def delete_all_messages(database_write, *, user_id, deleted=None):
    """Delete all anonymized messages from one user"""
    anon_user_id = anonymize_id(user_id)

    query = """
        UPDATE stat_messages
        SET deleted=:deleted
        WHERE sender_id=:anon_user_id
        OR recipient_id=:anon_user_id
    """

    values = {
        "deleted": deleted,
        "anon_user_id": anon_user_id,
    }
    return await database_write.execute(query=query, values=values)


async def delete_one_chat(database_write, *, user_id, contact_user_id):
    """Delete anonymized messages from one chat"""
    deleted = generate_utc_now_timestamp()
    anon_user_id = anonymize_id(user_id)
    anon_contact_user_id = anonymize_id(contact_user_id)

    query = """
        UPDATE stat_messages
        SET deleted=:deleted
        WHERE
        (sender_id=:anon_user_id AND recipient_id=:anon_contact_user_id)
        OR
        (sender_id=:anon_contact_user_id AND recipient_id=:anon_user_id)
    """

    values = {
        "deleted": deleted,
        "anon_user_id": anon_user_id,
        "anon_contact_user_id": anon_contact_user_id,
    }
    return await database_write.execute(query=query, values=values)


async def delete_mentor(database_write, *, user_id, deleted):
    """Mark mentor as deleted"""
    anon_user_id = anonymize_id(user_id)

    query = """
        UPDATE stat_mentors
        SET deleted=:deleted
        WHERE
        user_id=:anon_user_id
    """

    values = {
        "deleted": deleted,
        "anon_user_id": anon_user_id,
    }
    return await database_write.execute(query=query, values=values)
