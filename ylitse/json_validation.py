"""Defines a function that gives us some valid stuff."""
from copy import deepcopy
import jsonschema
from aiohttp import web

from ylitse.data_definitions import data_definitions
from ylitse.store_abstraction import StoreView
from ylitse.utils import fst


async def get_json(request, data_type=None, store=None, match_info=None):
    """Get some good json."""
    store_view = StoreView(store, data_type)
    try:
        json = await request.json()
    except Exception:
        raise web.HTTPBadRequest

    remove_ignored(data_type, json)

    schema = data_definitions[data_type]["schema"]

    if request.method == "PUT":
        schema = deepcopy(data_definitions[data_type]["schema"])
        json_id = request.match_info[match_info]

        old_json = await store_view.read(id=json_id)
        if not any(old_json):
            raise web.HTTPNotFound
        old_json = fst(old_json)

        ro_keys = [*data_definitions[data_type]["ro_keys"], "id", "created", "active"]

        for k in ro_keys:
            schema["properties"][k] = {"enum": [old_json[k]]}

    try:
        jsonschema.validate(json, schema)
    except jsonschema.exceptions.ValidationError as e:
        print(json)
        print(schema)
        print()
        print(data_type)
        print()
        print(e)
        print(data_definitions)
        raise web.HTTPBadRequest

    raise_if_extra_json_properties(data_type=data_type, schema=schema, json=json)

    return json


def remove_ignored(data_type, json):
    """
    Delete inplace the data we want to ignore.

    Example: `updated` is in almost every resource
    but we dont want it to reach db ever.

    Others we want to ignore are data that are generated only in the backend.
    """
    default_ignores = {"updated"}
    ignores = data_definitions[data_type].get("ignores", [])
    for i in default_ignores.union(set(ignores)):
        if i in json:
            del json[i]


def raise_if_extra_json_properties(*, data_type, schema, json):
    """
    Fail the request if there are properties
    not defined in data_definitions
    """
    expected_properties = set(schema["properties"].keys())
    unexpected_properties = [x for x in json.keys() if x not in expected_properties]
    if any(unexpected_properties):
        print("Unexpected properties:", data_type, unexpected_properties)
        raise web.HTTPUnprocessableEntity
