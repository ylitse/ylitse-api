"""
Mentors view module.

Contains '/mentors' endpoint implementation.
"""
from aiohttp import web
from ylitse.json_validation import get_json

from ylitse.store_abstraction import StoreView
from ylitse.utils import fst

from .db import HandlePostgresExceptions
from .crud import crud_mentor, crud_stat_mentor

# pylint: disable=missing-docstring


class MentorsView:
    def __init__(self, store):
        self.data_type = "MENTOR"
        self.match = "mentor_id"
        self.store = store
        self.mentors = StoreView(store, self.data_type)

    async def get_json(self, request):
        return await get_json(
            request, store=self.store, data_type=self.data_type, match_info=self.match
        )

    async def get(self, request):
        mentors = await self.mentors.read()
        return web.json_response({"resources": mentors})

    async def get_by_id(self, request):
        data_elem = fst(await self.mentors.read(id=request.match_info[self.match]))
        if not data_elem:
            raise web.HTTPNotFound
        return web.json_response(data_elem)

    @HandlePostgresExceptions
    async def post(self, request):
        client_json = await self.get_json(request)

        async with self.store.database.connection() as connection:
            async with connection.transaction():
                stored_json = await self.mentors.create(client_json)
                await crud_stat_mentor.refresh(self.store, mentor_id=stored_json["id"])
                return web.json_response(stored_json, status=201)

    @HandlePostgresExceptions
    async def put(self, request):
        client_json = await self.get_json(request)

        async with self.store.database.connection() as connection:
            async with connection.transaction():
                stored_json = await self.mentors.update(client_json)
                await crud_stat_mentor.refresh(self.store, mentor_id=stored_json["id"])
                return web.json_response(stored_json, status=200)

    async def patch(self, request):
        """
        PATCH /mentors/{mentor_id} is used only to change
        vacation and status information.
        Eg: Request json:
        {
            "is_vacationing": true,
            "status_message": "Gone Gardening"
        }
        """
        mentor_id = request.match_info[self.match]

        json = await get_json(
            request,
            data_type="PATCH_MENTOR",
        )

        if len(mentor_id) == 0:
            raise web.HTTPBadRequest

        old_mentor = fst(await self.mentors.read(id=mentor_id))
        if not old_mentor:
            raise web.HTTPNotFound

        await crud_mentor.update_vacation_status(
            self.store,
            mentor_id=mentor_id,
            is_vacationing=json["is_vacationing"],
            status_message=json["status_message"],
        )
        return web.Response(status=204)
