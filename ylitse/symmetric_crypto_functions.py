from cryptography.fernet import Fernet


class SymmetricCryptoFunctions:
    """Functions to handle crypto things."""

    def __init__(self, encryption_key_file):
        """Initialize secret box using path for encryption key"""

        with open(encryption_key_file, encoding="ascii") as f:
            # Convert string key back to binary
            encryption_key = f.read().encode("ascii")

            # Initialize secret box with binary key
            self.encryption_box = Fernet(encryption_key)

    def decrypt_to_string(self, encrypted_bytes):
        """Decrypt to utf-8 string"""
        return self.encryption_box.decrypt(encrypted_bytes).decode("utf-8")

    def encrypt_string(self, s):
        """Encrypt given utf-8 string"""
        return self.encryption_box.encrypt(s.encode("utf-8"))

    def encrypt_and_rename_item(self, a_dict, *, decrypted_key, encrypted_key):
        """Modifies given dict by removing plain text item
        and adding new encrypted item"""
        c = a_dict.pop(decrypted_key)
        a_dict[encrypted_key] = self.encrypt_string(c)

    def decrypt_and_rename_item(self, a_dict, *, decrypted_key, encrypted_key):
        """Modifies given dict by removing encrypted item
        and adding new plain text item"""
        e = a_dict.pop(encrypted_key)
        a_dict[decrypted_key] = self.decrypt_to_string(e)

    def decrypt_one(self, item, keys):
        """Loops over a dict to find out if there is something it can decrypt.
        Beware! Modifies the given dict in place.
        """
        for key in keys:
            if key["encrypted"] in item:
                self.decrypt_and_rename_item(
                    item,
                    decrypted_key=key["decrypted"],
                    encrypted_key=key["encrypted"],
                )

    def decrypt_all(self, items, keys):
        """Loops over items to find out if there is something to decrypt.
        Beware! Modifies the given collection in place.
        """
        for r in items:
            self.decrypt_one(r, keys)

    def encrypt_one(self, item, keys):
        """Loops over a dict to find out if there is something it can encrypt.
        Beware! Modifies the given dict in place.
        """
        for key in keys:
            if key["decrypted"] in item:
                self.encrypt_and_rename_item(
                    item,
                    decrypted_key=key["decrypted"],
                    encrypted_key=key["encrypted"],
                )
