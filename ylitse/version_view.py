"""Version view module.

Contains `/version` endpoint implementation.
"""

from aiohttp import web

from ylitse.store_abstraction import StoreView
from ylitse.version import __version__


class VersionView:
    """Endpoint methods."""

    def __init__(self, store):
        self.versions = StoreView(store, "MINIMUM_SUPPORTED_CLIENT_VERSIONS")

    async def get_version(self, request):
        """Return component versions."""
        return web.json_response({"api": __version__})

    async def get_minimum_versions(self, request):
        """Return minimum supported version for queried client."""
        clients = await self.versions.read()

        return web.json_response({"resources": clients})
