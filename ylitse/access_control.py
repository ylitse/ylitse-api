from aiohttp import web


class AccessControl:
    def __init__(self, jwt_functions):
        self.jwt = jwt_functions

    def access_role(self, request, role):
        token = self.jwt.extract_token(request, "access")
        return token["role"] == role

    def owner_access(self, request):
        token = self.jwt.extract_token(request, "access")
        for k in request.match_info.keys():
            if k in token["scopes"]:
                return token["scopes"][k] == request.match_info[k]
        return False

    def admin_only(self, handler):
        async def admin_only_handler(request):
            if not self.access_role(request, "admin"):
                raise web.HTTPUnauthorized
            return await handler(request)

        return admin_only_handler

    def is_admin_or_owner(self, request):
        return self.owner_access(request) or self.access_role(request, "admin")

    def admin_or_owner(self, handler):
        async def admin_or_owner_handler(request):
            if not self.is_admin_or_owner(request):
                raise web.HTTPUnauthorized
            return await handler(request)

        return admin_or_owner_handler

    def admin_or_owner_not_mentor(self, handler):
        async def admin_or_owner_not_mentor_handler(request):
            if not self.is_admin_or_owner(request) or self.access_role(
                request, "mentor"
            ):
                raise web.HTTPUnauthorized
            return await handler(request)

        return admin_or_owner_not_mentor_handler

    def owner_only(self, handler):
        async def owner_only_handler(request):
            if not self.owner_access(request):
                raise web.HTTPUnauthorized
            return await handler(request)

        return owner_only_handler

    def logged_in(self, handler):
        async def logged_in_handler(request):
            token = self.jwt.extract_token(request, "access")
            if not token:
                raise web.HTTPUnauthorized
            return await handler(request)

        return logged_in_handler

    def anyone(self, handler):
        async def anyone_handler(request):
            return await handler(request)

        return anyone_handler
