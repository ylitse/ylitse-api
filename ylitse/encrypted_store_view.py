from copy import deepcopy

from ylitse.store_abstraction import StoreView


class EncryptedStoreView:
    """Encrypt and decrypt certain items"""

    def __init__(self, *, store, crypto, data_type, keys):
        self.store = store
        self.crypto = crypto
        self.keys = keys
        self.data_type = data_type
        self.store_view = StoreView(store, data_type)

    async def read(self, **kwargs):
        """Read some and decrypt before returning"""
        res = await self.store_view.read(**kwargs)
        if not res:
            return res

        self.crypto.decrypt_all(res, self.keys)

        return res

    async def delete(self, **kwargs):
        """Delete some."""
        await self.store_view.delete(**kwargs)

    async def create(self, some_json):
        """Create some. Encrypt before creating and
        decrypt before returning."""
        to_insert = deepcopy(some_json)

        self.crypto.encrypt_one(to_insert, self.keys)
        created = await self.store_view.create(to_insert)
        self.crypto.decrypt_one(created, self.keys)

        return created

    async def update(self, some_json):
        """Update some. Encrypt before updating and
        decrypt before returning."""
        to_update = deepcopy(some_json)

        self.crypto.encrypt_one(to_update, self.keys)
        updated = await self.store_view.update(to_update)
        self.crypto.decrypt_one(updated, self.keys)

        return updated
