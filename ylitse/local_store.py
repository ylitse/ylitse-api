"""Local store module.

Contains local file store implementation.
"""
import json
from functools import reduce

from ylitse.store_abstraction import StoreAbstraction


def read_file_to_list(file_name):
    """Open file and read it to list."""
    # Gonna break if somebody touches the file
    # when API is running. Maybe fix someday.
    try:
        with open(file_name, "r", encoding="utf-8") as f:
            return json.load(f)
    except FileNotFoundError:
        with open(file_name, "w+", encoding="utf-8") as f:
            json.dump([], f)
            return []


def write_list(list_to_write, file_name):
    """Open file and write a list to it."""
    with open(file_name, "w+", encoding="utf-8") as f:
        json.dump(list_to_write, f)


def apply_filter(some_list, kwargs, filter_fun=lambda x, y: x == y):
    """Apply filter args from dict."""
    return list(
        filter(
            lambda x: reduce(
                # pylint: disable=undefined-variable
                lambda y, z: filter_fun(kwargs[z], x[z]) and y,
                kwargs.keys(),
                True,
            ),
            some_list,
        )
    )


class LocalStore(StoreAbstraction):
    """Store that stores to files."""

    def __init__(self, some_path):
        """Specify the path."""
        self.base_path = some_path

    async def on_startup(self, app=None):
        """Method to call on app startup"""

    async def on_shutdown(self, app=None):
        """Method to call on app shutdown"""

    def read_file(self, data_type):
        """Read some file to a list."""
        return read_file_to_list(self.base_path + "_" + data_type)

    def write_file(self, data_type, some_list):
        """Write some file to a list."""
        write_list(some_list, self.base_path + "_" + data_type)

    async def _read(self, data_type, **kwargs):
        """Implement get."""
        return apply_filter(self.read_file(data_type), kwargs)
        # return apply_filter([], kwargs)

    async def delete(self, data_type, **kwargs):
        """Implement delete."""
        self.write_file(
            data_type,
            apply_filter(
                self.read_file(data_type), kwargs, filter_fun=lambda x, y: x != y
            ),
        )

    async def _create(self, data_type, some_data):
        """Implement create."""
        # stored_data = {**some_data, 'id': str(time.time()).replace('.', '')}
        self.write_file(data_type, [*self.read_file(data_type), some_data])
        return some_data

    async def _update(self, data_type, some_data):
        """Implement update."""
        self.write_file(
            data_type,
            list(
                map(
                    lambda x: x if x["id"] != some_data["id"] else some_data,
                    self.read_file(data_type),
                )
            ),
        )
        return some_data
