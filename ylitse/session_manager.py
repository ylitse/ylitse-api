import time

from aiohttp import web

from ylitse.store_abstraction import StoreView
from ylitse.utils import fst
from ylitse.db.postgres_exception_handler import HandlePostgresExceptions
from .crud import crud_mentor, crud_stat_mentor


class SessionManager:
    """Add and Remove sessions."""

    def __init__(self, store, identity_manager, jwt):
        self.store = store
        self.jwt = jwt
        self.idm = identity_manager
        self.sessions = StoreView(store, "SESSION")

    async def new(self, identity, client):
        """Refresh tokens out"""
        login_name = identity["login_name"]
        account_id = identity["scopes"]["account_id"]
        session = await self.sessions.create(
            {
                "login_name": login_name,
                "account_id": account_id,
                "start_time": int(time.time()),
            }
        )
        return {
            "tokens": {
                "access_token": self.jwt.access_token(identity, session, client),
                "refresh_token": self.jwt.refresh_token(identity, session, client),
            },
            "scopes": identity["scopes"],
        }

    async def login(self, *, login_name, password, mfa_token=None, client="mobile"):
        """Tokens for creds."""
        identity = await self.idm.get_if_creds(login_name, password, mfa_token)
        await self.update_last_seen_for_mentor(identity)
        return await self.new(identity, client)

    async def forget(self, session_id):
        """Prevent refreshing session."""
        await self.sessions.delete(id=session_id)

    async def refresh(self, refresh_token):
        """Access token for refresh token."""
        session = fst(await self.sessions.read(id=refresh_token["session_id"]))
        if session:
            identity = await self.idm.get(login_name=session["login_name"])
            # print(identity)
            # print(session)

            if identity:
                await self.update_last_seen_for_mentor(identity)
                return self.jwt.access_token(identity, session)

            # If identity is not found, clean up old sessions.
            # This can happen when account is deleted but user is logged in.
            await self.sessions.delete(account_id=session["account_id"])

        raise web.HTTPUnauthorized

    @HandlePostgresExceptions
    async def update_last_seen_for_mentor(self, identity):
        if identity["role"] == "mentor":
            async with self.store.database.connection() as connection:
                async with connection.transaction():

                    await crud_mentor.update_last_seen(
                        self.store, mentor_id=identity["scopes"]["mentor_id"]
                    )

                    await crud_stat_mentor.refresh(
                        self.store, mentor_id=identity["scopes"]["mentor_id"]
                    )
