"""Postgres store module.

Contains Postgres store implementation.
"""
from copy import deepcopy


import sqlalchemy
import asyncpg

from aiohttp import web

from ylitse.db.postgres_exception_handler import HandlePostgresExceptions
from ylitse.db.postgres_helpers import now_utc, records_to_dict
from ylitse.postgres_tables import data_type_to_postgres_table, postgres_table_set

from ylitse.store_abstraction import StoreAbstraction, new_ylitse_id
from ylitse.utils import fst


# uncomment to see queries
# import logging  # noqa, pylint: disable=wrong-import-order
# logging.basicConfig(level=logging.DEBUG)
# logging.getLogger('databases').setLevel(logging.DEBUG)


class PostgresStore(StoreAbstraction):
    """Store that stores to postgres."""

    def __init__(self, database):
        self.database = database

    async def on_startup(self, app=None):
        """Method to call on app startup"""
        await self.database.connect()

    async def on_shutdown(self, app=None):
        """Method to call on app shutdown"""
        await self.database.disconnect()

    async def truncatedb(self):
        """Delete all data. Useful in tests."""
        for _, value in postgres_table_set():
            query = value.delete()
            await self.database.execute(query=query)

    @HandlePostgresExceptions
    async def _read(self, data_type, **kwargs):
        table = data_type_to_postgres_table(data_type)

        results = None
        if kwargs:
            if len(kwargs) != 1:
                raise AttributeError("Only one filter parameter allowed")

            filter_key = list(kwargs.keys())[0]
            filter_value = kwargs[filter_key]

            if filter_key == "query":
                results = await self.database.fetch_all(query=filter_value)
            else:
                try:
                    query = (
                        table.select()
                        .where(table.c[filter_key] == filter_value)
                        .order_by(sqlalchemy.asc(table.c.created))
                    )
                    results = await self.database.fetch_all(query=query)
                except KeyError:
                    raise AttributeError(
                        f"'{filter_key}' does not exist in '{data_type}'"
                    )
        else:
            query = table.select().order_by(sqlalchemy.asc(table.c.created))
            results = await self.database.fetch_all(query=query)

        return records_to_dict(results)

    @HandlePostgresExceptions
    async def delete(self, data_type, **kwargs):
        if not kwargs or len(kwargs) != 1:
            raise AttributeError("Provide one filter")

        table = data_type_to_postgres_table(data_type)
        filter_key = list(kwargs.keys())[0]
        filter_value = kwargs[filter_key]

        try:
            query = table.delete().where(table.c[filter_key] == filter_value)
            await self.database.execute(query=query)
        except KeyError:
            raise AttributeError(f"'{filter_key}' does not exist in '{data_type}'")

    async def create(self, data_type, data):
        """Create some. Check that no duplicates are created."""
        new_data = deepcopy(data)

        new_data["id"] = new_ylitse_id()
        new_data["updated"] = now_utc()
        new_data["created"] = new_data["updated"]
        new_data["active"] = True

        try:
            await self._create(data_type, new_data)
            return fst(await self._read(data_type, id=new_data["id"]))
        except asyncpg.exceptions.UniqueViolationError:
            raise web.HTTPConflict(text="already exists")

    @HandlePostgresExceptions
    async def _create(self, data_type, some_data):
        query = data_type_to_postgres_table(data_type).insert().values(some_data)
        await self.database.execute(query=query)

    @HandlePostgresExceptions
    async def _update(self, data_type, some_data):
        if "id" not in some_data:
            raise AttributeError("_update needs 'id'")

        new_data = deepcopy(some_data)
        existing_id = new_data.pop("id")

        # Created datetime should not update
        if "created" in new_data:
            del new_data["created"]

        new_data["updated"] = now_utc()

        table = data_type_to_postgres_table(data_type)
        query = table.update().where(table.c.id == existing_id).values(new_data)

        try:
            await self.database.execute(query=query)
            return fst(await self._read(data_type, id=existing_id))
        except asyncpg.exceptions.UniqueViolationError:
            raise web.HTTPConflict(text="already exists")
