"""analytical event creation module.

Contains `/events` endpoint implementation.
"""

from aiohttp import web
from .crud import crud_stat_event
from .schema import one_via_schema_as_list, StatEventsCreate

# pylint: disable=missing-docstring


class EventsView:
    def __init__(self, store):
        self.store = store

    async def post(self, request):
        client_json = one_via_schema_as_list(StatEventsCreate, await request.json())
        await crud_stat_event.create_all(self.store, events=client_json)
        return web.Response(status=201)
