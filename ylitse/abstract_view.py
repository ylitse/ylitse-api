"""abstract view module.

Contains `/accounts` endpoint implementation.
"""

from aiohttp import web

from ylitse.utils import fst
from ylitse.json_validation import get_json
from ylitse.store_abstraction import StoreView

# pylint: disable=missing-docstring


class AbstractView:
    def __init__(self, store, data_type, match):
        self.store = StoreView(store, data_type)
        self.match = match

    async def get_json(self, request):
        return await get_json(
            request,
            store=self.store.store,
            data_type=self.store.data_type,
            match_info=self.match,
        )

    async def get(self, request):
        return web.json_response({"resources": await self.store.read()})

    async def get_by_id(self, request):
        data_elem = fst(await self.store.read(id=request.match_info[self.match]))
        if not data_elem:
            raise web.HTTPNotFound
        return web.json_response(data_elem)

    async def post(self, request):
        client_json = await self.get_json(request)
        stored_json = await self.store.create(client_json)
        return web.json_response(stored_json, status=201)

    async def put(self, request):
        client_json = await self.get_json(request)
        stored_json = await self.store.update(client_json)
        return web.json_response(stored_json, status=200)

    async def delete(self, request):
        await self.store.delete(id=request.match_info[self.match])
        return web.Response(status=204)
