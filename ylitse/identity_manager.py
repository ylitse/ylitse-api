import pyotp

from passlib.hash import argon2
from aiohttp import web


from ylitse.store_abstraction import StoreView
from ylitse.utils import fst


def hash_password(password):
    """Create a password hash."""
    return argon2.hash(password)


class IdentityManager:
    """Add and Remove identities."""

    def __init__(self, store):
        self.identities = StoreView(store, "IDENTITY")

    async def new(self, *, login_name, password, role, scopes, mfa_secret=None):
        """Create auth object for jwt and login."""
        await self.identities.create(
            {
                "login_name": login_name,
                "password": hash_password(password),
                "role": role,
                "scopes": scopes,
                "mfa_secret": mfa_secret,
            }
        )

    async def delete(self, *, login_name):
        await self.identities.delete(login_name=login_name)

    async def get(self, *, login_name):
        return fst(await self.identities.read(login_name=login_name))

    async def get_if_creds(self, login_name, password, mfa_token=None):
        identity = await self.get(login_name=login_name)

        if identity is None:
            raise web.HTTPBadRequest
        # Admin requires MFA login
        if identity.get("role") == "admin":
            if not mfa_token:
                raise web.HTTPBadRequest
            failed_login_count = identity.get("failed_login_count", 0)
            if failed_login_count > 10:
                raise web.HTTPBadRequest
            if not identity.get("mfa_secret"):
                raise web.HTTPBadRequest
            mfa_secret = pyotp.TOTP(identity.get("mfa_secret"))
            if not mfa_secret.verify(mfa_token):
                await self.identities.update(
                    {
                        **identity,
                        "failed_login_count": failed_login_count + 1,
                    }
                )
                raise web.HTTPBadRequest
            # Reset counter to 0 after successful login
            await self.identities.update(
                {
                    **identity,
                    "failed_login_count": 0,
                }
            )

        if not argon2.verify(password, identity["password"]):
            raise web.HTTPBadRequest

        return identity

    async def change_password(self, login_name, current_password, new_password):
        """Change password."""
        identity = await self.get_if_creds(login_name, current_password)
        await self.identities.update(
            {**identity, "password": hash_password(new_password)}
        )

    async def set_password(self, login_name, password):
        identity = await self.get(login_name=login_name)
        if not identity:
            raise web.HTTPNotFound
        await self.identities.update(
            {
                **identity,
                "password": hash_password(password),
            }
        )
