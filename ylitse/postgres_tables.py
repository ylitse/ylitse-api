import sqlalchemy
from sqlalchemy.dialects.postgresql import ENUM, JSONB

ROLE_ENUM = ENUM("mentee", "mentor", "admin", name="role")
GENDER_ENUM = ENUM("male", "female", "other", name="gender")
PLATFORM_ENUM = ENUM("android", "ios", name="platform")
CONTACT_STATUS_ENUM = ENUM("ok", "banned", "deleted", "archived", name="contact_status")
REPORT_STATUS_ENUM = ENUM("received", "handled", name="report_status")


sqlalchemy_metadata = sqlalchemy.MetaData()


table_accounts = sqlalchemy.Table(
    "accounts",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("login_name", sqlalchemy.Unicode(length=100), unique=True),
    sqlalchemy.Column("role", ROLE_ENUM),
    sqlalchemy.Column(
        "email", sqlalchemy.Unicode(length=200), nullable=True, unique=True
    ),
    sqlalchemy.Column("phone", sqlalchemy.Unicode(length=20), nullable=True),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)

table_users = sqlalchemy.Table(
    "users",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("display_name", sqlalchemy.Unicode(length=30)),
    sqlalchemy.Column("role", ROLE_ENUM),
    sqlalchemy.Column("account_id", sqlalchemy.String(length=200), unique=True),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)


table_contacts = sqlalchemy.Table(
    "contacts",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("user_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("contact_user_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("status", CONTACT_STATUS_ENUM, default="ok"),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)

table_skills = sqlalchemy.Table(
    "skills",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.Unicode(length=40), unique=True),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)

table_messages = sqlalchemy.Table(
    "messages",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("sender_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("recipient_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("content_enc", sqlalchemy.LargeBinary()),
    sqlalchemy.Column("opened", sqlalchemy.Boolean()),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)

table_mentors = sqlalchemy.Table(
    "mentors",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column(
        "account_id",
        sqlalchemy.String(length=200),
    ),
    sqlalchemy.Column("user_id", sqlalchemy.String(length=200), unique=True),
    sqlalchemy.Column("languages", JSONB()),
    sqlalchemy.Column("birth_year", sqlalchemy.Integer()),
    sqlalchemy.Column("display_name", sqlalchemy.Unicode(length=30)),
    sqlalchemy.Column("gender", GENDER_ENUM),
    sqlalchemy.Column("region", sqlalchemy.Unicode(length=100)),
    sqlalchemy.Column("story", sqlalchemy.Unicode(length=2000)),
    sqlalchemy.Column("skills", JSONB()),
    sqlalchemy.Column("communication_channels", JSONB()),
    sqlalchemy.Column("is_vacationing", sqlalchemy.Boolean()),
    sqlalchemy.Column("status_message", sqlalchemy.Unicode(length=2000)),
    sqlalchemy.Column("last_seen", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)

table_identities = sqlalchemy.Table(
    "identities",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("login_name", sqlalchemy.Unicode(length=100), unique=True),
    sqlalchemy.Column("password", sqlalchemy.Unicode(length=100)),
    sqlalchemy.Column("mfa_secret", sqlalchemy.Unicode(length=100)),
    sqlalchemy.Column("role", ROLE_ENUM),
    sqlalchemy.Column(
        "scopes",
        JSONB(),
    ),
    sqlalchemy.Column("failed_login_count", sqlalchemy.Integer(), default=0),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)

table_sessions = sqlalchemy.Table(
    "sessions",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("login_name", sqlalchemy.Unicode(length=100)),
    sqlalchemy.Column("account_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("start_time", sqlalchemy.Integer()),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)


table_devices = sqlalchemy.Table(
    "devices",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("user_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("platform", PLATFORM_ENUM),
    sqlalchemy.Column("token", sqlalchemy.Unicode(length=300)),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)

table_private_datas = sqlalchemy.Table(
    "private_datas",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("user_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("starred_users", JSONB()),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False)),
)

table_stat_accounts = sqlalchemy.Table(
    "stat_accounts",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("role", ROLE_ENUM),
    sqlalchemy.Column("updated", sqlalchemy.Integer()),
    sqlalchemy.Column("created", sqlalchemy.Integer()),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("deleted", sqlalchemy.Integer()),
)

table_stat_events = sqlalchemy.Table(
    "stat_events",
    sqlalchemy_metadata,
    sqlalchemy.Column(
        "id", sqlalchemy.BigInteger, sqlalchemy.Identity(), primary_key=True
    ),
    sqlalchemy.Column("name", sqlalchemy.String(length=200)),
    sqlalchemy.Column("props", JSONB()),
    sqlalchemy.Column("created", sqlalchemy.Integer()),
)

table_stat_mentors = sqlalchemy.Table(
    "stat_mentors",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column(
        "user_id", sqlalchemy.String(length=200), nullable=False, unique=True
    ),
    sqlalchemy.Column("display_name", sqlalchemy.Unicode(length=30), nullable=False),
    sqlalchemy.Column("last_seen", sqlalchemy.Integer(), nullable=True),
    sqlalchemy.Column("deleted", sqlalchemy.Integer(), nullable=True),
    sqlalchemy.Column("created", sqlalchemy.Integer(), nullable=False),
)

table_stat_messages = sqlalchemy.Table(
    "stat_messages",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("from_role", ROLE_ENUM),
    sqlalchemy.Column("sender_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("recipient_id", sqlalchemy.String(length=200)),
    sqlalchemy.Column("opened", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.Integer()),
    sqlalchemy.Column("created", sqlalchemy.Integer()),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("time_of_day_fi", sqlalchemy.Integer()),
    sqlalchemy.Column("time_of_day_utc", sqlalchemy.Integer()),
    sqlalchemy.Column("deleted", sqlalchemy.Integer()),
)


table_feedback_questions = sqlalchemy.Table(
    "feedback_questions",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column(
        "rules",
        JSONB(),
        nullable=False,
    ),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False), nullable=False),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False), nullable=False),
)


table_feedback_answers = sqlalchemy.Table(
    "feedback_answers",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column(
        "feedback_question_id", sqlalchemy.String(length=200), nullable=False
    ),
    sqlalchemy.Column("user_id", sqlalchemy.String(length=200), nullable=False),
    sqlalchemy.Column("asked_at", sqlalchemy.DateTime(timezone=False), nullable=True),
    sqlalchemy.Column("retries", sqlalchemy.Integer(), nullable=False),
    sqlalchemy.Column("max_retries", sqlalchemy.Integer(), nullable=False),
    sqlalchemy.Column("value", sqlalchemy.Integer(), nullable=True),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False), nullable=False),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False), nullable=False),
)

table_reported_users = sqlalchemy.Table(
    "reported_users",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column(
        "reported_user_id", sqlalchemy.String(length=200), nullable=False
    ),
    sqlalchemy.Column(
        "reporter_user_id", sqlalchemy.String(length=200), nullable=False
    ),
    sqlalchemy.Column("contact_field", sqlalchemy.Unicode(length=200), nullable=True),
    sqlalchemy.Column("report_reason", sqlalchemy.Unicode(length=2000)),
    sqlalchemy.Column("status", REPORT_STATUS_ENUM, default="received"),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False), nullable=False),
    sqlalchemy.Column("comment", sqlalchemy.Unicode(length=2048), nullable=True),
)

table_chat_review_log = sqlalchemy.Table(
    "chat_review_log",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("chat_user_id_1", sqlalchemy.String(length=200), nullable=False),
    sqlalchemy.Column("chat_user_id_2", sqlalchemy.String(length=200), nullable=False),
    sqlalchemy.Column(
        "reviewer_user_id", sqlalchemy.Unicode(length=200), nullable=False
    ),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False), nullable=False),
)

table_minimum_supported_client_versions = sqlalchemy.Table(
    "minimum_supported_client_versions",
    sqlalchemy_metadata,
    sqlalchemy.Column("id", sqlalchemy.String(length=200), primary_key=True),
    sqlalchemy.Column("client", sqlalchemy.String(length=200), nullable=False),
    sqlalchemy.Column("version", sqlalchemy.String(length=200), nullable=False),
    sqlalchemy.Column("active", sqlalchemy.Boolean()),
    sqlalchemy.Column("updated", sqlalchemy.DateTime(timezone=False)),
    sqlalchemy.Column("created", sqlalchemy.DateTime(timezone=False), nullable=False),
)

to_table = {
    "ACCOUNT": table_accounts,
    "CHAT_REVIEW_LOG": table_chat_review_log,
    "CONTACT": table_contacts,
    "DEVICE": table_devices,
    "FEEDBACK_ANSWER": table_feedback_answers,
    "FEEDBACK_QUESTION": table_feedback_questions,
    "IDENTITY": table_identities,
    "MENTOR": table_mentors,
    "MESSAGE": table_messages,
    "PRIVATE_DATA": table_private_datas,
    "REPORTED_USERS": table_reported_users,
    "SESSION": table_sessions,
    "SKILL": table_skills,
    "STAT_ACCOUNT": table_stat_accounts,
    "STAT_EVENT": table_stat_events,
    "STAT_MENTOR": table_stat_mentors,
    "STAT_MESSAGE": table_stat_messages,
    "USER": table_users,
    "MINIMUM_SUPPORTED_CLIENT_VERSIONS": table_minimum_supported_client_versions,
}


def data_type_to_postgres_table(data_type):
    return to_table[data_type]


def postgres_table_set():
    return to_table.items()
