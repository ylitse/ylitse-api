import os
from pathlib import Path
import yaml
import jsonschema

CONFIG_SCHEMA = {
    "type": "object",
    "properties": {
        "store_path": {"type": "string"},
        "jwt_secret": {"type": "string"},
        "admin_username": {"type": "string"},
        "admin_password": {"type": "string"},
        "postgres_url": {"type": "string"},
        "encryption_key_file": {"type": "string"},
        "access_token_ttl": {"type": "integer", "default": 30 * 60},
        "web_refresh_token_ttl": {"type": "integer", "default": 60 * 60 * 24},
    },
}


def load_config(path):
    """Load and validate configuration file."""
    with open(path, encoding="utf-8") as f:
        c = yaml.safe_load(f)

        # encryption_key_file can be from config file or envvar.
        # Envvars takes priority over values loaded from a file.
        c["encryption_key_file"] = os.getenv(
            "ENCRYPTION_KEY_FILE", c.get("encryption_key_file", None)
        )
        if not c["encryption_key_file"]:
            raise ValueError(
                "Encryption key file missing. "
                "Add to config 'encryption_key_file: /etc/...' "
                "or use ENCRYPTION_KEY_FILE envvar"
            )

        c["access_token_ttl"] = c.get(
            "access_token_ttl",
            CONFIG_SCHEMA["properties"]["access_token_ttl"]["default"],
        )
        c["web_refresh_token_ttl"] = c.get(
            "web_refresh_token_ttl",
            CONFIG_SCHEMA["properties"]["web_refresh_token_ttl"]["default"],
        )

        jsonschema.validate(c, CONFIG_SCHEMA)

        return c


config_path = os.getenv("YLITSE_CONF", f"{str(Path.home())}/.config/ylitse/ylitse.conf")
config = load_config(config_path)
