# pylint: disable=too-many-locals,line-too-long
"""messages module.

Contains `/messages` endpoint implementation.
"""

import json
import os
import time

import aiohttp
import jwt
from aiohttp import web

from ylitse.json_validation import get_json
from ylitse.store_abstraction import StoreView
from ylitse.utils import fst

from .crud import crud_message, crud_contact

service_account = (
    "firebase-adminsdk-m87ag@ylitse-notifications.iam.gserviceaccount.com"  # noqa
)
default_path = (
    "/etc/ylitse/ylitse-notifications-firebase-adminsdk-m87ag-99e29d5c6d.json"  # noqa
)
file_path = os.getenv("FIREBASE_CONF", default_path)


def auth_token():
    scopes = [
        "https://www.googleapis.com/auth/firebase.messaging",
        "https://www.googleapis.com/auth/cloud-platform",
    ]
    iat = int(time.time())

    payload = {
        "iss": service_account,
        "scope": " ".join(scopes),
        "aud": "https://www.googleapis.com/oauth2/v4/token",
        "exp": iat + (60 * 30),
        "iat": iat,
    }

    with open(file_path, "r", encoding="utf-8") as f:
        read_json = json.load(f)
        private_key_str = read_json["private_key"]
        private_key_bytes = private_key_str.encode("utf-8")

        return jwt.encode(payload, private_key_bytes, algorithm="RS256")


class MessagesView:
    def __init__(self, store):
        self.store = store
        self.messages = StoreView(store, "MESSAGE")
        self.devices = StoreView(store, "DEVICE")
        self.users = StoreView(store, "USER")
        self.contact_statuses = StoreView(store, "CONTACT")
        self.mentors = StoreView(store, "MENTOR")

        self.refresh_token = None
        self.access_token = None

        try:
            self.refresh_token = auth_token()
        except FileNotFoundError:
            print("Warn: Firebase config was not found but continuing...")

    async def get_access_token(self, session):
        print("GETTING TOKENS")
        jwt_str = auth_token()
        url = "https://www.googleapis.com/oauth2/v4/token"
        payload = {
            "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
            "assertion": jwt_str,
        }
        async with session.post(url, data=payload) as resp:
            print(resp.status)
            resp_json = await resp.json()
            self.access_token = resp_json["access_token"]

    async def notify(self, device_token):
        if self.refresh_token is None:
            print("Warn: Cannot notify without firebase config")
            return

        print("notifguy")
        async with aiohttp.ClientSession() as session:
            if not self.access_token:
                await self.get_access_token(session)

            url = "https://fcm.googleapis.com/v1/projects/ylitse-notifications/messages:send"  # noqa
            headers = {
                "Authorization": f"Bearer {self.access_token}",
                "Content-Type": "application/json",
            }
            payload = {
                "message": {
                    "token": device_token,
                    "android": {
                        "notification": {
                            "title": "Uusi viesti",
                            "tag": "YLITSE_NEW_MESSAGE",
                        },
                    },
                    "notification": {"title": "Uusi viesti", "body": " "},
                }
            }

            async with session.post(
                url,
                headers=headers,
                json=payload,
            ) as resp:
                print("IMPORTANT LINE HERE")
                print(resp.status)
                asdf = await resp.json()
                print(asdf)
                if resp.status == 401:
                    await self.get_access_token(session)
                    headers = {
                        **headers,
                        "Authorization": f"Bearer {self.access_token}",
                    }
                    async with session.post(url, headers=headers, json=payload) as resp:
                        pass

    async def get_json(self, request):
        return await get_json(
            request,
            store=self.messages.store,
            data_type="MESSAGE",
        )

    async def delete(self, request):
        user_id = request.match_info["user_id"]
        contact_user_id = request.query.get("buddy_id")
        if not contact_user_id:
            return web.Response(status=400)

        await crud_message.delete(
            self.store, user_id=user_id, contact_user_id=contact_user_id
        )

        return web.Response(status=204)

    async def get(self, request):
        user_id = request.match_info["user_id"]
        contacts = await crud_contact.get_mine(
            self.store, user_id=user_id, ignore_deleted=False
        )

        from_message = await self.parse_message_id_and_get_message(
            user_id=user_id, message_id=request.query.get("from_message_id")
        )
        contact_user_ids = await self.parse_contact_user_ids(
            request.query.get("contact_user_ids"), all_contacts=contacts
        )
        is_desc = self.parse_boolean_from_string(request.query.get("desc"))

        # "desc" is required if given "from_*" filters or contact_user_ids
        if contact_user_ids and is_desc is None:
            raise web.HTTPBadRequest
        if from_message and is_desc is None:
            raise web.HTTPBadRequest

        max_messages = self.parse_positive_nonzero_int_from_string(
            request.query.get("max")
        )

        messages = await crud_message.get_mine(
            self.store,
            user_id=user_id,
            all_contacts=contacts,
            contact_user_ids=contact_user_ids,
            from_message=from_message,
            is_desc=is_desc,
            max_messages=max_messages,
        )

        return web.json_response(
            {
                "resources": self.patch_timezone(messages),
                "contacts": crud_contact.ignore_deleted(contacts),
            }
        )

    async def post(self, request):
        user_id = request.match_info["user_id"]
        client_json = await self.get_json(request)
        client_json = {**client_json, "sender_id": user_id}

        sender_id = user_id
        recipient_id = client_json["recipient_id"]
        sender_list = await self.users.read(id=sender_id)
        sender = fst(sender_list)

        if not sender:
            return web.json_response({"message": "Sender doesn't exist"}, status=400)
        if sender_id == recipient_id:
            return web.json_response(
                {"message": "Sender cannot be recipient"}, status=400
            )

        recipient_list = await self.users.read(id=recipient_id)
        recipient = fst(recipient_list)

        if not recipient:
            return web.json_response({"message": "Recipient doesn't exist"}, status=400)

        posted = await crud_message.post(
            self.store, data=client_json, sender_role=sender["role"]
        )

        device_info_list = await self.devices.read(user_id=recipient_id)
        device_info = fst(device_info_list)
        if (
            await self.should_msg_trigger_notification(sender_id, recipient)
            and device_info
        ):
            token = device_info["token"]
            await self.notify(token)

        return web.json_response(fst(self.patch_timezone([posted])), status=201)

    async def put(self, request):
        user_id = request.match_info["user_id"]
        message_id = request.match_info["id"]
        msg = await crud_message.mark_as_read(
            self.store, user_id=user_id, message_id=message_id
        )
        return web.json_response(fst(self.patch_timezone([msg])), status=200)

    async def is_vacationing_mentor(self, user):
        """
        Check if user is a mentor that is vacationing.
        """
        if user["role"] == "mentor":
            recipient_mentor = fst(await self.mentors.read(user_id=user["id"]))
            if recipient_mentor:
                return recipient_mentor.get("is_vacationing", False)

        return False

    async def should_msg_trigger_notification(self, sender_id, recipient):
        return not await self.is_vacationing_mentor(
            recipient
        ) and not await self.is_sender_chat_status_denied(sender_id, recipient["id"])

    async def is_sender_chat_status_denied(self, sender_id, recipient_id):
        deny_statuses = ["banned", "deleted", "archived"]
        recipients_contact_statuses = await self.contact_statuses.read(
            user_id=recipient_id
        )
        for contact_status in recipients_contact_statuses:
            if (
                contact_status["contact_user_id"] == sender_id
                and contact_status["status"] in deny_statuses
            ):
                return True

        return False

    async def parse_message_id_and_get_message(self, *, user_id, message_id):
        """Return message or raise web.HTTPBadRequest"""
        if not message_id:
            return None

        msg = await crud_message.get_by_id(
            self.store, user_id=user_id, message_id=message_id
        )
        if not msg:
            raise web.HTTPBadRequest

        return msg

    def parse_boolean_from_string(self, boolean_string):
        """Return True, False, None or raise web.HTTPBadRequest"""
        if boolean_string in [None, ""]:
            return None

        try:
            return self.str_to_bool(boolean_string)
        except ValueError:
            raise web.HTTPBadRequest

    def str_to_bool(self, value):
        """
        Return whether the provided string
        represents true/false. Otherwise raise ValueError.
        """
        trues = ("y", "yes", "t", "true", "on", "1")
        falses = ("n", "no", "f", "false", "off", "0")

        if str(value).lower() in trues:
            return True

        if str(value).lower() in falses:
            return False

        raise ValueError

    def parse_positive_nonzero_int_from_string(self, int_string):
        """Return None, integer or raise web.HTTPBadRequest"""
        if not int_string:
            return None

        try:
            num = int(int_string)
            if num <= 0:
                raise ValueError
            return num
        except ValueError:
            raise web.HTTPBadRequest

    async def parse_contact_user_ids(self, contact_user_ids, all_contacts):
        """Return list of user_ids if they exists
        or raise web.HTTPBadRequest"""
        if not contact_user_ids:
            return []

        user_ids = contact_user_ids.split(",")

        validated_user_ids = []
        for user_id in user_ids:
            contact = [c for c in all_contacts if c["id"] == user_id]
            if contact:
                validated_user_ids.append(user_id)

        # If cannot find any valid user_ids
        if len(validated_user_ids) == 0:
            raise web.HTTPBadRequest

        return validated_user_ids

    def patch_timezone(self, resources):
        """
        Add UTC timezone to created and updated.
        Delete this function when timezones are added to data someday.
        """
        resources_with_timezone = []
        for r in resources:
            r["updated"] = f"{r['updated']}+00:00"
            r["created"] = f"{r['created']}+00:00"
            resources_with_timezone.append(r)

        return resources_with_timezone
