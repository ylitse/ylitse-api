"""Route module.

All endpoints are added to the router here.
"""
# pylint: disable=too-many-locals


import os

from ylitse.abstract_view import AbstractView
from ylitse.access_control import AccessControl
from ylitse.accounts_view import AccountsView
from ylitse.contacts_view import ContactsView
from ylitse.device_view import DeviceView
from ylitse.events_view import EventsView
from ylitse.identity_manager import IdentityManager
from ylitse.jwt_functions import JwtFunctions
from ylitse.login_view import LoginView
from ylitse.login_web_view import WebLoginView
from ylitse.mentors_view import MentorsView
from ylitse.messages_view import MessagesView
from ylitse.my_user_view import MyUserView
from ylitse.search_view import SearchView
from ylitse.session_manager import SessionManager
from ylitse.stats_period_view import StatsPeriodView
from ylitse.stats_reader import StatsReader
from ylitse.stats_reader_mentors import StatsReaderMentors
from ylitse.stats_view import StatsView
from ylitse.user_account_manager import UserAccountManager
from ylitse.version_view import VersionView
from ylitse.views import FeedBackView
from ylitse.views.admin_chat_review import AdminMessagesView
from ylitse.views.chat_review_log_view import ChatReviewLog
from ylitse.views.reports_view import Reports


def interpret(app, access_control, routes):
    """Config routes and scopes according to definitions."""
    for scope in routes:
        for path in routes[scope]:
            for method in routes[scope][path]:
                {
                    "GET": app.router.add_get,
                    "POST": app.router.add_post,
                    "PUT": app.router.add_put,
                    "DELETE": app.router.add_delete,
                    "HEAD": app.router.add_head,
                    "PATCH": app.router.add_patch,
                }[method](
                    path,
                    {
                        "ADMIN_ONLY": access_control.admin_only,
                        "ADMIN_OR_OWNER": access_control.admin_or_owner,
                        "ADMIN_OR_OWNER_NOT_MENTOR": access_control.admin_or_owner_not_mentor,
                        "OWNER_ONLY": access_control.owner_only,
                        "LOGGED_IN": access_control.logged_in,
                        "ANYONE": access_control.anyone,
                    }[scope](routes[scope][path][method]),
                )


def setup_routes(*, app, store, secret):
    """Add endpoints to a router."""

    login_view = LoginView(
        SessionManager(store, IdentityManager(store), JwtFunctions(secret)),
        JwtFunctions(secret),
    )

    web_login_view = WebLoginView(
        SessionManager(store, IdentityManager(store), JwtFunctions(secret)),
        JwtFunctions(secret),
    )

    accounts_view = AbstractView(store, "ACCOUNT", "account_id")
    account_management_view = AccountsView(
        UserAccountManager(store, IdentityManager(store)), JwtFunctions(secret)
    )

    my_user_view = MyUserView(store)

    users_view = AbstractView(store, "USER", "user_id")
    skills_view = AbstractView(store, "SKILL", "id")
    private_data_view = AbstractView(store, "PRIVATE_DATA", "id")
    mentors_view = MentorsView(store)
    messages_view = MessagesView(store)
    version_view = VersionView(store)
    contacts_view = ContactsView(store)
    search = SearchView(store)
    device = DeviceView(store)
    stats_view = StatsView(store)
    stats_period_view = StatsPeriodView(store, reader=StatsReader())
    stats_period_view_mentors = StatsPeriodView(store, reader=StatsReaderMentors())

    events_view = EventsView(store)

    feedback_view = FeedBackView(store)

    reports_view = Reports(store)

    chat_review_log_view = ChatReviewLog(store)

    admin_messages_view = AdminMessagesView(store)

    SAFETY_FEATURE = os.getenv("SAFETY_FEATURE", "false")

    ROUTES = {
        "ADMIN_ONLY": {
            "/accounts": {"GET": accounts_view.get},
            "/users": {"GET": users_view.get},
            "/feedback/answers": {
                "GET": feedback_view.get_answers,
            },
            "/feedback/questions": {
                "GET": feedback_view.get_questions,
                "POST": feedback_view.post_question,
            },
            "/feedback/questions/{id}": {
                "DELETE": feedback_view.delete_question,
            },
            "/skills": {"POST": skills_view.post},
            "/skills/{id}": {"DELETE": skills_view.delete},
            "/stats/mentor/period/{start}/{end}": {
                "GET": stats_period_view_mentors.get_period
            },
            "/stats/period/{start}/{end}/daily": {"GET": stats_period_view.get_daily},
            "/stats/period/{start}/{end}/monthly": {
                "GET": stats_period_view.get_monthly
            },
            "/stats/period/{start}/{end}/weekly": {"GET": stats_period_view.get_weekly},
            "/stats/period/{start}/{end}": {"GET": stats_period_view.get_period},
            "/stats": {"GET": stats_view.get},
        },
        "ADMIN_OR_OWNER_NOT_MENTOR": {
            "/accounts/{account_id}": {
                "DELETE": account_management_view.delete,
            },
        },
        "ADMIN_OR_OWNER": {
            "/accounts/{account_id}/password": {
                "PUT": account_management_view.change_password,
            },
            "/accounts/{account_id}": {
                "GET": accounts_view.get_by_id,
                "PUT": accounts_view.put,
            },
            "/users/{user_id}": {
                "PUT": users_view.put,
            },
            "/mentors/{mentor_id}": {
                "GET": mentors_view.get_by_id,
                "PUT": mentors_view.put,
                "PATCH": mentors_view.patch,
            },
        },
        "OWNER_ONLY": {
            "/users/{user_id}/messages": {
                "GET": messages_view.get,
                "POST": messages_view.post,
                "DELETE": messages_view.delete,
            },
            "/users/{user_id}/messages/{id}": {
                "PUT": messages_view.put,
            },
            "/users/{user_id}/private_data": {
                "GET": private_data_view.get,
            },
            "/users/{user_id}/private_data/{id}": {
                "PUT": private_data_view.put,
            },
            "/users/{user_id}/contacts": {
                "GET": contacts_view.get,
                "PATCH": contacts_view.patch,
            },
            "/users/{user_id}/contacts/{contact_user_id}": {
                "PUT": contacts_view.put,
            },
            "/users/{user_id}/device": {
                "PUT": device.put,
                "GET": device.get,
            },
        },
        "LOGGED_IN": {
            "/users/{user_id}": {"GET": users_view.get_by_id},
            "/logout": {"GET": login_view.logout},
            "/skills": {"GET": skills_view.get},
            "/skills/{id}": {"GET": skills_view.get_by_id},
            "/myuser": {"GET": my_user_view.get},
            "/events": {"POST": events_view.post},
            "/feedback/needs_answers": {"GET": feedback_view.get_needs_answers},
            "/feedback/answer": {"POST": feedback_view.post_answer},
            "/version": {"GET": version_view.get_version},
            "/version/clients": {"GET": version_view.get_minimum_versions},
        },
        "ANYONE": {
            "/weblogin": {"POST": web_login_view.login},
            "/webrefresh": {"GET": web_login_view.refresh},
            "/login": {"POST": login_view.login},
            "/mentors": {"GET": mentors_view.get},
            "/refresh": {"POST": login_view.refresh},
            "/accounts": {"POST": account_management_view.post},
            "/search": {
                "HEAD": search.head,
            },
        },
    }

    if SAFETY_FEATURE == "true":
        ROUTES["ADMIN_ONLY"]["/reports"] = {"GET": reports_view.get_reports}
        ROUTES["ADMIN_ONLY"]["/reports/{report_id}"] = {
            "DELETE": reports_view.delete_report,
            "PATCH": reports_view.patch_report,
            "GET": reports_view.get_by_id,
        }
        ROUTES["ADMIN_ONLY"]["/users/{user_id}/messages_for_admin"] = {
            "GET": admin_messages_view.get
        }
        ROUTES["ADMIN_ONLY"]["/users/{user_id}/contacts_for_admin"] = {
            "GET": admin_messages_view.get_contacts
        }
        ROUTES["ADMIN_ONLY"]["/chat_review_logs"] = {"GET": chat_review_log_view.get}
        ROUTES["LOGGED_IN"]["/reports"] = {"POST": reports_view.post_report}

    interpret(app, AccessControl(JwtFunctions(secret)), ROUTES)
