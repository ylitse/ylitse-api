# pylint: disable=too-many-locals
"""
Generate stats about mentors.
"""
import io
import csv
from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel

from pytz import timezone as pytz_timezone

from .stats_collect import anonymize_id
from .stats_reader import (
    ChatForStat,
    average_chat_message_count,
    average_response_time,
    calc_chat_stats,
    new_chat_count,
    active_chat_count,
    unanswered_count,
)

timezone_helsinki = pytz_timezone("Europe/Helsinki")


class MentorForStat(BaseModel):
    anon_mentor_id: str
    anon_user_id: str
    display_name: str
    profile_views: int = 0
    last_seen: Optional[int] = None
    deleted: Optional[int] = None
    created: int
    chats: List[ChatForStat] = []

    @property
    def display_name_or_id(self):
        """
        Returns mentor's display_name
        or if account has been deleted and data cleaned
        return random string derived from created so that it doesn't change
        but there is some identifier to make reading stats easier.
        """
        if self.display_name:
            return self.display_name

        return anonymize_id(str(self.created))[0:20]


async def calc_profile_views(database, start, end):
    stats = {}

    query = """
        WITH profile_counter
        AS (
            SELECT props->>'mentor_id' AS mentor_id
            FROM stat_events
            WHERE
             name = :name
            AND
             created BETWEEN :start AND :end
        )
        SELECT mentor_id, COUNT(mentor_id) AS times_referenced
        FROM profile_counter
        GROUP BY mentor_id
        ORDER BY
         times_referenced DESC,
         mentor_id ASC
        LIMIT :limit;
        """
    values = {
        "name": "open_mentor_profile",
        "start": start,
        "end": end,
    }
    counts = await database.fetch_all(query=query, values=values)

    for c in counts:
        stats[anonymize_id(c["mentor_id"])] = c["times_referenced"]

    return stats


async def fetch_mentors(database):
    mentors = {}

    query = """
        SELECT id, user_id, display_name, last_seen, deleted, created
        FROM stat_mentors
        """
    res = await database.fetch_all(query=query)

    for r in res:
        mentors[r["id"]] = MentorForStat(
            anon_mentor_id=r["id"],
            anon_user_id=r["user_id"],
            display_name=r["display_name"],
            last_seen=r["last_seen"],
            deleted=r["deleted"],
            created=r["created"],
        )

    return mentors


class StatsReaderMentors:
    def csv_header(self):
        return [
            "Mentor",
            "Created UTC",
            "Deleted UTC",
            "Last seen UTC",
            "Profile views",
            "New chats",
            "Active chats",
            "Cumulative chats",
            "Avg msg count / chat / period",
            "Avg msg count / chat / total",
            "Avg first response time / period",
            "Avg first response time / total",
            "Unanswered / period",
            "Unanswered / total",
        ]

    async def period_csv(self, store, period_start_date, period_end_date):

        output = io.StringIO()
        writer = csv.writer(
            output, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        writer.writerow(self.csv_header())

        period_start_timestamp = round(
            timezone_helsinki.localize(
                datetime(
                    period_start_date.year,
                    period_start_date.month,
                    period_start_date.day,
                    hour=0,
                    minute=0,
                    second=0,
                )
            ).timestamp()
        )

        period_end_timestamp = round(
            timezone_helsinki.localize(
                datetime(
                    period_end_date.year,
                    period_end_date.month,
                    period_end_date.day,
                    hour=23,
                    minute=59,
                    second=59,
                )
            ).timestamp()
        )

        mentors = await fetch_mentors(
            store.database,
        )

        profile_views = await calc_profile_views(
            store.database, period_start_timestamp, period_end_timestamp
        )
        for v in profile_views.keys():
            # Show profile views for mentors that we can
            # link to mentor resource.
            if v in mentors:
                mentors[v].profile_views = profile_views[v]

        chats = await calc_chat_stats(store.database)

        for m in mentors:
            anon_user_id = mentors[m].anon_user_id
            c = list(
                filter(
                    lambda x, anon_user_id=anon_user_id: x.anon_mentor_user_id
                    == anon_user_id,
                    chats.values(),
                )
            )
            mentors[m] = mentors[m].copy(update={"chats": c})

        active_mentors = sorted(
            list(filter(lambda m: m.deleted is None, mentors.values())),
            key=lambda x: x.display_name,
        )
        deleted_mentors = sorted(
            list(filter(lambda m: m.deleted is not None, mentors.values())),
            key=lambda x: x.deleted,
        )

        # list active first and deleted last
        self.write(
            writer,
            period_start_timestamp,
            period_end_timestamp,
            active_mentors,
        )
        self.write(
            writer,
            period_start_timestamp,
            period_end_timestamp,
            deleted_mentors,
        )

        return output.getvalue()

    def write(self, writer, period_start_timestamp, period_end_timestamp, mentors):
        for mentor in mentors:
            writer.writerow(
                [
                    mentor.display_name_or_id,
                    datetime.utcfromtimestamp(mentor.created) if mentor.created else "",
                    datetime.utcfromtimestamp(mentor.deleted) if mentor.deleted else "",
                    datetime.utcfromtimestamp(mentor.last_seen)
                    if mentor.last_seen
                    else "",
                    mentor.profile_views,
                    new_chat_count(
                        chats=mentor.chats,
                        start=period_start_timestamp,
                        end=period_end_timestamp,
                    ),
                    active_chat_count(
                        chats=mentor.chats,
                        start=period_start_timestamp,
                        end=period_end_timestamp,
                    ),
                    new_chat_count(
                        chats=mentor.chats, start=0, end=period_end_timestamp
                    ),
                    average_chat_message_count(
                        chats=mentor.chats,
                        start=period_start_timestamp,
                        end=period_end_timestamp,
                    ),
                    average_chat_message_count(
                        chats=mentor.chats, start=0, end=period_end_timestamp
                    ),
                    average_response_time(
                        chats=mentor.chats,
                        start=period_start_timestamp,
                        end=period_end_timestamp,
                    ),
                    average_response_time(
                        chats=mentor.chats, start=0, end=period_end_timestamp
                    ),
                    unanswered_count(
                        chats=mentor.chats,
                        start=period_start_timestamp,
                        end=period_end_timestamp,
                    ),
                    unanswered_count(
                        chats=mentor.chats, start=0, end=period_end_timestamp
                    ),
                ]
            )
