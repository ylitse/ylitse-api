"""Web application module.

Provides methods for creating and configuring aiohhtp web application.
"""

import json

# from multiprocessing import Process
import time

import urllib.request
import urllib.parse

import aiohttp

from databases import Database

from .config import config
from .routes import setup_routes
from .middleware import setup_middleware
from .jwt_functions import JwtFunctions
from .postgres_store import PostgresStore


def create_admin_account(secret):
    """Create an admin account."""
    time.sleep(1)

    login_name = "admin"

    jwt = JwtFunctions(secret)
    access_token = jwt.access_token(
        {
            "role": "admin",
            "scopes": "Does not matter for this one.",
            "login_name": login_name,
        },
        {
            "id": "Is not checked",
        },
    )

    headers = {"Authorization": f"Bearer {access_token}"}

    payload = json.dumps(
        {
            "account": {
                "login_name": login_name,
                "role": "admin",
            },
            "password": "secret",
        }
    ).encode("utf-8")

    req = urllib.request.Request(
        "http://localhost:8080/accounts", data=payload, method="POST", headers=headers
    )
    try:
        urllib.request.urlopen(req)  # pylint: disable=consider-using-with
    except Exception:  # noqa, pylint: disable=broad-except
        pass


def create_app():
    """Create a web app to be passed to a server."""
    app = aiohttp.web.Application()

    database = Database(config["postgres_url"])
    store = PostgresStore(database)
    app.on_startup.append(store.on_startup)
    app.on_shutdown.append(store.on_shutdown)

    secret = config["jwt_secret"]

    setup_routes(app=app, store=store, secret=secret)
    setup_middleware(app)

    # I wish I could have an eventloop in my eventloop.
    # p = Process(target=create_admin_account, args=(secret,))
    # p.start()

    return app
