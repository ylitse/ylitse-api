"""Contacts module.

Contains `/user/{user_id}/contacts` endpoint implementation.
"""

import jsonschema
from aiohttp import web

from ylitse.data_definitions import data_definitions
from ylitse.json_validation import raise_if_extra_json_properties
from ylitse.store_abstraction import StoreView
from ylitse.crud import crud_contact


class ContactsView:
    def __init__(self, store):
        self.store = store
        self.messages = StoreView(store, "MESSAGE")
        self.users = StoreView(store, "USER")

    async def get(self, request):
        user_id = request.match_info["user_id"]

        contacts = await crud_contact.get_mine(
            self.store,
            user_id=user_id,
            ignore_deleted=True,
        )

        return web.json_response({"resources": contacts})

    async def put(self, request):
        user_id = request.match_info["user_id"]
        contact_user_id = request.match_info["contact_user_id"]

        schema = data_definitions["CONTACT"]["schema"]

        try:
            json = await request.json()
            jsonschema.validate(json, schema)
        except Exception:
            raise web.HTTPBadRequest

        raise_if_extra_json_properties(data_type="CONTACT", schema=schema, json=json)

        await crud_contact.update_status(
            self.store,
            user_id=user_id,
            contact_user_id=contact_user_id,
            status=json["status"],
        )
        contact = await crud_contact.get_by_contact_user_id(
            self.store, user_id=user_id, contact_user_id=contact_user_id
        )

        return web.json_response(
            contact,
            status=200,
        )

    async def patch(self, request):
        user_id = request.match_info["user_id"]

        try:
            json = await request.json()

            if len(json) == 0:
                raise web.HTTPBadRequest

            for contact in json:
                if (
                    "id" not in contact
                    or len(contact["id"]) == 0
                    or not crud_contact.is_allowed_status(contact["status"])
                ):
                    raise web.HTTPBadRequest

            patched_contacts = []
            for contact in json:
                await crud_contact.update_status(
                    self.store,
                    user_id=user_id,
                    contact_user_id=contact["id"],
                    status=contact["status"],
                )
                cs = await crud_contact.get_by_contact_user_id(
                    self.store, user_id=user_id, contact_user_id=contact["id"]
                )
                patched_contacts.append(cs)

            return web.json_response({"resources": patched_contacts})

        except Exception:
            raise web.HTTPBadRequest
