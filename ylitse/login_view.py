"""Login module.

Contains `/login`, `/refresh` and `/logout` endpoint implementation.
"""

from aiohttp import web
from multidict import MultiDict

from ylitse.json_validation import get_json


class LoginView:
    """Endpoint methods."""

    def __init__(self, session_manager, jwt_functions):
        self.session_manager = session_manager
        self.jwt = jwt_functions

    async def login(self, request):
        """Process login request."""
        json = await get_json(request, data_type="LOGIN")
        tokens_and_scopes = await self.session_manager.login(
            login_name=json["login_name"],
            password=json["password"],
            mfa_token=json.get("mfa_token", None),
        )
        return web.json_response({"token_type": "bearer", **tokens_and_scopes})

    async def refresh(self, request):
        """Process logout request."""
        refresh_token = (await get_json(request, data_type="REFRESH_TOKEN"))[
            "refresh_token"
        ]
        access_token = await self.session_manager.refresh(
            self.jwt.decode(refresh_token)
        )
        return web.json_response({"token_type": "bearer", "access_token": access_token})

    async def logout(self, request):
        """Invalidate session"""
        session_id = request["token"]["session_id"]
        await self.session_manager.forget(session_id)
        return web.json_response(
            {"message": "Related refresh token invalidated."},
            headers=MultiDict(
                [
                    (
                        "Set-Cookie",
                        'YLITSE_ACCESS="expired"; '
                        "HttpOnly; "
                        "Path=/; "
                        "Secure; "
                        "Max-Age=0; "
                        "SameSite=Strict",
                    ),
                    (
                        "Set-Cookie",
                        'YLITSE_REFRESH="expired"; '
                        "HttpOnly; "
                        "Path=/; "
                        "Secure; "
                        "Max-Age=0; "
                        "SameSite=Strict",
                    ),
                ]
            ),
        )
