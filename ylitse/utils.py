"""helper functions module.

Contains general and common functions that Python is lacking.
"""
from datetime import datetime


def fst(some_container):
    """Return first thing or None."""
    return next(iter(some_container), None)


def is_now_over_date_fraction(compare_date_string, days_float, over):
    now_seconds = round(datetime.utcnow().timestamp())
    added_time = days_float * 3600 * 24
    compare_seconds = (
        round(datetime.fromisoformat(compare_date_string).timestamp()) + added_time
    )
    if over:
        return now_seconds >= compare_seconds
    return now_seconds <= compare_seconds


def get_or(maybe_value, default_value):
    """Return default_value if value is None"""
    return maybe_value or default_value


def parse_date(value, date_format):
    """Parse date from string, returns tuple (is_date, value)"""
    try:
        return (True, datetime.strptime(value, date_format).date())
    except ValueError as error:
        return (False, str(error))


def get_date(maybe_value, default_value, date_format):
    """Return maybe date"""
    date_string = get_or(maybe_value, default_value)
    return parse_date(date_string, date_format)
