"""myuser module."""

from aiohttp import web

from ylitse.store_abstraction import StoreView
from ylitse.utils import fst


class MyUserView:
    def __init__(self, store):
        self.accounts = StoreView(store, "ACCOUNT")
        self.users = StoreView(store, "USER")
        self.mentors = StoreView(store, "MENTOR")

    async def get(self, request):
        scopes = request["token"]["scopes"]

        account = {"account": fst(await self.accounts.read(id=scopes["account_id"]))}

        user = {"user": fst(await self.users.read(id=scopes["user_id"]))}

        if "mentor_id" not in scopes:
            return web.json_response({**account, **user})

        mentor = {"mentor": fst(await self.mentors.read(id=scopes["mentor_id"]))}

        return web.json_response({**account, **user, **mentor})
