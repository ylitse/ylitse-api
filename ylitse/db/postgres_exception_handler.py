from functools import update_wrapper, partial
import sys
import traceback

import asyncpg
from aiohttp import web


class HandlePostgresExceptions:
    """Exception handling decorator for methods
    that do postgres queries"""

    def __init__(self, func):
        update_wrapper(self, func)
        self.func = func

    def __get__(self, obj, objtype):
        """Support instance methods."""
        return partial(self.__call__, obj)

    async def __call__(self, obj, *args, **kwargs):
        """Rescue from connection related exceptions
        and turn them into web.* exceptions"""

        try:
            return await self.func(obj, *args, **kwargs)
        except (
            # Check Violations
            asyncpg.exceptions.CheckViolationError,
            # eg: null characters
            asyncpg.exceptions.CharacterNotInRepertoireError,
        ):
            traceback.print_exc(file=sys.stderr)
            raise web.HTTPUnprocessableEntity
        except (
            # If postgres is not running
            # https://github.com/python/cpython/blob/3.6/Lib/asyncio/selector_events.py#L469
            OSError,
            # Pool related exceptions
            asyncpg.exceptions.InterfaceError,
            asyncpg.exceptions.InternalClientError,
        ):
            traceback.print_exc(file=sys.stderr)
            # raise so that we dont get empty response
            raise web.HTTPBadGateway()
