from datetime import datetime, date, timezone


def time_objects_to_strings(a_dict):
    """Transforms all dates and datetimes to strings in the given dict.
    Notice! Modifies the given dict."""

    # This is very performance critical function.
    # It would be safer to just call deepcopy(a_dict) and
    # do the operations on the new dict but it is super slow.

    for k, v in a_dict.items():
        if isinstance(v, (datetime, date)):
            a_dict[k] = v.isoformat()

    return a_dict


def records_to_dict(rows):
    """Convert db records to dicts to match local_store"""
    rows_as_dict = []

    for row in rows:
        d = dict(row)

        # Postgres will return null values for missing data
        # Flat files dont have the key if data is missing
        # Get rid of all the keys that are nulls in postgres
        new_row = {k: v for k, v in d.items() if v is not None}

        # Datetimes in postgres are returned as python DateTime objects
        # Datetimes in flat files are isoformatted strings
        new_row = time_objects_to_strings(new_row)

        rows_as_dict.append(new_row)

    return rows_as_dict


def now_utc():
    """Current time in UTC"""
    return datetime.utcnow()


def generate_utc_now_timestamp():
    dt = datetime.now(timezone.utc)
    return round(dt.timestamp())
