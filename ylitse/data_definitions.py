"""Define data."""

import datetime

schema_account = {
    "type": "object",
    "properties": {
        "login_name": {"type": "string", "minLength": 2, "maxLength": 100},
        "role": {"enum": ["mentee", "mentor", "admin"]},
        "email": {"type": "string", "maxLength": 200},
        "phone": {
            "type": "string",
            "maxLength": 20,
            "items": {
                "type": "string",
            },
        },
    },
    "required": [
        "login_name",
        "role",
    ],
}
ro_account = ["role", "login_name"]
uniques_account = ["login_name"]

schema_user = {
    "type": "object",
    "properties": {
        "account_id": {"type": "string", "minLength": 2, "maxLength": 200},
        "display_name": {"type": "string", "minLength": 2, "maxLength": 30},
        "role": {"enum": ["mentee", "mentor", "admin"]},
    },
    "required": ["account_id", "display_name", "role"],
}
ro_user = ["role", "account_id"]
uniques_user = ["account_id"]


max_birth_year = datetime.datetime.now().year - 17
schema_mentor = {
    "type": "object",
    "properties": {
        "account_id": {"type": "string", "maxLength": 200},
        "user_id": {"type": "string", "maxLength": 200},
        "languages": {
            "type": "array",
            # 'uniqueItems': True,
            # 'items': {
            #     'type': 'string',
            #     'enum': ['sv', 'ru', 'fi', 'en']
            # },
        },
        "birth_year": {
            "type": "integer",
            "minimum": 1900,
            "maximum": max_birth_year,
        },
        "display_name": {"type": "string", "minLength": 2, "maxLength": 30},
        "gender": {"enum": ["male", "female", "other"]},
        "region": {
            "type": "string",
            "maxLength": 100,
        },
        "story": {
            "type": "string",
            "maxLength": 2000,
        },
        "skills": {
            "type": "array",
            # Gonna trust admin with this one. #Good practice
            "uniqueItems": True,
            "items": {"type": "string"},
        },
        "communication_channels": {
            "type": "array",
            "uniqueItems": True,
            "items": {
                "type": "string",
                "enum": ["face_to_face", "phone", "chat", "email"],
            },
        },
        "is_vacationing": {
            "type": "boolean",
        },
        "status_message": {
            "type": "string",
            "maxLength": 2000,
        },
    },
    "required": [
        "account_id",
        "user_id",
        "birth_year",
        "communication_channels",
        "display_name",
        "gender",
        "languages",
        "region",
        "skills",
        "story",
    ],
}
ro_mentor = ["account_id", "user_id"]
uniques_mentor = ["account_id", "user_id"]
ignores_mentor = ["last_seen"]

schema_patch_mentor = {
    "type": "object",
    "properties": {
        "is_vacationing": {
            "type": "boolean",
        },
        "status_message": {
            "type": "string",
            "maxLength": 2000,
        },
    },
    "required": [
        "is_vacationing",
        "status_message",
    ],
}

schema_skill = {
    "type": "object",
    "properties": {
        "name": {"type": "string", "minLength": 2, "maxLength": 40},
    },
    "required": ["name"],
}
ro_skill = ["name"]
uniques_skill = ["name"]

schema_contact = {
    "type": "object",
    "properties": {
        "status": {"enum": ["banned", "ok", "deleted", "archived"]},
    },
    "required": ["status"],
}
ro_contact = []
uniques_contact = []

schema_message = {
    "type": "object",
    "properties": {
        "content": {"type": "string", "minLength": 1, "maxLength": 5000},
        "recipient_id": {"type": "string", "minLength": 10, "maxLength": 200},
        "sender_id": {"type": "string", "minLength": 10, "maxLength": 200},
        "opened": {"enum": [False]},
    },
    "required": ["content", "recipient_id", "sender_id", "opened"],
}
ro_message = ["content", "recipient_id", "sender_id"]
uniques_message = []

schema_private_data = {
    "type": "object",
    "properties": {
        "starred_users": {"type": "array", "minLength": 0, "maxLength": 1000},
    },
    "required": ["starred_users"],
}
ro_private_data = []
uniques_private_data = []


schema_password = {"type": "string", "minLength": 2, "maxLength": 100}

schema_change_password = {
    "type": "object",
    "properties": {
        "current_password": schema_password,
        "new_password": schema_password,
    },
    "required": ["current_password", "new_password"],
}

schema_set_password = {
    "type": "object",
    "properties": {
        "login_name": schema_password,
        "password": schema_password,
    },
    "required": ["login_name", "password"],
}


schema_login = {
    "type": "object",
    "properties": {
        "login_name": {"type": "string", "minLength": 2, "maxLength": 100},
        "password": {"type": "string", "minLength": 2, "maxLength": 100},
        "mfa_token": {"type": "string", "minLength": 6, "maxLength": 6},
    },
    "required": ["login_name", "password"],
}

schema_refresh_token = {
    "type": "object",
    "properties": {
        "refresh_token": {
            "type": "string",
            "minLength": 10,
            "maxLength": 1000,
        },
    },
    "required": ["refresh_token"],
}

schema_device = {
    "type": "object",
    "properties": {
        "platform": {"enum": ["android", "ios"]},
        "token": {"type": "string", "minLength": 10, "maxLength": 300},
    },
}

schema_report = {
    "type": "object",
    "properties": {
        "reported_user_id": {"type": "string", "minLength": 10, "maxLength": 200},
        "reporter_user_id": {"type": "string", "minLength": 10, "maxLength": 200},
        "report_reason": {"type": "string", "minLength": 1, "maxLength": 2000},
        "contact_field": {"type": "string", "minLength": 0, "maxLength": 200},
        "comment": {"type": "string", "minLength": 0, "maxLength": 2048},
        "status": {"enum": ["received", "handled"]},
    },
    "required": ["reported_user_id", "reporter_user_id"],
}

schema_chat_review_log = {
    "type": "object",
    "properties": {
        "chat_user_id_1": {"type": "string", "minLength": 10, "maxLength": 200},
        "chat_user_id_2": {"type": "string", "minLength": 10, "maxLength": 200},
        "reviewer_user_id": {"type": "string", "minLength": 10, "maxLength": 200},
    },
    "required": ["chat_user_id_1", "chat_user_id_2", "reviewer_user_id"],
}

schema_minimum_supported_client_versions = {
    "type": "object",
    "properties": {
        "client": {"type": "string", "minLength": 10, "maxLength": 200},
        "version": {"type": "string", "minLength": 6, "maxLength": 200},
    },
    "required": ["version", "client"],
}

data_definitions = {
    "DEVICE": {
        "schema": schema_device,
        "ro_keys": [],
        "uniques": [],
    },
    "ACCOUNT": {
        "schema": schema_account,
        "ro_keys": ro_account,
        "uniques": uniques_account,
    },
    "CHANGE_PASSWORD": {
        "schema": schema_change_password,
    },
    "SET_PASSWORD": {
        "schema": schema_set_password,
    },
    "PASSWORD": {
        "schema": schema_password,
    },
    "USER": {
        "schema": schema_user,
        "ro_keys": ro_user,
        "uniques": uniques_user,
    },
    "MENTOR": {
        "schema": schema_mentor,
        "ro_keys": ro_mentor,
        "uniques": uniques_mentor,
        "ignores": ignores_mentor,
    },
    "PATCH_MENTOR": {
        "schema": schema_patch_mentor,
    },
    "PRIVATE_DATA": {
        "schema": schema_private_data,
        "ro_keys": ro_private_data,
        "uniques": uniques_private_data,
    },
    "CONTACT": {
        "schema": schema_contact,
        "ro_keys": ro_contact,
        "uniques": uniques_contact,
    },
    "SKILL": {
        "schema": schema_skill,
        "ro_keys": ro_skill,
        "uniques": uniques_skill,
    },
    "MESSAGE": {
        "schema": schema_message,
        "ro_keys": ro_message,
        "uniques": uniques_message,
    },
    "LOGIN": {
        "schema": schema_login,
    },
    "REFRESH_TOKEN": {
        "schema": schema_refresh_token,
    },
    "IDENTITY": {"uniques": ["login_name", "scopes"]},
    "SESSION": {"uniques": []},
    "REPORTED_USERS": {"schema": schema_report},
    "CHAT_REVIEW_LOG": {"schema": schema_chat_review_log},
    "MINIMUM_SUPPORTED_CLIENT_VERSIONS": {
        "schema": schema_minimum_supported_client_versions
    },
}
