"""User creation and destruction module."""

from aiohttp import web

from ylitse.stats_collect import delete_account, insert_account
from ylitse.store_abstraction import StoreView
from ylitse.utils import fst
from .db import HandlePostgresExceptions
from .crud import crud_stat_mentor

# pylint: disable=missing-docstring


class UserAccountManager:
    def __init__(self, store, identity_manager):
        self.store = store
        self.accounts = StoreView(store, "ACCOUNT")
        self.users = StoreView(store, "USER")
        self.mentors = StoreView(store, "MENTOR")
        self.messages = StoreView(store, "MESSAGE")
        self.contact_statuses = StoreView(store, "CONTACT")
        self.sessions = StoreView(store, "SESSION")

        self.identity_manager = identity_manager

    @HandlePostgresExceptions
    async def create_user_account(self, json):
        # Unique checks raise web.HTTPConflict from `create`.
        # Wrap these in transactions so that we dont need extra
        # cleanup code.
        async with self.store.database.connection() as connection:
            async with connection.transaction():
                account = await self.accounts.create(json["account"])
                user = await self.users.create(
                    {
                        **json["user"],
                        "account_id": account["id"],
                    }
                )

                await insert_account(self.store.database, account=account)

                return {"account": account, "user": user}

    async def create_non_mentor(self, json):
        """Create admin or mentee."""
        created = await self.create_user_account(json)
        try:
            await self.identity_manager.new(
                login_name=created["account"]["login_name"],
                password=json["password"],
                mfa_secret=json.get("mfa_secret", None),
                role=created["user"]["role"],
                scopes={
                    "account_id": created["account"]["id"],
                    "user_id": created["user"]["id"],
                },
            )
        except web.HTTPConflict:
            await self.accounts.delete(id=created["account"]["id"])
            await self.users.delete(id=created["user"]["id"])
            raise web.HTTPConflict

        return created

    async def create_mentor(self, json):
        created = await self.create_user_account(json)
        try:
            mentor = await self.mentors.create(
                {
                    **json["mentor"],
                    "account_id": created["account"]["id"],
                    "user_id": created["user"]["id"],
                }
            )
            await crud_stat_mentor.refresh(self.store, mentor_id=mentor["id"])
        except web.HTTPConflict:
            await self.accounts.delete(id=created["account"]["id"])
            await self.users.delete(id=created["user"]["id"])
            raise web.HTTPConflict
        try:
            await self.identity_manager.new(
                login_name=created["account"]["login_name"],
                password=json["password"],
                role=created["user"]["role"],
                scopes={
                    "account_id": created["account"]["id"],
                    "user_id": created["user"]["id"],
                    "mentor_id": mentor["id"],
                },
            )
        except web.HTTPConflict:
            await self.accounts.delete(id=created["account"]["id"])
            await self.users.delete(id=created["user"]["id"])
            await self.mentors.delete(id=mentor["id"])
            raise web.HTTPBadRequest

        return {**created, "mentor": mentor}

    @HandlePostgresExceptions
    async def delete_user_account(self, account_id):
        account = fst(await self.accounts.read(id=account_id))
        if account is None:
            raise web.HTTPUnprocessableEntity

        related_user = fst(await self.users.read(account_id=account_id))
        if related_user is None:
            raise web.HTTPUnprocessableEntity
        user_id = related_user["id"]

        async with self.store.database.connection() as connection:
            async with connection.transaction():
                await self.identity_manager.delete(login_name=account["login_name"])
                await self.accounts.delete(id=account_id)
                await self.users.delete(id=user_id)
                await self.messages.delete(recipient_id=user_id)
                await self.messages.delete(sender_id=user_id)
                await self.contact_statuses.delete(user_id=user_id)
                await self.contact_statuses.delete(contact_user_id=user_id)
                await self.sessions.delete(account_id=account_id)

                # delete should not raise anythin bad if
                # there is nothing to delete.
                # reason: wanted state already achieved.
                await self.mentors.delete(account_id=account_id)

                await delete_account(
                    self.store.database,
                    account=account,
                    user_id=user_id,
                )
