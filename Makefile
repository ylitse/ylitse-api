YLITSE_CONF?=./ylitse_conf/ylitse.conf
# Use simple expansion with shell, otherwise slow:
# https://savannah.gnu.org/bugs/?65533
YLITSE_CONF_DIR:=${shell dirname ${YLITSE_CONF}}
YLITSE_CONF_BASENAME:=${shell basename ${YLITSE_CONF} .conf}
YLITSE_POSTGRES_DB:=${or ${YLITSE_POSTGRES_DB},${shell grep -s postgres_url ${YLITSE_CONF} | sed -nz 's/.*\/\(.*\)/\1/p'}}
YLITSE_POSTGRES_USER:=${or ${YLITSE_POSTGRES_USER},${shell grep -s postgres_url ${YLITSE_CONF} | sed -nz 's/.*\/\(.*\):.*@.*/\1/p'}}
YLITSE_POSTGRES_PASS:=${or ${YLITSE_POSTGRES_PASS},${shell grep -s postgres_url ${YLITSE_CONF} | sed -nz 's/.*:\(.*\)@.*/\1/p'}}
YLITSE_POSTGRES_HOST:=${or ${YLITSE_POSTGRES_HOST},${shell grep -s postgres_url ${YLITSE_CONF} | sed -n 's/.*:\/\/\([^:]*\)\/.*/\1/p'}}
YLITSE_POSTGRES_PORT:=${or ${YLITSE_POSTGRES_PORT},${shell grep -s postgres_url ${YLITSE_CONF} | sed -nz 's/.*:.*:.*@.*:\(.*\)\/.*/\1/p'}}
YLITSE_POSTGRES_DATA?=${YLITSE_CONF_DIR}/data
YLITSE_CONTAINER_STACK?=ylitse-local

env:
	python3.10 -m venv env
	. env/bin/activate && \
	python -m pip install pip==22.2.2 setuptools==68.2.2 && \
	pip install -r requirements.txt && \
	pip install -e .

upgrade:
	. env/bin/activate && \
	pip install -Ur requirements-base.txt && \
	pip freeze > requirements.txt

generate-key:
	@python -c 'from cryptography.fernet import Fernet; print(Fernet.generate_key().decode("ascii"))'

conf: ADMIN_USER?=admin
conf: ENCRYPTION_KEY_FILE?=${YLITSE_CONF_DIR}/${YLITSE_CONF_BASENAME}.key
conf: YLITSE_POSTGRES_URL?=postgresql://${YLITSE_POSTGRES_USER}:${YLITSE_POSTGRES_PASS}@${YLITSE_POSTGRES_HOST}/${YLITSE_POSTGRES_DB}
conf:
	@echo Creating ${YLITSE_CONF}...
	@mkdir -p ${YLITSE_CONF_DIR}
	@echo "store_path: /tmp/ylitse-test-data" > ${YLITSE_CONF}
	@echo "jwt_secret: ${shell python -c 'import secrets; print(secrets.token_hex(24))'}" >> ${YLITSE_CONF}
	@echo "admin_username: ${ADMIN_USER}" >> ${YLITSE_CONF}
	@echo "admin_password: ${shell python -c 'import secrets; print(secrets.token_hex(24))'}" >> ${YLITSE_CONF}
	@echo "admin_2fa: ${shell python -c 'import pyotp; print(pyotp.random_base32())'}" >> ${YLITSE_CONF}
	@echo "postgres_url: ${YLITSE_POSTGRES_URL}" >> ${YLITSE_CONF}
	@echo "encryption_key_file: ${ENCRYPTION_KEY_FILE}" >> ${YLITSE_CONF}
	@echo Creating ${ENCRYPTION_KEY_FILE} if missing...
	@test -f ${ENCRYPTION_KEY_FILE} || ${MAKE} --no-print-directory generate-key > ${ENCRYPTION_KEY_FILE}

build-image:
	docker build -f ./docker/Dockerfile . -t ylitse

image-conf: NEW_PG_USER?=postgres
image-conf: NEW_PG_PASS?=secret
image-conf: NEW_PG_HOST?=database
image-conf: NEW_PG_DB?=ylitse
image-conf:
	docker run --rm \
		-v $$(pwd)/ylitse_conf:/app/ylitse_conf \
		-e YLITSE_POSTGRES_URL="postgresql://${NEW_PG_USER}:${NEW_PG_PASS}@${NEW_PG_HOST}/${NEW_PG_DB}" \
		ylitse bash -c "make conf"

start:
	docker compose --project-name ${YLITSE_CONTAINER_STACK} up

stop:
	docker compose --project-name ${YLITSE_CONTAINER_STACK} stop

restart: stop start

remove:
	docker compose --project-name ${YLITSE_CONTAINER_STACK} down -v

db-upgrade-head:
	alembic upgrade head

db-downgrade-base:
	alembic downgrade base

db-delete-all-tables:
	python scripts/delete_all_tables.py

db-insert-admin:
	python scripts/insert_admin.py

db-reset: db-delete-all-tables db-upgrade-head db-insert-admin

format:
	black ylitse tests scripts

lint:
	pycodestyle --config=.pycodestyle alembic ylitse tests/**/*.py scripts/*.py *.py
	pylint -j0 alembic ylitse tests/**/*.py scripts/*.py *.py
	yamllint -c .yamllintrc tests/api
	black --check ylitse tests scripts

unittest:
	coverage run --branch --source=ylitse \
		-m unittest discover tests "*_tests.py" --failfast
	coverage report -m

apitest:
	pytest

run-standalone:
	PYTHONPATH=. python ylitse/main.py --host 0.0.0.0 --port 8080;

run-gunicorn:
	gunicorn -c gunicorn.conf.py --check-config ylitse.main:app
	gunicorn -c gunicorn.conf.py --access-logformat '%a %t "%r" %s %b "%{Referer}i" "%{User-Agent}i" %Tf' --reload ylitse.main:app;

clean:
	find . -type f -name '*~' -exec rm -f {} \;
	find . -type d -name '__pycache__' -prune -exec rm -rf {} \;
	coverage erase

.EXPORT_ALL_VARIABLES:

.PHONY: env upgrade generate-key conf start stop \
	restart db-upgrade-head db-downgrade-base \
	db-delete-all-tables db-insert-admin \
	db-reset lint unittest apitest build-image \
	run-standalone run-gunicorn clean image-conf
