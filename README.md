# Ylitse API README

[Ylitse Project][], started by SOS Children's Villages Finland, aims at
reducing the need for intergenerational child protecting services by
supporting young parents with a foster care background.

Ylitse API is a secure RESTful HTTP API is an interface to and implementation
of a digital peer mentoring service, which is at the core of Ylitse Project.

[Ylitse Project]: https://www.ylitse.fi/

## Prerequisites

It is recommended to have [make][] and GNU version of sed installed.

For MacOS users using HomeBrew, add following line to your `~/.zshrc` to use GNU sed instead of BSD sed:

    export PATH="/opt/homebrew/opt/gnu-sed/libexec/gnubin:$PATH"

> Make sure you have Docker service enabled and running.

## Docker development setup

### Build-image

Build image with

```
make build-image
```

### Create conf

Create conf-file with
```
make image-conf
```

This will result in a `ylitse_conf` directory in the project.
Directory will contain configuration- and encryption_key-files.
This directory is mounted to containers, to share the same configuration.

### Run the stack

```
make start
```
This will boot database-, api- and migration- containers

Stop containers
```
make stop
```

If you want to remove the container-stack, use
```
make remove
```

### Run the tests and quality 

To run make commands in containers, use `run-in-container`-script.
Parameter will be the command to run in the container.
If you need a db or api, just run `make start` first

Run tests
```
./docker/run-in-container.sh make unittest && make db-reset && make apitest
```

Run quality
```
./docker/run-in-container.sh make lint && make format
```

### Run with different DB

- To change DB we use docker-compose `--project-name` flag. By using different project-name, we can configure a separate stack. This is useful if we want to save some data but run api-tests which will flush the database.
- Use `YLITSE_CONTAINER_STACK` environment variable for setting a different stack!

For example 
```
YLITSE_CONTAINER_STACK=e2e make start
```

Will start docker container stack named `e2e`

## Local development

### Dependencies

Ylitse API is written in Python 3.10, but it also depends on several tools and
libraries:

* [aiohttp][]
* [docker][]
* [docker-compose][]
* [Gunicorn][]
* [pycodestyle][]
* [pydocstyle][]
* [Pylint][]
* [python-future][]
* [yamllint][]

[aiohttp]: http://aiohttp.readthedocs.io/
[docker]: https://docs.docker.com/
[docker-compose]: https://docs.docker.com/compose/
[Gunicorn]: http://gunicorn.org/
[make]: https://www.gnu.org/software/make/
[pycodestyle]: http://pycodestyle.pycqa.org/
[pydocstyle]: http://www.pydocstyle.org/
[Pylint]: https://www.pylint.org/
[python-future]: http://python-future.org/
[yamllint]: https://yamllint.readthedocs.io/

### Usage

To install all dependencies in a virtual environment run:

    make env

Activate the virtual environment:

    source env/bin/activate

or if using fish shell:

    source env/bin/activate.fish

Create configuration file. By default it is created to `~/.config/ylitse/ylitse.conf`:

    YLITSE_POSTGRES_URL=postgresql://YOUR_USER:YOUR_PASS@localhost/YOUR_DB make conf
    
> Make sure to replace `YOUR_*` with reasonable values.

Run Ylitse API with a single-threaded standalone `aiohttp` server:

    make run-standalone

Or start a `gunicorn` server with multiple workers:

    make run-gunicorn
    
If you don't want to persist data (stored to `~/.config/ylitse/data/` by deafult), you
can start the API like this:

    YLITSE_POSTGRES_DATA= make run-gunicorn

By default, the API will be available at localhost on port 8080. You can now
do a simple test:

    curl -w '\n' 127.0.0.1:8080/version

There are more targets in `Makefile` you might want to check out.

### Push notifications

To enable push notifications with Firebase, a separate config file is needed
for a service account. This can be downloaded from the Firebase console.
To use, start server with:

    FIREBASE_CONF=~/.config/ylitse/firebase.json make run-standalone

### Safety feature

To allow safety feature, add an env variable SAFETY_FEATURE=true, for example
		SAFETY_FEATURE=true make test
For user chats review quota, add an env variable REVIEW_QUOTA with the number of allowed
views per 24h period. On default 10 views / day allowed

### Database

By default connection pool size is 10. Configuration can be done in YLITSE_CONF:

    postgresql://USER:PASS@localhost/DATABASE?min_size=20&max_size=20

Start the database:

    make db-start

If needed, you can check database logs by running:

    make db-logs

Warning! This deletes all data. Run DB migrations and insert admin user:

    make db-reset

Or run any of the commands separately.

Delete all tables:

    make db-delete-all-tables

Or downgrade migrations all the way to beginning:

    make db-downgrade-base

Run all the migrations:

    make db-upgrade-head

Insert admin. This reads admin username and password from config file:

    make db-insert-admin

### Quality and tests

Contributed code should comply with [PEP 8] Style Guide and [PEP 257] Docstring
Conventions. You can check your code by running (this will also run Pylint code
analyzer):

    make lint

Unit tests are written using standard `unittest` module and integration tests
using `pytest`.

To run all tests and measure coverage, create separate test config,
run migrations and then run tests.

```console
YLITSE_CONF=~/.config/ylitse/ylitse_test.conf \
YLITSE_POSTGRES_URL=postgresql://YOUR_TEST_USER:YOUR_TEST_PASS@localhost/YOUR_TEST_DB \
ADMIN_USER=test-admin \
make conf
cat ~/.config/ylitse/ylitse_test.conf
YLITSE_CONF=~/.config/ylitse/ylitse_test.conf make test
```

[PEP 8]: https://www.python.org/dev/peps/pep-0008/
[PEP 257]: https://www.python.org/dev/peps/pep-0257/

## Releasing

To bump a version, edit `ylitse/version.py` and NEWS, where [ISO 8601][]
formatted dates should be used:

    0.1.1 (2017-12-10)

After a release bump the version again with `+git` suffix and start a new NEWS
entry:

    0.1.1+git (unreleased)

Always tag releases:

    git tag v$(python3.10 setup.py --version)
    git push --tags

[ISO 8601]: https://www.iso.org/iso-8601-date-and-time-format.html

## Deployment

TODO

## License

Ylitse API is offered under the MIT license.
