from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web
from databases import Database

from ylitse.app import create_app
from ylitse.config import config
from ylitse.postgres_store import PostgresStore
from ylitse.json_validation import get_json
from ylitse.store_abstraction import StoreView, new_ylitse_id
from test_helpers import random_string, dummy_request, gen_random


class GetJsonTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_get_json_on_POST(self):
        data_types = [
            "ACCOUNT",
            "USER",
            "MENTOR",
            "SKILL",
            "MESSAGE",
            "PRIVATE_DATA",
        ]
        for dt in data_types:
            async with Database(config["postgres_url"]) as database:
                store = PostgresStore(database)
                okay_json = gen_random(data_type=dt)
                req = dummy_request(json=okay_json, method="POST")

                got_json = await get_json(req, **{"data_type": dt, "store": store})
                self.assertEqual(got_json, okay_json)

    @unittest_run_loop
    async def test_get_json_bad_POST(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            bad_json = {"asdfs": random_string(10)}
            req = dummy_request(json=bad_json, method="POST")
            with self.assertRaises(web.HTTPBadRequest):
                await get_json(req, store=store, data_type="SKILL")

    @unittest_run_loop
    async def test_get_json_on_PUT(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            mentor_db = StoreView(store, "MENTOR")

            mentor = gen_random(data_type="MENTOR")
            # mentor['account_id'] = 'ACCOUT_ID'
            created_mentor = await mentor_db.create(mentor)

            client_updated_mentor = {**created_mentor, "story": "Cool story bro"}
            req = dummy_request(
                json=client_updated_mentor,
                match_info={"id": client_updated_mentor["id"]},
                method="PUT",
            )

            got_json = await get_json(
                req, data_type="MENTOR", store=mentor_db.store, match_info="id"
            )
            self.assertEqual(got_json, client_updated_mentor)

    @unittest_run_loop
    async def test_err_raising(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            mentor_db = StoreView(store, "MENTOR")

            class bad_req:
                def __init__(self):
                    pass

                def json(self):
                    raise ZeroDivisionError

            req = bad_req()
            with self.assertRaises(web.HTTPBadRequest):
                await get_json(req, data_type="MENTOR", store=mentor_db.store)

    @unittest_run_loop
    async def test_404_PUT(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            mentor_db = StoreView(store, "MENTOR")
            mentor = gen_random(data_type="MENTOR")
            req = dummy_request(
                json=mentor, match_info={"id": new_ylitse_id()}, method="PUT"
            )
            with self.assertRaises(web.HTTPNotFound):
                await get_json(
                    req,
                    data_type="MENTOR",
                    store=mentor_db.store,
                    match_info="id",
                )
