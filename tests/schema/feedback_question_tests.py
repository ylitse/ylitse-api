"""Tests for schema/feedback.py."""
# pylint: disable=line-too-long

import json

from unittest import TestCase

from ylitse.schema.feedback import FeedbackQuestionCreate

from tests.test_helpers import read_json


class FeedbackQuestionTests(TestCase):
    def test_create_with_predined_repetition(self):
        self.maxDiff = 5000

        q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")

        self.assertEqual(FeedbackQuestionCreate.parse_obj(q).json(), json.dumps(q))

    def test_create_with_repeat_until_repetition(self):
        self.maxDiff = 5000

        q = read_json(
            "tests/fixtures/feedback_question_repeat_until_week_from_registration.json"  # noqa
        )

        self.assertEqual(FeedbackQuestionCreate.parse_obj(q).json(), json.dumps(q))
