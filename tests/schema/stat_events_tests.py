"""Tests for schema/stat_events.py."""
# pylint: disable=line-too-long

import json

from unittest import TestCase

from pydantic import ValidationError

from ylitse.schema.stat_events import StatEventsCreate


class StatEventsTests(TestCase):
    def test_create(self):
        valids = [
            [],
            [
                {
                    "name": "filter_skills",
                    "props": {"skills": ["Eyebrow Dancing", "Limbo Skating"]},
                }
            ],  # noqa
            [{"name": "filter_skills", "props": {"skills": ["Limbo Skating"]}}],  # noqa
            [
                {"name": "filter_skills", "props": {"skills": ["Limbo Skating"] * 100}}
            ],  # noqa
            [
                {
                    "name": "open_mentor_profile",
                    "props": {"mentor_id": "some_mentor_id"},
                }
            ],  # noqa
        ]
        for v in valids:
            self.assertEqual(StatEventsCreate.parse_obj(v).json(), json.dumps(v))

        invalids = [
            [{"name": "non_existing_event"}],
            [{"name": "filter_skills"}],  # needs props
            [{"name": "filter_skills", "props": {}}],  # needs skills
            [{"name": "filter_skills", "props": {"skills": []}}],  # noqa. needs skills
            [
                {"name": "filter_skills", "props": {"skills": ["Limbo Skating"] * 101}}
            ],  # noqa. Too many skills
            {},  # not array
            [
                {
                    "name": "filter_skills",
                    "props": {"skills": ["Too long skill name" * 40]},
                }
            ],  # noqa
            [
                {
                    "name": "filter_skills",
                    "props": {
                        "skills": ["Eyebrow Dancing"],
                        "extra_params": "dont accept extra data",
                    },
                }
            ],  # noqa
        ]
        for v in invalids:
            with self.assertRaises(ValidationError, msg=f"params: {v}"):
                StatEventsCreate.parse_obj(v)
