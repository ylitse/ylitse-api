"""Tests for schema/types.py."""
# pylint: disable=line-too-long
from typing import Optional

import json

from unittest import TestCase

from pydantic import BaseModel, ValidationError
from pydantic import Extra  # pylint: disable=unused-import


from ylitse.schema.types import (
    DisplayName,
    MentorID,
    SkillName,
)
from ylitse.store_abstraction import new_ylitse_id


class TestModel(BaseModel, extra=Extra.forbid):
    DisplayName: Optional[DisplayName]
    SkillName: Optional[SkillName]
    MentorID: Optional[MentorID]


class TypesTests(TestCase):
    def test_types(self):
        valids = [
            {"DisplayName": "Some mentor"},
            {"DisplayName": "A"},
            {"DisplayName": "👀" * 30},
            {"SkillName": "AÖ"},
            {"SkillName": "👀" * 40},
            {"MentorID": new_ylitse_id()},
        ]
        for v in valids:
            self.assertEqual(
                TestModel.parse_obj(v).json(exclude_none=True), json.dumps(v)
            )

        invalids = [
            {"DisplayName": ""},
            {"DisplayName": "👀" * 31},  # too long
            {"SkillName": ""},
            {"SkillName": "A"},
            {"SkillName": "👀" * 41},  # too long
            {"MentorID": ""},
            {"MentorID": new_ylitse_id() * 100},  # too long
        ]
        for v in invalids:
            with self.assertRaises(ValidationError, msg=f"params: {v}"):
                TestModel.parse_obj(v)
