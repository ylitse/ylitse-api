"""Tests for postgres store."""
from copy import deepcopy

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web
from databases import Database

from ylitse.config import config
from ylitse.app import create_app
from ylitse.postgres_tables import data_type_to_postgres_table
from ylitse.postgres_store import PostgresStore
from ylitse.store_abstraction import StoreView
from ylitse.encrypted_store_view import EncryptedStoreView
from ylitse.symmetric_crypto_functions import SymmetricCryptoFunctions

from test_helpers import random_string


class PostgresStoreTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_postgres_store(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            postgres_store = PostgresStore(database)
            await database.execute(query=data_type_to_postgres_table("SKILL").delete())

            db = StoreView(postgres_store, "SKILL")

            random_name = random_string(10)
            skill = {
                "name": random_name,
            }

            dict_from_create = await db.create(skill)
            self.assertEqual("id" in dict_from_create, True)
            self.assertEqual("created" in dict_from_create, True)
            self.assertEqual("updated" in dict_from_create, True)

            self.assertEqual(1, len(await db.read()))

            id_ = dict_from_create["id"]
            dict_from_read = next(iter(await db.read(id=id_)))
            self.assertEqual(dict_from_create, dict_from_read)

            updated_dict = {**dict_from_create, "name": random_string(10)}
            dict_to_compare = deepcopy(updated_dict)
            await db.update(updated_dict)

            updated_dict_from_read = dict(next(iter(await db.read(id=id_))))
            self.assertLess(
                dict_to_compare.pop("updated"), updated_dict_from_read.pop("updated")
            )
            self.assertEqual(dict_to_compare, updated_dict_from_read)

            all_after_update = await db.read()
            self.assertEqual(1, len(all_after_update))

            await db.delete(id=id_)
            all_after_delete = await db.read()
            self.assertEqual(0, len(all_after_delete))

    @unittest_run_loop
    async def test_local_store_exception(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            postgres_store = PostgresStore(database)

            db = StoreView(postgres_store, "SKILL")

            random_name = random_string(10)
            skill = {
                "name": random_name,
            }

            await db.create(skill)

            # Dublicate crashes the thing
            with self.assertRaises(web.HTTPConflict):
                await db.create(skill)

    @unittest_run_loop
    async def test_local_store_wo_uniqs(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            postgres_store = PostgresStore(database)

            db = EncryptedStoreView(
                store=postgres_store,
                crypto=SymmetricCryptoFunctions(config["encryption_key_file"]),
                keys=[
                    {
                        "decrypted": "content",
                        "encrypted": "content_enc",
                    }
                ],
                data_type="MESSAGE",
            )

            query = data_type_to_postgres_table("MESSAGE").delete()
            await database.execute(query=query)

            msg = {
                "content": random_string(10),
                "recipient_id": random_string(10),
                "sender_id": random_string(10),
            }
            await db.create(msg)
            await db.create(msg)
            self.assertEqual(2, len(await db.read()))
            await db.delete(sender_id=msg["sender_id"])
            self.assertEqual(0, len(await db.read()))
