""" ylitse/stats_collect.py tests."""
# pylint: disable=too-many-function-args,line-too-long

from aiounittest import AsyncTestCase

from ylitse.stats_collect import anonymize_id, from_iso_format_to_utc_timestamp


class StatsCollectTest(AsyncTestCase):
    def test_anonymize_id(self):
        # If this test fails dont just update the expected string
        # this is used in data deduplication so that accounts and messages
        # are not counted many times.
        self.assertEqual(
            anonymize_id("WREVCQiq91pafvwmiG1jFdzqI633lHuJegfo0LdRVeg"),
            "0e7ff87017001e74e27e06ba97337ba24fb7d22aabf679211e677bac",
        )

    def test_from_iso_format_to_utc_timestamp(self):
        self.assertEqual(
            from_iso_format_to_utc_timestamp("2020-11-26T14:36:36.894263"), 1606401397
        )
