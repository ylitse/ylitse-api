from datetime import datetime, timedelta, timezone

import time_machine
from databases import Database

from aiounittest import AsyncTestCase
from tests.test_helpers import create_non_mentor, create_mentor, read_json, post_message

from ylitse.schema.feedback import FeedbackQuestionCreate
from ylitse.config import config
from ylitse.postgres_store import PostgresStore
from ylitse.crud import crud_feedback_question, crud_feedback_answer

# from ylitse.managers import FeedbackManager

travel_at = datetime(2021, 1, 2, tzinfo=timezone.utc)


class FeedbackTest(AsyncTestCase):
    def setUp(self):
        self.maxDiff = None
        super().setUp()

    @time_machine.travel(travel_at, tick=False)  # stop the clock
    async def test_question_with_predefined_repetition(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            store = PostgresStore(database)

            question = await crud_feedback_question.create(
                store,
                read_json("tests/fixtures/feedback_question_predefined_repeat.json"),
            )

            mentee = await create_non_mentor(store, role="mentee")
            # mentor = await create_mentor(store, role="mentor")
            # await post_message(store, sender=mentee["user"], recipient=mentor["user"], content="Hi!")

            unanswered = await crud_feedback_question.fetch_all_needing_answers(
                store, user_id=mentee["user"]["id"]
            )
            self.assertEqual(len(unanswered), 1)

            # save a new answer
            mentees_answer = 20
            the_answer = await crud_feedback_answer.answer(
                store,
                feedback_answer_id=unanswered[0]["answer_id"],
                user_id=mentee["user"]["id"],
                value=mentees_answer,
            )

            self.assertEqual(
                the_answer,
                {
                    "id": unanswered[0]["answer_id"],
                    "value": mentees_answer,
                    "feedback_question_id": question["id"],
                    "user_id": mentee["user"]["id"],
                    "asked_at": "2021-01-02T00:00:00",
                    "max_retries": 3,
                    "retries": 0,
                    "active": True,
                    "updated": "2021-01-02T00:00:00",
                    "created": "2021-01-02T00:00:00",
                },
            )

    @time_machine.travel(travel_at, tick=False)
    async def test_retries(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            store = PostgresStore(database)

            q_to_create = read_json(
                "tests/fixtures/feedback_question_predefined_repeat.json"
            )

            question = await crud_feedback_question.create(store, q_to_create)

            mentee = await create_non_mentor(store, role="mentee")

            unanswered = await crud_feedback_question.fetch_all_needing_answers(
                store, user_id=mentee["user"]["id"]
            )
            self.assertEqual(len(unanswered), 1)
            self.assertEqual(unanswered[0]["id"], question["id"])

            # trying to get new question right after should not return anything
            unanswered = await crud_feedback_question.fetch_all_needing_answers(
                store, user_id=mentee["user"]["id"]
            )
            self.assertEqual(len(unanswered), 0)

            # retry in following days
            for i in range(question["rules"]["schedule"]["remind_times_when_skipped"]):
                with time_machine.travel(travel_at + timedelta(days=1 + i), tick=False):
                    # should find the same question
                    unanswered = await crud_feedback_question.fetch_all_needing_answers(
                        store, user_id=mentee["user"]["id"]
                    )
                    self.assertEqual(len(unanswered), 1)
                    self.assertEqual(unanswered[0]["id"], question["id"])

                    # trying to get new question right after should not return anything
                    unanswered = await crud_feedback_question.fetch_all_needing_answers(
                        store, user_id=mentee["user"]["id"]
                    )
                    self.assertEqual(len(unanswered), 0)

            # should not repeat anymore
            with time_machine.travel(
                travel_at
                + timedelta(
                    days=question["rules"]["schedule"]["remind_times_when_skipped"]
                ),
                tick=False,
            ):
                unanswered = await crud_feedback_question.fetch_all_needing_answers(
                    store, user_id=mentee["user"]["id"]
                )
                self.assertEqual(len(unanswered), 0)

    @time_machine.travel(travel_at, tick=False)
    async def test_repetitions_and_retries(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            store = PostgresStore(database)

            q_to_create = read_json(
                "tests/fixtures/feedback_question_predefined_repeat.json"
            )

            question = await crud_feedback_question.create(store, q_to_create)

            mentee = await create_non_mentor(store, role="mentee")

            # should find the same question
            unanswered = await crud_feedback_question.fetch_all_needing_answers(
                store, user_id=mentee["user"]["id"]
            )
            self.assertEqual(unanswered[0]["id"], question["id"])

            await crud_feedback_answer.answer(
                store,
                feedback_answer_id=unanswered[0]["answer_id"],
                user_id=mentee["user"]["id"],
                value=20,
            )

            with time_machine.travel(
                travel_at
                + timedelta(
                    days=question["rules"]["schedule"]["repetitions"][
                        "days_since_previous_answer"
                    ]
                ),
                tick=False,
            ):
                unanswered_repetition = (
                    await crud_feedback_question.fetch_all_needing_answers(
                        store, user_id=mentee["user"]["id"]
                    )
                )
                self.assertEqual(len(unanswered_repetition), 1)
                self.assertEqual(unanswered_repetition[0]["id"], question["id"])
                # answer id shouldn't match because new one was generated
                self.assertNotEqual(
                    unanswered_repetition[0]["answer_id"], unanswered[0]["answer_id"]
                )

                with time_machine.travel(
                    travel_at
                    + timedelta(
                        days=question["rules"]["schedule"]["repetitions"][
                            "days_since_previous_answer"
                        ]
                        + 1
                    ),
                    tick=False,
                ):
                    unanswered_repetition_retry = (
                        await crud_feedback_question.fetch_all_needing_answers(
                            store, user_id=mentee["user"]["id"]
                        )
                    )
                    self.assertEqual(len(unanswered_repetition_retry), 1)
                    self.assertEqual(
                        unanswered_repetition_retry[0]["id"], question["id"]
                    )
                    # answer id should match because retrying latest
                    self.assertEqual(
                        unanswered_repetition_retry[0]["answer_id"],
                        unanswered_repetition[0]["answer_id"],
                    )

    @time_machine.travel(travel_at, tick=False)  # stop the clock
    async def test_question_with_repeat_until_repetition(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            store = PostgresStore(database)

            question = await crud_feedback_question.create(
                store,
                read_json(
                    "tests/fixtures/feedback_question_repeat_until_week_from_registration.json"
                ),
            )

            # "repeat until" question is asked only from active mentees so send 1 msg to be active
            mentee = await create_non_mentor(store, role="mentee")
            mentor = await create_mentor(store, role="mentor")
            await post_message(
                store, sender=mentee["user"], recipient=mentor["user"], content="Hi!"
            )

            # "repeat until" question is asked only after first week
            unanswered = await crud_feedback_question.fetch_all_needing_answers(
                store, user_id=mentee["user"]["id"]
            )
            self.assertEqual(len(unanswered), 0)

            after_registration = travel_at + timedelta(
                days=question["rules"]["schedule"]["first"]["days_since_registration"]
            )
            with time_machine.travel(after_registration, tick=False):
                unanswered = await crud_feedback_question.fetch_all_needing_answers(
                    store, user_id=mentee["user"]["id"]
                )
                self.assertEqual(len(unanswered), 1)

                # save a new answer
                mentees_answer = 0
                the_answer = await crud_feedback_answer.answer(
                    store,
                    feedback_answer_id=unanswered[0]["answer_id"],
                    user_id=mentee["user"]["id"],
                    value=mentees_answer,
                )

                self.assertEqual(
                    the_answer,
                    {
                        "id": unanswered[0]["answer_id"],
                        "value": mentees_answer,
                        "feedback_question_id": question["id"],
                        "user_id": mentee["user"]["id"],
                        "asked_at": after_registration.replace(tzinfo=None).isoformat(),
                        "max_retries": 0,
                        "retries": 0,
                        "active": True,
                        "updated": after_registration.replace(tzinfo=None).isoformat(),
                        "created": after_registration.replace(tzinfo=None).isoformat(),
                    },
                )


# TODO: test what happens if user opens with 1 year in between. it should generate new question every time
