from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_admin,
    get_random_admin,
    api_login,
    api_auth_header,
    api_delete_skills,
    random_string,
    api_get_skills,
    debug_post,
    debug_get,
    debug_delete,
    api_create_skill,
)


def test_add_a_skill_as_admin():
    """create skill as valid admin"""
    api_delete_accounts()
    api_delete_skills()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    new_skill = {"name": random_string(20)}
    skills_response = debug_post(f"{API_URL}/skills", headers=header, json=new_skill)
    skills = api_get_skills()
    expectedAttr = set(["active", "created", "id", "name", "updated"])
    assert skills_response.status_code == 201
    assert len(skills) == 1
    assert all(attr in skills_response.json() for attr in expectedAttr)


def test_get_all_skills_as_an_admin():
    """get all skills as an admin"""
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    api_delete_skills()
    skills_response_before = debug_get(f"{API_URL}/skills", headers=header)
    skill_one = random_string(10)
    skill_two = random_string(10)
    api_create_skill(skill_one, header)
    api_create_skill(skill_two, header)
    skills_response = debug_get(f"{API_URL}/skills", headers=header)
    skills = skills_response.json()["resources"]
    assert len(skills_response_before.json()["resources"]) == len(skills) - 2
    assert [s for s in skills if s["name"] == skill_one]
    assert [s for s in skills if s["name"] == skill_two]


def test_get_a_skill_as_an_admin():
    """get a skill as an admin"""
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    api_delete_skills()
    skill = random_string(10)
    created_skill_response = api_create_skill(skill, header)
    skill_id = created_skill_response.json()["id"]
    fetch_skill_response = debug_get(f"{API_URL}/skills/{skill_id}", headers=header)
    fetched_skill = fetch_skill_response.json()
    expectedAttr = set(["active", "created", "id", "name", "updated"])
    assert all(attr in fetched_skill for attr in expectedAttr)
    assert fetched_skill["name"] == skill
    assert fetched_skill["id"] == skill_id


def test_delete_a_skill_as_an_admin():
    """delete skill as an admin"""
    api_delete_accounts()
    api_delete_skills()

    assert len(api_get_skills()) == 0

    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)

    new_skill = {"name": random_string(20)}
    s = api_create_skill(new_skill["name"], header)
    created_skill = s.json()

    assert len(api_get_skills()) == 1

    debug_delete(f"{API_URL}/skills/{created_skill['id']}", headers=header)

    assert len(api_get_skills()) == 0
