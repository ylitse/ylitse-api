from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    get_random_mentee,
    get_random_mentor,
    debug_get,
    debug_put,
    debug_patch,
    api_post_message,
    random_string,
    api_create_and_login_mentee,
    api_create_and_login_mentor,
)


def test_archived_contact_can_chat_as_a_mentee():
    api_delete_accounts()

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    archived_response = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "archived",
        },
    )
    assert archived_response.status_code == 200

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
    )

    messages = res.json()["resources"]
    assert len(messages) == 2


def test_cannot_change_own_archive_status_as_a_mentee():
    api_delete_accounts()

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    archived_response = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "archived",
        },
    )
    assert archived_response.status_code == 200

    archived_response = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=sender_mentee_headers,
        json={
            "status": "ok",
        },
    )
    assert archived_response.status_code == 401
    assert archived_response.json() == {
        "error": "Unauthorized",
        "message": "User not authenticated, provide correct credentials!",
    }

    patch_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=sender_mentee_headers,
        json=[
            {
                "id": sender_mentee_myuser["user"]["id"],
                "status": "ok",
            }
        ],
    )
    assert patch_res.status_code == 401
    assert patch_res.json() == {
        "error": "Unauthorized",
        "message": "User not authenticated, provide correct credentials!",
    }
