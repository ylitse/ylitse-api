import os
import random
from tests.api.helpers import API_URL
from tests.api.helpers import (
    get_random_mentee,
    # api_delete_accounts,
    get_random_mentor,
    random_string,
    api_post_message,
    api_create_and_login_mentee,
    api_create_and_login_mentor,
    get_random_admin,
    api_create_admin,
    api_auth_header,
    api_access_token,
    debug_post,
    debug_get,
    # api_delete_all_reports,
)

SAFETY_FEATURE = os.getenv("SAFETY_FEATURE", "false")

conversation = [
    "Hey, I've been meaning to talk to you about something. Can we chat \
        for a bit?",
    "Of course, what's going on? Is everything alright?",
    "Well, it's about Sarah. I've noticed that she seems really unhappy \
        lately in her relationship.",
    "Really? I hadn't noticed. What's been going on?",
    "She mentioned that they've been arguing a lot, and there's a lack \
        of communication between them. She feels like her partner \
        doesn't listen to her anymore.",
    "That sounds tough. Have they tried talking about their issues and \
        finding a solution?",
    "I'm not sure if they have. Sarah mentioned that she's been hesitant \
        to bring up her concerns because she's afraid it will lead \
        to another argument.",
    "It's important for her to express her feelings and concerns openly. \
        Avoiding communication might make things worse in the long run.",
    "I agree. I've been trying to encourage her to have an honest \
        conversation with her partner and express how she's been \
        feeling. But it's easier said than done.",
    "It definitely is. Sometimes, people fear confrontation and the \
        possibility of things not working out. But it's crucial for her \
        to prioritize her own happiness and well-being.",
    "Exactly. She deserves to be in a healthy and fulfilling relationship. \
        I hope she finds the courage to address the issues and make \
        necessary changes.",
    "I hope so too. It's important for her to remember that she deserves \
        happiness and shouldn't settle for less. Supporting her through \
        this will mean a lot.",
    "Definitely. I'll continue to be there for her and offer my support. \
        It's just hard seeing someone you care about going through \
        relationship difficulties.",
    "I understand. It's never easy to witness a friend's struggles. Just \
        let her know that she's not alone and that you're there to \
        listen and support her.",
    "I will. Thanks for talking this through with me. It helps to have \
        someone to share these concerns with.",
    "Anytime. That's what friends are for. Remember, you're doing a great \
        job being there for Sarah. She's lucky to have you as a friend.",
    "Thanks for the kind words. I appreciate your support too. Let's hope \
        things work out for Sarah in the end.",
    "Absolutely. Sending positive vibes her way. If she ever needs someone \
        to talk to, let her know she can count on both of us.",
    "Will do. Thanks again. Take care, and let's catch up soon!",
    "You're welcome. Take care too, and looking forward to our next chat. \
        Have a great day!",
    "You too!",
    "No, you are stupid",
]


if SAFETY_FEATURE == "true":
    # api_delete_accounts()
    # api_delete_all_reports()
    new_mentee = get_random_mentee()
    new_mentee, mentee_myuser, mentee_headers = api_create_and_login_mentee(new_mentee)

    new_mentor = get_random_mentor()
    new_mentor, mentor_myuser, mentor_headers = api_create_and_login_mentor(new_mentor)

    new_admin = get_random_admin()
    api_create_admin(new_admin)
    headers = api_auth_header(
        api_access_token(new_admin["login_name"], new_admin["password"])
    )

    for index, word in enumerate(conversation):
        if index % 2 == 0:
            msg = word

            api_post_message(
                mentee_myuser["user"]["id"],
                mentor_myuser["user"]["id"],
                mentee_headers,
                msg,
            )

        else:
            reply_msg = word
            res = api_post_message(
                mentor_myuser["user"]["id"],
                mentee_myuser["user"]["id"],
                mentor_headers,
                reply_msg,
            )

    message_respond = res.json()
    report_reason = ""
    for x in range(100):
        report_reason += random_string(random.randint(1, 11))
        report_reason += " "

    mentor_id = mentor_myuser["user"]["id"]
    put_report_response = debug_post(
        f"{API_URL}/reports",
        headers=mentee_headers,
        json={
            "reported_user_id": mentor_id,
            "contact_field": "testi@testi.fi",
            "report_reason": report_reason,
            "reporter_user_id": mentee_myuser["user"]["id"],
        },
    )

    res = debug_get(
        API_URL + "/users/" + mentor_id + "/messages",
        headers=headers,
        params={
            "contact_user_ids": ",".join(
                [
                    mentee_myuser["user"]["id"],
                ]
            ),
            "from_message_id": message_respond["id"],
            "max": 100,
            "desc": True,
        },
    )
    messages = res.json()
    print(messages)
