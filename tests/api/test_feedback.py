# pylint: disable=line-too-long,too-many-statements
import time

from tests.test_helpers import read_json

from tests.api.helpers import (
    API_URL,
    api_admin_auth_headers,
    api_delete_accounts,
    api_delete_questions,
    api_get_questions,
    api_answer_question,
    api_create_question,
    api_create_and_login_mentee,
    api_create_and_login_mentor,
    api_get_questions_needs_answer,
    debug_delete,
    debug_post,
    debug_get,
    get_random_mentee,
    get_random_mentor,
    api_post_message,
)


def test_feedback_feature():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        mentee_myuser,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        mentor_myuser,
        _,
    ) = api_create_and_login_mentor(get_random_mentor())

    # create first question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    res = debug_post(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
        json=q,
    )
    assert res.status_code == 201

    # mentor fetches first question
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    assert questions_res.status_code == 200
    questions = questions_res.json()["resources"]
    assert len(questions) == 1

    question = questions[0]
    question_id = question.pop("id")
    assert len(question_id) == 43

    answer_id = question.pop("answer_id")
    assert len(answer_id) == 43

    assert question == {
        "titles": {
            "fi": "Kun ajattelet nykyhetkeä, kuinka tyytyväinen olet luottamuksellisiin ihmissuhteisiisi?",  # noqa
            "en": "When you think about the present, how satisfied are you with your confidential relationships?",  # noqa
        },
        "answer": {
            "type": "range",
            "step": 1,
            "min": {
                "value": 0,
                "labels": {
                    "fi": "Erittäin tyytymätön",
                    "en": "Very dissatisfied",
                },
            },
            "max": {
                "value": 100,
                "labels": {
                    "fi": "Erittäin tyytyväinen",
                    "en": "Very pleased",
                },
            },
        },
    }

    # mentee answers
    answer_res = debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "value": 42,
        },
    )
    assert answer_res.status_code == 201

    # mentee tries to fetch more questions but nothing needs answering
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    assert questions_res.status_code == 200
    assert len(questions_res.json()["resources"]) == 0

    # create second question
    q = read_json("tests/fixtures/feedback_question_repeat_until.json")
    res = debug_post(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
        json=q,
    )
    assert res.status_code == 201

    # mentee needs to be active,
    # meaning needs to have sent a msg for this question to trigger
    send_msg_res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        "Hi",
    )
    assert send_msg_res.status_code == 201

    #  mentee fetches first question
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    assert questions_res.status_code == 200
    questions = questions_res.json()["resources"]
    assert len(questions) == 1

    question = questions[0]
    question_id = question.pop("id")
    assert len(question_id) == 43

    answer_id = question.pop("answer_id")
    assert len(answer_id) == 43

    assert question == {
        "titles": {
            "fi": "Oletko saanut apua mentorilta?",
            "en": "Have you received help from a mentor?",
        },
        "answer": {
            "type": "yes-no",
            "yes": {
                "value": 1,
                "labels": {"fi": "Kyllä olen", "en": "Yes, I have"},
            },
            "no": {
                "value": 0,
                "labels": {"fi": "En ole", "en": "No, I haven't"},
            },
        },
    }

    # mentee answers
    answer_res = debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "value": 1,
        },
    )
    assert answer_res.status_code == 201

    # mentee tries to fetch more questions but nothing needs answering
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    assert questions_res.status_code == 200
    assert len(questions_res.json()["resources"]) == 0

    # does list all questions work
    res = debug_get(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
    )
    assert res.status_code == 200
    qs = res.json()["resources"]
    assert len(qs) == 2

    # delete all the questions
    for question in qs:
        res = debug_delete(
            f"{API_URL}/feedback/questions/{question['id']}",
            headers=api_admin_auth_headers(),
        )
        assert res.status_code == 204

    # after delete we should not have any questions
    res = debug_get(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
    )
    assert res.status_code == 200
    qs = res.json()["resources"]
    assert len(qs) == 0


def test_get_question_based_on_recipient():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create first question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(
        q,
        api_admin_auth_headers(),
    )

    # mentor cannot get question because role does not match
    questions = api_get_questions_needs_answer(mentor_headers)
    assert len(questions) == 0

    # mentee can get the question, because role matches
    questions_2 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_2) == 1


def test_get_question_based_on_user_id():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        mentee_myuser,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        _,
        mentee_2_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question with mentee user_id in recipients array
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    updated_rules = {**q["rules"], "recipients": [mentee_myuser["user"]["id"]]}
    updated_question = {**q, "rules": updated_rules}

    api_create_question(updated_question, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    # mentee_2 will not get the question
    questions_2 = api_get_questions_needs_answer(mentee_2_headers)
    assert len(questions_2) == 0


# test that cannot get question if account is too old
def test_get_question_max_old_account():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    updated_schedule_first = {
        **q["rules"]["schedule"]["first"],
        "max_old_account_in_days": 0.000011574074074,
    }
    updated_schedule = {
        **q["rules"]["schedule"],
        "first": updated_schedule_first,
    }
    updated_rules = {**q["rules"], "schedule": updated_schedule}
    updated_question = {**q, "rules": updated_rules}
    api_create_question(updated_question, api_admin_auth_headers())

    time.sleep(2)

    # mentee will not get the question, because account is too old
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 0


# test asking question again on same day
def test_get_question_reminder_if_skipped():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    # asking again and now mentee wont get the question
    questions_2 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_2) == 0


# test asking question again (reminder) on same day if short interval
def test_question_reminder_if_skipped_short_interval():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    updated_schedule = {
        **q["rules"]["schedule"],
        "remind_interval_days": 0.000011574074074,
    }
    updated_rules = {**q["rules"], "schedule": updated_schedule}
    updated_question = {**q, "rules": updated_rules}
    api_create_question(updated_question, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    time.sleep(1)

    # asking again and now mentee will get the question,
    # because of the short interval
    questions_2 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_2) == 1

    # now answering to the question and waiting 1 second
    answer = {"answer_id": questions_2[0]["answer_id"], "value": 0}
    api_answer_question(answer, mentee_headers)
    time.sleep(1)

    # asking again and now mentee will not get the question,
    # because already answered
    questions_3 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_3) == 0


# test max_retries (skipped until asked enough)
def test_question_max_retries():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    updated_schedule = {
        **q["rules"]["schedule"],
        "remind_interval_days": 0.000011574074074,
        "remind_times_when_skipped": 2,
    }
    updated_rules = {**q["rules"], "schedule": updated_schedule}
    updated_question = {**q, "rules": updated_rules}
    api_create_question(updated_question, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    time.sleep(1)

    # first remind
    # asking again and now mentee will get the question,
    # because of the short interval
    questions_2 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_2) == 1

    time.sleep(1)

    # second remind
    # asking again and mentee will get the question,
    questions_3 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_3) == 1

    time.sleep(1)

    # third remind
    # asking again and mentee will NOT get the question,
    # max-retries are full!
    questions_4 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_4) == 0


# test the reminder for repetitions
def test_question_repetition_reminders():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question
    q = read_json("tests/fixtures/feedback_question_instant_repetition.json")
    updated_schedule = {
        **q["rules"]["schedule"],
        "remind_interval_days": 0.000011574074074,
        "remind_times_when_skipped": 1,
    }
    updated_rules = {**q["rules"], "schedule": updated_schedule}
    updated_question = {**q, "rules": updated_rules}
    api_create_question(updated_question, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    # answer to the question
    answer = {"answer_id": questions[0]["answer_id"], "value": 0}
    api_answer_question(answer, mentee_headers)
    time.sleep(1)

    # asking again and now mentee will get the question,
    # because of the short repetition interval
    questions_2 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_2) == 1

    # dont answer to the question
    time.sleep(1)

    # asking again and now mentee will still get the question,
    # because the remind_interval is short
    questions_3 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_3) == 1

    time.sleep(1)

    # not anymore, times are up
    questions_4 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_4) == 0


def test_question_repetition_times():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question
    q = read_json("tests/fixtures/feedback_question_instant_repetition.json")
    api_create_question(q, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    # answer to the question
    answer = {"answer_id": questions[0]["answer_id"], "value": 0}
    api_answer_question(answer, mentee_headers)
    time.sleep(1)

    # asking again and now mentee will get the question,
    # because of the short repetition interval
    questions_2 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_2) == 1

    # now answering to the question and waiting again
    answer = {"answer_id": questions_2[0]["answer_id"], "value": 0}
    api_answer_question(answer, mentee_headers)
    time.sleep(1)

    # asking again and now mentee will still get the question,
    # because already one more time should happen
    questions_3 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_3) == 1

    time.sleep(1)

    # not anymore, times are up
    questions_4 = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_4) == 0


def test_question_repetition_days():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question with very small delay in repetition
    q = read_json("tests/fixtures/feedback_question_instant_repetition.json")
    updated_repetitions = {
        **q["rules"]["schedule"]["repetitions"],
        "min_days_since_previous_answer": 0.000011574074074,
    }
    updated_schedule = {
        **q["rules"]["schedule"],
        "repetitions": updated_repetitions,
    }
    updated_rules = {**q["rules"], "schedule": updated_schedule}
    updated_question = {**q, "rules": updated_rules}

    # question has 1 second repetition min-value
    api_create_question(updated_question, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    # mentee will not get the question again
    questions_again = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_again) == 0

    answer = {"answer_id": questions[0]["answer_id"], "value": 0}
    api_answer_question(answer, mentee_headers)

    # wait a second
    time.sleep(1)

    # will get question repetition again, because over min-days
    questions_again = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_again) == 1


def test_question_repetition_messages():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        mentee_myuser,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question with repetition
    # that will occur after user has sent 1 message
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    updated_repetitions = {
        **q["rules"]["schedule"]["repetitions"],
        "messages_since_previous_answer": 1,
        "min_days_since_previous_answer": 0.000011574074074,
    }
    updated_schedule = {
        **q["rules"]["schedule"],
        "repetitions": updated_repetitions,
    }
    updated_rules = {**q["rules"], "schedule": updated_schedule}
    updated_question = {**q, "rules": updated_rules}

    api_create_question(updated_question, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    # create a mentor and send a message to her/him
    (
        _,
        mentor_myuser,
        _,
    ) = api_create_and_login_mentor(get_random_mentor())

    api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        "Hi",
    )

    time.sleep(1)

    # mentee will get the question again
    questions_again = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_again) == 1


def test_question_repetition_until():
    api_delete_accounts()
    api_delete_questions()

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # create question with very small delay
    # in repetition and occur until answered 1
    q = read_json("tests/fixtures/feedback_question_repeat_until.json")
    updated_first = {
        **q["rules"]["schedule"]["first"],
        "sent_messages_threshold": 0,
    }
    updated_repetitions = {
        **q["rules"]["schedule"]["repetitions"],
        "days_since_previous_answer": 0.000011574074074,
    }
    updated_schedule = {
        **q["rules"]["schedule"],
        "first": updated_first,
        "repetitions": updated_repetitions,
    }
    updated_rules = {**q["rules"], "schedule": updated_schedule}
    updated_question = {**q, "rules": updated_rules}

    api_create_question(updated_question, api_admin_auth_headers())

    # mentee will get the question
    questions = api_get_questions_needs_answer(mentee_headers)
    assert len(questions) == 1

    # mentee will answer to the question, and wait a second
    answer = {"answer_id": questions[0]["answer_id"], "value": 0}
    api_answer_question(answer, mentee_headers)
    time.sleep(1)

    # mentee will get the question again
    # because the answer was not what expected
    questions_third = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_third) == 1

    # mentee will answer to the question and wait a second
    answer = {"answer_id": questions[0]["answer_id"], "value": 1}
    api_answer_question(answer, mentee_headers)
    time.sleep(1)

    # and now it wont appear again because the value is correct
    questions_last = api_get_questions_needs_answer(mentee_headers)
    assert len(questions_last) == 0


def test_create_question_with_correct_data():
    api_delete_accounts()
    api_delete_questions()

    # create question with correct data
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    res = debug_post(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
        json=q,
    )
    assert res.status_code == 201

    # create another
    q_2 = read_json("tests/fixtures/feedback_question_repeat_until.json")
    res_2 = debug_post(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
        json=q_2,
    )
    assert res_2.status_code == 201


def test_create_question_with_wrong_schedule_data():
    api_delete_accounts()
    api_delete_questions()

    # create question with wrong data
    # days_since_previous_answer neees to be more than 0
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")

    updated_repetitions = {
        **q["rules"]["schedule"]["repetitions"],
        "days_since_previous_answer": 0,
    }
    updated_schedule = {
        **q["rules"]["schedule"],
        "repetitions": updated_repetitions,
    }
    updated_rules = {**q["rules"], "schedule": updated_schedule}
    updated_question = {**q, "rules": updated_rules}
    res = debug_post(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
        json=updated_question,
    )
    assert res.status_code == 422

    # create other question with wrong data
    # have a numeric value to be a string
    q_2 = read_json("tests/fixtures/feedback_question_repeat_until.json")

    updated_repetitions_2 = {
        **q_2["rules"]["schedule"]["repetitions"],
        "days_since_previous_answer": "Seven",
    }
    updated_schedule_2 = {
        **q_2["rules"]["schedule"],
        "repetitions": updated_repetitions_2,
    }
    updated_rules_2 = {**q_2["rules"], "schedule": updated_schedule_2}
    updated_question_2 = {**q_2, "rules": updated_rules_2}
    res_2 = debug_post(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
        json=updated_question_2,
    )

    assert res_2.status_code == 422

    # one with totally wrong structure
    res_3 = debug_post(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
        json={"foo": "bar"},
    )

    assert res_3.status_code == 422


def test_list_all_questions():
    api_delete_accounts()
    api_delete_questions()

    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    q_2 = read_json("tests/fixtures/feedback_question_repeat_until.json")
    api_create_question(q, api_admin_auth_headers())
    api_create_question(q_2, api_admin_auth_headers())

    # admin sees questions
    res = debug_get(
        API_URL + "/feedback/questions",
        headers=api_admin_auth_headers(),
    )
    assert res.status_code == 200
    qs = res.json()["resources"]
    assert len(qs) == 2

    # questions should not show up for unauthenticated user
    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    res = debug_get(
        API_URL + "/feedback/questions",
        headers=mentee_headers,
    )
    assert res.status_code == 401
    assert res.json() == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_delete_question():
    api_delete_accounts()
    api_delete_questions()

    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())

    questions = api_get_questions()
    assert len(questions) == 1
    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # cannot delete as non-admin user
    res = debug_delete(
        f"{API_URL}/feedback/questions/{questions[0]['id']}",
        headers=mentee_headers,
    )
    assert res.status_code == 401
    assert res.json() == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }

    # admin can delete
    res = debug_delete(
        f"{API_URL}/feedback/questions/{questions[0]['id']}",
        headers=api_admin_auth_headers(),
    )
    assert res.status_code == 204

    questions_after = api_get_questions()
    assert len(questions_after) == 0


def test_delete_question_wrong_id():
    api_delete_accounts()
    api_delete_questions()

    res = debug_delete(
        f"{API_URL}/feedback/questions/foo",
        headers=api_admin_auth_headers(),
    )
    assert res.status_code == 404

    questions_after = api_get_questions()
    assert len(questions_after) == 0
