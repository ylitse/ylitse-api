from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_admin,
    get_random_admin,
    get_random_mentor,
    api_login,
    api_auth_header,
    api_delete_skills,
    random_string,
    api_get_skills,
    debug_get,
    api_create_mentor,
    api_create_skill,
)


def test_get_all_skills_as_a_mentor():
    """get all skills as a mentor"""
    api_delete_accounts()
    api_delete_skills()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    admin_login_response = api_login(new_admin)
    admin_access_token = admin_login_response["tokens"]["access_token"]
    admin_header = api_auth_header(admin_access_token)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    login_response = api_login(new_mentor)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    skills_response_before = api_get_skills()
    skill_one = random_string(10)
    skill_two = random_string(10)
    api_create_skill(skill_one, admin_header)
    api_create_skill(skill_two, admin_header)
    skills_response = debug_get(f"{API_URL}/skills", headers=header)
    skills = skills_response.json()["resources"]
    assert len(skills_response_before) == len(skills) - 2
    assert [s for s in skills if s["name"] == skill_one]
    assert [s for s in skills if s["name"] == skill_two]
