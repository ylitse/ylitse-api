import httpx
from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_admin,
    get_random_admin,
    get_random_mentor,
    api_login,
    get_random_mentee,
    api_auth_header,
    random_string,
    debug_post,
    debug_put,
    debug_get,
    api_create_mentor,
    api_create_mentee,
    api_get_users,
    api_my_user,
    api_post_message,
)


def test_fetch_an_existing_account_except_for_password_as_an_admin():
    """fetch existing account as an admin"""
    api_delete_accounts()
    mentee = get_random_mentee()
    myuser = api_create_mentee(mentee)
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    myuser_account_id = myuser["account"]["id"]
    res = debug_get(f"{API_URL}/accounts/{myuser_account_id}", headers=(header))
    account = res.json()
    expectedAttributes = set(
        ["active", "created", "email", "id", "login_name", "role", "updated"]
    )
    assert all(attr in account for attr in expectedAttributes)
    assert res.status_code == 200


# Not sure what 'private' means
def test_fetch_my_own_user_including_private_as_an_admin():
    """fetch my own user as an admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    res = debug_get(f"{API_URL}/myuser", headers=header)
    myuser = res.json()
    expectedAccountAttributes = set(
        [
            "active",
            "created",
            "email",
            "id",
            "login_name",
            "phone",
            "role",
            "updated",
        ]
    )
    expectedUserAttributes = set(
        [
            "account_id",
            "id",
            "active",
            "created",
            "display_name",
            "role",
            "id",
            "updated",
        ]
    )
    assert res.status_code == 200
    assert all(attr in myuser["user"] for attr in expectedUserAttributes)
    assert all(attr in myuser["account"] for attr in expectedAccountAttributes)


def test_create_a_new_user_mentor_as_admin():
    """create mentor as valid admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentor = {
        "login_name": "JohnLogin",
        "birth_year": 1998,
        "display_name": "John",
        "password": "secretsecret",
        "email": "john@doe.com",
        "phone": "666",
        "role": "mentor",
        "gender": "male",
        "languages": ["fi", "se", "en"],
        "region": "Järvenpää",
        "skills": [],
        "story": "Short one",
        "communication_channels": ["phone"],
    }
    post_mentor_response = debug_post(
        f"{API_URL}/accounts",
        headers=header,
        json={
            "password": mentor["password"],
            "account": {
                "role": mentor["role"],
                "login_name": mentor["login_name"],
                "email": mentor["email"],
                "phone": mentor["phone"],
            },
        },
    )
    mentor_json = post_mentor_response.json()
    mentor_id = mentor_json["mentor"]["id"]
    mentor_user_id = mentor_json["user"]["id"]
    put_user_response = debug_put(
        f"{API_URL}/users/{mentor_user_id}",
        headers=header,
        json={
            "active": mentor_json["user"]["active"],
            "account_id": mentor_json["user"]["account_id"],
            "id": mentor_json["user"]["id"],
            "created": mentor_json["user"]["created"],
            "updated": mentor_json["user"]["updated"],
            "role": mentor["role"],
            "display_name": mentor["display_name"],
        },
    )
    put_mentor_response = debug_put(
        f"{API_URL}/mentors/{mentor_id}",
        headers=header,
        json={
            "account_id": mentor_json["mentor"]["account_id"],
            "user_id": mentor_json["mentor"]["user_id"],
            "id": mentor_json["mentor"]["id"],
            "created": mentor_json["mentor"]["created"],
            "active": mentor_json["mentor"]["active"],
            "birth_year": mentor["birth_year"],
            "display_name": mentor["display_name"],
            "gender": mentor["gender"],
            "languages": mentor["languages"],
            "region": mentor["region"],
            "skills": mentor["skills"],
            "story": mentor["story"],
            "communication_channels": mentor["communication_channels"],
        },
    )
    created_mentor_response = debug_get(
        f"{API_URL}/mentors/{mentor_id}", headers=header
    )
    created_mentor_json = created_mentor_response.json()
    assert post_mentor_response.status_code == 201
    assert put_user_response.status_code == 200
    assert put_mentor_response.status_code == 200
    assert created_mentor_json["birth_year"] == mentor["birth_year"]
    assert created_mentor_json["region"] == mentor["region"]
    assert (
        created_mentor_json["communication_channels"]
        == mentor["communication_channels"]
    )


def test_create_a__new_user_mentor_empty_birth_year_as_admin():
    """create mentor as valid admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentor = {
        "login_name": "John_login",
        "display_name": "John",
        "password": "secretsecret",
        "email": "john@doe.com",
        "phone": "666",
        "role": "mentor",
        "gender": "male",
        "languages": ["fi", "se", "en"],
        "region": "Järvenpää",
        "skills": [],
        "story": "Short one",
        "communication_channels": ["phone"],
    }
    post_mentor_response = debug_post(
        f"{API_URL}/accounts",
        headers=header,
        json={
            "password": mentor["password"],
            "account": {
                "role": mentor["role"],
                "login_name": mentor["login_name"],
                "email": mentor["email"],
                "phone": mentor["phone"],
            },
        },
    )
    mentor_json = post_mentor_response.json()
    mentor_id = mentor_json["mentor"]["id"]
    mentor_user_id = mentor_json["user"]["id"]
    put_user_response = debug_put(
        f"{API_URL}/users/{mentor_user_id}",
        headers=header,
        json={
            "active": mentor_json["user"]["active"],
            "account_id": mentor_json["user"]["account_id"],
            "id": mentor_json["user"]["id"],
            "created": mentor_json["user"]["created"],
            "updated": mentor_json["user"]["updated"],
            "role": mentor["role"],
            "display_name": mentor["display_name"],
        },
    )
    put_mentor_response = debug_put(
        f"{API_URL}/mentors/{mentor_id}",
        headers=header,
        json={
            "account_id": mentor_json["mentor"]["account_id"],
            "user_id": mentor_json["mentor"]["user_id"],
            "id": mentor_json["mentor"]["id"],
            "created": mentor_json["mentor"]["created"],
            "active": mentor_json["mentor"]["active"],
            "display_name": mentor["display_name"],
            "gender": mentor["gender"],
            "languages": mentor["languages"],
            "region": mentor["region"],
            "skills": mentor["skills"],
            "story": mentor["story"],
            "communication_channels": mentor["communication_channels"],
        },
    )
    created_mentor_response = debug_get(
        f"{API_URL}/mentors/{mentor_id}", headers=header
    )
    created_mentor_json = created_mentor_response.json()
    assert post_mentor_response.status_code == 201
    assert put_user_response.status_code == 200
    assert put_mentor_response.status_code == 400
    assert created_mentor_json["birth_year"] == 1990
    assert created_mentor_json["region"] == "HKI"
    assert created_mentor_json["communication_channels"] == []
    assert created_mentor_json["display_name"] == "janteri"


def test_create_a_new_user_mentee_as_an_admin():
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    display_name = "actor_felix"
    mentee = {
        "account": {
            "login_name": random_string(10),
            "role": "mentee",
            "email": "acotor@felix.fi",
            "phone": "",
        },
        "password": random_string(10),
    }
    post_account_response = debug_post(
        f"{API_URL}/accounts", headers=header, json=mentee
    )
    menteeResponse = post_account_response.json()
    mentee_id = menteeResponse["user"]["id"]
    putUser = {
        "account_id": menteeResponse["account"]["id"],
        "active": menteeResponse["account"]["active"],
        "created": menteeResponse["user"]["created"],
        "display_name": display_name,
        "id": mentee_id,
        "role": menteeResponse["account"]["role"],
        "updated": menteeResponse["user"]["updated"],
    }
    put_user_response = debug_put(
        f"{API_URL}/users/{mentee_id}", headers=header, json=putUser
    )
    expectedAttr = set(
        [
            "display_name",
            "role",
            "account_id",
            "id",
            "created",
            "updated",
            "active",
        ]
    )
    created_mentee = put_user_response.json()
    assert post_account_response.status_code == 201
    assert put_user_response.status_code == 200
    assert created_mentee["display_name"] == display_name
    assert all(attr in created_mentee for attr in expectedAttr)


def test_create_a_new_user_admin_as_an_admin():
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    display_name = "felixadmin"
    admin = {
        "account": {
            "login_name": random_string(10),
            "role": "admin",
            "email": "",
            "phone": "",
        },
        "password": random_string(10),
    }
    post_account_response = debug_post(
        f"{API_URL}/accounts", headers=header, json=admin
    )
    adminResponse = post_account_response.json()
    admin_id = adminResponse["user"]["id"]
    putUser = {
        "account_id": adminResponse["account"]["id"],
        "active": adminResponse["account"]["active"],
        "created": adminResponse["user"]["created"],
        "display_name": display_name,
        "id": admin_id,
        "role": adminResponse["account"]["role"],
        "updated": adminResponse["user"]["updated"],
    }
    put_user_response = debug_put(
        f"{API_URL}/users/{admin_id}", headers=header, json=putUser
    )
    expectedAttr = set(
        [
            "display_name",
            "role",
            "account_id",
            "id",
            "created",
            "updated",
            "active",
        ]
    )
    created_admin = put_user_response.json()
    assert post_account_response.status_code == 201
    assert put_user_response.status_code == 200
    assert created_admin["display_name"] == display_name
    assert all(attr in created_admin for attr in expectedAttr)


def test_update_a_mentor_as_an_admin():
    """update mentor as admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentors_response = httpx.get(f"{API_URL}/mentors", headers=header)
    mentor_json = mentors_response.json()["resources"][0]
    mentor_id = mentor_json["id"]
    response_mentor = {
        "account_id": mentor_json["account_id"],
        "active": mentor_json["active"],
        "birth_year": mentor_json["birth_year"],
        "communication_channels": mentor_json["communication_channels"],
        "created": mentor_json["created"],
        "display_name": mentor_json["display_name"],
        "gender": mentor_json["gender"],
        "id": mentor_json["id"],
        "languages": mentor_json["languages"],
        "region": mentor_json["region"],
        "skills": mentor_json["skills"],
        "story": mentor_json["story"],
        "updated": mentor_json["updated"],
        "user_id": mentor_json["user_id"],
    }
    new_region = "Järvenpää"
    updated_mentor = {**response_mentor, "region": new_region}
    update_response = debug_put(
        f"{API_URL}/mentors/{mentor_id}", headers=header, json=updated_mentor
    )
    assert update_response.status_code == 200
    updated_json = update_response.json()
    assert updated_json["region"] == new_region


def test_update_my_own_user_as_an_admin():
    api_delete_accounts()
    admin = get_random_admin()
    created_admin = api_create_admin(admin)
    login_response = api_login(admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    admin_account_id = created_admin["account"]["id"]
    new_email = random_string(10) + "@admin.admin"
    updated_admin_account = {
        "active": created_admin["account"]["active"],
        "created": created_admin["account"]["created"],
        "email": new_email,
        "id": admin_account_id,
        "login_name": created_admin["account"]["login_name"],
        "role": created_admin["account"]["role"],
        "updated": created_admin["account"]["updated"],
    }
    account_response = debug_put(
        f"{API_URL}/accounts/{admin_account_id}",
        headers=header,
        json=updated_admin_account,
    )
    admin_user_id = created_admin["user"]["id"]
    updated_admin_user = {
        "account_id": created_admin["user"]["account_id"],
        "active": created_admin["user"]["active"],
        "created": created_admin["user"]["created"],
        "display_name": random_string(10),
        "id": admin_user_id,
        "role": created_admin["user"]["role"],
        "updated": created_admin["user"]["updated"],
    }
    user_response = debug_put(
        f"{API_URL}/users/{admin_user_id}",
        headers=header,
        json=updated_admin_user,
    )
    updated_my_user = api_my_user(access_token)
    assert account_response.status_code == 200
    assert user_response.status_code == 200
    assert updated_my_user["account"]["email"] == new_email
    assert admin_account_id == updated_my_user["account"]["id"]
    assert updated_my_user["account"]["updated"] != created_admin["account"]["updated"]


def test_update_my_own_account_including_password_as_an_admin():
    api_delete_accounts()
    admin = get_random_admin()
    created_admin = api_create_admin(admin)
    login_response = api_login(admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    admin_account_id = created_admin["account"]["id"]
    updated_admin = {
        "login_name": created_admin["account"]["login_name"],
        "password": random_string(10),
    }
    password_response = debug_put(
        f"{API_URL}/accounts/{admin_account_id}/password",
        headers=header,
        json=updated_admin,
    )
    assert password_response.status_code == 200
    assert password_response.json() == {"message": "Password update succesful"}


def test_update_a_user_except_for_private_as_an_admin():
    api_delete_accounts()
    admin = get_random_admin()
    api_create_admin(admin)
    login_response = api_login(admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentee = get_random_mentee()
    mentee = api_create_mentee(mentee)
    mentee_user_id = mentee["user"]["id"]
    mentee_account_id = mentee["account"]["id"]
    newUpdatedName = random_string(10)
    updated_mentee_account = {
        "active": mentee["account"]["active"],
        "created": mentee["account"]["created"],
        "email": mentee["account"]["email"],
        "id": mentee_account_id,
        "login_name": mentee["account"]["login_name"],
        "role": mentee["account"]["role"],
        "updated": mentee["account"]["updated"],
    }
    updated_mentee_user = {
        "display_name": newUpdatedName,
        "role": mentee["user"]["role"],
        "account_id": mentee["user"]["account_id"],
        "id": mentee_user_id,
        "updated": mentee["user"]["updated"],
    }
    account_res = debug_put(
        f"{API_URL}/accounts/{mentee_account_id}",
        headers=header,
        json=updated_mentee_account,
    )

    user_res = debug_put(
        f"{API_URL}/users/{mentee_user_id}",
        headers=header,
        json=updated_mentee_user,
    )
    user_json = user_res.json()
    assert account_res.status_code == 200
    assert user_res.status_code == 200
    assert user_json["display_name"] == newUpdatedName


def test_update_an_account_including_password_as_an_admin():
    api_delete_accounts()
    admin = get_random_admin()
    api_create_admin(admin)
    login_response = api_login(admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentee = get_random_mentee()
    created_mentee = api_create_mentee(mentee)
    mentee_id = created_mentee["account"]["id"]
    updated_mentee = {
        "login_name": created_mentee["account"]["login_name"],
        "password": random_string(10),
    }
    password_response = debug_put(
        f"{API_URL}/accounts/{mentee_id}/password",
        headers=header,
        json=updated_mentee,
    )
    assert password_response.status_code == 200
    assert password_response.json() == {"message": "Password update succesful"}


def test_delete_an_user_mentee_as_an_admin():
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentee = get_random_mentee()
    newMentee = api_create_mentee(mentee)
    mentee_id = newMentee["account"]["id"]
    assert len(api_get_users()) == 3  # supperadmin + admin +  1 user
    response = httpx.delete(f"{API_URL}/accounts/{mentee_id}", headers=header)
    assert response.status_code == 204
    assert len(api_get_users()) == 2  # admin


def test_delete_an_user_mentor_as_an_admin():
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentor = get_random_mentor()
    newMentor = api_create_mentor(mentor)
    mentor_id = newMentor["account"]["id"]
    assert len(api_get_users()) == 3  # supperadmin + admin +  1 user
    response = httpx.delete(f"{API_URL}/accounts/{mentor_id}", headers=header)
    assert response.status_code == 204
    assert len(api_get_users()) == 2  # admin


def test_fetch_mentor_id_as_admin():
    """fetch mentor by id as valid admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentors_response = debug_get(f"{API_URL}/mentors", headers=header)
    response_json = mentors_response.json()
    mentor_id = response_json["resources"][0]["id"]
    mentor_response = httpx.get(f"{API_URL}/mentors/{mentor_id}", headers=header)
    mentor_json = mentor_response.json()
    expectedAttributes = set(
        [
            "account_id",
            "active",
            "birth_year",
            "communication_channels",
            "created",
            "display_name",
            "gender",
            "id",
            "languages",
            "region",
            "skills",
            "story",
            "updated",
            "user_id",
        ]
    )
    assert mentor_response.status_code == 200
    assert mentor_json["id"] == mentor_id
    assert all(attr in mentor_json for attr in expectedAttributes)


def test_fetch_an_existing_user_except_for_private_as_an_admin():
    """fetch user by id as valid admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    users_response = httpx.get(f"{API_URL}/users", headers=header)
    response_json = users_response.json()
    mentee_id = response_json["resources"][2]["id"]
    user_response = debug_get(f"{API_URL}/users/{mentee_id}", headers=header)
    user_json = user_response.json()
    expectedAttributes = set(["account_id", "display_name", "id", "role", "updated"])
    assert user_response.status_code == 200
    assert user_json["id"] == mentee_id
    assert "display_name" in user_json
    assert all(attr in user_json for attr in expectedAttributes)


def test_fetch_all_accounts_as_an_admin():
    """fetch accounts (admin, mentor, mentee) as valid admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    accounts_response = debug_get(f"{API_URL}/accounts", headers=header)
    response_json = accounts_response.json()
    expectedAttributesForAdmin = set(
        ["active", "created", "id", "login_name", "role", "updated"]
    )
    expectedAttributesForMenteeAndMentor = set(
        ["active", "created", "email", "id", "login_name", "role", "updated"]
    )
    assert accounts_response.status_code == 200
    assert len(response_json["resources"]) == 4
    assert response_json["resources"][1]["login_name"] == new_admin["login_name"]
    assert response_json["resources"][2]["login_name"] == new_mentee["login_name"]
    assert response_json["resources"][3]["login_name"] == new_mentor["login_name"]
    assert all(
        attr in response_json["resources"][1] for attr in expectedAttributesForAdmin
    )
    assert all(
        attr in response_json["resources"][2]
        for attr in expectedAttributesForMenteeAndMentor
    )
    assert all(
        attr in response_json["resources"][3]
        for attr in expectedAttributesForMenteeAndMentor
    )


def test_fetch_all_mentors_as_admin():
    """fetch mentors as valid admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    new_mentor = get_random_mentor()
    new_mentor2 = get_random_mentor()
    api_create_mentor(new_mentor)
    api_create_mentor(new_mentor2)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    my_user_response = debug_get(f"{API_URL}/mentors", headers=header)
    response_json = my_user_response.json()
    expectedAttributes = set(
        [
            "account_id",
            "active",
            "birth_year",
            "communication_channels",
            "created",
            "display_name",
            "gender",
            "id",
            "languages",
            "region",
            "skills",
            "story",
            "updated",
            "user_id",
        ]
    )
    assert my_user_response.status_code == 200
    assert len(response_json["resources"]) == 2
    assert response_json["resources"][0]["birth_year"] == new_mentor["birth_year"]
    assert response_json["resources"][0]["story"] == new_mentor["story"]
    assert all(attr in response_json["resources"][0] for attr in expectedAttributes)


def test_fetch_all_users_as_an_admin():
    """fetch all users (admin, mentor, mentee) as valid admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    users_response = debug_get(f"{API_URL}/users", headers=header)
    response_json = users_response.json()
    expectedAttributesForAdmin = set(
        ["account_id", "active", "display_name", "id", "role", "updated"]
    )
    expectedAttributesForMentee = set(
        ["account_id", "display_name", "id", "role", "updated"]
    )
    expectedAttributesForMentor = set(
        [
            "account_id",
            "active",
            "created",
            "display_name",
            "id",
            "role",
            "updated",
        ]
    )
    assert users_response.status_code == 200
    assert len(response_json["resources"]) == 4
    assert response_json["resources"][1]["display_name"] == new_admin["display_name"]
    assert response_json["resources"][2]["display_name"] == new_mentee["display_name"]
    assert all(
        attr in response_json["resources"][1] for attr in expectedAttributesForAdmin
    )
    assert all(
        attr in response_json["resources"][2] for attr in expectedAttributesForMentee
    )
    assert all(
        attr in response_json["resources"][3] for attr in expectedAttributesForMentor
    )


def test_get_my_user_as_admin():
    """fetch my user as valid admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    my_user_response = debug_get(f"{API_URL}/myuser", headers=header)
    assert my_user_response.status_code == 200
    my_user_json = my_user_response.json()
    expectedAttr = set(["account", "user"])
    expectedInAccount = set(
        [
            "active",
            "created",
            "email",
            "id",
            "login_name",
            "phone",
            "role",
            "updated",
        ]
    )
    expectedInUser = set(
        ["account_id", "active", "display_name", "id", "role", "updated"]
    )
    assert my_user_json["account"]["active"]
    assert my_user_json["account"]["role"] == "admin"
    assert my_user_json["user"]["role"] == "admin"
    assert my_user_json["account"]["id"] == login_response["scopes"]["account_id"]
    assert my_user_json["user"]["id"] == login_response["scopes"]["user_id"]
    assert all(attr in my_user_json for attr in expectedAttr)
    assert all(attr in my_user_json["user"] for attr in expectedInUser)
    assert all(attr in my_user_json["account"] for attr in expectedInAccount)


def test_fetch_all_my_messages_as_an_admin():
    """fetch all my messages as an admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    msg = random_string(10)
    res = debug_get(f"{API_URL}/myuser", headers=header)
    myuser = res.json()
    admin_user_id = myuser["user"]["id"]
    mentee_user_id = mentee_myuser["user"]["id"]
    res = api_post_message(admin_user_id, mentee_user_id, header, msg)
    assert res.status_code == 201
    res = debug_get(f"{API_URL}/users/{admin_user_id}/messages", headers=(header))
    expectedInMessage = set(
        [
            "active",
            "content",
            "created",
            "id",
            "opened",
            "recipient_id",
            "sender_id",
            "updated",
        ]
    )
    messages = res.json()["resources"]
    assert len(messages) == 1
    assert all(attr in messages[0] for attr in expectedInMessage)
    assert messages[0]["content"] == msg
    assert not messages[0]["opened"]
    assert messages[0]["active"]
    assert messages[0]["sender_id"] == admin_user_id
    assert messages[0]["recipient_id"] == mentee_user_id
