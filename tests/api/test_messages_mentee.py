# pylint: disable=too-many-locals,too-many-statements

import httpx
from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_mentee,
    get_random_mentee,
    get_random_mentor,
    debug_post,
    debug_get,
    debug_put,
    api_post_message,
    api_create_mentor,
    random_string,
    api_auth_header,
    api_access_token,
    api_create_and_login_mentee,
    api_create_and_login_mentor,
)


def test_send_a_message_to_a_mentor_or_admin_as_a_mentee():
    api_delete_accounts()

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    recipient_mentor = get_random_mentor()
    recipient_mentor_myuser = api_create_mentor(recipient_mentor)
    recipient_mentor_headers = api_auth_header(
        api_access_token(recipient_mentor["login_name"], recipient_mentor["password"])
    )

    msg = random_string(10)

    send_msg_res = debug_post(
        API_URL + "/users/" + mentee_myuser["user"]["id"] + "/messages",
        headers=mentee_headers,
        json={
            "sender_id": mentee_myuser["user"]["id"],
            "recipient_id": recipient_mentor_myuser["user"]["id"],
            "content": msg,
            "opened": False,
        },
    )
    assert send_msg_res.status_code == 201

    res = httpx.get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/messages",
        headers=recipient_mentor_headers,
    )

    messages = res.json()["resources"]
    assert len(messages) == 1

    first_msg = messages[0]
    assert len(first_msg.pop("created")) == 32
    assert len(first_msg.pop("updated")) == 32
    assert len(first_msg.pop("id")) == 43

    assert first_msg == {
        "active": True,
        "content": msg,
        "opened": False,
        "recipient_id": recipient_mentor_myuser["user"]["id"],
        "sender_id": mentee_myuser["user"]["id"],
    }


def test_receive_a_message_from_mentor_as_a_reply_or_from_admin_as_a_mentee():
    api_delete_accounts()

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    msg = random_string(10)
    reply_msg = random_string(10)

    message_res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        msg,
    )
    assert message_res.status_code == 201
    reply_res = api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_headers,
        reply_msg,
    )
    assert reply_res.status_code == 201

    res = debug_get(
        API_URL + "/users/" + mentee_myuser["user"]["id"] + "/messages",
        headers=mentee_headers,
    )
    messages = res.json()["resources"]
    assert len(messages) == 2

    assert messages[0]["content"] == msg
    assert messages[0]["sender_id"] == mentee_myuser["user"]["id"]
    assert messages[0]["recipient_id"] == mentor_myuser["user"]["id"]

    assert messages[1]["content"] == reply_msg
    assert messages[1]["sender_id"] == mentor_myuser["user"]["id"]
    assert messages[1]["recipient_id"] == mentee_myuser["user"]["id"]


def test_mark_one_or_more_received_message_as_opened_as_a_mentee():
    api_delete_accounts()

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    msg = random_string(10)

    res = api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_headers,
        msg,
    )

    res = httpx.get(
        API_URL + "/users/" + mentee_myuser["user"]["id"] + "/messages",
        headers=mentee_headers,
    )
    messages = res.json()["resources"]
    assert not messages[0]["opened"]

    res = debug_put(
        API_URL
        + "/users/"
        + mentee_myuser["user"]["id"]
        + "/messages/"
        + messages[0]["id"],
        headers=mentee_headers,
        json={"opened": True},
    )
    assert res.status_code == 200

    res = httpx.get(
        API_URL + "/users/" + mentee_myuser["user"]["id"] + "/messages",
        headers=mentee_headers,
    )
    messages = res.json()["resources"]
    assert messages[0]["opened"]


def test_filtering_for_a_mentee():
    api_delete_accounts()

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        recipient_mentor_myuser1,
        _,
    ) = api_create_and_login_mentor(get_random_mentor())
    (
        _,
        recipient_mentor_myuser2,
        _,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg1_to_mentor1 = random_string(10)
    msg_res1 = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser1["user"]["id"],
        sender_mentee_headers,
        msg1_to_mentor1,
    )
    assert msg_res1.status_code == 201
    created1_to_mentor1 = msg_res1.json()

    msg2_to_mentor1 = random_string(10)
    msg_res2 = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser1["user"]["id"],
        sender_mentee_headers,
        msg2_to_mentor1,
    )
    assert msg_res2.status_code == 201
    created2_to_mentor1 = msg_res2.json()

    msg1_to_mentor2 = random_string(10)
    msg_res3 = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser2["user"]["id"],
        sender_mentee_headers,
        msg1_to_mentor2,
    )
    assert msg_res3.status_code == 201
    created1_to_mentor2 = msg_res3.json()

    # # Polling

    # get all msgs
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 3
    assert len(contacts) == 2
    assert messages[0]["content"] == msg1_to_mentor1
    assert messages[1]["content"] == msg2_to_mentor1
    assert messages[2]["content"] == msg1_to_mentor2

    # get msg newer than first
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        params={"from_message_id": created1_to_mentor1["id"], "desc": False},
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 2
    assert len(contacts) == 2
    assert messages[0]["content"] == msg2_to_mentor1
    assert messages[1]["content"] == msg1_to_mentor2

    # get msg newer than second but still return all contacts
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        params={"from_message_id": created2_to_mentor1["id"], "desc": False},
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 1
    assert len(contacts) == 2
    assert messages[0]["content"] == msg1_to_mentor2

    # For contact view in app
    # get x amount of latest messages from/to contacts listed in query params.
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        params={
            "contact_user_ids": ",".join(
                [
                    recipient_mentor_myuser1["user"]["id"],
                    recipient_mentor_myuser2["user"]["id"],
                ]
            ),
            "max": 1,
            "desc": True,
        },
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 2
    assert len(contacts) == 2
    assert messages[0]["content"] == msg2_to_mentor1
    assert messages[1]["content"] == msg1_to_mentor2

    # For contact view in app
    # get x amount of oldest messages from/to contacts listed in query params.
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        params={
            "contact_user_ids": ",".join(
                [
                    recipient_mentor_myuser1["user"]["id"],
                    recipient_mentor_myuser2["user"]["id"],
                ]
            ),
            "max": 1,
            "desc": False,
        },
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 2
    assert len(contacts) == 2
    assert messages[0]["content"] == msg1_to_mentor1
    assert messages[1]["content"] == msg1_to_mentor2

    # For contact view in app
    # get x amount of latest messages from/to 1 contact.
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        params={
            "contact_user_ids": ",".join(
                [
                    recipient_mentor_myuser1["user"]["id"],
                ]
            ),
            "max": 1,
            "desc": True,
        },
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 1
    assert len(contacts) == 2
    assert messages[0]["content"] == msg2_to_mentor1

    # For contact view in app
    # get x amount of oldest messages from/to 1 contact.
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        params={
            "contact_user_ids": ",".join(
                [
                    recipient_mentor_myuser1["user"]["id"],
                ]
            ),
            "max": 1,
            "desc": False,
        },
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 1
    assert len(contacts) == 2
    assert messages[0]["content"] == msg1_to_mentor1

    # For chat view infinityscroll in app
    # get older messages than the given msg for one contact.
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        params={
            "contact_user_ids": f"{recipient_mentor_myuser1['user']['id']}",
            "from_message_id": created2_to_mentor1["id"],
            "max": 1,
            "desc": True,
        },
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 1
    assert len(contacts) == 2
    assert messages[0]["content"] == msg1_to_mentor1

    # get older messages than the given msg for all contact.
    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        params={
            "from_message_id": created1_to_mentor2["id"],
            "max": 2,
            "desc": True,
        },
    )
    messages = res.json()["resources"]
    contacts = res.json()["contacts"]

    assert len(messages) == 2
    assert len(contacts) == 2
    assert messages[0]["content"] == msg1_to_mentor1
    assert messages[1]["content"] == msg2_to_mentor1

    # bad params
    bad_params = [
        {"max": "no, no, there's no limit"},  # NaN
        {"max": "0"},  # should be >= 1
        {"desc": "Nope"},  # true/false
        {"from_message_id": created1_to_mentor1["id"]},  # from without desc
        {"from_message_id": "fake_id", "desc": "true"},  # msg not found
    ]
    for bad in bad_params:
        assert (
            debug_get(
                API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
                headers=sender_mentee_headers,
                params=bad,
            ).status_code
            == 400
        )
