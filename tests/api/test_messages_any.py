from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_mentee,
    api_auth_header,
    api_access_token,
    get_random_mentee,
    get_random_mentor,
    api_post_message,
    debug_get,
    debug_post,
    debug_delete,
    api_create_mentor,
    random_string,
)


def test_messages_should_be_deleted_with_their_sender_or_recipient_as_any_user():  # noqa pylint: disable=line-too-long
    api_delete_accounts()

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    msg = random_string(10)

    send_msg_res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    fetch_msg_res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_headers,
    )
    assert fetch_msg_res.status_code == 200

    messages = fetch_msg_res.json()["resources"]
    assert len(messages) == 1

    res = debug_delete(
        API_URL + "/accounts/" + mentee_myuser["account"]["id"],
        headers=mentee_headers,
    )
    assert res.status_code == 204

    fetch_msg_after_del_res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_headers,
    )
    messages = fetch_msg_after_del_res.json()["resources"]
    assert len(messages) == 0


def test_not_be_able_to_fetch_any_other_than_own_messages_as_any_user():
    api_delete_accounts()

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    mentee = get_random_mentee()
    _ = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    own_res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_headers,
    )
    assert own_res.status_code == 200

    not_own_res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentee_headers,
    )
    assert not_own_res.status_code == 401
    assert not_own_res.json() == {
        "error": "Unauthorized",
        "message": "User not authenticated, provide correct credentials!",
    }


def test_send_a_message_with_same_sender_and_recipient():
    api_delete_accounts()

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    msg = random_string(10)

    send_msg_res = debug_post(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_headers,
        json={
            "sender_id": mentor_myuser["user"]["id"],
            "recipient_id": mentor_myuser["user"]["id"],
            "content": msg,
            "opened": False,
        },
    )
    res_json = send_msg_res.json()
    assert send_msg_res.status_code == 400
    assert res_json == {"message": "Sender cannot be recipient"}
