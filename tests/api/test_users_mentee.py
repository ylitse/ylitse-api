from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_mentee,
    api_create_mentor,
    api_create_admin,
    api_my_user,
    get_random_mentee,
    api_access_token,
    api_auth_header,
    get_random_mentor,
    get_random_admin,
    api_login,
    random_string,
    api_post_message,
    debug_get,
    debug_put,
    debug_post,
    debug_delete,
)


def test_fetch_my_own_account_as_a_mentee():
    """fetch my own account as a mentee"""
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    login_response = api_login(new_mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    res = debug_get(f"{API_URL}/myuser", headers=header)
    myuser = res.json()
    expectedAccountAttributes = set(
        ["active", "created", "email", "id", "login_name", "role", "updated"]
    )
    expectedUserAttributes = set(
        [
            "account_id",
            "id",
            "active",
            "created",
            "display_name",
            "role",
            "id",
            "updated",
        ]
    )
    assert res.status_code == 200
    assert all(attr in myuser["user"] for attr in expectedUserAttributes)
    assert all(attr in myuser["account"] for attr in expectedAccountAttributes)


def test_fetch_my_own_user_as_a_mentee():
    api_delete_accounts()
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)

    res = debug_get(
        API_URL + "/users/" + mentee_myuser["user"]["id"],
        headers=(
            api_auth_header(api_access_token(mentee["login_name"], mentee["password"]))
        ),
    )
    assert res.status_code == 200
    json = res.json()
    assert "updated" in json
    assert json["account_id"] == mentee_myuser["account"]["id"]
    assert json["id"] == mentee_myuser["user"]["id"]
    assert json["display_name"] == mentee["display_name"]
    assert json["role"] == "mentee"


def test_delete_my_user_account_as_a_mentee():
    """delete my user, account as a mentee"""
    api_delete_accounts()
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    login_response = api_login(mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentee_account_id = mentee_myuser["account"]["id"]
    admin = get_random_admin()
    api_create_admin(admin)
    login_response = api_login(admin)
    access_token = login_response["tokens"]["access_token"]
    admin_header = api_auth_header(access_token)
    res = debug_delete(f"{API_URL}/accounts/{mentee_account_id}", headers=header)
    assert res.status_code == 204
    res = debug_get(f"{API_URL}/users", headers=admin_header)
    users = res.json()["resources"]
    assert len(users) == 2  # admin + superadmin are left


def test_fetch_all_my_messages_as_a_mentee():
    api_delete_accounts()
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    msg = random_string(10)
    mentee_auth_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_auth_headers,
        msg,
    )

    assert res.status_code == 201

    res = debug_get(
        API_URL + "/users/" + mentee_myuser["user"]["id"] + "/messages",
        headers=mentee_auth_headers,
    )
    assert res.status_code == 200
    messages = res.json()["resources"]
    assert len(messages) == 1

    first_msg = messages[0]
    assert len(first_msg.pop("created")) == 32
    assert len(first_msg.pop("updated")) == 32
    assert len(first_msg.pop("id")) == 43

    assert first_msg == {
        "active": True,
        "content": msg,
        "opened": False,
        "recipient_id": mentor_myuser["user"]["id"],
        "sender_id": mentee_myuser["user"]["id"],
    }


def test_fetch_a_user_i_am_chatting_with_as_a_mentee():
    api_delete_accounts()
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    msg = random_string(10)

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentee_auth_header = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_auth_header,
        msg,
    )

    res = debug_get(
        API_URL + "/users/" + mentee_myuser["user"]["id"] + "/messages",
        headers=mentee_auth_header,
    )
    messages = res.json()["resources"]

    res = debug_get(
        API_URL + "/users/" + messages[0]["recipient_id"],
        headers=mentee_auth_header,
    )
    assert res.status_code == 200
    json = res.json()

    assert len(json.pop("updated")) == 26
    assert len(json.pop("created")) == 26
    assert json == {
        "account_id": mentor_myuser["account"]["id"],
        "id": mentor_myuser["user"]["id"],
        "display_name": mentor["display_name"],
        "role": "mentor",
        "active": True,
    }


def test_fetch_all_mentors_as_a_mentee():
    api_delete_accounts()
    mentor1 = get_random_mentor()
    mentor2 = get_random_mentor()
    api_create_mentor(mentor1)
    api_create_mentor(mentor2)

    mentee = get_random_mentee()
    api_create_mentee(mentee)

    res = debug_get(
        API_URL + "/mentors",
        headers=(
            api_auth_header(api_access_token(mentee["login_name"], mentee["password"]))
        ),
    )
    mentors = res.json()["resources"]
    assert len(mentors) == 2
    assert [m for m in mentors if m["display_name"] == mentor1["display_name"]][0][
        "display_name"
    ] == mentor1["display_name"]
    assert [m for m in mentors if m["display_name"] == mentor2["display_name"]][0][
        "display_name"
    ] == mentor2["display_name"]


def test_update_my_own_user_and_all_sub_resources_as_a_mentee():
    """update my own user and all sub resources as a mentee"""
    api_delete_accounts()
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_user_id = mentee_myuser["user"]["id"]
    mentee_account_id = mentee_myuser["account"]["id"]
    login_response = api_login(mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    new_name = random_string(10)
    updatedUser = {
        "display_name": new_name,
        "role": mentee["role"],
        "account_id": mentee_myuser["account"]["id"],
        "id": mentee_myuser["user"]["id"],
        "active": True,
    }
    res = debug_put(
        f"{API_URL}/users/{mentee_user_id}", headers=header, json=updatedUser
    )
    assert res.status_code == 200
    new_email = random_string(10) + "_updated@mentee.mentee"
    updatedMentee = {
        "id": mentee_account_id,
        "email": new_email,
        "role": mentee["role"],
        "login_name": mentee["login_name"],
    }
    res = debug_put(
        f"{API_URL}/accounts/{mentee_account_id}",
        headers=header,
        json=updatedMentee,
    )
    assert res.status_code == 200
    updated = debug_get(f"{API_URL}/myuser", headers=header).json()
    assert updated["user"]["display_name"] == new_name
    assert updated["account"]["email"] == new_email


def test_wrong_password_mentee_my_user():
    """test that not be able to login with wrong password as mentee"""
    api_delete_accounts()
    mentee = get_random_mentee()
    api_create_mentee(mentee)
    mentee_with_wrong_password = {
        "login_name": mentee["login_name"],
        "password": "thisiswrong",
    }
    login_response = debug_post(API_URL + "/login", json=mentee_with_wrong_password)
    response_json = login_response.json()
    assert response_json == {"error": "Bad Request", "message": "Bad request."}
    assert login_response.status_code == 400


def test_non_existant_mentee_my_user():
    """test that not be able to login as non-existant mentee"""
    api_delete_accounts()
    non_valid_mentee = get_random_mentee()
    mentee_not_created = {
        "login_name": non_valid_mentee["login_name"],
        "password": non_valid_mentee["password"],
    }
    login_response = debug_post(API_URL + "/login", json=mentee_not_created)
    response_json = login_response.json()
    assert response_json == {"error": "Bad Request", "message": "Bad request."}
    assert login_response.status_code == 400


def test_mentee_my_user():
    """fetch my user as valid mentee"""
    api_delete_accounts()
    mentee = get_random_mentee()
    api_create_mentee(mentee)
    login_response = api_login(mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    my_user_response = debug_get(API_URL + "/myuser", headers=header)
    my_user_json = my_user_response.json()
    expectedAttr = set(["account", "user"])
    expectedInAccount = set(
        ["active", "created", "email", "id", "login_name", "role", "updated"]
    )
    expectedInUser = set(["account_id", "display_name", "id", "role", "updated"])
    assert my_user_response.status_code == 200
    assert my_user_json["account"]["active"]
    assert my_user_json["account"]["role"] == "mentee"
    assert my_user_json["user"]["role"] == "mentee"
    assert my_user_json["account"]["id"] == login_response["scopes"]["account_id"]
    assert my_user_json["user"]["id"] == login_response["scopes"]["user_id"]
    assert all(attr in my_user_json for attr in expectedAttr)
    assert all(attr in my_user_json["user"] for attr in expectedInUser)
    assert all(attr in my_user_json["account"] for attr in expectedInAccount)


def test_no_token_mentee_my_user():
    """fetch my user without token as mentee"""
    api_delete_accounts()
    mentee = get_random_mentee()
    api_create_mentee(mentee)
    header = {}
    my_user_response = debug_get(API_URL + "/myuser", headers=header)
    response_json = my_user_response.json()
    assert my_user_response.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_cannot_update_account_role_as_a_mentee():
    api_delete_accounts()

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)

    mentee_account_id = mentee_myuser["account"]["id"]

    login_response = api_login(mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)

    updated_account = {
        "role": "admin",
        "id": mentee_myuser["account"]["id"],
        "login_name": mentee_myuser["account"]["login_name"],
        "email": mentee_myuser["account"]["email"],
    }

    res = debug_put(
        f"{API_URL}/accounts/{mentee_account_id}",
        headers=header,
        json=updated_account,
    )
    assert res.status_code == 400
    assert res.json() == {"error": "Bad Request", "message": "Bad request."}


def test_update_email_as_a_mentee():
    api_delete_accounts()

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)

    mentee_account_id = mentee_myuser["account"]["id"]

    login_response = api_login(mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)

    assert api_my_user(access_token) == mentee_myuser

    updated_mentee = get_random_mentee()
    updated_account = {
        "role": mentee_myuser["account"]["role"],
        "id": mentee_myuser["account"]["id"],
        "login_name": mentee_myuser["account"]["login_name"],
        "email": updated_mentee["email"],
    }

    res = debug_put(
        f"{API_URL}/accounts/{mentee_account_id}",
        headers=header,
        json=updated_account,
    )
    assert res.status_code == 200

    json_res = res.json()
    assert json_res.pop("active")
    assert len(json_res.pop("created")) == 26
    assert len(json_res.pop("updated")) == 26
    assert json_res == updated_account

    updated_my_user = api_my_user(access_token)
    comparison = mentee_myuser
    comparison["account"]["updated"] = updated_my_user["account"]["updated"]
    comparison["account"]["email"] = updated_mentee["email"]
    assert len(updated_my_user["account"]["updated"]) == 26
    assert comparison == updated_my_user
