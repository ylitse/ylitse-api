from tests.api.helpers import API_URL
from tests.api.helpers import debug_get


def test_index_any_user():
    res = debug_get(API_URL + "/")
    assert res.status_code == 404
