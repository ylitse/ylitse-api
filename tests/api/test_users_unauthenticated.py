from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_mentor,
    api_create_mentee,
    get_random_mentee,
    api_auth_header,
    api_login,
    get_random_mentor,
    debug_get,
    debug_put,
    debug_post,
    invalid_header,
)


def test_fetch_all_mentors_as_a_unauthenticated_user():
    api_delete_accounts()
    mentor1 = get_random_mentor()
    mentor2 = get_random_mentor()
    api_create_mentor(mentor1)
    api_create_mentor(mentor2)

    res = debug_get(API_URL + "/mentors")
    mentors = res.json()["resources"]
    assert len(mentors) == 2
    assert [m for m in mentors if m["display_name"] == mentor1["display_name"]][0][
        "display_name"
    ] == mentor1["display_name"]
    assert [m for m in mentors if m["display_name"] == mentor2["display_name"]][0][
        "display_name"
    ] == mentor2["display_name"]


def test_create_a_mentee_account_as_an_unauthenticated_user():
    """create a mentee account as an unauthenticated user"""
    api_delete_accounts()
    mentee = get_random_mentee()
    mentee_account = {
        "password": mentee["password"],
        "account": {
            "role": mentee["role"],
            "login_name": mentee["login_name"],
            "email": mentee["email"],
        },
    }
    debug_post(f"{API_URL}/accounts", json=mentee_account)
    login_response = api_login(mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    res = debug_get(f"{API_URL}/myuser", headers=header)
    myuser = res.json()
    myuser_id = myuser["user"]["id"]
    mentee_user = {
        "display_name": mentee["display_name"],
        "role": mentee["role"],
        "account_id": myuser["account"]["id"],
        "id": myuser["user"]["id"],
    }
    res = debug_put(f"{API_URL}/users/{myuser_id}", headers=header, json=mentee_user)
    res = debug_get(API_URL + "/myuser", headers=header)
    myuser = res.json()
    assert myuser["account"]["email"] == mentee["email"]
    assert myuser["account"]["active"]
    assert myuser["user"]["display_name"] == mentee["display_name"]
    assert myuser["user"]["role"] == "mentee"


def test_fetch_mentor_id_no_header():
    """fetch mentor by id without header"""
    api_delete_accounts()
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    header = {}
    mentors_response = debug_get(API_URL + "/mentors", headers=header)
    response_json = mentors_response.json()
    mentor_id = response_json["resources"][0]["id"]
    mentor_response = debug_get(API_URL + f"/mentors/{mentor_id}", headers=header)
    mentor_json = mentor_response.json()
    assert mentor_response.status_code == 401
    assert mentor_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_fetch_mentor_id_invalid_header():
    """fetch mentor by id with invalid token"""
    api_delete_accounts()
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    header = invalid_header()
    mentors_response = debug_get(API_URL + "/mentors", headers=header)
    response_json = mentors_response.json()
    mentor_id = response_json["resources"][0]["id"]
    mentor_response = debug_get(API_URL + f"/mentors/{mentor_id}", headers=header)
    mentor_json = mentor_response.json()
    assert mentor_response.status_code == 401
    assert mentor_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_fetch_user_id_no_header():
    """fetch user by id without header"""
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    header = {}
    users_response = debug_get(API_URL + "/users", headers=header)
    response_json = users_response.json()
    assert users_response.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_fetch_user_id_invalid_header():
    """fetch mentor by id with invalid token"""
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    header = invalid_header()
    users_response = debug_get(API_URL + "/users", headers=header)
    response_json = users_response.json()
    assert users_response.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_fetch_accounts_no_header_admin():
    """fetch accounts (admin, mentor, mentee) without header"""
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    header = {}
    accounts_response = debug_get(API_URL + "/accounts", headers=header)
    response_json = accounts_response.json()
    assert accounts_response.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_fetch_accounts_invalid_header_admin():
    """fetch accounts (admin, mentor, mentee) with invalid token"""
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    header = invalid_header()
    accounts_response = debug_get(API_URL + "/accounts", headers=header)
    response_json = accounts_response.json()
    assert accounts_response.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_fetch_mentors_no_header():
    """fetch mentors without header"""
    api_delete_accounts()
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    header = {}
    mentors_response = debug_get(API_URL + "/mentors", headers=header)
    response_json = mentors_response.json()
    assert mentors_response.status_code == 200
    assert len(response_json["resources"]) == 1
    assert response_json["resources"][0]["birth_year"] == new_mentor["birth_year"]
    assert response_json["resources"][0]["story"] == new_mentor["story"]


def test_fetch_mentors_invalid_header():
    """fetch mentors with invalid token"""
    api_delete_accounts()
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    header = invalid_header()
    mentors_response = debug_get(API_URL + "/mentors", headers=header)
    response_json = mentors_response.json()
    assert mentors_response.status_code == 200
    assert len(response_json["resources"]) == 1
    assert response_json["resources"][0]["birth_year"] == new_mentor["birth_year"]
    assert response_json["resources"][0]["story"] == new_mentor["story"]


def test_fetch_users_no_header():
    """fetch users (admin, mentor, mentee) without header"""
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    header = {}
    users_response = debug_get(API_URL + "/users", headers=header)
    response_json = users_response.json()
    assert users_response.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_fetch_users_invalid_header():
    """fetch users (admin, mentor, mentee) with invalid token"""
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    header = invalid_header()
    users_response = debug_get(API_URL + "/users", headers=header)
    response_json = users_response.json()
    assert users_response.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }
