# pylint: disable=line-too-long,too-many-statements

from datetime import datetime
from tests.test_helpers import read_json

from tests.api.helpers import (
    API_URL,
    api_admin_auth_headers,
    api_delete_accounts,
    api_delete_questions,
    api_create_question,
    api_create_and_login_mentee,
    debug_post,
    debug_get,
    get_random_mentee,
    is_date,
)


def test_answer_question():
    api_delete_accounts()
    api_delete_questions()

    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())
    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    assert questions_res.status_code == 200
    questions = questions_res.json()["resources"]
    assert len(questions) == 1

    question = questions[0]
    question_id = question.pop("id")
    assert len(question_id) == 43

    answer_id = question.pop("answer_id")
    assert len(answer_id) == 43

    # mentee answers
    answer_res = debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "value": 42,
        },
    )
    assert answer_res.status_code == 201


def test_answer_question_wrong():
    api_delete_accounts()
    api_delete_questions()

    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())
    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    assert questions_res.status_code == 200
    questions = questions_res.json()["resources"]
    assert len(questions) == 1

    question = questions[0]
    question_id = question.pop("id")
    assert len(question_id) == 43

    answer_id = question.pop("answer_id")
    assert len(answer_id) == 43

    # mentee answers with nonexisting id
    answer_res = debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": 42,
            "value": 42,
        },
    )
    assert answer_res.status_code == 404

    # mentee answers with wrong object
    answer_res_2 = debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "foo": 42,
        },
    )
    assert answer_res_2.status_code == 422


# test get all answers
def test_get_answers():
    api_delete_accounts()
    api_delete_questions()

    admin_headers = api_admin_auth_headers()

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        _,
        mentee_2_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # mentee 1 gets question
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    questions = questions_res.json()["resources"]
    question = questions[0]
    answer_id = question.pop("answer_id")

    # mentee 1 answers
    debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "value": 42,
        },
    )

    # mentee 2 gets question
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_2_headers,
    )
    questions = questions_res.json()["resources"]
    question = questions[0]
    answer_id = question.pop("answer_id")

    # mentee 2 answers
    debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_2_headers,
        json={
            "answer_id": answer_id,
            "value": 13,
        },
    )

    answers_result = debug_get(f"{API_URL}/feedback/answers", headers=admin_headers)

    assert answers_result.status_code == 200
    rows = answers_result.text.split("\n")
    # header + 2 rows
    assert len(rows) == 3
    # columns
    columns = rows[0].strip().split(",")
    expected_columns = [
        "ID",
        "QUESTION_ID",
        "USER_ID",
        "ASKED_AT",
        "RETRIES",
        "MAX_RETRIES",
        "VALUE",
        "ACTIVE",
        "UPDATED",
        "CREATED",
    ]
    assert columns == expected_columns
    # first row values
    values = rows[1].strip().split(",")
    assert len(values) == len(expected_columns)
    assert is_date(values[3])
    assert values[4] == "0"
    assert values[5] == "3"
    assert values[6] == "42"
    assert values[7] == "True"
    assert is_date(values[8])
    assert is_date(values[9])


# test get all answers, use params to filter
# testing to filter nonexistant answers
# i.e. no results
def test_get_filtered_no_results():
    api_delete_accounts()
    api_delete_questions()

    admin_headers = api_admin_auth_headers()

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        _,
        mentee_2_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # mentee 1 gets question
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    questions = questions_res.json()["resources"]
    question = questions[0]
    answer_id = question.pop("answer_id")

    # mentee 1 answers
    debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "value": 42,
        },
    )

    # mentee 2 gets question
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_2_headers,
    )
    questions = questions_res.json()["resources"]
    question = questions[0]
    answer_id = question.pop("answer_id")

    # mentee 2 answers
    debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_2_headers,
        json={
            "answer_id": answer_id,
            "value": 13,
        },
    )

    answers_result = debug_get(
        f"{API_URL}/feedback/answers?start=1970-01-01&end=2019-01-01",
        headers=admin_headers,
    )

    assert answers_result.status_code == 200
    rows = answers_result.text.split("\n")

    # just the header
    assert len(rows) == 1

    # first row values
    values = rows[0].split(",")
    assert values[6].lower() == "value"


# test get all answers, use params to filter
# test to filter todays answers
# should get both answers from today if search params are
# start=today & end=today
def test_get_filtered_results():
    api_delete_accounts()
    api_delete_questions()

    admin_headers = api_admin_auth_headers()

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        _,
        mentee_2_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # mentee 1 gets question
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_headers,
    )
    questions = questions_res.json()["resources"]
    question = questions[0]
    answer_id = question.pop("answer_id")

    # mentee 1 answers
    debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "value": 42,
        },
    )

    # mentee 2 gets question
    questions_res = debug_get(
        API_URL + "/feedback/needs_answers",
        headers=mentee_2_headers,
    )
    questions = questions_res.json()["resources"]
    question = questions[0]
    answer_id = question.pop("answer_id")

    # mentee 2 answers
    debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_2_headers,
        json={
            "answer_id": answer_id,
            "value": 13,
        },
    )

    today = datetime.strftime(datetime.utcnow().date(), "%Y-%m-%d")

    answers_result = debug_get(
        f"{API_URL}/feedback/answers?start={today}&end={today}",
        headers=admin_headers,
    )

    assert answers_result.status_code == 200
    rows = answers_result.text.split("\n")

    # three rows, two answers today
    assert len(rows) == 3

    # first row values
    values = rows[1].split(",")
    assert values[6] == "42"


# test invalid date-range-query
def test_get_invalid_date_query():
    api_delete_accounts()
    api_delete_questions()
    admin_headers = api_admin_auth_headers()

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # mentee gets question
    questions_res = debug_get(
        f"{API_URL}/feedback/needs_answers", headers=mentee_headers
    )

    questions = questions_res.json()["resources"]
    question = questions[0]
    answer_id = question.pop("answer_id")

    # mentee answers
    debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "value": 42,
        },
    )

    today = datetime.strftime(datetime.utcnow().date(), "%Y-%m-%d")

    answers_result = debug_get(
        f"{API_URL}/feedback/answers?start=lol&end={today}", headers=admin_headers
    )

    assert answers_result.status_code == 400


# test unauthorized
def test_get_unauthorized():
    api_delete_accounts()
    api_delete_questions()

    # create question
    q = read_json("tests/fixtures/feedback_question_predefined_repeat.json")
    api_create_question(q, api_admin_auth_headers())

    (
        _,
        _,
        mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    # mentee gets question
    questions_res = debug_get(
        f"{API_URL}/feedback/needs_answers", headers=mentee_headers
    )

    questions = questions_res.json()["resources"]
    question = questions[0]
    answer_id = question.pop("answer_id")

    # mentee answers
    debug_post(
        API_URL + "/feedback/answer",
        headers=mentee_headers,
        json={
            "answer_id": answer_id,
            "value": 42,
        },
    )

    answers_result = debug_get(f"{API_URL}/feedback/answers", headers=mentee_headers)

    assert answers_result.status_code == 401
    assert answers_result.json() == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }
