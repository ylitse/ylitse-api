# pylint: disable=too-many-locals,too-many-statements,too-many-lines

from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_admin,
    get_random_admin,
    api_login,
    get_random_mentee,
    api_auth_header,
    random_string,
    debug_post,
    debug_put,
    debug_get,
    api_create_mentee,
    api_post_message,
)


def test_send_a_message_to_any_other_user_as_an_admin():
    """send a message as an admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    msg = random_string(10)
    res = debug_get(f"{API_URL}/myuser", headers=header)
    myuser = res.json()
    admin_user_id = myuser["user"]["id"]
    mentee_user_id = mentee_myuser["user"]["id"]
    postObject = {
        "sender_id": admin_user_id,
        "recipient_id": mentee_user_id,
        "content": msg,
        "opened": False,
    }
    postResponse = debug_post(
        f"{API_URL}/users/{admin_user_id}/messages",
        headers=header,
        json=postObject,
    )
    getResponse = debug_get(f"{API_URL}/users/{admin_user_id}/messages", headers=header)
    messages = getResponse.json()["resources"]
    expectedInMessage = set(
        [
            "active",
            "content",
            "created",
            "id",
            "opened",
            "recipient_id",
            "sender_id",
            "updated",
        ]
    )
    assert postResponse.status_code == 201
    assert len(messages) == 1
    assert all(attr in messages[0] for attr in expectedInMessage)
    assert messages[0]["active"]
    assert messages[0]["content"] == msg
    assert not messages[0]["opened"]
    assert messages[0]["sender_id"] == admin_user_id
    assert messages[0]["recipient_id"] == mentee_user_id


def test_receive_a_message_from_any_other_user_as_an_admin():
    """receive a message from any other user as an admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    admin_login_response = api_login(new_admin)
    admin_access_token = admin_login_response["tokens"]["access_token"]
    admin_header = api_auth_header(admin_access_token)
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_login_response = api_login(mentee)
    mentee_access_token = mentee_login_response["tokens"]["access_token"]
    mentee_header = api_auth_header(mentee_access_token)
    mentee_user_id = mentee_myuser["user"]["id"]
    msg = random_string(10)
    res = debug_get(API_URL + "/myuser", headers=admin_header)
    myuser = res.json()
    admin_user_id = myuser["user"]["id"]
    res = debug_get(f"{API_URL}/users/{admin_user_id}/messages", headers=admin_header)
    messagesBefore = res.json()["resources"]
    res = api_post_message(mentee_user_id, admin_user_id, mentee_header, msg)
    assert res.status_code == 201
    res = debug_get(f"{API_URL}/users/{admin_user_id}/messages", headers=admin_header)
    messages = res.json()["resources"]
    assert len(messagesBefore) == 0
    assert len(messages) == 1
    assert messages[0]["sender_id"] == mentee_user_id


def test_mark_one_or_more_received_message_as_opened_as_an_admin():
    """mark one or more message received as opened as an admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    admin_login_response = api_login(new_admin)
    admin_access_token = admin_login_response["tokens"]["access_token"]
    admin_header = api_auth_header(admin_access_token)
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_login_response = api_login(mentee)
    mentee_access_token = mentee_login_response["tokens"]["access_token"]
    mentee_header = api_auth_header(mentee_access_token)
    mentee_user_id = mentee_myuser["user"]["id"]
    msg = random_string(10)
    res = debug_get(f"{API_URL}/myuser", headers=admin_header)
    myuser = res.json()
    admin_user_id = myuser["user"]["id"]
    res = api_post_message(mentee_user_id, admin_user_id, mentee_header, msg)
    assert res.status_code == 201
    res = debug_get(f"{API_URL}/users/{admin_user_id}/messages", headers=admin_header)
    messages = res.json()["resources"]
    received_message = messages[0]
    received_message_id = received_message["id"]
    assert not received_message["opened"]
    opened_object = {"opened": True}
    res = debug_put(
        f"{API_URL}/users/{admin_user_id}/messages/{received_message_id}",
        headers=admin_header,
        json=opened_object,
    )
    assert res.status_code == 200
    res = debug_get(f"{API_URL}/users/{admin_user_id}/messages", headers=admin_header)
    messages = res.json()["resources"]
    opened_message = messages[0]
    assert opened_message["opened"]
    assert received_message["content"] == opened_message["content"]
    assert received_message["created"] == opened_message["created"]
