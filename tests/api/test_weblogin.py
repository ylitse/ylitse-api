import pyotp

from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_mentor,
    api_create_mentee,
    api_create_admin,
    get_random_mentee,
    get_random_admin,
    get_random_mentor,
    debug_post,
)


def test_failed_weblogin_as_a_unauthenticated_user():
    api_delete_accounts()

    res = debug_post(
        API_URL + "/weblogin",
        data={"username": "doesnt_exist", "password": "wrong"},
    )
    json = res.json()

    assert res.status_code == 400
    assert json == {"error": "Bad Request", "message": "Bad request."}


def test_weblogin_as_a_mentee():
    api_delete_accounts()
    mentee = get_random_mentee()
    api_create_mentee(mentee)

    res = debug_post(
        API_URL + "/weblogin",
        data={
            "username": mentee["login_name"],
            "password": mentee["password"],
        },
    )
    json = res.json()

    assert res.status_code == 200
    assert "HttpOnly; Path=/" in res.headers["set-cookie"]
    assert 'YLITSE_ACCESS="' in res.headers["set-cookie"]
    assert 'YLITSE_REFRESH="' in res.headers["set-cookie"]
    assert isinstance(res.cookies["YLITSE_ACCESS"], str)
    assert isinstance(res.cookies["YLITSE_REFRESH"], str)
    assert json == {"message": "You were logged in."}


def test_weblogin_as_a_mentor():
    api_delete_accounts()
    mentor = get_random_mentor()
    api_create_mentor(mentor)

    res = debug_post(
        API_URL + "/weblogin",
        data={
            "username": mentor["login_name"],
            "password": mentor["password"],
        },
    )
    json = res.json()

    assert res.status_code == 200
    assert "HttpOnly; Path=/" in res.headers["set-cookie"]
    assert 'YLITSE_ACCESS="' in res.headers["set-cookie"]
    assert 'YLITSE_REFRESH="' in res.headers["set-cookie"]
    assert isinstance(res.cookies["YLITSE_ACCESS"], str)
    assert isinstance(res.cookies["YLITSE_REFRESH"], str)
    assert json == {"message": "You were logged in."}


def test_weblogin_as_an_admin():
    api_delete_accounts()
    admin = get_random_admin()
    api_create_admin(admin)

    res = debug_post(
        API_URL + "/weblogin",
        data={
            "username": admin["login_name"],
            "password": admin["password"],
            "mfa_token": pyotp.TOTP(admin["mfa_secret"]).now(),
        },
    )
    json = res.json()

    assert res.status_code == 200
    assert "HttpOnly; Path=/" in res.headers["set-cookie"]
    assert 'YLITSE_ACCESS="' in res.headers["set-cookie"]
    assert 'YLITSE_REFRESH="' in res.headers["set-cookie"]
    assert isinstance(res.cookies["YLITSE_ACCESS"], str)
    assert isinstance(res.cookies["YLITSE_REFRESH"], str)
    assert json == {"message": "You were logged in."}


def test_weblogin_fails_as_an_admin_without_2fa():
    api_delete_accounts()
    admin = get_random_admin()
    api_create_admin(admin)

    res = debug_post(
        API_URL + "/weblogin",
        data={
            "username": admin["login_name"],
            "password": admin["password"],
        },
    )
    json = res.json()

    assert res.status_code == 400
    assert json == {"error": "Bad Request", "message": "Bad request."}


def test_weblogin_fails_as_an_admin_with_wrong_2fa():
    api_delete_accounts()
    admin = get_random_admin()
    api_create_admin(admin)

    res = debug_post(
        API_URL + "/weblogin",
        data={
            "username": admin["login_name"],
            "password": admin["password"],
            "2fa": "000000",  # 1 / 999999 chance of getting this right
        },
    )
    json = res.json()

    assert res.status_code == 400
    assert json == {"error": "Bad Request", "message": "Bad request."}
