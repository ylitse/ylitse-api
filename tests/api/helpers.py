# pylint: disable=too-many-locals, no-name-in-module
"""Helper functions for tests"""

from datetime import datetime
from json.decoder import JSONDecodeError

import typing
import os
import sys
import random
import string
import yaml
import httpx
import pyotp

from pprintpp import pprint, pformat
from httpx._config import DEFAULT_TIMEOUT_CONFIG
from httpx._types import (
    AuthTypes,
    CertTypes,
    CookieTypes,
    HeaderTypes,
    ProxiesTypes,
    QueryParamTypes,
    RequestContent,
    RequestData,
    RequestFiles,
    TimeoutTypes,
    URLTypes,
    VerifyTypes,
)

from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import Terminal256Formatter
from pygments.style import Style
from pygments.token import String, Number, Keyword

CONFIG = {}
with open(os.getenv("YLITSE_CONF"), encoding="utf-8") as f:
    try:
        CONFIG = yaml.safe_load(f)
    except FileNotFoundError:
        pass

API_URL = os.getenv("YLITSE_API_URL", "http://localhost:8080")
API_USER = os.getenv("YLITSE_API_USER", CONFIG.get("admin_username"))
API_PASS = os.getenv("YLITSE_API_PASS", CONFIG.get("admin_password"))
API_2FA = os.getenv("YLITSE_API_2FA", CONFIG.get("admin_2fa"))


def api_admin_auth_headers():
    admin_access_token = api_access_token(API_USER, API_PASS)
    return api_auth_header(admin_access_token)


def api_access_token(login_name, password):
    res = httpx.post(
        API_URL + "/login",
        json={
            "login_name": login_name,
            "password": password,
            "mfa_token": pyotp.TOTP(API_2FA).now(),
        },
    )
    return res.json()["tokens"]["access_token"]


def api_auth_header(access_token):
    return {"Authorization": "Bearer " + access_token}


def api_auth_header_from_username_password(login_name, password):
    access_token = api_access_token(login_name, password)
    return api_auth_header(access_token)


def validate_api_connection():
    try:
        api_admin_auth_headers()
    except KeyError:
        print(
            "Look like I cannot connect to API. "
            "Envvars maybe missing? "
            "eg: YLITSE_API_USER=admin "
            "YLITSE_API_PASS=... "
            "YLITSE_API_URL=http://localhost:8080 make test"
        )
        sys.exit(-1)


validate_api_connection()


class CodeStyle(Style):
    default_style = ""

    uablue = "#003d64"
    uared = "#7e002f"
    vividbrown = "#d79a46"
    green = "#007e11"

    styles = {
        String: uablue,
        Number: vividbrown,
        Keyword.Constant: uared,
    }


def pprint_color(obj):
    print(highlight(pformat(obj), PythonLexer(), Terminal256Formatter(style=CodeStyle)))


def random_string(num):
    return "".join(random.choices(string.ascii_uppercase + string.digits, k=num))


def api_get_users():
    admin_auth_header = api_admin_auth_headers()
    res = httpx.get(API_URL + "/users", headers=admin_auth_header)
    return res.json()["resources"]


def api_get_questions():
    admin_auth_header = api_admin_auth_headers()
    res = httpx.get(API_URL + "/feedback/questions", headers=admin_auth_header)
    return res.json()["resources"]


def api_answer_question(answer, headers):
    return httpx.post(
        API_URL + "/feedback/answer",
        headers=headers,
        json=answer,
    )


def api_get_questions_needs_answer(headers):
    res = httpx.get(API_URL + "/feedback/needs_answers", headers=headers)
    return res.json()["resources"]


def api_delete_accounts():
    admin_access_token = api_access_token(API_USER, API_PASS)
    admin_auth_header = api_auth_header(admin_access_token)

    my_user = api_my_user(admin_access_token)

    for user in api_get_users():
        # Delete all accounts except the admin user
        if user["account_id"] != my_user["account"]["id"]:
            httpx.delete(
                API_URL + "/accounts/" + user["account_id"],
                headers=admin_auth_header,
            )


def api_delete_questions():
    admin_access_token = api_access_token(API_USER, API_PASS)
    admin_auth_header = api_auth_header(admin_access_token)

    for question in api_get_questions():
        httpx.delete(
            API_URL + "/feedback/questions/" + question["id"],
            headers=admin_auth_header,
        )


def api_delete_skills():
    admin_access_token = api_access_token(API_USER, API_PASS)
    admin_auth_header = api_auth_header(admin_access_token)
    for skill in api_get_skills():
        httpx.delete(API_URL + "/skills/" + skill["id"], headers=admin_auth_header)


def api_get_skills():
    admin_auth_header = api_admin_auth_headers()
    res = httpx.get(API_URL + "/skills", headers=admin_auth_header)
    return res.json()["resources"]


def api_create_events(events, auth_headers):
    httpx.post(
        API_URL + "/events",
        headers=auth_headers,
        json=events,
    )


def api_create_mentee(mentee):
    httpx.post(
        API_URL + "/accounts",
        json={
            "password": mentee["password"],
            "account": {
                "role": mentee["role"],
                "login_name": mentee["login_name"],
                "email": mentee["email"],
            },
        },
    )
    access_token = api_access_token(mentee["login_name"], mentee["password"])
    auth_header = api_auth_header(access_token)
    res = httpx.get(API_URL + "/myuser", headers=auth_header)
    my_user = res.json()
    res = httpx.put(
        API_URL + "/users/" + my_user["user"]["id"],
        headers=auth_header,
        json={
            "display_name": mentee["display_name"],
            "role": mentee["role"],
            "account_id": my_user["account"]["id"],
            "id": my_user["user"]["id"],
            "active": True,
            "created": my_user["user"]["created"],
        },
    )
    res = httpx.get(API_URL + "/myuser", headers=auth_header)
    my_user = res.json()
    return my_user


def api_login(mentee):
    data = {
        "login_name": mentee["login_name"],
        "password": mentee["password"],
    }
    if mentee["role"] == "admin":
        data["mfa_token"] = pyotp.TOTP(API_2FA).now()
    return httpx.post(
        API_URL + "/login",
        json=data,
    ).json()


def api_my_user(access_token):
    auth_header = api_auth_header(access_token)
    res = httpx.get(API_URL + "/myuser", headers=auth_header)
    return res.json()


def get_random_mentee():
    return {
        "login_name": random_string(10),
        "password": random_string(10),
        "display_name": random_string(10),
        "role": "mentee",
        "email": random_string(10) + "@mentee.mentee",
    }


COMMUNICATION_CHANNELS = ["phone", "email", "chat"]
LANGUAGE = ["fi", "se", "en"]
GENDERS = ["female", "male", "other"]
REGIONS = ["HEL", "Tampesteri", "other"]


def get_random_mentor():
    return {
        "login_name": random_string(10),
        "display_name": random_string(10),
        "password": random_string(10),
        "email": random_string(10) + "@mentor.mentor",
        "birth_year": random.randint(1900, 2003),
        "phone": "555-" + str(random.randint(1000, 9999)),
        "story": random_string(50),
        "languages": [random.choice(LANGUAGE)],
        "skills": [random_string(5), random_string(7), random_string(8)],
        "communication_channels": [random.choice(COMMUNICATION_CHANNELS)],
        "gender": random.choice(GENDERS),
        "region": random.choice(REGIONS),
        "role": "mentor",
    }


def invalid_header():
    return {"Authorization": f"Bearer {random_string(20)}"}


def get_random_admin():
    return {
        "login_name": random_string(10),
        "password": random_string(10),
        "display_name": random_string(10),
        "role": "admin",
        "email": random_string(10) + "@admin.admin",
        "phone": "234-234234",
        "mfa_secret": API_2FA,
    }


def api_create_skill(name, auth_headers):
    return httpx.post(API_URL + "/skills", json={"name": name}, headers=auth_headers)


def api_create_question(question, auth_headers):
    return httpx.post(
        API_URL + "/feedback/questions", json=question, headers=auth_headers
    )


def api_create_admin(admin):
    admin_headers = api_admin_auth_headers()
    account_response = httpx.post(
        API_URL + "/accounts",
        headers=admin_headers,
        json={
            "account": {
                "login_name": admin["login_name"],
                "role": "admin",
                "email": admin["email"],
                "phone": admin["phone"],
            },
            "password": admin["password"],
            "mfa_secret": admin["mfa_secret"],
        },
    )
    new_account = account_response.json()
    new_admin_headers = api_auth_header(
        api_access_token(admin["login_name"], admin["password"])
    )
    myuser = httpx.get(API_URL + "/myuser", headers=new_admin_headers).json()
    httpx.put(
        API_URL + "/users/" + myuser["user"]["id"],
        headers=new_admin_headers,
        json={
            "display_name": admin["display_name"],
            "role": "admin",
            "account_id": myuser["account"]["id"],
            "id": myuser["user"]["id"],
            "active": True,
            "created": new_account["user"]["created"],
        },
    )
    return httpx.get(API_URL + "/myuser", headers=new_admin_headers).json()


def api_post_message(sender_user_id, recipient_user_id, auth_headers, msg):
    return httpx.post(
        API_URL + "/users/" + sender_user_id + "/messages",
        headers=auth_headers,
        json={
            "sender_id": sender_user_id,
            "recipient_id": recipient_user_id,
            "content": msg,
            "opened": False,
        },
    )


def api_create_mentor(mentor):
    admin_headers = api_admin_auth_headers()
    httpx.post(
        API_URL + "/accounts",
        headers=admin_headers,
        json={
            "password": mentor["password"],
            "account": {
                "role": mentor["role"],
                "login_name": mentor["login_name"],
                "email": mentor["email"],
                "phone": mentor["phone"],
            },
        },
    )
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )
    myuser = httpx.get(API_URL + "/myuser", headers=mentor_headers).json()
    httpx.put(
        API_URL + "/users/" + myuser["user"]["id"],
        headers=mentor_headers,
        json={
            "display_name": mentor["display_name"],
            "role": mentor["role"],
            "account_id": myuser["account"]["id"],
            "id": myuser["user"]["id"],
            "active": True,
            "created": myuser["user"]["created"],
        },
    )
    httpx.put(
        API_URL + "/mentors/" + myuser["mentor"]["id"],
        headers=admin_headers,
        json={
            "birth_year": mentor["birth_year"],
            "display_name": mentor["display_name"],
            "gender": mentor["gender"],
            "languages": mentor["languages"],
            "region": mentor["region"],
            "skills": mentor["skills"],
            "story": mentor["story"],
            "communication_channels": mentor["communication_channels"],
            "account_id": myuser["account"]["id"],
            "user_id": myuser["user"]["id"],
            "id": myuser["mentor"]["id"],
            "created": myuser["mentor"]["created"],
            "active": True,
        },
    )
    myuser = httpx.get(API_URL + "/myuser", headers=mentor_headers).json()
    return myuser


def get_accounts_admin():
    admin_headers = api_admin_auth_headers()
    return admin_headers


def api_create_and_login_mentee(mentee):
    myuser = api_create_mentee(mentee)
    headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )
    return mentee, myuser, headers


def api_create_and_login_mentor(mentor):
    myuser = api_create_mentor(mentor)
    headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )
    return mentor, myuser, headers


def api_ban(user_id, user_id_to_ban, headers):
    return httpx.put(
        API_URL + "/users/" + user_id + "/contacts/" + user_id_to_ban,
        headers=headers,
        json={
            "status": "banned",
        },
    )


def api_permaban(user_id, user_id_to_ban, headers):
    return httpx.patch(
        API_URL + "/users/" + user_id + "/contacts",
        headers=headers,
        json=[{"id": user_id_to_ban, "status": "deleted"}],
    )


def api_get_reports():
    admin_auth_header = api_admin_auth_headers()
    res = httpx.get(API_URL + "/reports", headers=admin_auth_header)
    return res.json()["resources"]


def api_get_reports_with_headers(headers):
    res = httpx.get(API_URL + "/reports", headers=headers)
    return res.json()["resources"]


def api_get_single_report(report_id, headers):
    res = httpx.get(API_URL + "/reports/" + report_id, headers=headers)
    return res


def api_post_report(report, auth_headers):
    httpx.post(
        API_URL + "/reports",
        headers=auth_headers,
        json=report,
    )


def api_delete_report(report_id):
    admin_auth_header = api_admin_auth_headers()
    res = httpx.delete(API_URL + "/reports/{report_id}", headers=admin_auth_header)
    return res.json()["resources"]


def api_delete_all_reports():
    admin_access_token = api_access_token(API_USER, API_PASS)
    admin_auth_header = api_auth_header(admin_access_token)

    for report in api_get_reports():
        # Delete all accounts except the admin user
        httpx.delete(
            API_URL + "/reports/" + report["id"],
            headers=admin_auth_header,
            params={
                "limit": 100,
                "offset": 0,
                "desc": "desc",
            },
        )


def api_update_report(report_id, status, comment, headers):
    return httpx.patch(
        API_URL + "/reports/" + report_id,
        headers=headers,
        json={"status": status, "comment": comment},
    )


def api_create_mentee_mentor_and_report():
    new_mentee = get_random_mentee()
    new_mentee, mentee_myuser, mentee_headers = api_create_and_login_mentee(new_mentee)

    new_mentor = get_random_mentor()
    new_mentor, mentor_myuser, mentor_headers = api_create_and_login_mentor(new_mentor)

    msg = random_string(10)
    reply_msg = random_string(10)

    api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        msg,
    )
    api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_headers,
        reply_msg,
    )

    report_reason = ""
    for _ in range(100):
        report_reason += random_string(random.randint(1, 11))
        report_reason += " "

    mentor_id = mentor_myuser["user"]["id"]
    put_report_response = debug_post(
        f"{API_URL}/reports",
        headers=mentee_headers,
        json={
            "reported_user_id": mentor_id,
            "contact_field": "testi@testi.fi",
            "report_reason": report_reason,
            "reporter_user_id": mentee_myuser["user"]["id"],
        },
    )

    return put_report_response


def create_different_users():
    users = {}
    users["mentee"] = get_random_mentee()
    users["mentee_myuser"] = api_create_mentee(users["mentee"])
    users["mentee_headers"] = api_auth_header_from_username_password(
        users["mentee"]["login_name"], users["mentee"]["password"]
    )
    users["mentor"] = get_random_mentor()
    users["mentor_myuser"] = api_create_mentor(users["mentor"])
    users["mentor_headers"] = api_auth_header_from_username_password(
        users["mentor"]["login_name"], users["mentor"]["password"]
    )
    users["admin"] = get_random_admin()
    users["admin_myuser"] = api_create_admin(users["admin"])
    users["admin_headers"] = api_auth_header_from_username_password(
        users["admin"]["login_name"], users["admin"]["password"]
    )
    users["invalid_headers"] = invalid_header()
    return users


COLOR_CODES = {
    "red": "\033[0;31m",
    "green": "\033[0;32m",
    "yellow": "\033[0;33m",
    "blue": "\033[0;34m",
    "magenta": "\033[0;35m",
    "cyan": "\033[0;36m",
    "none": "\033[m",
    "red_bold": "\033[1;31m",
    "green_bold": "\033[1;32m",
    "yellow_bold": "\033[1;33m",
    "blue_bold": "\033[1;34m",
    "magenta_bold": "\033[1;35m",
    "cyan_bold": "\033[1;36m",
    "end": "\033[0m",
}


def debug_get(
    url: URLTypes,
    *,
    params: QueryParamTypes = None,
    headers: HeaderTypes = None,
    cookies: CookieTypes = None,
    auth: AuthTypes = None,
    proxies: ProxiesTypes = None,
    follow_redirects: bool = False,
    cert: CertTypes = None,
    verify: VerifyTypes = True,
    timeout: TimeoutTypes = DEFAULT_TIMEOUT_CONFIG,
    trust_env: bool = True,
):
    res = httpx.get(
        url,
        params=params,
        headers=headers,
        cookies=cookies,
        auth=auth,
        proxies=proxies,
        follow_redirects=follow_redirects,
        cert=cert,
        verify=verify,
        timeout=timeout,
        trust_env=trust_env,
    )
    print("")
    print(COLOR_CODES["magenta_bold"] + "Request: " + COLOR_CODES["end"])
    print(COLOR_CODES["magenta_bold"] + "GET", end=COLOR_CODES["end"] + " ")
    print(COLOR_CODES["yellow_bold"] + url + COLOR_CODES["end"])

    if params:
        print(COLOR_CODES["magenta"] + "Params", end=COLOR_CODES["end"] + " ")
        pprint_color(params)
    if headers:
        print(COLOR_CODES["magenta"] + "Headers", end=COLOR_CODES["end"] + " ")
        pprint_color(headers)
    if cookies:
        print(COLOR_CODES["magenta"] + "Cookies", end=COLOR_CODES["end"] + " ")
        pprint_color(cookies)

    print("")
    print(COLOR_CODES["cyan_bold"] + "Response: " + COLOR_CODES["end"])
    print(COLOR_CODES["cyan"] + "Status code: ", end=COLOR_CODES["end"] + " ")
    pprint(res.status_code)

    try:
        j = res.json()
        print(COLOR_CODES["cyan"] + "Json: ", end=COLOR_CODES["end"] + " ")
        pprint_color(j)
    except JSONDecodeError:
        print(COLOR_CODES["cyan"] + "Text: ", end=COLOR_CODES["end"] + " ")
        pprint(res.text)

    return res


def debug_post(
    url: URLTypes,
    *,
    content: RequestContent = None,
    data: RequestData = None,
    files: RequestFiles = None,
    json: typing.Any = None,
    params: QueryParamTypes = None,
    headers: HeaderTypes = None,
    cookies: CookieTypes = None,
    auth: AuthTypes = None,
    proxies: ProxiesTypes = None,
    follow_redirects: bool = False,
    cert: CertTypes = None,
    verify: VerifyTypes = True,
    timeout: TimeoutTypes = DEFAULT_TIMEOUT_CONFIG,
    trust_env: bool = True,
):
    res = httpx.post(
        url,
        content=content,
        data=data,
        files=files,
        json=json,
        params=params,
        headers=headers,
        cookies=cookies,
        auth=auth,
        proxies=proxies,
        follow_redirects=follow_redirects,
        cert=cert,
        verify=verify,
        timeout=timeout,
        trust_env=trust_env,
    )
    print("")
    print(COLOR_CODES["magenta_bold"] + "Request: " + COLOR_CODES["end"])
    print(COLOR_CODES["magenta_bold"] + "POST", end=COLOR_CODES["end"] + " ")
    print(COLOR_CODES["yellow_bold"] + url + COLOR_CODES["end"])

    if content:
        print(COLOR_CODES["magenta"] + "Content", end=COLOR_CODES["end"] + " ")
        pprint_color(content)
    if data:
        print(COLOR_CODES["magenta"] + "Data", end=COLOR_CODES["end"] + " ")
        pprint_color(data)
    if files:
        print(COLOR_CODES["magenta"] + "Files", end=COLOR_CODES["end"] + " ")
        raise Exception("TODO: How to print files")
    if json:
        print(COLOR_CODES["magenta"] + "Json", end=COLOR_CODES["end"] + " ")
        pprint_color(json)
    if params:
        print(COLOR_CODES["magenta"] + "Params", end=COLOR_CODES["end"] + " ")
        pprint_color(params)
    if headers:
        print(COLOR_CODES["magenta"] + "Headers", end=COLOR_CODES["end"] + " ")
        pprint_color(headers)
    if cookies:
        print(COLOR_CODES["magenta"] + "Cookies", end=COLOR_CODES["end"] + " ")
        pprint_color(cookies)

    print("")
    print(COLOR_CODES["cyan_bold"] + "Response: " + COLOR_CODES["end"])
    print(COLOR_CODES["cyan"] + "Status code: ", end=COLOR_CODES["end"] + " ")
    pprint(res.status_code)

    try:
        j = res.json()
        print(COLOR_CODES["cyan"] + "Json: ", end=COLOR_CODES["end"] + " ")
        pprint_color(j)
    except JSONDecodeError:
        print(COLOR_CODES["cyan"] + "Text: ", end=COLOR_CODES["end"] + " ")
        pprint(res.text)

    return res


def debug_put(
    url: URLTypes,
    *,
    content: RequestContent = None,
    data: RequestData = None,
    files: RequestFiles = None,
    json: typing.Any = None,
    params: QueryParamTypes = None,
    headers: HeaderTypes = None,
    cookies: CookieTypes = None,
    auth: AuthTypes = None,
    proxies: ProxiesTypes = None,
    follow_redirects: bool = False,
    cert: CertTypes = None,
    verify: VerifyTypes = True,
    timeout: TimeoutTypes = DEFAULT_TIMEOUT_CONFIG,
    trust_env: bool = True,
):
    res = httpx.put(
        url,
        content=content,
        data=data,
        files=files,
        json=json,
        params=params,
        headers=headers,
        cookies=cookies,
        auth=auth,
        proxies=proxies,
        follow_redirects=follow_redirects,
        cert=cert,
        verify=verify,
        timeout=timeout,
        trust_env=trust_env,
    )
    print("")
    print(COLOR_CODES["magenta_bold"] + "Request: " + COLOR_CODES["end"])
    print(COLOR_CODES["magenta_bold"] + "PUT", end=COLOR_CODES["end"] + " ")
    print(COLOR_CODES["yellow_bold"] + url + COLOR_CODES["end"])

    if content:
        print(COLOR_CODES["magenta"] + "Content", end=COLOR_CODES["end"] + " ")
        pprint_color(content)
    if data:
        print(COLOR_CODES["magenta"] + "Data", end=COLOR_CODES["end"] + " ")
        pprint_color(data)
    if files:
        print(COLOR_CODES["magenta"] + "Files", end=COLOR_CODES["end"] + " ")
        raise Exception("TODO: How to print files")
    if json:
        print(COLOR_CODES["magenta"] + "Json", end=COLOR_CODES["end"] + " ")
        pprint_color(json)
    if params:
        print(COLOR_CODES["magenta"] + "Params", end=COLOR_CODES["end"] + " ")
        pprint_color(params)
    if headers:
        print(COLOR_CODES["magenta"] + "Headers", end=COLOR_CODES["end"] + " ")
        pprint_color(headers)
    if cookies:
        print(COLOR_CODES["magenta"] + "Cookies", end=COLOR_CODES["end"] + " ")
        pprint_color(cookies)

    print("")
    print(COLOR_CODES["cyan_bold"] + "Response: " + COLOR_CODES["end"])
    print(COLOR_CODES["cyan"] + "Status code: ", end=COLOR_CODES["end"] + " ")
    pprint(res.status_code)

    try:
        j = res.json()
        print(COLOR_CODES["cyan"] + "Json: ", end=COLOR_CODES["end"] + " ")
        pprint_color(j)
    except JSONDecodeError:
        print(COLOR_CODES["cyan"] + "Text: ", end=COLOR_CODES["end"] + " ")
        pprint(res.text)

    return res


def debug_patch(
    url: URLTypes,
    *,
    content: RequestContent = None,
    data: RequestData = None,
    files: RequestFiles = None,
    json: typing.Any = None,
    params: QueryParamTypes = None,
    headers: HeaderTypes = None,
    cookies: CookieTypes = None,
    auth: AuthTypes = None,
    proxies: ProxiesTypes = None,
    follow_redirects: bool = False,
    cert: CertTypes = None,
    verify: VerifyTypes = True,
    timeout: TimeoutTypes = DEFAULT_TIMEOUT_CONFIG,
    trust_env: bool = True,
):
    res = httpx.patch(
        url,
        content=content,
        data=data,
        files=files,
        json=json,
        params=params,
        headers=headers,
        cookies=cookies,
        auth=auth,
        proxies=proxies,
        follow_redirects=follow_redirects,
        cert=cert,
        verify=verify,
        timeout=timeout,
        trust_env=trust_env,
    )
    print("")
    print(COLOR_CODES["magenta_bold"] + "Request: " + COLOR_CODES["end"])
    print(COLOR_CODES["magenta_bold"] + "PATCH", end=COLOR_CODES["end"] + " ")
    print(COLOR_CODES["yellow_bold"] + url + COLOR_CODES["end"])

    if content:
        print(COLOR_CODES["magenta"] + "Content", end=COLOR_CODES["end"] + " ")
        pprint_color(content)
    if data:
        print(COLOR_CODES["magenta"] + "Data", end=COLOR_CODES["end"] + " ")
        pprint_color(data)
    if files:
        print(COLOR_CODES["magenta"] + "Files", end=COLOR_CODES["end"] + " ")
        raise Exception("TODO: How to print files")
    if json:
        print(COLOR_CODES["magenta"] + "Json", end=COLOR_CODES["end"] + " ")
        pprint_color(json)
    if params:
        print(COLOR_CODES["magenta"] + "Params", end=COLOR_CODES["end"] + " ")
        pprint_color(params)
    if headers:
        print(COLOR_CODES["magenta"] + "Headers", end=COLOR_CODES["end"] + " ")
        pprint_color(headers)
    if cookies:
        print(COLOR_CODES["magenta"] + "Cookies", end=COLOR_CODES["end"] + " ")
        pprint_color(cookies)

    print("")
    print(COLOR_CODES["cyan_bold"] + "Response: " + COLOR_CODES["end"])
    print(COLOR_CODES["cyan"] + "Status code: ", end=COLOR_CODES["end"] + " ")
    pprint(res.status_code)

    try:
        j = res.json()
        print(COLOR_CODES["cyan"] + "Json: ", end=COLOR_CODES["end"] + " ")
        pprint_color(j)
    except JSONDecodeError:
        print(COLOR_CODES["cyan"] + "Text: ", end=COLOR_CODES["end"] + " ")
        pprint(res.text)

    return res


def debug_delete(
    url: URLTypes,
    *,
    params: QueryParamTypes = None,
    headers: HeaderTypes = None,
    cookies: CookieTypes = None,
    auth: AuthTypes = None,
    proxies: ProxiesTypes = None,
    follow_redirects: bool = False,
    cert: CertTypes = None,
    verify: VerifyTypes = True,
    timeout: TimeoutTypes = DEFAULT_TIMEOUT_CONFIG,
    trust_env: bool = True,
):
    res = httpx.delete(
        url,
        params=params,
        headers=headers,
        cookies=cookies,
        auth=auth,
        proxies=proxies,
        follow_redirects=follow_redirects,
        cert=cert,
        verify=verify,
        timeout=timeout,
        trust_env=trust_env,
    )
    print("")
    print(COLOR_CODES["magenta_bold"] + "Request: " + COLOR_CODES["end"])
    print(COLOR_CODES["magenta_bold"] + "DELETE", end=COLOR_CODES["end"] + " ")
    print(COLOR_CODES["yellow_bold"] + url + COLOR_CODES["end"])

    if params:
        print(COLOR_CODES["magenta"] + "Params", end=COLOR_CODES["end"] + " ")
        pprint_color(params)
    if headers:
        print(COLOR_CODES["magenta"] + "Headers", end=COLOR_CODES["end"] + " ")
        pprint_color(headers)
    if cookies:
        print(COLOR_CODES["magenta"] + "Cookies", end=COLOR_CODES["end"] + " ")
        pprint_color(cookies)

    print("")
    print(COLOR_CODES["cyan_bold"] + "Response: " + COLOR_CODES["end"])
    print(COLOR_CODES["cyan"] + "Status code: ", end=COLOR_CODES["end"] + " ")
    pprint(res.status_code)

    try:
        j = res.json()
        print(COLOR_CODES["cyan"] + "Json: ", end=COLOR_CODES["end"] + " ")
        pprint_color(j)
    except JSONDecodeError:
        print(COLOR_CODES["cyan"] + "Text: ", end=COLOR_CODES["end"] + " ")
        pprint(res.text)

    return res


def debug_head(
    url: URLTypes,
    *,
    params: QueryParamTypes = None,
    headers: HeaderTypes = None,
    cookies: CookieTypes = None,
    auth: AuthTypes = None,
    proxies: ProxiesTypes = None,
    follow_redirects: bool = False,
    cert: CertTypes = None,
    verify: VerifyTypes = True,
    timeout: TimeoutTypes = DEFAULT_TIMEOUT_CONFIG,
    trust_env: bool = True,
):
    res = httpx.head(
        url,
        params=params,
        headers=headers,
        cookies=cookies,
        auth=auth,
        proxies=proxies,
        follow_redirects=follow_redirects,
        cert=cert,
        verify=verify,
        timeout=timeout,
        trust_env=trust_env,
    )
    print("")
    print(COLOR_CODES["magenta_bold"] + "Request: " + COLOR_CODES["end"])
    print(COLOR_CODES["magenta_bold"] + "HEAD", end=COLOR_CODES["end"] + " ")
    print(COLOR_CODES["yellow_bold"] + url + COLOR_CODES["end"])

    if params:
        print(COLOR_CODES["magenta"] + "Params", end=COLOR_CODES["end"] + " ")
        pprint_color(params)
    if headers:
        print(COLOR_CODES["magenta"] + "Headers", end=COLOR_CODES["end"] + " ")
        pprint_color(headers)
    if cookies:
        print(COLOR_CODES["magenta"] + "Cookies", end=COLOR_CODES["end"] + " ")
        pprint_color(cookies)

    pprint("")
    print(COLOR_CODES["cyan_bold"] + "Response: " + COLOR_CODES["end"])
    print(COLOR_CODES["cyan"] + "Status code: ", end=COLOR_CODES["end"] + " ")
    pprint(res.status_code)

    try:
        j = res.json()
        print(COLOR_CODES["cyan"] + "Json: ", end=COLOR_CODES["end"] + " ")
        pprint_color(j)
    except JSONDecodeError:
        print(COLOR_CODES["cyan"] + "Text: ", end=COLOR_CODES["end"] + " ")
        pprint(res.text)

    return res


def is_date(value):
    try:
        datetime.fromisoformat(value)
        return True
    except ValueError:
        return False
