# pylint: disable=line-too-long

from tests.api.helpers import (
    API_URL,
    api_admin_auth_headers,
    api_delete_accounts,
    debug_post,
)


def test_insert_events_as_an_admin():
    api_delete_accounts()

    res = debug_post(
        API_URL + "/events",
        headers=api_admin_auth_headers(),
        json=[
            {
                "name": "filter_skills",
                "props": {"skills": ["Eyebrow Dancing", "Limbo Skating"]},
            },  # noqa
            {"name": "filter_skills", "props": {"skills": ["Eyebrow Dancing"]}},  # noqa
            {
                "name": "open_mentor_profile",
                "props": {"mentor_id": "some_mentor_id"},
            },  # noqa
            {
                "name": "open_mentor_profile",
                "props": {"mentor_id": "some_mentor_id"},
            },  # noqa
        ],
    )
    assert res.status_code == 201


def test_invalid_events_as_an_admin():
    api_delete_accounts()

    res = debug_post(
        API_URL + "/events",
        headers=api_admin_auth_headers(),
        json=[
            {"name": "non_existing"},  # noqa
        ],
    )
    assert res.status_code == 422
