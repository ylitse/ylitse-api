from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_mentee,
    get_random_mentee,
    api_login,
    api_access_token,
    api_auth_header,
    debug_post,
    debug_get,
)


def test_login_as_an_unauthenticated_user():
    api_delete_accounts()
    mentee = get_random_mentee()
    api_create_mentee(mentee)

    res = debug_post(
        API_URL + "/login",
        json={
            "login_name": mentee["login_name"],
            "password": mentee["password"],
        },
    )
    json = res.json()
    assert res.status_code == 200
    assert "scopes" in json
    assert json["token_type"] == "bearer"
    assert "tokens" in json

    assert "account_id" in json["scopes"]
    assert "user_id" in json["scopes"]

    assert "refresh_token" in json["tokens"]
    assert "access_token" in json["tokens"]


def test_login_as_an_unauthenticated_user_wrong_password():
    api_delete_accounts()
    mentee = get_random_mentee()

    res = debug_post(
        API_URL + "/login",
        json={
            "login_name": mentee["login_name"],
            "password": mentee["password"],
        },
    )
    json = res.json()

    assert res.status_code == 400
    assert json == {"error": "Bad Request", "message": "Bad request."}


def test_logout_as_a_logged_in_user():
    api_delete_accounts()
    mentee = get_random_mentee()
    api_create_mentee(mentee)

    api_login(mentee)

    access_token = api_access_token(mentee["login_name"], mentee["password"])
    auth_header = api_auth_header(access_token)

    res = debug_get(API_URL + "/logout", headers=auth_header)

    assert res.status_code == 200
    json = res.json()
    assert json == {"message": "Related refresh token invalidated."}
