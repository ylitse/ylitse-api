from tests.api.helpers import (
    API_URL,
    api_delete_accounts,
    api_create_mentor,
    api_access_token,
    api_auth_header,
    get_random_mentor,
    debug_get,
    debug_put,
)


def test_upsert_device_as_mentor():
    """upsert mentor with device token"""
    api_delete_accounts()
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    # no token yet
    get_res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/device",
        headers=mentor_headers,
    )
    assert get_res.status_code == 200
    device = get_res.json()
    assert device == {
        "platform": "none",
        "token": "none",
    }

    # create
    first_token = "my-token" * 2
    device_res = debug_put(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/device",
        headers=mentor_headers,
        json={
            "platform": "android",
            "token": first_token,
        },
    )
    assert device_res.status_code == 200
    device = device_res.json()
    assert len(device.pop("created")) == 26
    assert len(device.pop("updated")) == 26
    assert len(device.pop("id")) == 43
    assert device == {
        "active": True,
        "platform": "android",
        "token": first_token,
        "user_id": mentor_myuser["user"]["id"],
    }

    get_res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/device",
        headers=mentor_headers,
    )
    assert get_res.status_code == 200
    device = get_res.json()
    assert len(device.pop("created")) == 26
    assert len(device.pop("updated")) == 26
    assert len(device.pop("id")) == 43
    assert device == {
        "active": True,
        "platform": "android",
        "token": first_token,
        "user_id": mentor_myuser["user"]["id"],
    }

    # update
    second_token = "my-token2" * 2
    device_res = debug_put(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/device",
        headers=mentor_headers,
        json={
            "platform": "ios",
            "token": second_token,
        },
    )
    assert device_res.status_code == 200
    device = device_res.json()
    assert len(device.pop("created")) == 26
    assert len(device.pop("updated")) == 26
    assert len(device.pop("id")) == 43
    assert device == {
        "active": True,
        "platform": "ios",
        "token": second_token,
        "user_id": mentor_myuser["user"]["id"],
    }

    get_res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/device",
        headers=mentor_headers,
    )
    assert get_res.status_code == 200
    device = get_res.json()
    assert len(device.pop("created")) == 26
    assert len(device.pop("updated")) == 26
    assert len(device.pop("id")) == 43
    assert device == {
        "active": True,
        "platform": "ios",
        "token": second_token,
        "user_id": mentor_myuser["user"]["id"],
    }
