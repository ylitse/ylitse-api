from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_mentee,
    get_random_mentee,
    get_random_mentor,
    debug_post,
    debug_get,
    api_create_mentor,
    random_string,
)


def test_not_be_able_to_send_receive_messages_as_an_unauthenticated_user():
    api_delete_accounts()

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)

    msg = random_string(10)

    empty_auth_headers = {}

    send_msg_res = debug_post(
        API_URL + "/users/" + mentee_myuser["user"]["id"] + "/messages",
        headers=empty_auth_headers,
        json={
            "sender_id": mentee_myuser["user"]["id"],
            "recipient_id": mentor_myuser["user"]["id"],
            "content": msg,
            "opened": False,
        },
    )
    assert send_msg_res.status_code == 401
    assert send_msg_res.json() == {
        "error": "Unauthorized",
        "message": "User not authenticated, provide correct credentials!",
    }

    receive_msg_res = debug_get(
        API_URL + "/users/" + mentee_myuser["user"]["id"] + "/messages",
        headers=empty_auth_headers,
    )
    assert receive_msg_res.status_code == 401
    assert receive_msg_res.json() == {
        "error": "Unauthorized",
        "message": "User not authenticated, provide correct credentials!",
    }
