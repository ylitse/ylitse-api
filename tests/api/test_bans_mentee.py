from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    get_random_mentee,
    get_random_mentor,
    debug_get,
    debug_put,
    debug_patch,
    api_post_message,
    random_string,
    api_create_and_login_mentee,
    api_create_and_login_mentor,
)


def test_banned_contact_can_chat_as_a_mentee():
    api_delete_accounts()

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "banned",
        },
    )
    assert ban_res.status_code == 200

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
    )

    messages = res.json()["resources"]
    assert len(messages) == 2


def test_delete_a_banned_contact_as_a_mentee():
    api_delete_accounts()

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        recipient_mentor_myuser,
        _,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    ban_status_res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/contacts",
        headers=sender_mentee_headers,
    )
    assert ban_status_res.status_code == 200
    assert len(ban_status_res.json()["resources"]) == 1

    ban_res = debug_put(
        API_URL
        + "/users/"
        + sender_mentee_myuser["user"]["id"]
        + "/contacts/"
        + recipient_mentor_myuser["user"]["id"],
        headers=sender_mentee_headers,
        json={
            "status": "banned",
        },
    )
    assert ban_res.status_code == 200

    contact_user_id = recipient_mentor_myuser["user"]["id"]
    ban_res = debug_patch(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/contacts",
        headers=sender_mentee_headers,
        json=[
            {
                "id": contact_user_id,
                "status": "deleted",
            }
        ],
    )
    assert ban_res.status_code == 200
    assert len(ban_res.json()["resources"]) == 1

    ban_status_res = debug_get(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/contacts",
        headers=sender_mentee_headers,
    )
    assert ban_status_res.status_code == 200
    assert len(ban_status_res.json()["resources"]) == 0


def test_cannot_change_own_ban_as_a_mentee():
    api_delete_accounts()

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "banned",
        },
    )
    assert ban_res.status_code == 200

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=sender_mentee_headers,
        json={
            "status": "ok",
        },
    )
    assert ban_res.status_code == 401
    assert ban_res.json() == {
        "error": "Unauthorized",
        "message": "User not authenticated, provide correct credentials!",
    }

    patch_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=sender_mentee_headers,
        json=[
            {
                "id": sender_mentee_myuser["user"]["id"],
                "status": "ok",
            }
        ],
    )
    assert patch_res.status_code == 401
    assert patch_res.json() == {
        "error": "Unauthorized",
        "message": "User not authenticated, provide correct credentials!",
    }
