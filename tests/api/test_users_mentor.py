# pylint: disable=too-many-locals
import httpx
from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_admin,
    get_random_admin,
    get_random_mentor,
    api_login,
    get_random_mentee,
    api_auth_header,
    random_string,
    debug_put,
    debug_get,
    debug_delete,
    api_create_mentor,
    api_create_mentee,
    api_post_message,
    api_access_token,
)


def test_fetch_all_my_messages_as_a_mentor():
    api_delete_accounts()
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    msg = random_string(10)
    mentor_auth_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    res = api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_auth_headers,
        msg,
    )

    assert res.status_code == 201

    res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_auth_headers,
    )
    messages = res.json()["resources"]
    assert len(messages) == 1
    assert messages[0]["active"]
    assert messages[0]["content"] == msg
    assert not messages[0]["opened"]
    assert messages[0]["sender_id"] == mentor_myuser["user"]["id"]
    assert messages[0]["recipient_id"] == mentee_myuser["user"]["id"]
    assert "created" in messages[0]
    assert "updated" in messages[0]
    assert "id" in messages[0]


def test_fetch_my_own_account_as_a_mentor():
    """fetch my own account as a mentor"""
    api_delete_accounts()
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    login_response = api_login(mentor)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentor_account_id = mentor_myuser["account"]["id"]
    res = debug_get(f"{API_URL}/accounts/{mentor_account_id}", headers=header)
    expectedAttr = set(
        [
            "active",
            "created",
            "email",
            "id",
            "login_name",
            "phone",
            "role",
            "updated",
        ]
    )
    mentor_account = res.json()
    assert mentor_account["email"] == mentor["email"]
    assert mentor_account["id"] == mentor_account_id
    assert res.status_code == 200
    assert all(attr in mentor_account for attr in expectedAttr)


def test_fetch_my_own_user_as_a_mentor():
    """fetch my own user as a mentor"""
    api_delete_accounts()
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    login_response = api_login(mentor)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentor_user_id = mentor_myuser["user"]["id"]
    res = debug_get(f"{API_URL}/users/{mentor_user_id}", headers=header)
    expectedAttr = set(
        [
            "account_id",
            "active",
            "created",
            "display_name",
            "id",
            "role",
            "updated",
        ]
    )
    mentor_user = res.json()
    assert res.status_code == 200
    assert mentor_user["display_name"] == mentor["display_name"]
    assert mentor_user["id"] == mentor_user_id
    assert all(attr in mentor_user for attr in expectedAttr)


def test_update_my_own_user_and_all_sub_resources_as_a_mentor():
    """update my own user and all sub resources as a mentor"""
    api_delete_accounts()
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_user_id = mentor_myuser["user"]["id"]
    mentor_account_id = mentor_myuser["account"]["id"]
    mentor_mentor_id = mentor_myuser["mentor"]["id"]
    login_response = api_login(mentor)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    new_name = random_string(10)
    updatedUser = {
        "display_name": new_name,
        "role": mentor["role"],
        "account_id": mentor_myuser["account"]["id"],
        "id": mentor_myuser["user"]["id"],
        "active": True,
    }
    res = debug_put(
        f"{API_URL}/users/{mentor_user_id}", headers=header, json=updatedUser
    )
    assert res.status_code == 200
    new_email = random_string(10) + "_updated@mentor.mentor"
    updatedAccount = {
        "id": mentor_account_id,
        "email": new_email,
        "role": mentor["role"],
        "login_name": mentor["login_name"],
    }
    res = debug_put(
        f"{API_URL}/accounts/{mentor_account_id}",
        headers=header,
        json=updatedAccount,
    )
    new_story = random_string(20)
    updatedMentor = {
        "account_id": mentor_account_id,
        "active": mentor_myuser["mentor"]["active"],
        "birth_year": mentor_myuser["mentor"]["birth_year"],
        "communication_channels": mentor_myuser["mentor"]["communication_channels"],
        "created": mentor_myuser["mentor"]["created"],
        "display_name": mentor_myuser["mentor"]["display_name"],
        "gender": mentor_myuser["mentor"]["gender"],
        "id": mentor_myuser["mentor"]["id"],
        "languages": mentor_myuser["mentor"]["languages"],
        "region": mentor_myuser["mentor"]["region"],
        "skills": mentor_myuser["mentor"]["skills"],
        "story": new_story,
        "updated": mentor_myuser["mentor"]["updated"],
        "user_id": mentor_user_id,
    }
    res = debug_put(
        f"{API_URL}/mentors/{mentor_mentor_id}",
        headers=header,
        json=updatedMentor,
    )
    assert res.status_code == 200
    updated = debug_get(f"{API_URL}/myuser", headers=header).json()
    assert updated["user"]["display_name"] == new_name
    assert updated["account"]["email"] == new_email
    assert updated["mentor"]["story"] == new_story


def test_delete_my_user_account_mentor_resource_as_a_mentor():
    """delete my user, account, mentor resource as a mentor"""
    api_delete_accounts()
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    login_response = api_login(mentor)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentor_account_id = mentor_myuser["account"]["id"]
    admin = get_random_admin()
    api_create_admin(admin)
    login_response = api_login(admin)
    access_token = login_response["tokens"]["access_token"]
    admin_header = api_auth_header(access_token)
    res = debug_delete(f"{API_URL}/accounts/{mentor_account_id}", headers=header)
    assert res.status_code == 401
    res = debug_get(f"{API_URL}/users", headers=admin_header)
    users = res.json()["resources"]
    assert len(users) == 3  # admin + superadmin are left


def test_fetch_a_user_i_am_chatting_with_as_a_mentor():
    """fetch a user i am chatting with as a mentor"""
    api_delete_accounts()
    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    msg = random_string(10)
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_user_id = mentor_myuser["user"]["id"]
    mentor_auth_header = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )
    api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_auth_header,
        msg,
    )
    res = debug_get(
        f"{API_URL}/users/{mentor_user_id}/messages",
        headers=mentor_auth_header,
    )
    messages = res.json()["resources"]
    last_correspondant_id = messages[0]["recipient_id"]
    res = debug_get(
        f"{API_URL}/users/{last_correspondant_id}", headers=mentor_auth_header
    )
    expectedAttr = set(
        [
            "account_id",
            "active",
            "created",
            "display_name",
            "id",
            "role",
            "updated",
        ]
    )
    assert res.status_code == 200
    last_correspondant = res.json()
    assert last_correspondant["account_id"] == mentee_myuser["account"]["id"]
    assert last_correspondant["id"] == mentee_myuser["user"]["id"]
    assert last_correspondant["display_name"] == mentee["display_name"]
    assert last_correspondant["role"] == "mentee"
    assert all(attr in last_correspondant for attr in expectedAttr)


def test_fetch_all_mentors_as_a_mentor():
    """fetch all mentors as a mentor"""
    api_delete_accounts()
    new_mentor = get_random_mentor()
    api_create_mentor(new_mentor)
    new_mentor2 = get_random_mentor()
    new_mentor3 = get_random_mentor()
    api_create_mentor(new_mentor2)
    api_create_mentor(new_mentor3)
    login_response = api_login(new_mentor)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentors_response = debug_get(f"{API_URL}/mentors", headers=header)
    mentors = mentors_response.json()
    expectedAttributes = set(
        [
            "account_id",
            "active",
            "birth_year",
            "communication_channels",
            "created",
            "display_name",
            "gender",
            "id",
            "languages",
            "region",
            "skills",
            "story",
            "updated",
            "user_id",
        ]
    )
    assert mentors_response.status_code == 200
    assert len(mentors["resources"]) == 3
    assert mentors["resources"][0]["birth_year"] == new_mentor["birth_year"]
    assert mentors["resources"][0]["story"] == new_mentor["story"]
    assert all(attr in mentors["resources"][0] for attr in expectedAttributes)


def test_chat_with_deleted_recipient_as_a_mentor():
    api_delete_accounts()

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_auth_header = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_auth_header = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    msg = random_string(10)
    post_msg_res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_auth_header,
        msg,
    )
    assert post_msg_res.status_code == 201

    delete_res = httpx.delete(
        API_URL + "/accounts/" + mentee_myuser["account"]["id"],
        headers=mentee_auth_header,
    )
    assert delete_res.status_code == 204

    msg_after_delete = random_string(10)
    post_msg_after_delete_res = api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_auth_header,
        msg_after_delete,
    )
    assert post_msg_after_delete_res.status_code == 400
    assert post_msg_after_delete_res.json() == {"message": "Recipient doesn't exist"}


def test_chat_with_deleted_sender_as_a_mentor():
    api_delete_accounts()

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_auth_header = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_id = mentor_myuser["account"]["id"]
    mentor_auth_header = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    msg = random_string(10)
    post_msg_res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_auth_header,
        msg,
    )
    assert post_msg_res.status_code == 201

    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    admin_headers = api_auth_header(access_token)

    delete_res = httpx.delete(f"{API_URL}/accounts/{mentor_id}", headers=admin_headers)

    assert delete_res.status_code == 204

    msg_after_delete = random_string(10)
    post_msg_after_delete_res = api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_auth_header,
        msg_after_delete,
    )
    assert post_msg_after_delete_res.status_code == 400
    assert post_msg_after_delete_res.json() == {"message": "Sender doesn't exist"}
