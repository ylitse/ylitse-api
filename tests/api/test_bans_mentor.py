from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    get_random_mentee,
    get_random_mentor,
    debug_get,
    debug_put,
    debug_patch,
    api_post_message,
    random_string,
    api_create_and_login_mentee,
    api_create_and_login_mentor,
)


def test_ban_a_contact_as_a_mentor():
    api_delete_accounts()

    (
        sender_mentee,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "banned",
        },
    )
    assert ban_res.status_code == 200
    ban_res_json = ban_res.json()
    assert len(ban_res_json.pop("created")) == 26
    assert len(ban_res_json.pop("updated")) == 26
    assert ban_res_json == {
        "account_id": sender_mentee_myuser["account"]["id"],
        "active": True,
        "display_name": sender_mentee["display_name"],
        "id": sender_mentee_myuser["user"]["id"],
        "role": "mentee",
        "status": "banned",
    }

    ban_status_res = debug_get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
    )
    assert ban_status_res.status_code == 200
    ban_status_res_json = ban_status_res.json()["resources"][0]
    assert len(ban_status_res_json.pop("created")) == 26
    assert len(ban_status_res_json.pop("updated")) == 26
    assert ban_status_res_json == {
        "id": sender_mentee_myuser["user"]["id"],
        "account_id": sender_mentee_myuser["account"]["id"],
        "active": True,
        "display_name": sender_mentee["display_name"],
        "role": "mentee",
        "status": "banned",
    }

    # delete the banned contact
    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "deleted",
        },
    )
    assert ban_res.status_code == 200
    ban_res_json = ban_res.json()
    assert len(ban_res_json.pop("created")) == 26
    assert len(ban_res_json.pop("updated")) == 26
    assert ban_res_json == {
        "account_id": sender_mentee_myuser["account"]["id"],
        "active": True,
        "display_name": sender_mentee["display_name"],
        "id": sender_mentee_myuser["user"]["id"],
        "role": "mentee",
        "status": "deleted",
    }

    # the only contact is deleted so size of contacts is 0
    ban_status_res = debug_get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
    )
    assert ban_status_res.status_code == 200
    assert len(ban_status_res.json()["resources"]) == 0


def test_unban_a_contact_as_a_mentor():
    api_delete_accounts()

    (
        sender_mentee,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "banned",
        },
    )
    assert ban_res.status_code == 200

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "ok",
        },
    )
    assert ban_res.status_code == 200

    ban_status_res = debug_get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
    )
    assert ban_status_res.status_code == 200
    ban_status_res_json = ban_status_res.json()["resources"][0]
    assert len(ban_status_res_json.pop("created")) == 26
    assert len(ban_status_res_json.pop("updated")) == 26
    assert ban_status_res_json == {
        "id": sender_mentee_myuser["user"]["id"],
        "account_id": sender_mentee_myuser["account"]["id"],
        "active": True,
        "display_name": sender_mentee["display_name"],
        "role": "mentee",
    }


def test_delete_a_banned_contact_as_a_mentor():
    api_delete_accounts()

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "banned",
        },
    )
    assert ban_res.status_code == 200

    contact_user_id = sender_mentee_myuser["user"]["id"]
    ban_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
        json=[
            {
                "id": contact_user_id,
                "status": "deleted",
            }
        ],
    )
    assert ban_res.status_code == 200
    resources = ban_res.json()["resources"]
    assert len(resources) == 1
    assert resources[0]["id"] == contact_user_id
    assert resources[0]["status"] == "deleted"

    ban_status_res = debug_get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
    )
    assert ban_status_res.status_code == 200
    assert len(ban_status_res.json()["resources"]) == 0


def test_patch_multiple_contacts_as_a_mentor():
    api_delete_accounts()

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        sender_mentee_myuser2,
        sender_mentee_headers2,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser2["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers2,
        msg,
    )
    assert send_msg_res.status_code == 201

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "banned",
        },
    )
    assert ban_res.status_code == 200

    ban_res = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser2["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "banned",
        },
    )
    assert ban_res.status_code == 200

    ban_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
        json=[
            {
                "id": sender_mentee_myuser["user"]["id"],
                "status": "deleted",
            },
            {
                "id": sender_mentee_myuser2["user"]["id"],
                "status": "banned",
            },
        ],
    )
    assert ban_res.status_code == 200
    resources = ban_res.json()["resources"]
    assert len(resources) == 2
    assert resources[0]["id"] == sender_mentee_myuser["user"]["id"]
    assert resources[0]["status"] == "deleted"
    assert resources[1]["id"] == sender_mentee_myuser2["user"]["id"]
    assert resources[1]["status"] == "banned"

    get_contacts_res = debug_get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
    )
    assert get_contacts_res.status_code == 200
    resources = get_contacts_res.json()["resources"]
    assert len(resources) == 1
    assert resources[0]["id"] == sender_mentee_myuser2["user"]["id"]
    assert resources[0]["status"] == "banned"


def test_delete_a_contact_with_invalid_request():
    api_delete_accounts()

    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    # no json
    ban_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
    )
    assert ban_res.status_code == 400

    # empty json
    ban_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
        json=[],
    )
    assert ban_res.status_code == 400

    # without id
    ban_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
        json=[{"status": "deleted"}],
    )
    assert ban_res.status_code == 400

    # blank id
    ban_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
        json=[{"id": "", "status": "deleted"}],
    )
    assert ban_res.status_code == 400

    # invalid status
    ban_res = debug_patch(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
        json=[{"id": "test_id", "status": "invalid_status"}],
    )
    assert ban_res.status_code == 400
