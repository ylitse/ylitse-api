# pylint: disable=too-many-locals

from tests.api.helpers import (
    API_URL,
    api_admin_auth_headers,
    api_create_and_login_mentee,
    debug_patch,
    debug_post,
    get_random_mentee,
    api_delete_accounts,
    api_create_mentor,
    api_access_token,
    api_auth_header,
    get_random_mentor,
    random_string,
    debug_get,
    debug_put,
)


def test_display_name_is_really_validated_mentor():
    """update mentor and check display_name validation"""
    api_delete_accounts()
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )
    too_long_display_name = random_string(31)
    updated_user = {
        **mentor_myuser["user"],
        "display_name": too_long_display_name,
    }
    res = debug_put(
        f"{API_URL}/users/{mentor_myuser['user']['id']}",
        headers=mentor_headers,
        json=updated_user,
    )
    assert res.status_code == 400

    updated_mentor = {
        **mentor_myuser["mentor"],
        "display_name": too_long_display_name,
    }

    res = debug_put(
        f"{API_URL}/mentors/{mentor_myuser['mentor']['id']}",
        headers=mentor_headers,
        json=updated_mentor,
    )
    assert res.status_code == 400
    updated = debug_get(f"{API_URL}/myuser", headers=mentor_headers).json()
    assert updated["user"]["display_name"] == mentor["display_name"]
    assert updated["mentor"]["display_name"] == mentor["display_name"]


def test_how_extra_data_is_handled():
    """extra json properties should fail"""
    api_delete_accounts()
    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    (
        _,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    updated_user = {
        **mentor_myuser["user"],
        "extra-stuff": "extra-stuff",
    }

    res = debug_put(
        f"{API_URL}/users/{mentor_myuser['user']['id']}",
        headers=mentor_headers,
        json=updated_user,
    )
    assert res.status_code == 422

    ban_res = debug_put(
        API_URL
        + "/users/"
        + mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=mentor_headers,
        json={
            "status": "banned",
            "extra-stuff": "extra-stuff",
        },
    )
    assert ban_res.status_code == 422

    device_res = debug_put(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/device",
        headers=mentor_headers,
        json={
            "platform": "android",
            "token": "my-token" * 2,
            "extra-stuff": "extra-stuff",
        },
    )
    assert device_res.status_code == 422

    patch_mentor_response = debug_patch(
        f"{API_URL}/mentors/{mentor_myuser['mentor']['id']}",
        headers=mentor_headers,
        json={
            "is_vacationing": True,
            "status_message": "Gone Gardening",
            "extra-stuff": "extra-stuff",
        },
    )
    assert patch_mentor_response.status_code == 422

    msg = random_string(10)

    send_msg_res = debug_post(
        API_URL + "/users/" + sender_mentee_myuser["user"]["id"] + "/messages",
        headers=sender_mentee_headers,
        json={
            "sender_id": sender_mentee_myuser["user"]["id"],
            "recipient_id": mentor_myuser["user"]["id"],
            "content": msg,
            "opened": False,
            # this was in data_definitions but not in message table
            "timestamp": 0,
        },
    )

    assert send_msg_res.status_code == 422

    # null character handling
    null_character_skill = {"name": random_string(20) + "\0"}
    skills_res = debug_post(
        f"{API_URL}/skills",
        headers=api_admin_auth_headers(),
        json=null_character_skill,
    )

    assert skills_res.status_code == 422

    # empty multipart/form-data form
    login_res = debug_post(
        f"{API_URL}/weblogin",
        headers={"Content-Type": "multipart/form-data; boundary=a-boundary"},
        content=b"--a-boundary\r\n",
    )
    assert login_res.status_code == 400

    # invalid multipart/form-data form
    login_res = debug_post(
        f"{API_URL}/weblogin",
        headers={"Content-Type": "multipart/form-data; boundary=a-boundary"},
    )
    assert login_res.status_code == 400
