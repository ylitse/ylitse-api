import os
from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_get_reports,
    api_admin_auth_headers,
    debug_get,
    api_create_mentee_mentor_and_report,
    get_random_admin,
    api_create_admin,
    api_auth_header,
    api_access_token,
    debug_delete,
    api_get_single_report,
    api_update_report,
    api_delete_accounts,
    api_delete_all_reports,
)

SAFETY_FEATURE = os.getenv("SAFETY_FEATURE", "false")


def test_get_as_admin():
    api_delete_accounts()

    if SAFETY_FEATURE == "true":
        api_delete_all_reports()
        put_report_response = api_create_mentee_mentor_and_report()
        assert len(api_get_reports()) == 1
        assert put_report_response.status_code == 201
        headers = api_admin_auth_headers()

        res = debug_get(
            API_URL + "/reports",
            headers=headers,
            params={"limit": 10, "offset": 0, "desc": "true"},
        )

        created_report = res.json()
        assert res.status_code == 200
        assert len(created_report["resources"]) == 1


def test_delete_as_admin():
    put_report_response = api_create_mentee_mentor_and_report()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    headers = api_auth_header(
        api_access_token(new_admin["login_name"], new_admin["password"])
    )

    if SAFETY_FEATURE == "true":
        created_report = put_report_response.json()
        report_id = created_report["id"]
        assert len(api_get_reports()) == 2
        assert put_report_response.status_code == 201

        deleted_report_response = debug_delete(
            f"{API_URL}/reports/{report_id}",
            headers=headers,
        )

        assert deleted_report_response.status_code == 204
        assert len(api_get_reports()) == 1
        assert api_get_reports()[0]["id"] != created_report["id"]
    else:
        assert put_report_response.status_code == 404


def test_delete_as_admin_wrong_id():
    if SAFETY_FEATURE == "true":
        new_admin = get_random_admin()
        api_create_admin(new_admin)
        headers = api_auth_header(
            api_access_token(new_admin["login_name"], new_admin["password"])
        )
        report_id = "0000000000000000000000000000000000000000000"

        deleted_report_response = debug_delete(
            f"{API_URL}/reports/{report_id}",
            headers=headers,
        )

        assert deleted_report_response.status_code == 204


def test_get_by_id_as_admin():

    put_report_response = api_create_mentee_mentor_and_report()

    if SAFETY_FEATURE == "true":
        headers = api_admin_auth_headers()
        created_report = put_report_response.json()
        assert put_report_response.status_code == 201

        report_id = created_report["id"]
        get_report_response = api_get_single_report(report_id, headers)
        print(get_report_response)
        response_json = get_report_response.json()["resources"]
        print(response_json)

        expectedAttr = set(
            [
                "id",
                "reported_user_id",
                "contact_field",
                "report_reason",
                "status",
                "created",
                "updated",
                "active",
            ]
        )

        assert get_report_response.status_code == 200
        assert all(attr in response_json for attr in expectedAttr)
        assert response_json["id"] == created_report["id"]
    else:
        assert put_report_response.status_code == 404


def test_get_by_id_as_admin_wrong_id():
    if SAFETY_FEATURE == "true":
        headers = api_admin_auth_headers()
        report_id = "0000000000000000000000000000000000000000000"
        get_report_response = api_get_single_report(report_id, headers)

        assert get_report_response.status_code == 400


def test_update_report_status_as_admin():

    put_report_response = api_create_mentee_mentor_and_report()

    if SAFETY_FEATURE == "true":
        headers = api_admin_auth_headers()
        created_report = put_report_response.json()
        assert put_report_response.status_code == 201

        report_id = created_report["id"]
        status = "handled"
        comment = "could not resolve, no contact info"
        patch_report_response = api_update_report(report_id, status, comment, headers)
        print(patch_report_response)
        response_json = patch_report_response.json()["resources"]
        print(response_json)

        expectedAttr = set(
            [
                "id",
                "reported_user_id",
                "contact_field",
                "report_reason",
                "status",
                "comment",
                "created",
                "updated",
                "active",
            ]
        )

        assert patch_report_response.status_code == 200
        assert all(attr in response_json for attr in expectedAttr)
        assert response_json["id"] == created_report["id"]
        assert response_json["reported_user_id"] == created_report["reported_user_id"]
        assert response_json["contact_field"] == created_report["contact_field"]
        assert response_json["created"] == created_report["created"]
        assert response_json["updated"] != created_report["updated"]
        assert response_json["status"] == status
        assert response_json["comment"] == comment
    else:
        assert put_report_response.status_code == 404


def test_update_report_false_status_as_admin():

    put_report_response = api_create_mentee_mentor_and_report()

    if SAFETY_FEATURE == "true":
        headers = api_admin_auth_headers()
        created_report = put_report_response.json()
        assert put_report_response.status_code == 201

        report_id = created_report["id"]
        status = "ok"
        comment = "asd"
        patch_report_response = api_update_report(report_id, status, comment, headers)

        assert patch_report_response.status_code == 400
    else:
        assert put_report_response.status_code == 404


def test_update_report_false_id_as_admin():
    if SAFETY_FEATURE == "true":
        headers = api_admin_auth_headers()

        report_id = "0000000000000000000000000000000000000000000"
        status = "handled"
        comment = "asd"
        patch_report_response = api_update_report(report_id, status, comment, headers)

        assert patch_report_response.status_code == 400
