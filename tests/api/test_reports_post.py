import os
import random
from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_get_reports,
    get_random_mentee,
    api_delete_accounts,
    debug_post,
    api_delete_all_reports,
    random_string,
    api_post_message,
    api_create_and_login_mentee,
    api_create_mentee_mentor_and_report,
    get_random_mentor,
    api_create_and_login_mentor,
)

SAFETY_FEATURE = os.getenv("SAFETY_FEATURE", "false")


def test_post_mentor_report_as_mentee():
    if SAFETY_FEATURE == "true":
        api_delete_accounts()
        api_delete_all_reports()
        put_report_response = api_create_mentee_mentor_and_report()

        expectedAttr = set(
            [
                "id",
                "reported_user_id",
                "contact_field",
                "report_reason",
                "status",
                "comment",
                "created",
                "updated",
                "active",
            ]
        )

        created_report = put_report_response.json()
        assert put_report_response.status_code == 201
        assert all(attr in created_report for attr in expectedAttr)
        assert created_report["status"] == "received"
        assert len(api_get_reports()) == 1
        assert api_get_reports()[0]["id"] == created_report["id"]
    else:
        put_report_response = api_create_mentee_mentor_and_report()
        assert put_report_response.status_code == 404


def test_post_mentee_report_as_mentee():
    """ "Only mentors can be reported"""

    new_mentee = get_random_mentee()
    new_mentee, mentee_myuser, mentee_headers = api_create_and_login_mentee(new_mentee)

    new_mentee2 = get_random_mentee()
    new_mentee2, mentee_myuser2, mentee_headers2 = api_create_and_login_mentee(
        new_mentee2
    )

    msg = random_string(10)
    reply_msg = random_string(10)

    api_post_message(
        mentee_myuser["user"]["id"],
        mentee_myuser2["user"]["id"],
        mentee_headers,
        msg,
    )
    api_post_message(
        mentee_myuser2["user"]["id"],
        mentee_myuser["user"]["id"],
        mentee_headers2,
        reply_msg,
    )

    mentor_id = mentee_myuser2["user"]["id"]
    put_report_response = debug_post(
        f"{API_URL}/reports",
        headers=mentee_headers,
        json={
            "reported_user_id": mentor_id,
            "contact_field": "testi@testi.fi",
            "report_reason": "testijuttu 2",
            "reporter_user_id": mentee_myuser["user"]["id"],
        },
    )

    if SAFETY_FEATURE == "true":
        assert put_report_response.status_code == 400
        assert len(api_get_reports()) == 1
    else:
        assert put_report_response.status_code == 404


def test_post_mentor_report_as_mentee_no_contact_field():
    """ "Contact field can be empty"""
    if SAFETY_FEATURE == "true":
        new_mentee = get_random_mentee()
        new_mentee, mentee_myuser, mentee_headers = api_create_and_login_mentee(
            new_mentee
        )

        new_mentor = get_random_mentor()
        new_mentor, mentor_myuser, mentor_headers = api_create_and_login_mentor(
            new_mentor
        )

        msg = random_string(10)
        reply_msg = random_string(10)

        api_post_message(
            mentee_myuser["user"]["id"],
            mentor_myuser["user"]["id"],
            mentee_headers,
            msg,
        )
        api_post_message(
            mentor_myuser["user"]["id"],
            mentee_myuser["user"]["id"],
            mentor_headers,
            reply_msg,
        )

        report_reason = ""
        for _ in range(100):
            report_reason += random_string(random.randint(1, 11))
            report_reason += " "

        mentor_id = mentor_myuser["user"]["id"]
        put_report_response = debug_post(
            f"{API_URL}/reports",
            headers=mentee_headers,
            json={
                "reported_user_id": mentor_id,
                "report_reason": report_reason,
                "reporter_user_id": mentee_myuser["user"]["id"],
            },
        )

        expectedAttr = set(
            [
                "id",
                "reported_user_id",
                "report_reason",
                "status",
                "comment",
                "created",
                "updated",
                "active",
            ]
        )

        created_report = put_report_response.json()
        assert put_report_response.status_code == 201
        assert all(attr in created_report for attr in expectedAttr)
        assert created_report["status"] == "received"
        assert len(api_get_reports()) == 2
        assert api_get_reports()[1]["id"] == created_report["id"]
    else:
        put_report_response = api_create_mentee_mentor_and_report()
        assert put_report_response.status_code == 404
