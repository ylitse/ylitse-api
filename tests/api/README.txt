Setup api details
$ export YLITSE_API_PASS=secret
$ export YLITSE_API_URL=http://localhost:8080
$ export YLITSE_API_USER=admin

run all apitests
$ make apitest

run one file
$ make run-standalone
$ pytest tests/api/test_version_any.py

run one test in one file
$ make run-standalone
$ pytest tests/api/test_version_any.py -k "test_as_any_user_fetch_version_resource"

run one test in one file and print out print() and pprint()
$ make run-standalone
$ pytest -s tests/api/test_version_any.py -k "test_as_any_user_fetch_version_resource"

run one test with alot of useful output
$ make run-standalone
$ pytest -svv tests/api/test_version_any.py -k "test_as_any_user_fetch_version_resource"
