from tests.api.helpers import API_URL
from tests.api.helpers import (
    debug_get,
    api_create_mentee,
    api_login,
    api_auth_header,
    api_delete_accounts,
    get_random_mentee,
)


def test_as_mentee_user_fetch_version():
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    login_response = api_login(new_mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    res = debug_get(f"{API_URL}/version", headers=header)

    assert res.status_code == 200
    assert res.json() == {"api": "0.9.1"}


def test_as_any_user_fetch_version():
    res = debug_get(f"{API_URL}/version")
    response_json = res.json()

    assert res.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }


def test_as_mentee_user_fetch_minimum_version():
    api_delete_accounts()
    new_mentee = get_random_mentee()
    api_create_mentee(new_mentee)
    login_response = api_login(new_mentee)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)

    res = debug_get(f"{API_URL}/version/clients", headers=header)
    versions = res.json()["resources"]
    expectedAttr = set(
        [
            "id",
            "client",
            "version",
            "active",
            "created",
            "updated",
        ]
    )

    assert len(versions) == 2
    assert all(attr in versions[0] for attr in expectedAttr)
    assert versions[0]["version"] == "2.14.0"
    assert versions[0]["client"] == "ylitse_ios"
    assert res.status_code == 200


def test_as_any_user_fetch_minimum_version():
    res = debug_get(f"{API_URL}/version/clients")
    response_json = res.json()

    assert res.status_code == 401
    assert response_json == {
        "message": "User not authenticated, provide correct credentials!",
        "error": "Unauthorized",
    }
