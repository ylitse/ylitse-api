import os
import random
from tests.api.helpers import API_URL
from tests.api.helpers import (
    get_random_mentee,
    api_delete_accounts,
    get_random_mentor,
    random_string,
    api_post_message,
    api_create_and_login_mentee,
    api_create_and_login_mentor,
    get_random_admin,
    api_create_admin,
    api_auth_header,
    api_access_token,
    debug_get,
)

SAFETY_FEATURE = os.getenv("SAFETY_FEATURE", "false")


def test_review_conversation_admin():
    if SAFETY_FEATURE == "true":
        api_delete_accounts()
        _, mentee_myuser, mentee_headers = api_create_and_login_mentee(
            get_random_mentee()
        )

        _, mentor_myuser, mentor_headers = api_create_and_login_mentor(
            get_random_mentor()
        )

        new_admin = get_random_admin()
        api_create_admin(new_admin)
        headers = api_auth_header(
            api_access_token(new_admin["login_name"], new_admin["password"])
        )

        for _ in range(100):
            msg = random_string(10)
            reply_msg = random_string(10)

            api_post_message(
                mentee_myuser["user"]["id"],
                mentor_myuser["user"]["id"],
                mentee_headers,
                msg,
            )
            res_message = api_post_message(
                mentor_myuser["user"]["id"],
                mentee_myuser["user"]["id"],
                mentor_headers,
                reply_msg,
            )

        message_respond = res_message.json()
        report_reason = ""
        for _ in range(100):
            report_reason += random_string(random.randint(1, 11))
            report_reason += " "

        mentor_id = mentor_myuser["user"]["id"]
        res = debug_get(
            API_URL + "/users/" + mentor_id + "/messages_for_admin",
            headers=headers,
            params={
                "contact_user_ids": ",".join(
                    [
                        mentee_myuser["user"]["id"],
                    ]
                ),
                "from_message_id": message_respond["id"],
                "desc": True,
            },
        )
        messages = res.json()["resources"]

        assert res.status_code == 200
        assert len(messages) == 199


def test_review_contacts_admin():
    if SAFETY_FEATURE == "true":
        _, mentee_myuser, mentee_headers = api_create_and_login_mentee(
            get_random_mentee()
        )

        _, mentee_myuser2, mentee_headers2 = api_create_and_login_mentee(
            get_random_mentee()
        )

        _, mentor_myuser, mentor_headers = api_create_and_login_mentor(
            get_random_mentor()
        )

        new_admin = get_random_admin()
        api_create_admin(new_admin)
        headers = api_auth_header(
            api_access_token(new_admin["login_name"], new_admin["password"])
        )

        for _ in range(100):
            msg = random_string(10)
            reply_msg = random_string(10)

            api_post_message(
                mentee_myuser["user"]["id"],
                mentor_myuser["user"]["id"],
                mentee_headers,
                msg,
            )
            api_post_message(
                mentor_myuser["user"]["id"],
                mentee_myuser["user"]["id"],
                mentor_headers,
                reply_msg,
            )

        report_reason = ""
        for _ in range(100):
            report_reason += random_string(random.randint(1, 11))
            report_reason += " "

        for _ in range(100):
            msg = random_string(10)
            reply_msg = random_string(10)

            api_post_message(
                mentee_myuser2["user"]["id"],
                mentor_myuser["user"]["id"],
                mentee_headers2,
                msg,
            )
            api_post_message(
                mentor_myuser["user"]["id"],
                mentee_myuser2["user"]["id"],
                mentor_headers,
                reply_msg,
            )

        report_reason = ""
        for _ in range(100):
            report_reason += random_string(random.randint(1, 11))
            report_reason += " "

        mentor_id = mentor_myuser["user"]["id"]
        res = debug_get(
            API_URL + "/users/" + mentor_id + "/contacts_for_admin",
            headers=headers,
        )
        contacts = res.json()["resources"]

        assert res.status_code == 200
        assert len(contacts) == 2
