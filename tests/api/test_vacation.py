# pylint: disable=too-many-locals

from tests.api.helpers import (
    API_URL,
    api_access_token,
    api_delete_accounts,
    api_create_mentor,
    api_create_admin,
    api_auth_header,
    api_login,
    get_random_mentor,
    get_random_admin,
    debug_get,
    debug_put,
    debug_post,
    debug_patch,
)


def test_fetch_all_mentors():
    api_delete_accounts()
    mentor1 = get_random_mentor()
    mentor2 = get_random_mentor()
    api_create_mentor(mentor1)
    api_create_mentor(mentor2)

    res = debug_get(API_URL + "/mentors")
    mentors = res.json()["resources"]
    assert len(mentors) == 2
    assert not mentors[0]["is_vacationing"]
    assert mentors[0]["status_message"] == ""
    assert not mentors[1]["is_vacationing"]
    assert mentors[1]["status_message"] == ""


def test_create_a_new_vacationing_mentor():
    """create mentor on vacation as admin"""
    api_delete_accounts()
    new_admin = get_random_admin()
    api_create_admin(new_admin)
    login_response = api_login(new_admin)
    access_token = login_response["tokens"]["access_token"]
    header = api_auth_header(access_token)
    mentor = {
        "login_name": "JohnLogin",
        "birth_year": 1998,
        "display_name": "John",
        "password": "secretsecret",
        "email": "john@doe.com",
        "phone": "666",
        "role": "mentor",
        "gender": "male",
        "languages": ["fi", "se", "en"],
        "region": "Järvenpää",
        "skills": [],
        "story": "Short one",
        "communication_channels": ["phone"],
        "is_vacationing": True,
        "status_message": "Gone Gardening",
    }
    post_mentor_response = debug_post(
        f"{API_URL}/accounts",
        headers=header,
        json={
            "password": mentor["password"],
            "account": {
                "role": mentor["role"],
                "login_name": mentor["login_name"],
                "email": mentor["email"],
                "phone": mentor["phone"],
            },
        },
    )
    mentor_json = post_mentor_response.json()
    mentor_id = mentor_json["mentor"]["id"]
    mentor_user_id = mentor_json["user"]["id"]
    put_user_response = debug_put(
        f"{API_URL}/users/{mentor_user_id}",
        headers=header,
        json={
            "active": mentor_json["user"]["active"],
            "account_id": mentor_json["user"]["account_id"],
            "id": mentor_json["user"]["id"],
            "created": mentor_json["user"]["created"],
            "updated": mentor_json["user"]["updated"],
            "role": mentor["role"],
            "display_name": mentor["display_name"],
        },
    )
    put_mentor_response = debug_put(
        f"{API_URL}/mentors/{mentor_id}",
        headers=header,
        json={
            "account_id": mentor_json["mentor"]["account_id"],
            "user_id": mentor_json["mentor"]["user_id"],
            "id": mentor_json["mentor"]["id"],
            "created": mentor_json["mentor"]["created"],
            "active": mentor_json["mentor"]["active"],
            "birth_year": mentor["birth_year"],
            "display_name": mentor["display_name"],
            "gender": mentor["gender"],
            "languages": mentor["languages"],
            "region": mentor["region"],
            "skills": mentor["skills"],
            "story": mentor["story"],
            "communication_channels": mentor["communication_channels"],
            "is_vacationing": mentor["is_vacationing"],
            "status_message": mentor["status_message"],
        },
    )

    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )
    created_mentor_response = debug_get(
        f"{API_URL}/mentors/{mentor_id}", headers=mentor_headers
    )
    created_mentor_json = created_mentor_response.json()
    assert post_mentor_response.status_code == 201
    assert put_user_response.status_code == 200
    assert put_mentor_response.status_code == 200
    assert created_mentor_json["is_vacationing"] == mentor["is_vacationing"]
    assert created_mentor_json["status_message"] == mentor["status_message"]

    patch_mentor_response = debug_patch(
        f"{API_URL}/mentors/{mentor_id}",
        headers=mentor_headers,
        json={
            "is_vacationing": False,
            "status_message": "",
        },
    )
    assert patch_mentor_response.status_code == 204

    patched_mentor_response = debug_get(
        f"{API_URL}/mentors/{mentor_id}", headers=mentor_headers
    )
    patched_mentor_json = patched_mentor_response.json()
    assert not patched_mentor_json["is_vacationing"]
    assert patched_mentor_json["status_message"] == ""
