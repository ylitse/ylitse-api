import os
from tests.api.helpers import API_URL
from tests.api.helpers import (
    create_different_users,
    debug_get,
    api_create_mentee_mentor_and_report,
    api_delete_all_reports,
    api_get_reports,
    debug_delete,
    api_get_single_report,
)


SAFETY_FEATURE = os.getenv("SAFETY_FEATURE", "false")


def test_get_all_reports_with_invalid_users():
    users = create_different_users()
    if SAFETY_FEATURE == "true":
        api_delete_all_reports()
    res = debug_get(
        API_URL + "/reports",
        headers=users["mentee_headers"],
        params={"limit": 10, "offset": 0, "desc": "desc"},
    )
    if SAFETY_FEATURE == "true":
        assert res.status_code == 401
    else:
        assert res.status_code == 404

    res = debug_get(
        API_URL + "/reports",
        headers=users["mentor_headers"],
        params={"limit": 10, "offset": 0, "desc": "desc"},
    )
    if SAFETY_FEATURE == "true":
        assert res.status_code == 401
    else:
        assert res.status_code == 404

    res = debug_get(
        API_URL + "/reports",
        headers=users["invalid_headers"],
        params={"limit": 10, "offset": 0, "desc": "desc"},
    )
    if SAFETY_FEATURE == "true":
        assert res.status_code == 401
    else:
        assert res.status_code == 404


def test_delete_with_all_invalid_usergroups():
    users = create_different_users()
    put_report_response = api_create_mentee_mentor_and_report()
    if SAFETY_FEATURE == "true":
        created_report = put_report_response.json()
        report_id = created_report["id"]
        deleted_report_response = debug_delete(
            f"{API_URL}/reports/{report_id}",
            headers=users["mentee_headers"],
        )

        assert deleted_report_response.status_code == 401
        assert len(api_get_reports()) == 1

        deleted_report_response = debug_delete(
            f"{API_URL}/reports/{report_id}",
            headers=users["mentor_headers"],
        )

        assert deleted_report_response.status_code == 401
        assert len(api_get_reports()) == 1

        deleted_report_response = debug_delete(
            f"{API_URL}/reports/{report_id}",
            headers=users["invalid_headers"],
        )

        assert deleted_report_response.status_code == 401
        assert len(api_get_reports()) == 1

    else:
        assert put_report_response.status_code == 404


def test_get_by_id_all_invalid_usergroups():
    users = create_different_users()
    put_report_response = api_create_mentee_mentor_and_report()

    if SAFETY_FEATURE == "true":
        created_report = put_report_response.json()
        assert put_report_response.status_code == 201

        report_id = created_report["id"]
        get_report_response = api_get_single_report(report_id, users["mentee_headers"])
        assert get_report_response.status_code == 401

        get_report_response = api_get_single_report(report_id, users["mentor_headers"])
        assert get_report_response.status_code == 401

        get_report_response = api_get_single_report(report_id, users["invalid_headers"])
        assert get_report_response.status_code == 401

    else:
        assert put_report_response.status_code == 404
