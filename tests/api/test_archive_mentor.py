from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    get_random_mentee,
    get_random_mentor,
    debug_get,
    debug_put,
    api_post_message,
    random_string,
    api_create_and_login_mentee,
    api_create_and_login_mentor,
)


def test_archive_a_contact_as_a_mentor():
    api_delete_accounts()

    (
        sender_mentee,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())
    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    archive_response = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "archived",
        },
    )
    assert archive_response.status_code == 200
    archive_res_json = archive_response.json()
    assert len(archive_res_json.pop("created")) == 26
    assert len(archive_res_json.pop("updated")) == 26
    assert archive_res_json == {
        "account_id": sender_mentee_myuser["account"]["id"],
        "active": True,
        "display_name": sender_mentee["display_name"],
        "id": sender_mentee_myuser["user"]["id"],
        "role": "mentee",
        "status": "archived",
    }

    contacts_response = debug_get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
    )
    assert contacts_response.status_code == 200
    contacts_json = contacts_response.json()["resources"][0]
    assert len(contacts_json.pop("created")) == 26
    assert len(contacts_json.pop("updated")) == 26
    assert contacts_json == {
        "id": sender_mentee_myuser["user"]["id"],
        "account_id": sender_mentee_myuser["account"]["id"],
        "active": True,
        "display_name": sender_mentee["display_name"],
        "role": "mentee",
        "status": "archived",
    }


def test_unarchive_a_contact_as_a_mentor():
    api_delete_accounts()

    (
        sender_mentee,
        sender_mentee_myuser,
        sender_mentee_headers,
    ) = api_create_and_login_mentee(get_random_mentee())

    (
        _,
        recipient_mentor_myuser,
        recipient_mentor_headers,
    ) = api_create_and_login_mentor(get_random_mentor())

    msg = random_string(10)
    send_msg_res = api_post_message(
        sender_mentee_myuser["user"]["id"],
        recipient_mentor_myuser["user"]["id"],
        sender_mentee_headers,
        msg,
    )
    assert send_msg_res.status_code == 201

    archive_response = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "archived",
        },
    )
    assert archive_response.status_code == 200

    archive_response = debug_put(
        API_URL
        + "/users/"
        + recipient_mentor_myuser["user"]["id"]
        + "/contacts/"
        + sender_mentee_myuser["user"]["id"],
        headers=recipient_mentor_headers,
        json={
            "status": "ok",
        },
    )
    assert archive_response.status_code == 200

    contacts_response = debug_get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/contacts",
        headers=recipient_mentor_headers,
    )
    assert contacts_response.status_code == 200
    contacts_json = contacts_response.json()["resources"][0]
    assert len(contacts_json.pop("created")) == 26
    assert len(contacts_json.pop("updated")) == 26
    assert contacts_json == {
        "id": sender_mentee_myuser["user"]["id"],
        "account_id": sender_mentee_myuser["account"]["id"],
        "active": True,
        "display_name": sender_mentee["display_name"],
        "role": "mentee",
    }
