# pylint: disable=too-many-locals
from tests.api.helpers import API_URL, API_USER, API_PASS
from tests.api.helpers import (
    api_ban,
    api_permaban,
    api_admin_auth_headers,
    api_my_user,
    api_delete_accounts,
    api_create_mentee,
    get_random_mentee,
    get_random_mentor,
    debug_get,
    api_post_message,
    api_create_mentor,
    random_string,
    api_auth_header,
    api_access_token,
)


def test_fetch_stats_as_an_admin():
    api_delete_accounts()

    my_user = api_my_user(api_access_token(API_USER, API_PASS))

    res = debug_get(
        f"{API_URL}/stats",
        headers=api_admin_auth_headers(),
    )
    assert res.status_code == 200
    assert res.json() == {
        "stats": {
            my_user["account"]["id"]: {
                "received": 0,
                "sent": 0,
                "banned": 0,
            },
        },
        "totals": {
            "admins": {"received": 0, "sent": 0, "banned": 0},
            "mentees": {"received": 0, "sent": 0, "banned": 0},
            "mentors": {"received": 0, "sent": 0, "banned": 0},
        },
    }

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    mentee2 = get_random_mentee()
    mentee2_myuser = api_create_mentee(mentee2)
    mentee2_headers = api_auth_header(
        api_access_token(mentee2["login_name"], mentee2["password"])
    )

    msg = random_string(10)
    reply_msg = random_string(10)

    message_res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        msg,
    )
    assert message_res.status_code == 201
    reply_res = api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_headers,
        reply_msg,
    )
    assert reply_res.status_code == 201
    reply_res = api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_headers,
        reply_msg,
    )
    assert reply_res.status_code == 201

    message_res2 = api_post_message(
        mentee2_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee2_headers,
        msg,
    )
    assert message_res2.status_code == 201

    ban_res = api_ban(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_headers,
    )
    assert ban_res.status_code == 200

    ban_res = api_permaban(
        mentor_myuser["user"]["id"],
        mentee2_myuser["user"]["id"],
        mentor_headers,
    )
    assert ban_res.status_code == 200

    res = debug_get(
        f"{API_URL}/stats",
        headers=api_admin_auth_headers(),
    )
    assert res.status_code == 200
    stats = res.json()

    assert stats["totals"] == {
        "admins": {"received": 0, "sent": 0, "banned": 0},
        "mentees": {"received": 2, "sent": 2, "banned": 2},
        "mentors": {"received": 2, "sent": 2, "banned": 0},
    }

    assert stats["stats"][my_user["account"]["id"]] == {
        "received": 0,
        "sent": 0,
        "banned": 0,
    }
    assert stats["stats"][mentee_myuser["account"]["id"]] == {
        "received": 2,
        "sent": 1,
        "banned": 1,
    }
    assert stats["stats"][mentee2_myuser["account"]["id"]] == {
        "received": 0,
        "sent": 1,
        "banned": 1,
    }
    assert stats["stats"][mentor_myuser["account"]["id"]] == {
        "received": 2,
        "sent": 2,
        "banned": 0,
    }
