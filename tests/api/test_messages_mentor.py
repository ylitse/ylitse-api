import httpx
from tests.api.helpers import API_URL
from tests.api.helpers import (
    api_delete_accounts,
    api_create_mentee,
    get_random_mentee,
    get_random_mentor,
    debug_post,
    debug_get,
    debug_put,
    api_post_message,
    api_create_mentor,
    random_string,
    api_auth_header,
    api_access_token,
)


def test_send_a_message_to_a_mentor_or_admin_as_a_mentor():
    api_delete_accounts()

    sender_mentor = get_random_mentor()
    sender_mentor_myuser = api_create_mentor(sender_mentor)
    sender_mentor_headers = api_auth_header(
        api_access_token(sender_mentor["login_name"], sender_mentor["password"])
    )

    recipient_mentor = get_random_mentor()
    recipient_mentor_myuser = api_create_mentor(recipient_mentor)
    recipient_mentor_headers = api_auth_header(
        api_access_token(recipient_mentor["login_name"], recipient_mentor["password"])
    )

    msg = random_string(10)

    send_msg_res = debug_post(
        API_URL + "/users/" + sender_mentor_myuser["user"]["id"] + "/messages",
        headers=sender_mentor_headers,
        json={
            "sender_id": sender_mentor_myuser["user"]["id"],
            "recipient_id": recipient_mentor_myuser["user"]["id"],
            "content": msg,
            "opened": False,
        },
    )
    assert send_msg_res.status_code == 201

    res = httpx.get(
        API_URL + "/users/" + recipient_mentor_myuser["user"]["id"] + "/messages",
        headers=recipient_mentor_headers,
    )

    messages = res.json()["resources"]
    assert len(messages) == 1

    first_msg = messages[0]
    assert len(first_msg.pop("created")) == 32
    assert len(first_msg.pop("updated")) == 32
    assert len(first_msg.pop("id")) == 43

    assert first_msg == {
        "active": True,
        "content": msg,
        "opened": False,
        "recipient_id": recipient_mentor_myuser["user"]["id"],
        "sender_id": sender_mentor_myuser["user"]["id"],
    }


def test_reply_only_to_a_mentees_message_as_a_mentor():
    api_delete_accounts()

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    msg = random_string(10)
    reply_msg = random_string(10)

    res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        msg,
    )
    res = api_post_message(
        mentor_myuser["user"]["id"],
        mentee_myuser["user"]["id"],
        mentor_headers,
        reply_msg,
    )

    res = debug_get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_headers,
    )
    assert res.status_code == 200

    messages = res.json()["resources"]
    assert len(messages) == 2
    assert messages[0]["content"] == msg
    assert messages[0]["sender_id"] == mentee_myuser["user"]["id"]
    assert messages[0]["recipient_id"] == mentor_myuser["user"]["id"]

    assert messages[1]["content"] == reply_msg
    assert messages[1]["sender_id"] == mentor_myuser["user"]["id"]
    assert messages[1]["recipient_id"] == mentee_myuser["user"]["id"]


def test_receive_a_message_from_any_user_as_a_mentor():
    api_delete_accounts()

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    msg = random_string(10)

    res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        msg,
    )

    res = httpx.get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_headers,
    )
    messages = res.json()["resources"]
    assert len(messages) == 1
    assert messages[0]["content"] == msg
    assert messages[0]["sender_id"] == mentee_myuser["user"]["id"]
    assert messages[0]["recipient_id"] == mentor_myuser["user"]["id"]
    assert messages[0]["updated"].endswith("+00:00")
    assert messages[0]["created"].endswith("+00:00")


def test_mark_one_or_more_received_message_as_opened_as_a_mentor():
    api_delete_accounts()

    mentor = get_random_mentor()
    mentor_myuser = api_create_mentor(mentor)
    mentor_headers = api_auth_header(
        api_access_token(mentor["login_name"], mentor["password"])
    )

    mentee = get_random_mentee()
    mentee_myuser = api_create_mentee(mentee)
    mentee_headers = api_auth_header(
        api_access_token(mentee["login_name"], mentee["password"])
    )

    msg = random_string(10)

    res = api_post_message(
        mentee_myuser["user"]["id"],
        mentor_myuser["user"]["id"],
        mentee_headers,
        msg,
    )

    res = httpx.get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_headers,
    )
    messages = res.json()["resources"]
    assert not messages[0]["opened"]

    res = debug_put(
        API_URL
        + "/users/"
        + mentor_myuser["user"]["id"]
        + "/messages/"
        + messages[0]["id"],
        headers=mentor_headers,
        json={"opened": True},
    )
    assert res.status_code == 200

    res = httpx.get(
        API_URL + "/users/" + mentor_myuser["user"]["id"] + "/messages",
        headers=mentor_headers,
    )
    messages = res.json()["resources"]
    assert messages[0]["opened"]
