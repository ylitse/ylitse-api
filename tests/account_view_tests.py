"""Tests for auth."""
from aiohttp.test_utils import (
    AioHTTPTestCase,
    unittest_run_loop,
)
from databases import Database

# pylint: disable=missing-docstring

from ylitse.app import create_app
from ylitse.config import config
from ylitse.postgres_store import PostgresStore

from test_helpers import (
    gen_random_account,
    random_string,
    admin_headers,
    make_env,
)


def gen_identity(account, user):
    return {
        "login_name": account["login_name"],
        "password": random_string(10),
        "role": user["role"],
        "scopes": {
            "account_id": account["id"],
            "user_id": user["id"],
        },
    }


class UserAccountTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_post_mentee(self):
        my_json = {
            "account": gen_random_account(role="mentee"),
            "password": random_string(10),
        }
        resp_okay = await self.client.request("POST", "/accounts", json=my_json)
        self.assertEqual(resp_okay.status, 201)
        resp_json = await resp_okay.json()
        self.assertTrue("account" in resp_json)
        self.assertTrue("user" in resp_json)

        created = await resp_okay.json()
        account, user = created.values()

        resp_conflict = await self.client.request("POST", "/accounts", json=my_json)
        self.assertEqual(resp_conflict.status, 409)

        resp_login = await self.client.request(
            "POST",
            "/login",
            json={
                "login_name": my_json["account"]["login_name"],
                "password": my_json["password"],
            },
        )
        self.assertEqual(resp_login.status, 200)

        my_tokens = (await resp_login.json())["tokens"]
        self.assertTrue("access_token" in my_tokens)
        self.assertTrue("refresh_token" in my_tokens)

        access_token = my_tokens["access_token"]
        header = {"Authorization": f"Bearer {access_token}"}
        resp_del_account = await self.client.request(
            "DELETE", f'/accounts/{resp_json["account"]["id"]}', headers=header
        )
        self.assertEqual(resp_del_account.status, 204)
        resp_not_found = await self.client.request(
            "GET", f'/accounts/{account["id"]}', headers=header
        )
        self.assertEqual(resp_not_found.status, 404)

        resp_not_found = await self.client.request(
            "GET", f'/users/{user["id"]}', headers=header
        )
        self.assertEqual(resp_not_found.status, 404)

    @unittest_run_loop
    async def test_post_admin_and_mentor(self):
        my_json = {
            "account": {**gen_random_account(role="admin")},
            "password": random_string(20),
        }
        resp = await self.client.request(
            "POST",
            "/accounts",
            json=my_json,
            **admin_headers(),
        )
        self.assertEqual(resp.status, 201)

    @unittest_run_loop
    async def test_post_mentor(self):
        my_json = {
            "account": {**gen_random_account(role="mentor")},
            "password": random_string(20),
        }
        resp = await self.client.request(
            "POST",
            "/accounts",
            json=my_json,
            **admin_headers(),
        )
        self.assertEqual(resp.status, 201)

    @unittest_run_loop
    async def test_set_password(self):
        my_json = {
            "account": {**gen_random_account(role="mentor")},
            "password": random_string(20),
        }
        resp = await self.client.request(
            "POST",
            "/accounts",
            json=my_json,
            **admin_headers(),
        )
        self.assertEqual(resp.status, 201)
        resp_json = await resp.json()
        account_id = resp_json["account"]["id"]

        new_pass_json = {
            "login_name": my_json["account"]["login_name"],
            "password": "secret",
        }

        print(new_pass_json)

        resp = await self.client.request(
            "PUT",
            f"/accounts/{account_id}/password",
            json=new_pass_json,
            **admin_headers(),
        )
        self.assertEqual(resp.status, 200)
        resp = await self.client.request(
            "PUT",
            f"/accounts/{account_id}",
            json={
                "login_name": my_json["account"]["login_name"],
                "password": "secret",
            },
        )
        self.assertEqual(resp.status, 401)

    @unittest_run_loop
    async def test_delete_account(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            idm, uam, jwt, sm = make_env(store)

            account_json = gen_random_account(role="mentee")
            my_json = {
                "account": account_json,
                "user": {
                    "display_name": account_json["login_name"],
                    "role": "mentee",
                },
            }
            user_account = await uam.create_user_account(my_json)

            account = user_account["account"]
            account_id = account["id"]
            user = user_account["user"]

            identity = gen_identity(account, user)
            await idm.new(**identity)

            tokens1 = (await sm.new(identity, "mobile"))["tokens"]
            tokens1 = {k: jwt.decode(tokens1[k]) for k in tokens1.keys()}
            tokens2 = (await sm.new(identity, "mobile"))["tokens"]

            accounts = await store.read("ACCOUNT", id=account_id)
            self.assertEqual(1, len(accounts))
            users = await store.read("USER", account_id=account_id)
            self.assertEqual(1, len(users))
            sessions = await store.read("SESSION", account_id=account_id)
            self.assertEqual(2, len(sessions))

            await uam.delete_user_account(account_id)

            accounts = await store.read("ACCOUNT", id=account_id)
            self.assertEqual(0, len(accounts))
            users = await store.read("USER", account_id=account_id)
            self.assertEqual(0, len(users))
            sessions = await store.read("SESSION", account_id=account_id)
            self.assertEqual(0, len(sessions))
