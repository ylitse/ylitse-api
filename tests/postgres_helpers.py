""" ylitse/db/postgres_helpers.py tests."""

from datetime import datetime, timezone

import time_machine

from aiounittest import AsyncTestCase

from ylitse.db.postgres_helpers import generate_utc_now_timestamp


class PostgresHelpersTest(AsyncTestCase):
    @time_machine.travel(datetime(2021, 1, 2, hour=0, minute=2, tzinfo=timezone.utc))
    def test_daterange_inclusive(self):
        self.assertEqual(generate_utc_now_timestamp(), 1609545720)
