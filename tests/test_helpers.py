import json
import os
import random
import string
from datetime import datetime
from pathlib import Path

from ylitse.config import config
from ylitse.crud import crud_message
from ylitse.identity_manager import IdentityManager
from ylitse.jwt_functions import JwtFunctions
from ylitse.session_manager import SessionManager
from ylitse.store_abstraction import new_ylitse_id
from ylitse.symmetric_crypto_functions import SymmetricCryptoFunctions
from ylitse.user_account_manager import UserAccountManager


FIXTURE_PATH = Path(os.path.realpath(__file__)).parent.absolute()


def make_env(store):
    idm = IdentityManager(store)
    uam = UserAccountManager(store, idm)
    jwt = JwtFunctions("secret")
    sm = SessionManager(store, idm, jwt)
    return (idm, uam, jwt, sm)


def symmetric_crypto_functions():
    return SymmetricCryptoFunctions(config["encryption_key_file"])


def from_iso_format_to_utc(datetime_string):
    """Convert datetime from isoformat string to utc object"""
    return datetime.fromisoformat(datetime_string)


def wo_store_stamps(d):
    return {
        x: d[x] for x in d.keys() if x not in ["id", "created", "updated", "active"]
    }


def random_string(n):
    return "".join(random.choices(string.ascii_uppercase + string.digits, k=n))


class dummy_request:
    def __init__(self, json=None, match_info=None, method=None, headers=None):
        self.data = json
        self.method = method
        self.match_info = match_info
        self.headers = headers

    async def json(self):
        return self.data


def gen_random_skill():
    return {
        "name": random_string(8),
    }


def gen_random_account(role="mentee"):
    return {
        "login_name": random_string(8),
        "email": random_string(10),
        "phone": random_string(10),
        "role": role,
    }


def gen_random_user(role="mentee"):
    return {
        "account_id": new_ylitse_id(),
        "display_name": random_string(10),
        "role": role,
    }


def gen_random_mentor():
    return {
        "account_id": new_ylitse_id(),
        "user_id": new_ylitse_id(),
        "languages": ["fi", "sv"],
        "birth_year": 1990,
        "display_name": random_string(10),
        "gender": "other",
        "region": "HKI",
        "story": random_string(10),
        "skills": ["asdf", "dfga"],
        "communication_channels": ["phone", "email"],
    }


def gen_random_message():
    return {
        "recipient_id": new_ylitse_id(),
        "sender_id": new_ylitse_id(),
        "content": random_string(10),
        "opened": False,
    }


def gen_random_private_data():
    return {"starred_users": [new_ylitse_id()]}


def gen_random(data_type=None):
    return {
        "ACCOUNT": gen_random_account,
        "USER": gen_random_user,
        "MENTOR": gen_random_mentor,
        "SKILL": gen_random_skill,
        "MESSAGE": gen_random_message,
        "PRIVATE_DATA": gen_random_private_data,
    }[data_type]()


def admin_headers():
    secret = config["jwt_secret"]
    jwt = JwtFunctions(secret)
    access_token = jwt.access_token(
        {
            "role": "admin",
            "scopes": "Does not matter for this one.",
            "login_name": "janteri",
        },
        {
            "id": "Is not checked",
        },
    )
    return {"headers": {"Authorization": f"Bearer {access_token}"}}


async def create_non_mentor(store, *, role):
    idm, uam, jwt, sm = make_env(store)

    account_json = gen_random_account(role=role)
    my_json = {
        "account": account_json,
        "user": {
            "display_name": account_json["login_name"],
            "role": role,
        },
    }
    return await uam.create_user_account(my_json)


async def create_mentor(store, *, role):
    idm, uam, jwt, sm = make_env(store)

    account_json = gen_random_account(role=role)
    my_json = {
        "password": random_string(10),
        "account": account_json,
        "user": {
            "display_name": account_json["login_name"],
            "role": role,
        },
        "mentor": {
            "languages": ["fi"],
            "birth_year": 1980,
            "display_name": account_json["login_name"],
            "gender": "female",
            "region": "Kontula",
            "story": "My story",
            "skills": [],
            "communication_channels": [],
        },
    }
    return await uam.create_mentor(my_json)


async def post_message(store, *, sender, recipient, content):
    return await crud_message.post(
        store,
        data={
            "sender_id": sender["id"],
            "recipient_id": recipient["id"],
            "content": content,
        },
        sender_role=sender["role"],
    )


async def delete_account(store, *, account_id):
    idm, uam, jwt, sm = make_env(store)
    await uam.delete_user_account(account_id)


def read_json(filename):
    with open(filename, "r") as f:
        return json.load(f)
