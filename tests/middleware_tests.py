from unittest.mock import patch

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from ylitse.app import create_app


class MiddlewareTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_error_404_handler(self):
        res = await self.client.request("GET", "/hep")

        self.assertEqual(res.status, 404)

    @unittest_run_loop
    async def test_original_handler(self):
        with patch("ylitse.middleware.ERROR_HANDLERS", {}):
            res = await self.client.request("GET", "/hep")

            self.assertEqual(res.status, 404)
