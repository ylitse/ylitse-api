"""Tests for crypto."""

from copy import deepcopy

from unittest import TestCase

from test_helpers import symmetric_crypto_functions


class SymmetricCryptoFunctionsTests(TestCase):
    def test_encrypt_decrypt_string(self):
        plaintexts = [
            "Hi yöû ส ✋ 𓃠",
            b"\xf0\x93\x83\xa0".decode("utf-8") * 5000,
            "",
        ]
        crypto = symmetric_crypto_functions()

        for plaintext in plaintexts:
            encrypted = crypto.encrypt_string(plaintext)

            # Do we get same content back
            self.assertEqual(crypto.decrypt_to_string(encrypted), plaintext)

    def test_encrypt_decrypt_dict(self):
        original = {
            "msg": "Hi yöû\nส ✋",
            "dont_touch_this": "should not change this",
        }
        crypto = symmetric_crypto_functions()

        # Make a copy that we can mutate
        a_dict = deepcopy(original)

        # encrypt "msg" to "encrypted_msg"
        crypto.encrypt_and_rename_item(
            a_dict, decrypted_key="msg", encrypted_key="encrypted_msg"
        )
        self.assertEqual(len(original), len(a_dict))  # size should not change
        self.assertIn("encrypted_msg", a_dict)
        self.assertNotIn("msg", a_dict)  # plaintext version should be gone
        self.assertEqual(a_dict["dont_touch_this"], original["dont_touch_this"])

        # Do we get same content back
        crypto.decrypt_and_rename_item(
            a_dict, decrypted_key="msg", encrypted_key="encrypted_msg"
        )
        self.assertEqual(a_dict, original)
