"""Tests for jwt."""
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web

from ylitse.app import create_app
from ylitse.jwt_functions import JwtFunctions


class JwtFunctionsTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_jwt_encode_decode(self):
        jwt = JwtFunctions("SECRET")
        payload = {"some": "stuff"}
        encoded = jwt.encode(payload)
        self.assertTrue(isinstance(encoded, str))
        decoded = jwt.decode(encoded)
        self.assertTrue(isinstance(decoded, dict))
        self.assertEqual(payload, decoded)

        with self.assertRaises(web.HTTPUnauthorized):
            jwt.decode("asdasdfasdf")
