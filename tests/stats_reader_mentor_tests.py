""" ylitse/stats_reader_mentors.py tests."""
# pylint: disable=too-many-function-args


from datetime import datetime, date

from pytz import timezone as pytz_timezone
import time_machine
from databases import Database

from aiounittest import AsyncTestCase
from tests.test_helpers import (
    create_mentor,
    create_non_mentor,
    post_message,
    delete_account,
)

from ylitse.config import config
from ylitse.crud.stat_event import crud_stat_event
from ylitse.postgres_store import PostgresStore
from ylitse.stats_reader_mentors import (
    calc_profile_views,
    calc_chat_stats,
    StatsReaderMentors,
)
from ylitse.stats_collect import anonymize_id
from ylitse.crud import crud_mentor, crud_stat_mentor


timezone_helsinki = pytz_timezone("Europe/Helsinki")
timezone_utc = pytz_timezone("UTC")


class StatsReaderMentorsTest(AsyncTestCase):
    maxDiff = 5000

    def csv_header(self):
        return (
            ",".join(
                [
                    "Mentor",
                    "Created UTC",
                    "Deleted UTC",
                    "Last seen UTC",
                    "Profile views",
                    "New chats",
                    "Active chats",
                    "Cumulative chats",
                    "Avg msg count / chat / period",
                    "Avg msg count / chat / total",
                    "Avg first response time / period",
                    "Avg first response time / total",
                    "Unanswered / period",
                    "Unanswered / total",
                ]
            )
            + "\r\n"
        )

    async def insert_test_data(self, store):
        await store.truncatedb()

        mentors = []
        mentee = None

        # 2021-05-01 15.30
        # Create 5 mentors, one mentee
        # mentee sends message to mentor[1] (1msg in conv/total)
        with time_machine.travel(
            timezone_helsinki.localize(
                datetime(
                    2021,
                    1,
                    5,
                    hour=15,
                    minute=30,
                    second=0,
                    microsecond=0,
                )
            ),
            tick=False,
        ):
            mentors.append(await create_mentor(store, role="mentor"))
            mentors.append(await create_mentor(store, role="mentor"))
            mentors.append(await create_mentor(store, role="mentor"))
            mentors.append(await create_mentor(store, role="mentor"))
            mentors.append(await create_mentor(store, role="mentor"))
            mentors.sort(key=lambda x: x["mentor"]["display_name"])

            mentee = await create_non_mentor(store, role="mentee")
            await crud_mentor.update_last_seen(
                store, mentor_id=mentors[1]["mentor"]["id"]
            )
            await crud_stat_mentor.refresh(store, mentor_id=mentors[0]["mentor"]["id"])
            await post_message(
                store,
                sender=mentee["user"],
                recipient=mentors[1]["user"],
                content="I wanted to talk to you about",
            )  # noqa

        # 2021-05-01 17.30
        # mentor[1] sends message to mentee (2msg in conv/total)
        with time_machine.travel(
            timezone_utc.localize(
                datetime(
                    2021,
                    1,
                    5,
                    hour=17,
                    minute=31,
                    second=0,
                )
            ),
            tick=False,
        ):
            await post_message(
                store,
                sender=mentors[1]["user"],
                recipient=mentee["user"],
                content="Alright, what did you have on mind",
            )  # noqa

        # 2021-06-01 17.30
        # mentee sends message to mentor[0] (1msg in conv/total)
        # mentor[1] ends message to mentee (3msg in conv/total)
        with time_machine.travel(
            timezone_utc.localize(
                datetime(
                    2021,
                    1,
                    6,
                    hour=17,
                    minute=30,
                    second=0,
                )
            ),
            tick=False,
        ):
            await post_message(
                store,
                sender=mentee["user"],
                recipient=mentors[0]["user"],
                content="Hi!",
            )  # noqa
            await post_message(
                store,
                sender=mentee["user"],
                recipient=mentors[1]["user"],
                content="I have this thing",
            )  # noqa

        # 2021-06-01 17.33
        # mentee sends message to mentor[0] (2msg in conv/total)
        # mentee sends message to mentor[2] (1msg in conv/total)
        with time_machine.travel(
            timezone_utc.localize(
                datetime(
                    2021,
                    1,
                    6,
                    hour=17,
                    minute=33,
                    second=0,
                )
            ),
            tick=False,
        ):
            await post_message(
                store,
                sender=mentee["user"],
                recipient=mentors[0]["user"],
                content="Why no response?",
            )  # noqa
            await post_message(
                store,
                sender=mentee["user"],
                recipient=mentors[2]["user"],
                content="No response chat",
            )  # noqa

            await crud_stat_event.create_all(
                store,
                events=[
                    {
                        "name": "open_mentor_profile",
                        "props": {"mentor_id": mentors[0]["mentor"]["id"]},
                    },  # noqa
                    {
                        "name": "open_mentor_profile",
                        "props": {"mentor_id": mentors[0]["mentor"]["id"]},
                    },  # noqa
                ],
            )

        # 2021-07-01 20.30
        # mentor[0] sends message to mentee (3msg in conv/total)
        # mentor[1] sends message to mentee (4msg in conv/total)
        with time_machine.travel(
            timezone_utc.localize(
                datetime(
                    2021,
                    1,
                    7,
                    hour=20,
                    minute=30,
                    second=0,
                )
            ),
            tick=False,
        ):
            await crud_mentor.update_last_seen(
                store, mentor_id=mentors[0]["mentor"]["id"]
            )
            await crud_stat_mentor.refresh(store, mentor_id=mentors[0]["mentor"]["id"])
            await post_message(
                store,
                sender=mentors[0]["user"],
                recipient=mentee["user"],
                content="Hi!",
            )  # noqa
            await post_message(
                store,
                sender=mentors[1]["user"],
                recipient=mentee["user"],
                content="That does not sound easy",
            )  # noqa

            await crud_mentor.update_last_seen(
                store, mentor_id=mentors[4]["mentor"]["id"]
            )
            await crud_stat_mentor.refresh(store, mentor_id=mentors[4]["mentor"]["id"])

            await crud_stat_event.create_all(
                store,
                events=[
                    {
                        "name": "open_mentor_profile",
                        "props": {"mentor_id": mentors[1]["mentor"]["id"]},
                    },  # noqa
                ],
            )
            await crud_stat_event.create_all(
                store,
                events=[
                    {
                        "name": "open_mentor_profile",
                        "props": {"mentor_id": mentors[4]["mentor"]["id"]},
                    },  # noqa
                ],
            )

        # 2021-08-01 20.30
        # mentor[1] sends message to mentee (5msg in conv/total)
        # mentor[4] profile deleted
        with time_machine.travel(
            timezone_utc.localize(
                datetime(
                    2021,
                    1,
                    8,
                    hour=20,
                    minute=30,
                    second=0,
                    microsecond=0,
                )
            ),
            tick=False,
        ):
            await crud_mentor.update_last_seen(
                store, mentor_id=mentors[1]["mentor"]["id"]
            )
            await crud_stat_mentor.refresh(store, mentor_id=mentors[1]["mentor"]["id"])
            await post_message(
                store,
                sender=mentors[1]["user"],
                recipient=mentee["user"],
                content="Hi!",
            )  # noqa

            await delete_account(store, account_id=mentors[4]["account"]["id"])

        # 2022-01-01 20.30
        # mentee sends message to mentor[3] (1msg in conv/total)
        # mentor sends message to mentee (2msg in conv/total)
        with time_machine.travel(
            timezone_utc.localize(
                datetime(
                    2022,
                    1,
                    1,  # much later
                    hour=20,
                    minute=30,
                    second=0,
                    microsecond=0,
                )
            ),
            tick=False,
        ):
            await crud_mentor.update_last_seen(
                store, mentor_id=mentors[2]["mentor"]["id"]
            )
            await crud_stat_mentor.refresh(store, mentor_id=mentors[2]["mentor"]["id"])
            await post_message(
                store,
                sender=mentee["user"],
                recipient=mentors[3]["user"],
                content="Hi!",
            )  # noqa
            await post_message(
                store,
                sender=mentors[3]["user"],
                recipient=mentee["user"],
                content="Hi!",
            )  # noqa

        return mentee, mentors

    async def test_calc_profile_views(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    mentee, mentors = await self.insert_test_data(store)

                    period_start_date = date(2021, 1, 1)
                    period_end_date = date(2021, 2, 1)

                    first_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_start_date.year,
                                period_start_date.month,
                                period_start_date.day,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        ).timestamp()
                    )

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    res = await calc_profile_views(
                        database, first_of_period_timestamp, last_of_period_timestamp
                    )
                    self.assertEqual(
                        res,
                        {
                            anonymize_id(mentors[0]["mentor"]["id"]): 2,
                            anonymize_id(mentors[1]["mentor"]["id"]): 1,
                            anonymize_id(mentors[4]["mentor"]["id"]): 1,
                        },
                    )

    async def test_calc_chat_stats(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    mentee, mentors = await self.insert_test_data(store)

                    res = await calc_chat_stats(database)
                    self.assertEqual(len(res), 4)

                    self.assertDictEqual(
                        res[
                            (
                                anonymize_id(mentee["user"]["id"])
                                + anonymize_id(mentors[0]["user"]["id"])
                            )
                        ].dict(),
                        {
                            "chat_length": 3,
                            "first_created": 1609954200,
                            "anon_mentee_user_id": anonymize_id(mentee["user"]["id"]),
                            "anon_mentor_user_id": anonymize_id(
                                mentors[0]["user"]["id"]
                            ),
                            "message_events": [1609954380, 1609954200, 1610051400],
                            "response_created": 1610051400,
                        },
                    )

                    self.assertDictEqual(
                        res[
                            (
                                anonymize_id(mentee["user"]["id"])
                                + anonymize_id(mentors[1]["user"]["id"])
                            )
                        ].dict(),
                        {
                            "chat_length": 5,
                            "first_created": 1609853400,
                            "anon_mentee_user_id": anonymize_id(mentee["user"]["id"]),
                            "anon_mentor_user_id": anonymize_id(
                                mentors[1]["user"]["id"]
                            ),
                            "message_events": [
                                1609954200,
                                1609853400,
                                1610137800,
                                1610051400,
                                1609867860,
                            ],
                            "response_created": 1609867860,
                        },
                    )

                    self.assertDictEqual(
                        res[
                            (
                                anonymize_id(mentee["user"]["id"])
                                + anonymize_id(mentors[2]["user"]["id"])
                            )
                        ].dict(),
                        {
                            "chat_length": 1,
                            "first_created": 1609954380,
                            "anon_mentee_user_id": anonymize_id(mentee["user"]["id"]),
                            "anon_mentor_user_id": anonymize_id(
                                mentors[2]["user"]["id"]
                            ),
                            "message_events": [1609954380],
                            "response_created": 0,
                        },
                    )

                    self.assertDictEqual(
                        res[
                            (
                                anonymize_id(mentee["user"]["id"])
                                + anonymize_id(mentors[3]["user"]["id"])
                            )
                        ].dict(),
                        {
                            "chat_length": 2,
                            "first_created": 1641069000,
                            "anon_mentee_user_id": anonymize_id(mentee["user"]["id"]),
                            "anon_mentor_user_id": anonymize_id(
                                mentors[3]["user"]["id"]
                            ),
                            "message_events": [1641069000, 1641069000],
                            "response_created": 1641069000,
                        },
                    )

    async def test_period_csv(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    mentee, mentors = await self.insert_test_data(store)

                    period_start_date = date(2021, 1, 6)
                    period_end_date = date(2021, 1, 7)

                    res = await StatsReaderMentors().period_csv(
                        store, period_start_date, period_end_date
                    )

                    self.assertEqual(
                        res,
                        self.csv_header()
                        + f'{mentors[0]["mentor"]["display_name"]},2021-01-05 13:30:00,,2021-01-07 20:30:00,2,1,1,1,3,3,"1 day, 3:00:00","1 day, 3:00:00",0,0\r\n'
                        + f'{mentors[1]["mentor"]["display_name"]},2021-01-05 13:30:00,,2021-01-08 20:30:00,1,0,1,1,2,4,,4:01:00,0,0\r\n'
                        + f'{mentors[2]["mentor"]["display_name"]},2021-01-05 13:30:00,,2022-01-01 20:30:00,0,1,1,1,1,1,,,1,1\r\n'
                        + f'{mentors[3]["mentor"]["display_name"]},2021-01-05 13:30:00,,,0,0,0,0,0,0,,,0,0\r\n'
                        + f'{mentors[4]["mentor"]["display_name"]},2021-01-05 13:30:00,2021-01-08 20:30:00,2021-01-07 20:30:00,1,0,0,0,0,0,,,0,0\r\n',
                    )
