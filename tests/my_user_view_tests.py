"""myuser endpoint tests."""
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from ylitse.app import create_app

from test_helpers import (
    gen_random_account,
    random_string,
)


class MyUserViewTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_mentee_myUser(self):
        my_json = {
            "account": gen_random_account(role="mentee"),
            "password": random_string(10),
        }
        resp = await self.client.request("POST", "/accounts", json=my_json)
        self.assertEqual(resp.status, 201)
        user_and_account = await resp.json()
        resp = await self.client.request(
            "POST",
            "/login",
            json={
                "login_name": my_json["account"]["login_name"],
                "password": my_json["password"],
            },
        )
        access_token = (await resp.json())["tokens"]["access_token"]
        resp = await self.client.request(
            "GET", "/myuser", headers={"Authorization": f"Bearer {access_token}"}
        )
        self.assertEqual(resp.status, 200)
        self.assertEqual(user_and_account, await resp.json())
