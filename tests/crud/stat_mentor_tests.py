"""Tests for postgres store."""
from datetime import datetime
from pytz import timezone as pytz_timezone
from dateutil.relativedelta import relativedelta

from aiounittest import AsyncTestCase
from databases import Database
import time_machine

from ylitse.config import config
from ylitse.postgres_store import PostgresStore
from ylitse.crud import crud_mentor, crud_stat_mentor
from ylitse.crud.stat_mentor import years_ago_unix_utc_timestamp
from ylitse.stats_collect import anonymize_id, from_iso_format_to_utc_timestamp

from tests.test_helpers import create_mentor, delete_account

timezone_utc = pytz_timezone("UTC")


class CrudStatMentorTest(AsyncTestCase):
    async def test_last_seen(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            store = PostgresStore(database)

            resources = await create_mentor(store, role="mentor")

            mentor = await crud_mentor.get_by_id(
                store, mentor_id=resources["mentor"]["id"]
            )
            self.assertTrue("last_seen" not in mentor)

            # check that the stat row was created also
            stat_mentor = await crud_stat_mentor.get_by_id(
                store, mentor_id=mentor["id"]
            )
            self.assertEqual(anonymize_id(mentor["id"]), stat_mentor["id"])
            self.assertEqual(anonymize_id(mentor["user_id"]), stat_mentor["user_id"])
            self.assertEqual(mentor["display_name"], stat_mentor["display_name"])
            self.assertTrue("last_seen" not in stat_mentor)
            self.assertTrue("deleted" not in stat_mentor)
            self.assertEqual(
                from_iso_format_to_utc_timestamp(mentor["created"]),
                stat_mentor["created"],
            )

            # Check that last_seen is updated
            now = datetime(
                2021,
                1,
                6,
                hour=15,
                minute=30,
                second=0,
                microsecond=0,
            )

            with time_machine.travel(timezone_utc.localize(now), tick=False):
                await crud_mentor.update_last_seen(store, mentor_id=mentor["id"])
                await crud_stat_mentor.refresh(store, mentor_id=mentor["id"])

            mentor = await crud_mentor.get_by_id(
                store, mentor_id=resources["mentor"]["id"]
            )
            self.assertEqual(mentor["last_seen"], now.isoformat())

            # check that the stat row was updated
            stat_mentor = await crud_stat_mentor.get_by_id(
                store, mentor_id=mentor["id"]
            )
            self.assertEqual(
                from_iso_format_to_utc_timestamp(mentor["last_seen"]),
                stat_mentor["last_seen"],
            )

    async def test_old_data_anonymization(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            store = PostgresStore(database)

            resources = await create_mentor(store, role="mentor")

            mentor = await crud_mentor.get_by_id(
                store, mentor_id=resources["mentor"]["id"]
            )

            await delete_account(store, account_id=resources["account"]["id"])

            # recently deleted data should still have a display_name
            recent_stat_mentor = await crud_stat_mentor.get_by_id(
                store, mentor_id=mentor["id"]
            )
            self.assertEqual(recent_stat_mentor["display_name"], mentor["display_name"])

            # Fast forward little
            # run clean up and display_name should still be there
            month_from_now = datetime.now(timezone_utc) + relativedelta(months=1)

            with time_machine.travel(month_from_now, tick=False):
                await crud_stat_mentor.anonymize_old_data(store)

                old_stat_mentor = await crud_stat_mentor.get_by_id(
                    store, mentor_id=mentor["id"]
                )
                self.assertEqual(
                    old_stat_mentor["display_name"], mentor["display_name"]
                )

            # Fast forward 1 year, run clean up and display_name should be gone
            year_from_now = datetime.now(timezone_utc) + relativedelta(years=1)

            with time_machine.travel(year_from_now, tick=False):
                await crud_stat_mentor.anonymize_old_data(store)

                really_old_stat_mentor = await crud_stat_mentor.get_by_id(
                    store, mentor_id=mentor["id"]
                )
                self.assertEqual(
                    really_old_stat_mentor["display_name"],
                    "",
                )

    def test_year_ago(self):
        now = datetime(
            2022,
            1,
            6,
            hour=15,
            minute=30,
            second=0,
            microsecond=0,
            tzinfo=timezone_utc,
        )
        # for these calculations we only change year number
        year_ago = datetime(
            2021,
            1,
            6,
            hour=15,
            minute=30,
            second=0,
            microsecond=0,
            tzinfo=timezone_utc,
        )
        self.assertEqual((now - year_ago).days, 365)  # not leap year

        with time_machine.travel(now, tick=False):
            self.assertEqual(
                years_ago_unix_utc_timestamp(years=1),
                round(year_ago.timestamp()),
            )
            self.assertEqual(
                timezone_utc.localize(
                    datetime.utcfromtimestamp(years_ago_unix_utc_timestamp(years=1))
                ),
                year_ago,
            )

    def test_year_ago_leap_year(self):
        now = datetime(
            2024,
            6,
            20,
            hour=15,
            minute=30,
            second=0,
            microsecond=0,
            tzinfo=timezone_utc,
        )
        # for these calculations we only change year number
        year_ago = datetime(
            2023,
            6,
            20,
            hour=15,
            minute=30,
            second=0,
            microsecond=0,
            tzinfo=timezone_utc,
        )
        self.assertEqual((now - year_ago).days, 366)  # is leap year

        with time_machine.travel(now, tick=False):
            self.assertEqual(
                years_ago_unix_utc_timestamp(years=1),
                round(year_ago.timestamp()),
            )
