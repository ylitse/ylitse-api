"""Tests for postgres store."""
from datetime import datetime
from pytz import timezone as pytz_timezone

from aiounittest import AsyncTestCase
from databases import Database
import time_machine

from ylitse.config import config
from ylitse.postgres_store import PostgresStore
from ylitse.crud import crud_mentor, crud_stat_mentor

from tests.test_helpers import create_mentor

timezone_utc = pytz_timezone("UTC")


class CrudMentorTest(AsyncTestCase):
    async def test_last_seen(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            store = PostgresStore(database)

            resources = await create_mentor(store, role="mentor")

            mentor = await crud_mentor.get_by_id(
                store, mentor_id=resources["mentor"]["id"]
            )

            # last_seen should be missing until mentor logs in
            self.assertTrue("last_seen" not in mentor)

            # Check that last_seen is updated
            now = datetime(
                2021,
                1,
                6,
                hour=15,
                minute=30,
                second=0,
                microsecond=0,
            )

            with time_machine.travel(timezone_utc.localize(now), tick=False):
                await crud_mentor.update_last_seen(store, mentor_id=mentor["id"])
                await crud_stat_mentor.refresh(store, mentor_id=mentor["id"])

            mentor = await crud_mentor.get_by_id(
                store, mentor_id=resources["mentor"]["id"]
            )
            self.assertEqual(mentor["last_seen"], now.isoformat())
