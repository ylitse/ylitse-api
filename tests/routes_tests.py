import unittest
import os

import aiohttp

from ylitse.app import create_app

SAFETY_FEATURE = os.getenv("SAFETY_FEATURE", "false")


class RoutesTest(unittest.TestCase):
    def test_added_resources(self):
        resources = {
            "/login",
            "/weblogin",
            "/webrefresh",
            "/logout",
            "/refresh",
            "/users",
            "/version",
            "/version/clients",
            "/accounts",
            "/skills",
            "/mentors",
            "/myuser",
            "/search",
            "/stats",
            "/events",
            "/feedback/questions",
            "/feedback/answer",
            "/feedback/answers",
            "/feedback/needs_answers",
        }

        if SAFETY_FEATURE == "true":
            additional_resources = {"/reports", "/chat_review_logs"}
            resources.update(additional_resources)

        app = create_app()
        res = app.router.resources()
        pls = list(filter(lambda x: isinstance(x, aiohttp.web.PlainResource), res))
        new_resources = set(map(lambda x: str(x.url_for()), pls))

        self.assertEqual(new_resources, resources)
