"""Tests for localstore."""
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web

from ylitse.app import create_app
from ylitse.store_abstraction import StoreView
from ylitse.local_store import LocalStore
from test_helpers import random_string


class LocalStoreTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_local_store(self):
        db = StoreView(LocalStore("/tmp/ylitse_test"), "SKILL")

        random_name = random_string(10)
        skill = {
            "name": random_name,
        }

        dict_from_create = await db.create(skill)
        self.assertEqual("id" in dict_from_create, True)
        self.assertEqual("created" in dict_from_create, True)
        self.assertEqual("updated" in dict_from_create, True)

        self.assertEqual(1, len(await db.read()))

        # Dublicate crashes the thing
        with self.assertRaises(web.HTTPConflict):
            await db.create(skill)

        id_ = dict_from_create["id"]
        dict_from_read = next(iter(await db.read(id=id_)))
        self.assertEqual(dict_from_create, dict_from_read)

        updated_dict = {**dict_from_create, "d": random_string(10)}
        await db.update(updated_dict)

        updated_dict_from_read = next(iter(await db.read(id=id_)))
        self.assertEqual(updated_dict, updated_dict_from_read)

        all_after_update = await db.read()
        self.assertEqual(1, len(all_after_update))

        await db.delete(id=id_)
        all_after_delete = await db.read()
        self.assertEqual(0, len(all_after_delete))

    @unittest_run_loop
    async def test_local_store_wo_uniqs(self):
        db = StoreView(LocalStore("/tmp/ylitse_test"), "MESSAGE")
        msg = {
            "content": random_string(10),
            "recipient_id": random_string(10),
            "sender_id": random_string(10),
        }
        await db.create(msg)
        await db.create(msg)
        self.assertEqual(2, len(await db.read()))
        await db.delete(content=msg["content"])
        self.assertEqual(0, len(await db.read()))
