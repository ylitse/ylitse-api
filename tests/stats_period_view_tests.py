""" ylitse/stats_period_view.py tests."""


from datetime import date

from aiounittest import AsyncTestCase

from ylitse.stats_period_view import parse_period


class StatsPeriodViewTest(AsyncTestCase):
    def test_parse_period(self):
        self.assertEqual(
            parse_period("2021-12-31", "2022-03-31"),
            (date(2021, 12, 31), date(2022, 3, 31)),
        )

    def test_parse_period_error(self):
        with self.assertRaises(ValueError):
            parse_period("2021-02-31", "2022-03-31")
