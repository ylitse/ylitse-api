"""Login endpoint tests."""
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from ylitse.app import create_app  # noqa, pylint: disable=wrong-import-position

# pylint: disable=missing-docstring


class AccessControlTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    async def check_get(self, path):
        resp = await self.client.request(
            "GET",
            path,
        )
        self.assertEqual(resp.status, 401)

    @unittest_run_loop
    async def test_routes(self):
        await self.check_get("accounts")
        await self.check_get("accounts/asdf")
        await self.check_get("users")
        await self.check_get("users/asdf/messages")
        await self.check_get("users/asdf/private_data")
        await self.check_get("users/asdf")
        await self.check_get("version")
