"""Login endpoint tests."""
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from ylitse.app import create_app
from test_helpers import (
    gen_random_account,
    random_string,
)

# pylint: disable=missing-docstring


class LoginTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_mentee_login(self):
        my_json = {
            "account": gen_random_account(role="mentee"),
            "password": random_string(10),
        }
        req = await self.client.request("POST", "/accounts", json=my_json)
        self.assertEqual(req.status, 201)
        my_user_and_account = await req.json()
        account = my_user_and_account["account"]

        resp_login = await self.client.request(
            "POST",
            "/login",
            json={
                "login_name": my_json["account"]["login_name"],
                "password": "wrong password",
            },
        )
        self.assertEqual(resp_login.status, 400)

        resp_login = await self.client.request(
            "POST",
            "/login",
            json={
                "login_name": my_json["account"]["login_name"],
                "password": my_json["password"],
            },
        )
        self.assertEqual(resp_login.status, 200)
        my_tokens = (await resp_login.json())["tokens"]

        self.assertTrue("access_token" in my_tokens)
        self.assertTrue("refresh_token" in my_tokens)

        access_token = my_tokens["access_token"]

        self.assertTrue(isinstance(access_token, str))

        header = {"Authorization": f"Bearer {access_token}"}
        resp_get_my_account = await self.client.request(
            "GET", f"accounts/{account['id']}", headers=header
        )
        self.assertEqual(resp_get_my_account.status, 200)

        resp_get_my_account = await self.client.request(
            "GET", "accounts/1234sdf", headers=header
        )
        self.assertEqual(resp_get_my_account.status, 401)

        resp_get_my_account = await self.client.request(
            "GET", "accounts", headers=header
        )
        self.assertEqual(resp_get_my_account.status, 401)

    @unittest_run_loop
    async def test_mentee_change_password(self):
        my_json = {
            "account": gen_random_account(role="mentee"),
            "password": random_string(10),
        }
        resp_create_mentee = await self.client.request(
            "POST", "/accounts", json=my_json
        )
        account = (await resp_create_mentee.json())["account"]

        self.assertEqual(resp_create_mentee.status, 201)

        resp_login = await self.client.request(
            "POST",
            "/login",
            json={
                "login_name": my_json["account"]["login_name"],
                "password": my_json["password"],
            },
        )
        self.assertEqual(resp_login.status, 200)

        my_tokens = (await resp_login.json())["tokens"]
        access_token = my_tokens["access_token"]

        resp = await self.client.request(
            "PUT",
            f"accounts/{account['id']}/password",
            headers={"Authorization": f"Bearer {access_token}"},
            json={
                "current_password": my_json["password"],
                "new_password": "secret",
            },
        )

        self.assertEqual(resp.status, 200)

        resp_login = await self.client.request(
            "POST",
            "/login",
            json={
                "login_name": my_json["account"]["login_name"],
                "password": "secret",
            },
        )
        self.assertEqual(resp_login.status, 200)
