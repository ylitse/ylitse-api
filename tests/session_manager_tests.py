"""Tests for auth."""
import pyotp
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web
from databases import Database

from ylitse.config import config
from ylitse.app import create_app
from ylitse.utils import fst
from ylitse.identity_manager import IdentityManager
from ylitse.jwt_functions import JwtFunctions
from ylitse.session_manager import SessionManager
from ylitse.postgres_store import PostgresStore

from test_helpers import random_string, wo_store_stamps


def setup_store_and_auth(store):
    idm = IdentityManager(store)
    jwt = JwtFunctions("secret")
    sm = SessionManager(store, idm, jwt)
    return (idm, jwt, sm)


def gen_admin_identity():
    return {
        "login_name": random_string(10),
        "password": random_string(10),
        "role": "admin",
        "mfa_secret": "base32secret3232",
        "scopes": {
            "account_id": random_string(10),
            "user_id": random_string(10),
        },
    }


class SessionTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    async def new_identity_creation(self, store):
        idm, jwt, sm = setup_store_and_auth(store)

        dude = gen_admin_identity()
        await idm.new(**dude)
        d = await idm.get_if_creds(
            dude["login_name"], dude["password"], pyotp.TOTP(dude["mfa_secret"]).now()
        )

        def wo_pw(d):
            return {x: d[x] for x in d.keys() if x != "password"}

        self.assertEqual(*map(lambda x: wo_store_stamps(wo_pw(x)), [dude, d]))
        return (idm, jwt, sm, d)

    @unittest_run_loop
    async def test_session_creation(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            _, jwt, sm, identity = await self.new_identity_creation(store)
            tokens = (await sm.new(identity, "mobile"))["tokens"]
            tokens = {k: jwt.decode(tokens[k]) for k in tokens.keys()}
            self.assertEqual(
                tokens["access_token"]["session_id"],
                tokens["refresh_token"]["session_id"],
            )
            self.assertEqual(tokens["access_token"]["sub"], identity["login_name"])

    @unittest_run_loop
    async def test_login(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            idm, jwt, sm = setup_store_and_auth(store)
            dude = gen_admin_identity()
            await idm.new(**dude)

            tokens = (
                await sm.login(
                    login_name=dude["login_name"],
                    password=dude["password"],
                    mfa_token=pyotp.TOTP(dude["mfa_secret"]).now(),
                )
            )["tokens"]
            tokens = {k: jwt.decode(tokens[k]) for k in tokens.keys()}

            self.assertEqual(
                tokens["access_token"]["session_id"],
                tokens["refresh_token"]["session_id"],
            )
            self.assertEqual(tokens["access_token"]["sub"], dude["login_name"])
            with self.assertRaises(web.HTTPBadRequest):
                await sm.login(
                    login_name=dude["login_name"],
                    password="password",
                    mfa_token=pyotp.TOTP(dude["mfa_secret"]).now(),
                )
            with self.assertRaises(web.HTTPBadRequest):
                await sm.login(
                    login_name="wrong namae",
                    password=dude["password"],
                    mfa_token=pyotp.TOTP(dude["mfa_secret"]).now(),
                )
            with self.assertRaises(web.HTTPBadRequest):
                await sm.login(
                    login_name="wrong namae",
                    password=dude["password"],
                    mfa_token="not valid",
                )

    @unittest_run_loop
    async def test_session_deletion(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            _, jwt, sm, identity = await self.new_identity_creation(store)
            tokens = (await sm.new(identity, "mobile"))["tokens"]
            tokens = {k: jwt.decode(tokens[k]) for k in tokens.keys()}
            session_id = tokens["access_token"]["session_id"]

            session = fst(await store.read("SESSION", id=session_id))
            self.assertTrue(session is not None)

            await sm.forget(session_id=session_id)

            session = fst(await store.read("SESSION", id=session_id))
            self.assertTrue(session is None)

    @unittest_run_loop
    async def test_refresh(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            _, jwt, sm, identity = await self.new_identity_creation(store)

            tokens = (await sm.new(identity, "mobile"))["tokens"]
            refresh_token = tokens["refresh_token"]

            old_access_token = jwt.decode(tokens["access_token"])
            new_access_token = await sm.refresh(jwt.decode(refresh_token))
            new_access_token = jwt.decode(new_access_token)
            self.assertEqual(old_access_token.keys(), new_access_token.keys())

    @unittest_run_loop
    async def test_refresh_after_deletion(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            idm, jwt, sm, identity = await self.new_identity_creation(store)

            tokens1 = (await sm.new(identity, "mobile"))["tokens"]
            tokens1 = {k: jwt.decode(tokens1[k]) for k in tokens1.keys()}
            tokens2 = (await sm.new(identity, "mobile"))["tokens"]
            account_id = tokens1["access_token"]["scopes"]["account_id"]

            sessions = await store.read("SESSION", account_id=account_id)
            self.assertEqual(2, len(sessions))

            await idm.delete(login_name=identity["login_name"])

            with self.assertRaises(web.HTTPUnauthorized):
                await sm.refresh(jwt.decode(tokens2["refresh_token"]))

            sessions = await store.read("SESSION", account_id=account_id)
            self.assertEqual(0, len(sessions))
