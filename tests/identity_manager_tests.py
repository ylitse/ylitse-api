"""Tests for id man."""
import pyotp

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from databases import Database

from ylitse.config import config
from ylitse.app import create_app
from ylitse.identity_manager import IdentityManager
from ylitse.postgres_store import PostgresStore

from test_helpers import random_string, wo_store_stamps


def new_store_and_idm(store):
    idm = IdentityManager(store)
    return (store, idm)


def gen_admin_identity():
    return {
        "login_name": random_string(10),
        "password": random_string(10),
        "role": "admin",
        "mfa_secret": "base32secret3232",
        "scopes": {
            "account_id": random_string(10),
            "user_id": random_string(10),
        },
    }


class IdManTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    async def new_identity_creation(self, store):
        store, idm = new_store_and_idm(store)
        dude = gen_admin_identity()

        await idm.new(**dude)
        d = await idm.get_if_creds(
            dude["login_name"], dude["password"], pyotp.TOTP("base32secret3232").now()
        )

        def wo_pw(d):
            return {x: d[x] for x in d.keys() if x != "password"}

        self.assertEqual(*map(lambda x: wo_store_stamps(wo_pw(x)), [dude, d]))
        return (idm, d)

    @unittest_run_loop
    async def test_identity_deletion(self):
        async with Database(config["postgres_url"]) as database:
            store = PostgresStore(database)
            auth, dude = await self.new_identity_creation(store)
            dudes = await store.read("IDENTITY", login_name=dude["login_name"])
            self.assertEqual(1, len(dudes))
            await auth.delete(login_name=dude["login_name"])
            dudes = await store.read("IDENTITY", login_name=dude["login_name"])
            self.assertEqual(0, len(dudes))
