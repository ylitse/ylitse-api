"""Tests for schemas."""
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

import jsonschema

from ylitse.app import create_app
from ylitse.data_definitions import data_definitions
import test_helpers


class SchemaTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_skill_schema(self):
        jsonschema.validate(
            test_helpers.gen_random_skill(), data_definitions["SKILL"]["schema"]
        )
        with self.assertRaises(jsonschema.exceptions.ValidationError):
            jsonschema.validate(
                {"asdf": "asdfasdf"}, data_definitions["SKILL"]["schema"]
            )

    @unittest_run_loop
    async def test_account_schema(self):
        jsonschema.validate(
            test_helpers.gen_random_account("mentor"),
            data_definitions["ACCOUNT"]["schema"],
        )
        with self.assertRaises(jsonschema.exceptions.ValidationError):
            jsonschema.validate(
                {"asdf": "asdfasdf"}, data_definitions["ACCOUNT"]["schema"]
            )

    @unittest_run_loop
    async def test_user_schema(self):
        jsonschema.validate(
            test_helpers.gen_random_user(), data_definitions["USER"]["schema"]
        )
        with self.assertRaises(jsonschema.exceptions.ValidationError):
            jsonschema.validate(
                {"asdf": "asdfasdf"}, data_definitions["USER"]["schema"]
            )

    @unittest_run_loop
    async def test_mentor_schema(self):
        jsonschema.validate(
            test_helpers.gen_random_mentor(), data_definitions["MENTOR"]["schema"]
        )
        with self.assertRaises(jsonschema.exceptions.ValidationError):
            jsonschema.validate(
                {"asdf": "asdfasdf"}, data_definitions["USER"]["schema"]
            )

    @unittest_run_loop
    async def test_message_schema(self):
        jsonschema.validate(
            test_helpers.gen_random_message(), data_definitions["MESSAGE"]["schema"]
        )
        with self.assertRaises(jsonschema.exceptions.ValidationError):
            jsonschema.validate(
                {"asdf": "asdfasdf"}, data_definitions["MESSAGE"]["schema"]
            )

    @unittest_run_loop
    async def test_private_data_schema(self):
        jsonschema.validate(
            test_helpers.gen_random_private_data(),
            data_definitions["PRIVATE_DATA"]["schema"],
        )
        with self.assertRaises(jsonschema.exceptions.ValidationError):
            jsonschema.validate(
                {"asdf": "asdfasdf"}, data_definitions["PRIVATE_DATA"]["schema"]
            )
