""" ylitse/stats_reader.py tests."""
# pylint: disable=too-many-function-args


from datetime import datetime, date, timezone

from pytz import timezone as pytz_timezone
import time_machine
from databases import Database

from aiounittest import AsyncTestCase

from ylitse.config import config
from ylitse.crud.stat_event import crud_stat_event
from ylitse.postgres_store import PostgresStore
from ylitse.stats_reader import (
    daterange_inclusive,
    monthrange_inclusive,
    weekrange_inclusive,
    calc_joined,
    calc_deleted_accounts,
    calc_messages,
    calc_mentors,
    calc_active_accounts,
    calc_active_chats,
    calc_time_of_day_for_messages,
    calc_top_skills,
    StatsReader,
)

from tests.test_helpers import create_non_mentor, create_mentor, make_env, post_message


def round_clock_65959(res):
    # Handle the special case of 6:59:59
    return res.replace("6:59:59", "7:00:00")


timezone_helsinki = pytz_timezone("Europe/Helsinki")
timezone_utc = pytz_timezone("UTC")


class StatsReaderTest(AsyncTestCase):
    def csv_header(self, first_column):
        self.maxDiff = 5000

        return (
            f"{first_column},Accounts,Admins,Mentors,Mentees,"
            "Accounts joined,Admins joined,Mentors joined,"
            "Mentees joined,Accounts deleted,Admins deleted,"
            "Mentors deleted,Mentees deleted,Mentors in system,"
            "Active Mentors,Active Mentees,Messages from mentees,"
            "Messages from mentors,Messages from mentees cumulative,"
            "Messages from mentors cumulative,Most active hour fi,"
            "Top skills,"
            "Active chats,New chats,Cumulative chats,"
            "Avg msg count / chat / period,Avg msg count / chat / total,"
            "Avg first response time / period,Avg first response time / total,"
            "Unanswered / period,Unanswered / total"
            "\r\n"
        )

    async def insert_test_data(self, store):
        await store.truncatedb()

        mentee = None
        mentor = None

        with time_machine.travel(
            timezone_helsinki.localize(
                datetime(
                    2021,
                    1,
                    6,
                    hour=15,
                    minute=30,
                    second=0,
                )
            )
        ):
            mentee = await create_non_mentor(store, role="mentee")
            mentor = await create_mentor(store, role="mentor")
            await post_message(
                store, sender=mentee["user"], recipient=mentor["user"], content="Hi!"
            )

            # create few events
            await crud_stat_event.create_all(
                store,
                events=[
                    {
                        "name": "filter_skills",
                        "props": {"skills": ["Extreme Pen Spinning", "Towel snapping"]},
                    },
                    {
                        "name": "filter_skills",
                        "props": {"skills": ["Belly roll", "Towel snapping"]},
                    },
                ],
            )

        with time_machine.travel(
            timezone_utc.localize(
                datetime(
                    2021,
                    1,
                    7,
                    hour=20,
                    minute=30,
                    second=0,
                )
            )
        ):
            await post_message(
                store, sender=mentor["user"], recipient=mentee["user"], content="Hi!"
            )
            await post_message(
                store, sender=mentee["user"], recipient=mentor["user"], content="Hi!"
            )
            # create few events
            await crud_stat_event.create_all(
                store,
                events=[
                    {
                        "name": "filter_skills",
                        "props": {"skills": ["Eyebrow Dancing", "Limbo Skating"]},
                    },
                    {"name": "filter_skills", "props": {"skills": ["Eyebrow Dancing"]}},
                    {
                        "name": "filter_skills",
                        "props": {
                            "skills": ["Extreme Pen Spinning", "Finger snapping"]
                        },
                    },
                ],
            )

    def test_daterange_inclusive(self):
        period_start_date = date(2020, 12, 31)
        period_end_date = date(2021, 1, 2)

        dates = []

        for d in daterange_inclusive(period_start_date, period_end_date):
            dates.append(d)

        self.assertEqual(
            dates, [date(2020, 12, 31), date(2021, 1, 1), date(2021, 1, 2)]
        )

    def test_monthrange_inclusive(self):
        period_start_date = date(2020, 12, 1)
        period_end_date = date(2021, 1, 31)

        dates = []

        for d in monthrange_inclusive(period_start_date, period_end_date):
            dates.append(d)

        self.assertEqual(dates, [date(2020, 12, 1), date(2021, 1, 1)])

    def test_weekrange_inclusive(self):
        period_start_date = date(2020, 12, 31)
        period_end_date = date(2021, 1, 4)

        dates = []

        for d in weekrange_inclusive(period_start_date, period_end_date):
            dates.append(d)

        self.assertEqual(dates, [date(2020, 12, 31), date(2021, 1, 7)])

    async def test_calc_joined(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2021, 1, 1)
                    period_end_date = date(2021, 2, 1)

                    first_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_start_date.year,
                                period_start_date.month,
                                period_start_date.day,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        ).timestamp()
                    )

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    res = await calc_joined(
                        database, first_of_period_timestamp, last_of_period_timestamp
                    )
                    self.assertEqual(
                        res,
                        {
                            "account": 2,
                            "admin": 0,
                            "mentee": 1,
                            "mentor": 1,
                        },
                    )

    @time_machine.travel(datetime(2020, 12, 28, hour=0, minute=2, tzinfo=timezone.utc))
    async def test_calc_deleted_accounts(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await store.truncatedb()

                    await create_non_mentor(store, role="mentee")
                    await create_non_mentor(store, role="mentee")
                    created = await create_mentor(store, role="mentor")

                    idm, uam, jwt, sm = make_env(store)

                    mentor_account_id = created["account"]["id"]

                    # delete the mentor
                    await uam.delete_user_account(mentor_account_id)

                    # period covers the moment of deletion
                    period_start_date = date(2020, 12, 1)
                    period_end_date = date(2020, 12, 31)

                    first_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_start_date.year,
                                period_start_date.month,
                                period_start_date.day,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        ).timestamp()
                    )

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    res = await calc_deleted_accounts(
                        database, first_of_period_timestamp, last_of_period_timestamp
                    )
                    self.assertEqual(
                        res, {"account": 1, "admin": 0, "mentee": 0, "mentor": 1}
                    )

    async def test_calc_messages(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2020, 12, 30)
                    period_end_date = date(2021, 1, 8)

                    first_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_start_date.year,
                                period_start_date.month,
                                period_start_date.day,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        ).timestamp()
                    )

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    res = await calc_messages(
                        database, first_of_period_timestamp, last_of_period_timestamp
                    )
                    self.assertEqual(
                        res, {"account": 3, "admin": 0, "mentee": 2, "mentor": 1}
                    )

    async def test_calc_mentors(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_end_date = date(2021, 1, 8)

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    # one mentor from test_data
                    res = await calc_mentors(database, last_of_period_timestamp)
                    self.assertEqual(res, 1)

                    # no mentors in the golden nineties
                    res = await calc_mentors(
                        database,
                        round(
                            timezone_helsinki.localize(
                                datetime(
                                    1990,
                                    1,
                                    1,
                                    hour=0,
                                    minute=0,
                                    second=0,
                                )
                            ).timestamp()
                        ),
                    )
                    self.assertEqual(res, 0)

                    # deleted mentor should not be counted after deletion
                    mentor_account_id = None
                    with time_machine.travel(
                        timezone_helsinki.localize(
                            datetime(
                                2021,
                                1,
                                1,  # created in 2021
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        )
                    ):
                        created = await create_mentor(store, role="mentor")
                        idm, uam, jwt, sm = make_env(store)
                        mentor_account_id = created["account"]["id"]

                    with time_machine.travel(
                        timezone_helsinki.localize(
                            datetime(
                                2022,
                                1,
                                1,  # deleted in 2022
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        )
                    ):
                        await uam.delete_user_account(mentor_account_id)

                    res = await calc_mentors(
                        database,
                        round(
                            timezone_helsinki.localize(
                                datetime(
                                    2021,
                                    6,  # mentor was still available
                                    1,
                                    hour=0,
                                    minute=0,
                                    second=0,
                                )
                            ).timestamp()
                        ),
                    )
                    self.assertEqual(res, 2)

                    res = await calc_mentors(
                        database,
                        round(
                            timezone_helsinki.localize(
                                datetime(
                                    2022,
                                    1,
                                    2,  # mentor was deleted the day before
                                    hour=0,
                                    minute=0,
                                    second=0,
                                )
                            ).timestamp()
                        ),
                    )
                    self.assertEqual(res, 1)

    async def test_calc_active_accounts(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2020, 12, 30)
                    period_end_date = date(2021, 1, 8)

                    first_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_start_date.year,
                                period_start_date.month,
                                period_start_date.day,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        ).timestamp()
                    )

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    res = await calc_active_accounts(
                        database, first_of_period_timestamp, last_of_period_timestamp
                    )
                    self.assertEqual(
                        res, {"account": 2, "admin": 0, "mentee": 1, "mentor": 1}
                    )

    async def test_calc_time_of_day_for_messages(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2020, 12, 30)
                    period_end_date = date(2021, 1, 8)

                    first_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_start_date.year,
                                period_start_date.month,
                                period_start_date.day,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        ).timestamp()
                    )

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    res = await calc_time_of_day_for_messages(
                        database, first_of_period_timestamp, last_of_period_timestamp
                    )

                    self.assertEqual(
                        res, [{"count": 2, "hour": 22}, {"count": 1, "hour": 15}]
                    )

    @time_machine.travel(datetime(2020, 12, 28, hour=0, minute=2, tzinfo=timezone.utc))
    async def test_calc_top_skills(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await store.truncatedb()

                    # create few events
                    await crud_stat_event.create_all(
                        store,
                        events=[
                            {
                                "name": "filter_skills",
                                "props": {
                                    "skills": ["Eyebrow Dancing", "Limbo Skating"]
                                },
                            },
                            {
                                "name": "filter_skills",
                                "props": {"skills": ["Eyebrow Dancing"]},
                            },
                            {
                                "name": "filter_skills",
                                "props": {
                                    "skills": [
                                        "Extreme Pen Spinning",
                                        "Finger snapping",
                                    ]
                                },
                            },
                            {
                                "name": "filter_skills",
                                "props": {
                                    "skills": ["Extreme Pen Spinning", "Towel snapping"]
                                },
                            },
                            {
                                "name": "filter_skills",
                                "props": {"skills": ["Belly roll", "Towel snapping"]},
                            },
                        ],
                    )

                    # period covers the moment of insert
                    period_start_date = date(2020, 12, 1)
                    period_end_date = date(2020, 12, 31)

                    first_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_start_date.year,
                                period_start_date.month,
                                period_start_date.day,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        ).timestamp()
                    )

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    res = await calc_top_skills(
                        database,
                        first_of_period_timestamp,
                        last_of_period_timestamp,
                        limit=5,
                    )
                    self.assertEqual(
                        res,
                        "Extreme Pen Spinning 2; Eyebrow Dancing 2; Towel snapping 2; Belly roll 1; Finger snapping 1",
                    )

    async def test_calc_active_chats(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2020, 12, 30)
                    period_end_date = date(2021, 1, 8)

                    first_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_start_date.year,
                                period_start_date.month,
                                period_start_date.day,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        ).timestamp()
                    )

                    last_of_period_timestamp = round(
                        timezone_helsinki.localize(
                            datetime(
                                period_end_date.year,
                                period_end_date.month,
                                period_end_date.day,
                                hour=23,
                                minute=59,
                                second=59,
                            )
                        ).timestamp()
                    )

                    # test data has 1 chat
                    res = await calc_active_chats(
                        database, first_of_period_timestamp, last_of_period_timestamp
                    )
                    self.assertEqual(res, 1)

                    # new chat with new accounts, started in the past
                    # but 1 msg is during the period we are interested in
                    mentee = None
                    mentor = None
                    with time_machine.travel(
                        timezone_helsinki.localize(
                            datetime(
                                1990,
                                1,
                                1,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        )
                    ):
                        mentee = await create_non_mentor(store, role="mentee")
                        mentor = await create_mentor(store, role="mentor")
                        await post_message(
                            store,
                            sender=mentee["user"],
                            recipient=mentor["user"],
                            content="Hi!",
                        )
                        await post_message(
                            store,
                            sender=mentor["user"],
                            recipient=mentee["user"],
                            content="Hey!",
                        )

                        res = await calc_active_chats(
                            database,
                            first_of_period_timestamp,
                            last_of_period_timestamp,
                        )
                        self.assertEqual(
                            res,
                            1,  # test data has 1 chat, the chat above should not be counted yet
                        )

                    with time_machine.travel(
                        timezone_helsinki.localize(
                            datetime(
                                2021,
                                1,
                                6,
                                hour=0,
                                minute=0,
                                second=0,
                            )
                        )
                    ):
                        await post_message(
                            store,
                            sender=mentee["user"],
                            recipient=mentor["user"],
                            content="Notice me!",
                        )

                        res = await calc_active_chats(
                            database,
                            first_of_period_timestamp,
                            last_of_period_timestamp,
                        )
                        self.assertEqual(
                            res,
                            2,  # test data has 1 chat, plus the chat that got new message above
                        )

                        # new message again with mentee to mentor
                        mentee_2 = await create_non_mentor(store, role="mentee")
                        await post_message(
                            store,
                            sender=mentee_2["user"],
                            recipient=mentor["user"],
                            content="Hi!",
                        )

                        res = await calc_active_chats(
                            database,
                            first_of_period_timestamp,
                            last_of_period_timestamp,
                        )
                        self.assertEqual(
                            res,
                            2,  # still the same, because no answer to make the chat active
                        )

                        # mentor answers
                        await post_message(
                            store,
                            sender=mentor["user"],
                            recipient=mentee_2["user"],
                            content="Hey!",
                        )

                        res = await calc_active_chats(
                            database,
                            first_of_period_timestamp,
                            last_of_period_timestamp,
                        )
                        self.assertEqual(res, 3)  # now it is 3 active chats

    async def test_daily_csv(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2021, 1, 6)
                    period_end_date = date(2021, 1, 7)

                    res = round_clock_65959(
                        await StatsReader().daily_csv(
                            store, period_start_date, period_end_date
                        )
                    )

                    self.assertEqual(
                        res,
                        self.csv_header("Date")
                        + '2021-01-06,2,0,1,1,2,0,1,1,0,0,0,0,1,0,1,1,0,1,0,15,Towel snapping 2; Belly roll 1; Extreme Pen Spinning 1,0,1,1,1,1,"1 day, 7:00:00","1 day, 7:00:00",0,0\r\n'
                        + '2021-01-07,2,0,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,2,1,22,Eyebrow Dancing 2; Extreme Pen Spinning 1; Finger snapping 1; Limbo Skating 1,1,0,1,2,3,,"1 day, 7:00:00",0,0\r\n',
                    )

    async def test_monthly_csv(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2020, 12, 1)
                    period_end_date = date(2021, 2, 28)

                    res = round_clock_65959(
                        await StatsReader().monthly_csv(
                            store, period_start_date, period_end_date
                        )
                    )

                    self.assertEqual(
                        res,
                        self.csv_header("Month")
                        + "2020-12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,,,0,0,0,0,0,,,0,0\r\n"
                        + '2021-01,2,0,1,1,2,0,1,1,0,0,0,0,1,1,1,2,1,2,1,22,Extreme Pen Spinning 2; Eyebrow Dancing 2; Towel snapping 2; Belly roll 1; Finger snapping 1,1,1,1,3,3,"1 day, 7:00:00","1 day, 7:00:00",0,0\r\n'
                        + '2021-02,2,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,2,1,,,0,0,1,0,3,,"1 day, 7:00:00",0,0\r\n',
                    )

    async def test_weekly_csv(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2020, 12, 28)
                    period_end_date = date(2021, 1, 7)

                    res = round_clock_65959(
                        await StatsReader().weekly_csv(
                            store, period_start_date, period_end_date
                        )
                    )

                    self.assertEqual(
                        res,
                        self.csv_header("Week")
                        + "2020-53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,,,0,0,0,0,0,,,0,0\r\n"
                        + '2021-01,2,0,1,1,2,0,1,1,0,0,0,0,1,1,1,2,1,2,1,22,Extreme Pen Spinning 2; Eyebrow Dancing 2; Towel snapping 2; Belly roll 1; Finger snapping 1,1,1,1,3,3,"1 day, 7:00:00","1 day, 7:00:00",0,0\r\n'
                        + '2021-02,2,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,2,1,,,0,0,1,0,3,,"1 day, 7:00:00",0,0\r\n',
                    )

    async def test_period_csv(self):
        async with Database(config["postgres_url"], force_rollback=True) as database:
            async with database.connection() as connection:
                async with connection.transaction():

                    store = PostgresStore(database)
                    await self.insert_test_data(store)

                    period_start_date = date(2020, 9, 1)
                    period_end_date = date(2021, 10, 31)

                    res = round_clock_65959(
                        await StatsReader().period_csv(
                            store, period_start_date, period_end_date
                        )
                    )

                    self.assertEqual(
                        res,
                        self.csv_header("Period")
                        + '2020-09-01 2021-10-31,2,0,1,1,2,0,1,1,0,0,0,0,1,1,1,2,1,2,1,22,Extreme Pen Spinning 2; Eyebrow Dancing 2; Towel snapping 2; Belly roll 1; Finger snapping 1,1,1,1,3,3,"1 day, 7:00:00","1 day, 7:00:00",0,0\r\n',
                    )
