from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from ylitse.app import create_app

from test_helpers import gen_random_account, random_string, admin_headers


class VersionViewTests(AioHTTPTestCase):
    async def get_application(self):
        return create_app()

    @unittest_run_loop
    async def test_included_versions(self):
        my_json = {
            "account": {**gen_random_account(role="admin")},
            "password": random_string(20),
        }
        req = await self.client.request(
            "GET",
            "/version",
            json=my_json,
            **admin_headers(),
        )
        self.assertEqual(req.status, 200)

        versions = await req.json()
        self.assertEqual(list(versions.keys()), ["api"])
