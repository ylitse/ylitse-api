bind = '0.0.0.0:8080'
workers = 4
worker_class = 'aiohttp.worker.GunicornWebWorker'
errorlog = '-'
accesslog = '-'
loglevel = 'info'
pidfile = '/tmp/ylitse.pid'
