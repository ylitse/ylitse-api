#!/usr/bin/env python3.10

from setuptools import setup

from ylitse.version import __version__


setup(
    name='ylitse-api',
    version=__version__,
    description='Ylitse peer mentoring service HTTP API',
    author='Futurice Chilicorn Fund',
    author_email='spice@futurice.com',
    url='https://gitlab.com/ylitse/ylitse-api',
    license='MIT',
    packages=['ylitse'],
)
