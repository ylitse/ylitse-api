#!/usr/bin/env bash

url="http://localhost:8080/mentors"

while true; do
    response=$(curl -s -o /dev/null -w "%{http_code}" "$url")
    if [ "$response" == "200" ]; then
        echo "Server up"
        break
    else
        echo "Server not yet accessible. Retrying in 1 seconds..."
        sleep 1
    fi
done
