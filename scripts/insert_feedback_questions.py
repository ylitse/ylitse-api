#!/usr/bin/env python3.10
# pylint: disable=line-too-long

import asyncio
from databases import Database

from ylitse.config import config
from ylitse.postgres_store import PostgresStore
from ylitse.schema import FeedbackQuestionCreate
from ylitse.crud import crud_feedback_question


questions = [
    {
        "rules": {
            "titles": {
                "fi": "Kun ajattelet nykyhetkeä, kuinka tyytyväinen olet luottamuksellisiin ihmissuhteisiisi?",  # noqa
                "en": "When you think about the present, how satisfied are you with your confidential relationships?",  # noqa
            },
            "recipients": ["mentee", "mentor"],
            "answer": {
                "type": "range",
                "step": 1,
                "min": {
                    "value": 0,
                    "labels": {
                        "fi": "Erittäin tyytymätön",
                        "en": "Very dissatisfied",
                    },
                },
                "max": {
                    "value": 100,
                    "labels": {
                        "fi": "Erittäin tyytyväinen",
                        "en": "Very pleased",
                    },
                },
            },
            "schedule": {
                "remind_times_when_skipped": 3,
                "remind_interval_days": 1.0,
                "first": {
                    "days_since_registration": 0.0,
                    "sent_messages_threshold": 0,
                    "max_old_account_in_days": 100,
                },
                "repetitions": {
                    "type": "predefined",
                    "times": 2,
                    "days_since_previous_answer": 0.000011574074074,
                    "min_days_since_previous_answer": 1.0,
                    "messages_since_previous_answer": 4,
                },
            },
        }
    },
    {
        "rules": {
            "titles": {
                "fi": "Oletko saanut apua mentorilta?",
                "en": "Have you received help from a mentor?",
            },
            "recipients": ["mentee", "mentor"],
            "answer": {
                "type": "yes-no",
                "yes": {
                    "value": 1,
                    "labels": {"fi": "Kyllä olen", "en": "Yes, I have"},
                },
                "no": {
                    "value": 0,
                    "labels": {"fi": "En ole", "en": "No, I haven't"},
                },
            },
            "schedule": {
                "remind_times_when_skipped": 1,
                "remind_interval_days": 1.0,
                "first": {
                    "days_since_registration": 0.0,
                    "sent_messages_threshold": 1,
                    "max_old_account_in_days": 100,
                },
                "repetitions": {
                    "type": "until_value",
                    "comparison": "exact",
                    "value": 1,
                    "days_since_previous_answer": 0.000011574074074,
                },
            },
        }
    },
]


async def main():
    async with Database(config["postgres_url"], min_size=1, max_size=1) as database:
        store = PostgresStore(database)

        q_to_create = questions[0]
        await crud_feedback_question.create(
            store, FeedbackQuestionCreate.parse_obj(q_to_create).dict()
        )

        q_to_create = questions[1]
        await crud_feedback_question.create(
            store, FeedbackQuestionCreate.parse_obj(q_to_create).dict()
        )


LOOP = asyncio.get_event_loop()
LOOP.run_until_complete(main())
