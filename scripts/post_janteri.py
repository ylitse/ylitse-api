import asyncio
import json
import random
import string

import aiohttp


def random_string(n):
    return "".join(random.choices(string.ascii_uppercase + string.digits, k=n))


async def login(session, api_url):
    async with session.post(
        f"{api_url}/login",
        data=json.dumps({"login_name": "admin", "password": "secret"}),
    ) as response:
        data = await response.json()
        print(data)
        return data


async def post_mentor(account, session, api_url, headers=None):
    async with session.post(
        f"{api_url}/accounts", data=json.dumps(account), headers=headers
    ) as response:
        created = await response.json()

        # user_id = created['user']['id']
        mentor_id = created["mentor"]["id"]

        put_mentor = {
            **created["mentor"],
            "display_name": random_string(10),
            "story": random_string(200),
            "skills": [random_string(5), random_string(7), random_string(8)],
            "languages": ["fi", "ru"],
        }

        async with session.put(
            f"{api_url}/mentors/{mentor_id}",
            data=json.dumps(put_mentor),
            headers=headers,
        ) as resp:
            new_mentor = await resp.json()
            print(new_mentor)

        return created


async def main():
    async with aiohttp.ClientSession() as session:
        api_url = "http://localhost:8080"

        access_token = (await login(session, api_url))["tokens"]["access_token"]

        headers = {"Authorization": f"Bearer {access_token}"}

        mentor_account = {
            "account": {
                "role": "mentor",
                "login_name": "janteri",
            },
            "password": "janteri",
        }

        some = await post_mentor(mentor_account, session, api_url, headers=headers)

        print(some["account"]["id"])
        print(some["user"]["id"])
        print(some["mentor"]["id"])


asyncio.run(main())
