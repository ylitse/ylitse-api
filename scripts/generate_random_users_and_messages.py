#!/usr/bin/env python3.10
# pylint: disable=too-many-locals,too-many-statements,too-many-arguments

""" Generate random users and messages.
This is useful for when developing statistics or
just to see how the system handles large amount of users.

Example: YLITSE_PERIOD_START=2020-01-01 \
YLITSE_PERIOD_END=2020-03-31 \
YLITSE_MAX_NEW_USERS_A_DAY=4 \
YLITSE_MAX_NEW_MESSAGES_A_DAY=50 \
./scripts/generate_random_users_and_messages.py """


from datetime import datetime, timedelta
import os
import random
import string

import asyncio
import time_machine

from databases import Database

from ylitse.config import config
from ylitse.symmetric_crypto_functions import SymmetricCryptoFunctions
from ylitse.postgres_store import PostgresStore
from ylitse.identity_manager import IdentityManager
from ylitse.user_account_manager import UserAccountManager
from ylitse.crud.message import crud_message


previous_month_last_day_d = datetime.utcnow().date().replace(day=1) - timedelta(days=1)
previous_month_first_day_d = previous_month_last_day_d.replace(day=1)

PERIOD_START = os.getenv(
    "YLITSE_PERIOD_START", previous_month_first_day_d.strftime("%Y-%m-%d")
)
PERIOD_END = os.getenv(
    "YLITSE_PERIOD_END", previous_month_last_day_d.strftime("%Y-%m-%d")
)
UTCOFFSET = os.getenv("YLITSE_UTCOFFSET", "+00:00")
MAX_NEW_USERS_A_DAY = int(os.getenv("YLITSE_MAX_NEW_USERS_A_DAY", "10"))
MAX_NEW_MESSAGES_A_DAY = int(os.getenv("YLITSE_MAX_NEW_MESSAGES_A_DAY", "100"))


period_start_dt = datetime.fromisoformat(f"{PERIOD_START}T00:00:00.000{UTCOFFSET}")
period_end_dt = datetime.fromisoformat(f"{PERIOD_END}T23:59:59.000{UTCOFFSET}")

print(f"Generating for {period_start_dt} - {period_end_dt}")

symmetric_crypto_functions = SymmetricCryptoFunctions(config["encryption_key_file"])


async def create_mentor(uam, mentor, traveller):
    json = {
        "account": {
            "login_name": mentor["login_name"],
            "role": mentor["role"],
            "email": mentor["email"],
            "phone": mentor["phone"],
        },
        "password": mentor["password"],
        "user": {"display_name": mentor["display_name"], "role": mentor["role"]},
        "mentor": {
            "birth_year": mentor["birth_year"],
            "communication_channels": mentor["communication_channels"],
            "display_name": mentor["display_name"],
            "gender": mentor["gender"],
            "languages": mentor["languages"],
            "region": mentor["region"],
            "skills": mentor["skills"],
            "story": mentor["story"],
        },
    }
    traveller.start()
    created = await uam.create_mentor(json)
    traveller.stop()
    return created


async def create_non_mentor(uam, mentee_or_admin, traveller):
    json = {
        "account": {
            "login_name": mentee_or_admin["login_name"],
            "role": mentee_or_admin["role"],
            "email": mentee_or_admin["email"],
            "phone": "",
        },
        "password": mentee_or_admin["password"],
        "user": {
            "display_name": mentee_or_admin["display_name"],
            "role": mentee_or_admin["role"],
        },
    }
    traveller.start()
    created = await uam.create_non_mentor(json)
    traveller.stop()
    return created


async def post_message(
    store, sender_user_id, sender_role, recipient_user_id, msg, traveller
):
    json = {
        "sender_id": sender_user_id,
        "recipient_id": recipient_user_id,
        "content": msg,
        "opened": False,
    }

    traveller.start()
    created = await crud_message.post(store, data=json, sender_role=sender_role)
    traveller.stop()
    return created


async def delete_account(uam, account_id):
    await uam.delete_user_account(account_id)


def random_string(num, multiline=False):
    whitespace = " \t\n" if multiline else " \t"
    return "".join(
        random.choices(
            string.ascii_letters
            + string.digits
            + string.punctuation
            + whitespace
            + "öäå😱😇👻👀",
            k=num,
        )
    )


def random_email(num):
    return (
        "".join(
            random.choices(
                string.ascii_letters + string.digits,
                k=num,
            )
        )
        + "@example.com"
    )


def get_random_mentee():
    return {
        "login_name": random_string(10),
        "password": random_string(10),
        "display_name": random_string(10),
        "role": "mentee",
        "email": random_email(10),
    }


COMMUNICATION_CHANNELS = ["phone", "email", "chat"]
LANGUAGE = ["fi", "se", "en"]
GENDERS = ["female", "male", "other"]
REGIONS = ["HEL", "Tampesteri", "other"]


def get_random_mentor():
    return {
        "login_name": random_string(10),
        "display_name": random_string(10),
        "password": random_string(10),
        "email": random_email(10),
        "birth_year": random.randint(1900, 2003),
        "phone": "555-" + str(random.randint(1000, 9999)),
        "story": random_string(50, multiline=True),
        "languages": [random.choice(LANGUAGE)],
        "skills": [random_string(5), random_string(7), random_string(8)],
        "communication_channels": [random.choice(COMMUNICATION_CHANNELS)],
        "gender": random.choice(GENDERS),
        "region": random.choice(REGIONS),
        "role": "mentor",
    }


def daterange_inclusive(start_date, end_date):
    for n in range(int((end_date - start_date).days + 1)):
        yield start_date + timedelta(n)


async def main():
    database = Database(config["postgres_url"], min_size=1, max_size=1)
    postgres_store = PostgresStore(database)
    await postgres_store.on_startup()
    idm = IdentityManager(postgres_store)
    uam = UserAccountManager(postgres_store, idm)

    mentors = []
    mentees = []

    for d in daterange_inclusive(period_start_dt.date(), period_end_dt.date()):
        random_hour = random.randint(0, 23)
        random_minute = random.randint(0, 59)
        random_second = random.randint(0, 59)
        random_datetime = datetime.fromisoformat(
            f"{d.year}-{d.month:02}-{d.day:02}"
            f"T{random_hour:02}:{random_minute:02}:{random_second:02}"
            f"{UTCOFFSET}"
        )
        traveller = time_machine.travel(random_datetime)
        print(f"Generating data for {random_datetime}")

        # make sure there are at least 1 new mentor and 1 new mentee a day
        mentor = get_random_mentor()
        mentor_myuser = await create_mentor(uam, mentor, traveller)
        mentor["user_id"] = mentor_myuser["user"]["id"]
        mentor["account_id"] = mentor_myuser["account"]["id"]
        mentors.append(mentor)

        mentee = get_random_mentee()
        mentee_myuser = await create_non_mentor(uam, mentee, traveller)
        mentee["user_id"] = mentee_myuser["user"]["id"]
        mentee["account_id"] = mentee_myuser["account"]["id"]
        mentees.append(mentee)

        for _ in range(0, random.randint(1, MAX_NEW_USERS_A_DAY)):
            if random.randint(0, 1):
                mentor = get_random_mentor()
                mentor_myuser = await create_mentor(uam, mentor, traveller)
                mentor["user_id"] = mentor_myuser["user"]["id"]
                mentor["account_id"] = mentor_myuser["account"]["id"]
                mentors.append(mentor)
            else:
                mentee = get_random_mentee()
                mentee_myuser = await create_non_mentor(uam, mentee, traveller)
                mentee["user_id"] = mentee_myuser["user"]["id"]
                mentee["account_id"] = mentee_myuser["account"]["id"]
                mentees.append(mentee)

        for _ in range(0, random.randint(1, MAX_NEW_MESSAGES_A_DAY)):
            if random.randint(0, 1):
                random_mentor = random.choice(mentors)
                random_mentee = random.choice(mentees)
                await post_message(
                    postgres_store,
                    random_mentor["user_id"],
                    "mentor",
                    random_mentee["user_id"],
                    random_string(10, multiline=True),
                    traveller,
                )
            else:
                random_mentor = random.choice(mentors)
                random_mentee = random.choice(mentees)
                await post_message(
                    postgres_store,
                    random_mentee["user_id"],
                    "mentee",
                    random_mentor["user_id"],
                    random_string(10, multiline=True),
                    traveller,
                )

        # 20% chance to delete an account
        if random.random() < 0.20:
            if random.randint(0, 1):
                mentor_to_delete = mentors.pop(random.randrange(len(mentors)))
                await delete_account(uam, mentor_to_delete["account_id"])
            else:
                mentee_to_delete = mentees.pop(random.randrange(len(mentees)))
                await delete_account(uam, mentee_to_delete["account_id"])


asyncio.run(main())
