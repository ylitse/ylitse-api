#!/usr/bin/env python3.10

import sqlalchemy as sa

from ylitse.config import config
from ylitse.postgres_tables import (
    sqlalchemy_metadata,
)


def main():
    # Remove configuration options from url.
    # asyncpg allows to specify some configuration options in url params
    # but psycopg2 doesn't support them
    # and they are not needed here.
    postgres_url_without_options = config["postgres_url"].split("?")[0]

    engine = sa.create_engine(postgres_url_without_options, echo=False)

    # drop all tables this codebase knows about
    sqlalchemy_metadata.drop_all(engine)

    # drop alembic table so that we will run all migrations from beginning
    try:
        engine.execute("DROP TABLE alembic_version;")
    except sa.exc.ProgrammingError as e:
        if 'table "alembic_version" does not exist' not in str(e):
            raise e


if __name__ == "__main__":
    main()
