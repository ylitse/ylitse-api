"""Get mfa-token for admin-login

Example:
./docker/run-in-container.sh python scripts/get_mfa_token.py | pbcopy
"""
import pyotp
from ylitse.config import config

API_2FA = config["admin_2fa"]


def get_mfa_token(mfa_secret):
    return pyotp.TOTP(mfa_secret).now()


print(get_mfa_token(API_2FA))
