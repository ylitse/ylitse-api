#!/usr/bin/env python3.10

""" Check if config file has the word 'test' in it.

Trust if the config file path includes 'test' then go ahead
without prompt. Otherwise ask for confirmation.

Skip check in CI.
"""

import os
import sys
from pathlib import Path

config_path = os.getenv("YLITSE_CONF", f"{str(Path.home())}/.config/ylitse/ylitse.conf")


def confirm_prompt(question):
    reply = None
    while reply not in ("y", "n"):
        reply = input(f"{question} (y/n): ").lower()
    return reply == "y"


def main():
    if "test" not in config_path and os.getenv("CI") != "true":
        if not confirm_prompt(
            "This will delete all data!"
            + f" Are you sure you want to run tests with {config_path}?"
        ):
            sys.exit(-1)


if __name__ == "__main__":
    main()
