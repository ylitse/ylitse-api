#!/usr/bin/env python3.10
# pylint: disable=too-many-locals,too-many-statements

""" Generate random messages between mentor and mentee.
This is useful for when developing statistics or
just to see how the system handles large amount of messages.

Example:
YLITSE_NEW_MESSAGES=30000 \
YLITSE_MAX_MESSAGE_LENGTH=100 \
YLITSE_NEW_PASSWORD=secret \
YLITSE_MENTOR_USERNAME=mentor30k \
YLITSE_MENTEE_USERNAME=mentee30k \
./scripts/generate_random_messages.py """

import os
import random
import string
import math
import itertools
import sys
import datetime

import asyncio

from databases import Database

from ylitse.config import config
from ylitse.utils import fst
from ylitse.postgres_store import PostgresStore
from ylitse.store_abstraction import StoreView
from ylitse.identity_manager import IdentityManager
from ylitse.user_account_manager import UserAccountManager
from ylitse.crud.message import crud_message


def random_string(num, multiline=False):
    whitespace = " \t\n" if multiline else " \t"
    return "".join(
        random.choices(
            string.ascii_letters
            + string.digits
            + string.punctuation
            + whitespace
            + "öäå😱😇👻👀",
            k=num,
        )
    )


def random_email(num):
    return (
        "".join(
            random.choices(
                string.ascii_letters + string.digits,
                k=num,
            )
        )
        + "@example.com"
    )


MENTOR_USERNAME = os.getenv("YLITSE_MENTOR_USERNAME", "")
MENTEE_USERNAME = os.getenv("YLITSE_MENTEE_USERNAME", "")
NEW_PASSWORD = os.getenv("YLITSE_NEW_PASSWORD", random_string(20))
NEW_MESSAGES = int(os.getenv("YLITSE_NEW_MESSAGES", "100"))
MAX_MESSAGE_LENGTH = int(os.getenv("YLITSE_MAX_MESSAGE_LENGTH", "100"))

spinner = itertools.cycle(["-", "/", "|", "\\"])


def progress_spinner():
    sys.stdout.write(next(spinner))  # write the next character
    sys.stdout.flush()  # flush stdout buffer (actual character display)
    sys.stdout.write("\b")  # erase the last written char


async def post_message(store, sender_user_id, sender_role, recipient_user_id, msg):
    json = {
        "sender_id": sender_user_id,
        "recipient_id": recipient_user_id,
        "content": msg,
        "opened": True,
    }

    return await crud_message.post(store, data=json, sender_role=sender_role)


async def create_mentor(uam, mentor):
    json = {
        "account": {
            "login_name": mentor["login_name"],
            "role": mentor["role"],
            "email": mentor["email"],
            "phone": mentor["phone"],
        },
        "password": mentor["password"],
        "user": {
            "display_name": mentor["display_name"],
            "role": mentor["role"],
        },
        "mentor": {
            "birth_year": mentor["birth_year"],
            "communication_channels": mentor["communication_channels"],
            "display_name": mentor["display_name"],
            "gender": mentor["gender"],
            "languages": mentor["languages"],
            "region": mentor["region"],
            "skills": mentor["skills"],
            "story": mentor["story"],
        },
    }
    created = await uam.create_mentor(json)
    return created


async def create_non_mentor(uam, mentee_or_admin):
    json = {
        "account": {
            "login_name": mentee_or_admin["login_name"],
            "role": mentee_or_admin["role"],
            "email": mentee_or_admin["email"],
            "phone": "",
        },
        "password": mentee_or_admin["password"],
        "user": {
            "display_name": mentee_or_admin["display_name"],
            "role": mentee_or_admin["role"],
        },
    }
    created = await uam.create_non_mentor(json)
    return created


def get_random_mentee():
    return {
        "login_name": random_string(10),
        "password": random_string(10),
        "display_name": random_string(10),
        "role": "mentee",
        "email": random_email(10),
    }


COMMUNICATION_CHANNELS = ["phone", "email", "chat"]
LANGUAGE = ["fi", "se", "en"]
GENDERS = ["female", "male", "other"]
REGIONS = ["HEL", "Tampesteri", "other"]


def get_random_mentor():
    return {
        "login_name": random_string(10),
        "display_name": random_string(10),
        "password": random_string(10),
        "email": random_email(10),
        "birth_year": random.randint(1900, 2003),
        "phone": "555-" + str(random.randint(1000, 9999)),
        "story": random_string(50, multiline=True),
        "languages": [random.choice(LANGUAGE)],
        "skills": [random_string(5), random_string(7), random_string(8)],
        "communication_channels": [random.choice(COMMUNICATION_CHANNELS)],
        "gender": random.choice(GENDERS),
        "region": random.choice(REGIONS),
        "role": "mentor",
    }


async def main():
    database = Database(config["postgres_url"], min_size=1, max_size=1)
    postgres_store = PostgresStore(database)
    await postgres_store.on_startup()
    idm = IdentityManager(postgres_store)
    uam = UserAccountManager(postgres_store, idm)
    identity_store = StoreView(postgres_store, "IDENTITY")

    mentor_user_id = None
    mentee_user_id = None

    # try to find existing users
    existing_mentor_identity = fst(
        await identity_store.read(login_name=MENTOR_USERNAME)
    )
    existing_mentee_identity = fst(
        await identity_store.read(login_name=MENTEE_USERNAME)
    )

    if existing_mentor_identity:
        mentor_user_id = existing_mentor_identity["scopes"]["user_id"]

    if existing_mentee_identity:
        mentee_user_id = existing_mentee_identity["scopes"]["user_id"]

    # create mentor if it doesnt exist
    if not mentor_user_id:
        mentor = get_random_mentor()
        mentor["login_name"] = MENTOR_USERNAME
        mentor["display_name"] = MENTOR_USERNAME
        mentor["password"] = NEW_PASSWORD
        mentor_myuser = await create_mentor(uam, mentor)
        mentor_user_id = mentor_myuser["user"]["id"]

    # create mentee if it doesnt exist
    if not mentee_user_id:
        mentee = get_random_mentee()
        mentee["login_name"] = MENTEE_USERNAME
        mentee["display_name"] = MENTEE_USERNAME
        mentee["password"] = NEW_PASSWORD
        mentee_myuser = await create_non_mentor(uam, mentee)
        mentee_user_id = mentee_myuser["user"]["id"]

    i = 0
    for _ in range(0, math.floor(NEW_MESSAGES / 2)):
        now = datetime.datetime.utcnow().isoformat()
        msg = random_string(random.randint(1, MAX_MESSAGE_LENGTH), multiline=True)
        await post_message(
            postgres_store,
            mentor_user_id,
            "mentor",
            mentee_user_id,
            (f"{i} {MENTOR_USERNAME}->{MENTEE_USERNAME}\n" f"{now}\n" f"{msg}"),
        )

        now = datetime.datetime.utcnow().isoformat()
        reply_msg = random_string(random.randint(1, MAX_MESSAGE_LENGTH), multiline=True)
        await post_message(
            postgres_store,
            mentee_user_id,
            "mentee",
            mentor_user_id,
            (f"{i} {MENTEE_USERNAME}->{MENTOR_USERNAME}\n" f"{now}\n" f"{reply_msg}"),
        )

        progress_spinner()
        i = i + 1


asyncio.run(main())
