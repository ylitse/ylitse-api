#!/usr/bin/env python3.10

import asyncio
from aiohttp import web
from databases import Database

from ylitse.config import config
from ylitse.identity_manager import IdentityManager
from ylitse.postgres_store import PostgresStore
from ylitse.user_account_manager import UserAccountManager

API_USER = config["admin_username"]
API_PASS = config["admin_password"]
API_2FA = config["admin_2fa"]

if len(API_USER) == 0 or len(API_PASS) == 0:
    raise AttributeError("YLITSE_API_USER or YLITSE_API_PASS is empty")


async def create_non_mentor(uam, mentee_or_admin):
    json = {
        "account": {
            "login_name": mentee_or_admin["login_name"],
            "role": mentee_or_admin["role"],
            "email": mentee_or_admin["email"],
            "phone": "",
        },
        "password": mentee_or_admin["password"],
        "mfa_secret": mentee_or_admin.get("mfa_secret", None),
        "user": {
            "display_name": mentee_or_admin["display_name"],
            "role": mentee_or_admin["role"],
        },
    }
    created = await uam.create_non_mentor(json)
    return created


def get_super_admin():
    return {
        "login_name": API_USER,
        "password": API_PASS,
        "mfa_secret": API_2FA,
        "display_name": "superadmin",
        "role": "admin",
        "email": "superadmin@example.com",
    }


async def main():
    async with Database(config["postgres_url"], min_size=1, max_size=1) as database:
        store = PostgresStore(database)
        idm = IdentityManager(store)
        uam = UserAccountManager(store, idm)

        admin = get_super_admin()
        try:
            await create_non_mentor(uam, admin)
        except web.HTTPConflict:
            print(f"Admin with username '{API_USER}' already exists")


asyncio.run(main())
