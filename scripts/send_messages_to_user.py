""" Send message from one user to other

argv[1] = id of sender
argv[2] = id of reciever
argv[3] = content
Example:
./scripts/send_message.py sender_id reciever_id "Hello World"
./docker/run-in-container.sh python scripts/send_message.py  sendrer_id reciever_id "Hello World"
"""

import asyncio
import random
import sys

from databases import Database

from ylitse.config import config
from ylitse.postgres_store import PostgresStore

print("Recipient:")
print(sys.argv[1])
print("*---------*")
print("Amount of new senders")
print(sys.argv[2])
print("*---------*")
print("Messages per sender to be sent")
print(sys.argv[3])
print("*---------*\n")

print("Starting")
print("*---------*")


def get_mentor(name):
    return {
        "login_name": name,
        "display_name": name,
        "password": name,
        "email": "perse",
        "birth_year": random.randint(1900, 2003),
        "phone": "555-" + str(random.randint(1000, 9999)),
        "story": "my story",
        "languages": ["fi"],
        "skills": ["nvim", "beer"],
        "communication_channels": [],
        "gender": "male",
        "region": "helsinki",
        "role": "mentor",
    }


async def create_non_mentor(uam, mentee_or_admin, traveller):
    json = {
        "account": {
            "login_name": mentee_or_admin["login_name"],
            "role": mentee_or_admin["role"],
            "email": mentee_or_admin["email"],
            "phone": "",
        },
        "password": mentee_or_admin["password"],
        "user": {
            "display_name": mentee_or_admin["display_name"],
            "role": mentee_or_admin["role"],
        },
    }
    traveller.start()
    created = await uam.create_non_mentor(json)
    traveller.stop()
    return created


async def main():
    database = Database(config["postgres_url"], min_size=1, max_size=1)
    postgres_store = PostgresStore(database)
    await postgres_store.on_startup()


asyncio.run(main())

print("Done")
