#!/usr/bin/env python3.10

"""
Run cleanups to follow the data handling practices of the project.

Execute this script either as a cron job or on "filing days".
"""

import asyncio
from databases import Database

from ylitse.config import config
from ylitse.postgres_store import (
    PostgresStore,
)
from ylitse.crud import crud_stat_mentor


async def main():
    async with Database(config["postgres_url"], min_size=1, max_size=1) as database:
        store = PostgresStore(database)

        await crud_stat_mentor.anonymize_old_data(store)

        print("Done")


asyncio.run(main())
