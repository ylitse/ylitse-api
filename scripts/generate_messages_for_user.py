#!/usr/bin/env python3.10
# pylint: disable=too-many-locals,too-many-statements,too-many-arguments

""" Create a user and generate messages for the user.
This is useful for to see how the system handles
large amount of messages for one user.

Example: START_YEAR=2021 \
END_YEAR=2022 \
USER_NAME="userlol" \
MESSAGES_FOR_CONVERSATION=100 \
BUDDIES_AMOUNT=50 \
./scripts/generate_messages_for_user.py """


from datetime import datetime, timedelta
import os
import random
import string

import asyncio
import time_machine

from databases import Database

from ylitse.config import config
from ylitse.symmetric_crypto_functions import SymmetricCryptoFunctions
from ylitse.postgres_store import PostgresStore
from ylitse.identity_manager import IdentityManager
from ylitse.user_account_manager import UserAccountManager
from ylitse.crud.message import crud_message


previous_month_last_day_d = datetime.utcnow().date().replace(day=1) - timedelta(days=1)
previous_month_first_day_d = previous_month_last_day_d.replace(day=1)

PERIOD_START = f"{os.getenv('START_YEAR')}-01-01"
PERIOD_END = f"{os.getenv('END_YEAR')}-12-31"

UTCOFFSET = os.getenv("YLITSE_UTCOFFSET", "+00:00")
USER_NAME = os.getenv("USER_NAME")
MESSAGES_FOR_CONVERSATION = int(os.getenv("MESSAGES_FOR_CONVERSATION", "0"))
BUDDIES_AMOUNT = int(os.getenv("BUDDIES_AMOUNT", "0"))

period_start_dt = datetime.fromisoformat(f"{PERIOD_START}T00:00:00.000{UTCOFFSET}")
period_end_dt = datetime.fromisoformat(f"{PERIOD_END}T23:59:59.000{UTCOFFSET}")

# print(f"Generating messages for user: {USER_NAME},
# time period: {period_start_dt} - {period_end_dt}")

symmetric_crypto_functions = SymmetricCryptoFunctions(config["encryption_key_file"])


async def create_mentor(uam, mentor, traveller):
    json = {
        "account": {
            "login_name": mentor["login_name"],
            "role": mentor["role"],
            "email": mentor["email"],
            "phone": mentor["phone"],
        },
        "password": mentor["password"],
        "user": {"display_name": mentor["display_name"], "role": mentor["role"]},
        "mentor": {
            "birth_year": mentor["birth_year"],
            "communication_channels": mentor["communication_channels"],
            "display_name": mentor["display_name"],
            "gender": mentor["gender"],
            "languages": mentor["languages"],
            "region": mentor["region"],
            "skills": mentor["skills"],
            "story": mentor["story"],
        },
    }
    traveller.start()
    created = await uam.create_mentor(json)
    traveller.stop()
    return created


async def create_non_mentor(uam, mentee_or_admin, traveller):
    json = {
        "account": {
            "login_name": mentee_or_admin["login_name"],
            "role": mentee_or_admin["role"],
            "email": mentee_or_admin["email"],
            "phone": "",
        },
        "password": mentee_or_admin["password"],
        "user": {
            "display_name": mentee_or_admin["display_name"],
            "role": mentee_or_admin["role"],
        },
    }
    traveller.start()
    created = await uam.create_non_mentor(json)
    traveller.stop()
    return created


async def post_message(
    store, sender_user_id, sender_role, recipient_user_id, msg, traveller
):
    json = {
        "sender_id": sender_user_id,
        "recipient_id": recipient_user_id,
        "content": msg,
        "opened": False,
    }

    traveller.start()
    created = await crud_message.post(store, data=json, sender_role=sender_role)
    traveller.stop()
    return created


async def delete_account(uam, account_id):
    await uam.delete_user_account(account_id)


def random_string(num, multiline=False):
    whitespace = " \t\n" if multiline else " \t"
    return "".join(
        random.choices(
            string.ascii_letters
            + string.digits
            + string.punctuation
            + whitespace
            + "öäå😱😇👻👀",
            k=num,
        )
    )


def random_email(num):
    return (
        "".join(
            random.choices(
                string.ascii_letters + string.digits,
                k=num,
            )
        )
        + "@example.com"
    )


def get_random_mentee():
    return {
        "login_name": random_string(10),
        "password": random_string(10),
        "display_name": random_string(10),
        "role": "mentee",
        "email": random_email(10),
    }


COMMUNICATION_CHANNELS = ["phone", "email", "chat"]
LANGUAGE = ["fi", "se", "en"]
GENDERS = ["female", "male", "other"]
REGIONS = ["HEL", "Tampesteri", "other"]


def get_random_mentor(name=random_string(10)):
    return {
        "login_name": name,
        "display_name": name,
        "password": name,
        "email": random_email(10),
        "birth_year": random.randint(1900, 2003),
        "phone": "555-" + str(random.randint(1000, 9999)),
        "story": random_string(50, multiline=True),
        "languages": [random.choice(LANGUAGE)],
        "skills": [random_string(5), random_string(7), random_string(8)],
        "communication_channels": [random.choice(COMMUNICATION_CHANNELS)],
        "gender": random.choice(GENDERS),
        "region": random.choice(REGIONS),
        "role": "mentor",
    }


async def main():
    database = Database(config["postgres_url"], min_size=1, max_size=1)
    postgres_store = PostgresStore(database)
    await postgres_store.on_startup()
    idm = IdentityManager(postgres_store)
    uam = UserAccountManager(postgres_store, idm)

    traveller = time_machine.travel(
        datetime.fromisoformat(
            f"{os.getenv('START_YEAR')}-01-01" f"T00:00:00" f"{UTCOFFSET}"
        )
    )
    mentor = get_random_mentor(USER_NAME)
    mentor_myuser = await create_mentor(uam, mentor, traveller)
    user_id = mentor_myuser["user"]["id"]

    for i in range(BUDDIES_AMOUNT):
        print("buddy number", i)
        mentee = get_random_mentee()
        mentee_myuser = await create_non_mentor(uam, mentee, traveller)
        mentee_user_id = mentee_myuser["user"]["id"]
        for j in range(MESSAGES_FOR_CONVERSATION):
            random_year = random.randint(period_start_dt.year, period_end_dt.year)
            random_month = random.randint(1, 12)
            random_day = random.randint(1, 28)
            random_hour = random.randint(0, 23)
            random_minute = random.randint(0, 59)
            random_second = random.randint(0, 59)
            random_datetime = datetime.fromisoformat(
                f"{random_year}-{random_month:02}-{random_day:02}"
                f"T{random_hour:02}:{random_minute:02}:{random_second:02}"
                f"{UTCOFFSET}"
            )
            traveller = time_machine.travel(random_datetime)
            print(
                f"Generating message for buddy number: \
                                    {i} and message number: \
                                    {j}, datetime: {random_datetime}"
            )
            if random.randint(0, 1):
                await post_message(
                    postgres_store,
                    user_id,
                    "mentor",
                    mentee_user_id,
                    random_string(10, multiline=True),
                    traveller,
                )
            else:
                await post_message(
                    postgres_store,
                    mentee_user_id,
                    "mentee",
                    user_id,
                    random_string(10, multiline=True),
                    traveller,
                )


asyncio.run(main())
