"""create minimum supported client versions api

Revision ID: 5ff0388b13e7
Revises: fac9f4af401c
Create Date: 2024-07-06 10:48:38.395653+00:00

"""
from alembic import op
import datetime
import sqlalchemy as sa
from ylitse.store_abstraction import (
    new_ylitse_id,
)  # noqa, pylint: disable=wrong-import-position
# revision identifiers, used by Alembic.

revision = "5ff0388b13e7"
down_revision = "fac9f4af401c"
branch_labels = None
depends_on = None


def upgrade():
    table = "minimum_supported_client_versions"

    op.create_table(
        table,
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column("client", sa.String(length=200), nullable=False),
        sa.Column("version", sa.String(length=200), nullable=False),
        sa.Column("active", sa.Boolean()),
        sa.Column("updated", sa.DateTime(timezone=False)),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    ylitse_ios = f"('{new_ylitse_id()}', 'ylitse_ios', '2.14.0', true, '{datetime.datetime.utcnow().isoformat()}', '{datetime.datetime.utcnow().isoformat()}')"
    ylitse_android = f"('{new_ylitse_id()}', 'ylitse_android', '2.14.0', true, '{datetime.datetime.utcnow().isoformat()}', '{datetime.datetime.utcnow().isoformat()}')"

    op.execute(f'insert into {table} (id, client, version, active, updated, created) values {ylitse_ios}')
    op.execute(f'insert into {table} (id, client, version, active, updated, created) values {ylitse_android}')


def downgrade():
    op.drop_table("minimum_supported_client_versions")
