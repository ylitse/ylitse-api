"""create safety feature logging table

Revision ID: b5024f14d4cc
Revises: 16662224ed71
Create Date: 2023-07-11 08:09:15.606661+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b5024f14d4cc'
down_revision = '16662224ed71'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "chat_review_log",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "chat_user_id_1", sa.String(length=200), nullable=False
        ),
        sa.Column(
            "chat_user_id_2", sa.String(length=200), nullable=False
        ),
        sa.Column(
            "reviewer_user_id", sa.Unicode(length=200), nullable=False
        ),
        sa.Column("active", sa.Boolean()),
        sa.Column("updated", sa.DateTime(timezone=False)),
        sa.Column(
            "created", sa.DateTime(timezone=False), nullable=False
        ),
    )


def downgrade():
    op.drop_table('chat_review_log')
