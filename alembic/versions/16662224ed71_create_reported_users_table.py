"""create reported_users table

Revision ID: 16662224ed71
Revises: 2c0b54d14d91
Create Date: 2023-05-30 13:26:15.564018+00:00

"""
from sqlalchemy.dialects.postgresql import ENUM
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '16662224ed71'
down_revision = '2c0b54d14d91'
branch_labels = None
depends_on = None

REPORT_STATUS_ENUM = ENUM("received", "handled", name="report_status")


def upgrade():
    op.create_table(
        "reported_users",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "reported_user_id", sa.String(length=200), nullable=False
        ),
        sa.Column(
            "reporter_user_id", sa.String(length=200), nullable=False
        ),
        sa.Column(
            "contact_field", sa.Unicode(length=200), nullable=True
        ),
        sa.Column("report_reason", sa.Unicode(length=2000)),
        sa.Column("status", REPORT_STATUS_ENUM, default="received"),
        sa.Column("active", sa.Boolean()),
        sa.Column("updated", sa.DateTime(timezone=False)),
        sa.Column(
            "created", sa.DateTime(timezone=False), nullable=False
        ),
    )


def downgrade():
    op.drop_table('reported_users')
    REPORT_STATUS_ENUM.drop(op.get_bind())
