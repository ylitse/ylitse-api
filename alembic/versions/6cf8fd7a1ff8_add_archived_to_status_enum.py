"""add archived to status enum

Revision ID: 6cf8fd7a1ff8
Revises: 69c4dec42c24
Create Date: 2022-04-17 07:51:55.319016+00:00

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "6cf8fd7a1ff8"
down_revision = "69c4dec42c24"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        f""" ALTER TABLE contacts ALTER COLUMN status TYPE VARCHAR(255);
        DROP TYPE IF EXISTS contact_status;
        CREATE TYPE contact_status
        AS ENUM('ok', 'banned', 'deleted', 'archived');
        ALTER TABLE contacts ALTER COLUMN status
        TYPE contact_status USING status::text::contact_status; """
    )


def downgrade():
    # https://blog.yo1.dog/updating-enum-values-in-postgresql-the-safe-and-easy-way/
    op.execute(
        f""" UPDATE contacts SET status = 'ok' WHERE status = 'archived';
        ALTER TYPE contact_status RENAME TO contact_status_old;
        CREATE TYPE contact_status AS ENUM('ok', 'banned', 'deleted');
        ALTER TABLE contacts ALTER COLUMN status
        TYPE contact_status USING status::text::contact_status;
        DROP TYPE contact_status_old; """
    )
