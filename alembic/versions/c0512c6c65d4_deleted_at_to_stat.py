"""deleted_at to stat_*

Revision ID: c0512c6c65d4
Revises: a1ca1aa08571
Create Date: 2021-12-20 13:06:33.095033+00:00

"""
from datetime import datetime, timezone

from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table


# revision identifiers, used by Alembic.
revision = "c0512c6c65d4"
down_revision = "a1ca1aa08571"
branch_labels = None
depends_on = None

stat_accounts_during_migration = table(
    "stat_accounts",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("deleted", sa.Integer()),
)
stat_messages_during_migration = table(
    "stat_messages",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("deleted", sa.Integer()),
)


def generate_utc_now_timestamp():
    dt = datetime.now(timezone.utc)
    return round(dt.timestamp())


def upgrade():

    # rename
    op.alter_column(
        "stat_accounts",
        "stat_last_seen",
        nullable=True,
        new_column_name="deleted",
    )
    op.alter_column(
        "stat_messages",
        "stat_last_seen",
        nullable=True,
        new_column_name="deleted",
    )

    # Existing stat_last_seen has been recording when things get deleted.
    # Only thing we need to do is to mark the existing ones not deleted.
    # If resource has not been deleted, it has `deleted == MAX(deleted)`
    # set `deleted` to null in those.
    connection = op.get_bind()
    max_seen_a_subquery = (
        stat_accounts_during_migration.select()
        .with_only_columns(
            [sa.func.max(stat_accounts_during_migration.c.deleted)]
        )
        .scalar_subquery()
    )
    update_a_q = (
        stat_accounts_during_migration.update()
        .where(
            stat_accounts_during_migration.c.deleted == (max_seen_a_subquery)
        )
        .values({"deleted": None})
    )
    connection.execute(update_a_q)

    max_seen_m_subquery = (
        stat_accounts_during_migration.select()
        .with_only_columns(
            [sa.func.max(stat_messages_during_migration.c.deleted)]
        )
        .scalar_subquery()
    )
    update_m_q = (
        stat_messages_during_migration.update()
        .where(
            stat_messages_during_migration.c.deleted == (max_seen_m_subquery)
        )
        .values({"deleted": None})
    )
    connection.execute(update_m_q)


def downgrade():
    # Put current timestamp to all deleted IS NULL.
    now = generate_utc_now_timestamp()
    connection = op.get_bind()
    update_a_q = (
        stat_accounts_during_migration.update()
        .where(
            stat_accounts_during_migration.c.deleted.is_(None)
        )
        .values({"deleted": now})
    )
    connection.execute(update_a_q)

    update_m_q = (
        stat_messages_during_migration.update()
        .where(
            stat_messages_during_migration.c.deleted.is_(None)
        )
        .values({"deleted": now})
    )
    connection.execute(update_m_q)

    # rename back to stat_last_seen not null
    op.alter_column(
        "stat_messages",
        "deleted",
        nullable=False,
        new_column_name="stat_last_seen",
    )
    op.alter_column(
        "stat_accounts",
        "deleted",
        nullable=False,
        new_column_name="stat_last_seen",
    )
