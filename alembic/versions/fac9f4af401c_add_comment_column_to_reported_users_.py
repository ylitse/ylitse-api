"""Add comment-column to reported_users table

Revision ID: fac9f4af401c
Revises: 5e19f8fa8590
Create Date: 2023-11-23 06:58:21.615623+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fac9f4af401c'
down_revision = '5e19f8fa8590'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "reported_users",
        sa.Column("comment", sa.String(length=2048), nullable=True),
    )


def downgrade():
    op.drop_column("reported_users", "comment")
