"""keep mentor data after deletion

Revision ID: 69c4dec42c24
Revises: 3372045817d4
Create Date: 2022-03-19 08:09:33.193870+00:00

"""
import os
import sys
import traceback
from datetime import datetime

from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table
from sqlalchemy.dialects.postgresql import ENUM

# Path hack, for sibling directory
sys.path.insert(0, os.path.abspath("."))

from ylitse.stats_collect import (
    anonymize_id,
)  # noqa, pylint: disable=wrong-import-position
from ylitse.store_abstraction import (
    new_ylitse_id,
)  # noqa, pylint: disable=wrong-import-position


# revision identifiers, used by Alembic.
revision = "69c4dec42c24"
down_revision = "3372045817d4"
branch_labels = None
depends_on = None

ROLE_ENUM = ENUM("mentee", "mentor", "admin", name="role")

mentors_during_migration = table(
    "mentors",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("user_id", sa.String(length=200), nullable=False),
    sa.Column("display_name", sa.Unicode(length=30), nullable=False),
    sa.Column("last_seen", sa.DateTime(timezone=False), nullable=True),
    sa.Column("created", sa.DateTime(timezone=False), nullable=False),
)

stat_messages_during_migration = table(
    "stat_messages",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("from_role", ROLE_ENUM),
    sa.Column("sender_id", sa.String(length=200)),
    sa.Column("created", sa.Integer(), index=True),
    sa.Column("deleted", sa.Integer()),
)

stat_mentors_during_migration = table(
    "stat_mentors",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("user_id", sa.String(length=200), nullable=False),
    sa.Column("display_name", sa.Unicode(length=30), nullable=False),
    sa.Column("last_seen", sa.Integer(), nullable=True),
    sa.Column("deleted", sa.Integer(), nullable=True),
    sa.Column("created", sa.Integer(), nullable=False),
)


def timestamp_from_datetime_or_none(dt):
    """Convert datetime to timestamp
    and round to nearest second"""
    if dt is None:
        return None

    try:
        return round(datetime.timestamp(dt))
    except TypeError:
        print(f"Invalid datetime {dt}")
        traceback.print_exc(file=sys.stderr)
        return None


def upgrade():
    op.create_table(
        "stat_mentors",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "user_id", sa.String(length=200), nullable=False, unique=True
        ),
        sa.Column("display_name", sa.Unicode(length=30), nullable=False),
        sa.Column("last_seen", sa.Integer(), nullable=True),
        sa.Column("deleted", sa.Integer(), nullable=True),
        sa.Column("created", sa.Integer(), nullable=False),
    )

    connection = op.get_bind()
    populate_stat_mentors(connection)
    populate_deleted_stat_mentors(connection)


def downgrade():
    op.drop_table("stat_mentors")


def populate_stat_mentors(connection):
    """
    Populate stat_mentors
    """
    q = mentors_during_migration.select()
    rows = connection.execute(q)
    for mentor in rows:
        to_insert = {
            "id": anonymize_id(mentor.id),
            "user_id": anonymize_id(mentor.user_id),
            "display_name": mentor.display_name,
            "last_seen": timestamp_from_datetime_or_none(mentor.last_seen),
            "deleted": None,
            "created": timestamp_from_datetime_or_none(mentor.created),
        }
        insert_query = stat_mentors_during_migration.insert().values(to_insert)
        connection.execute(insert_query)


def populate_deleted_stat_mentors(connection):
    """
    Populate stat_mentors from already deleted messages
    """
    # get all anonymized user_ids of mentors from stat_messages
    q = (
        stat_messages_during_migration.select()
        .with_only_columns([stat_messages_during_migration.c.sender_id])
        .where(stat_messages_during_migration.c.from_role == "mentor")
        .group_by(stat_messages_during_migration.c.sender_id)
    )
    rows = connection.execute(q)
    for msg in rows:
        # find out when first and last msg was sent
        first_and_last_q = (
            stat_messages_during_migration.select()
            .with_only_columns(
                [
                    sa.func.max(
                        stat_messages_during_migration.c.created
                    ).label("max"),
                    sa.func.min(
                        stat_messages_during_migration.c.created
                    ).label("min"),
                ]
            )
            .where(stat_messages_during_migration.c.sender_id == msg.sender_id)
        )

        first_and_last_msg = connection.execute(first_and_last_q).fetchone()
        if not first_and_last_msg:
            continue

        to_insert = {
            "id": anonymize_id(
                new_ylitse_id()
            ),  # no way to get the deleted mentor_id anymore
            "user_id": msg.sender_id,  # user_id is already anonymized
            "display_name": "",
            "last_seen": first_and_last_msg.max,
            "deleted": first_and_last_msg.max,
            "created": first_and_last_msg.min,
        }
        # Make sure insert doesnt throw on duplicate.
        # We are already inside migration transaction and
        # I could not figure out how to do nested transaction here.
        insert_query = (
            sa.dialects.postgresql.insert(
                stat_mentors_during_migration, bind=connection
            )
            .values(to_insert)
            .on_conflict_do_nothing(index_elements=["user_id"])
        )

        connection.execute(insert_query)
