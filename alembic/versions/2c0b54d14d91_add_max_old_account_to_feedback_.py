"""add max_old_account to feedback_questions

Revision ID: 2c0b54d14d91
Revises: 033de5f6e261
Create Date: 2022-10-08 12:09:50.490501+00:00

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table
from sqlalchemy.dialects.postgresql import JSONB
from copy import deepcopy
from pydantic import ValidationError
from ylitse.schema.feedback import FeedbackQuestionCreate

# revision identifiers, used by Alembic.
revision = "2c0b54d14d91"
down_revision = "033de5f6e261"
branch_labels = None
depends_on = None


questions_during_migration = table(
    "feedback_questions",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("rules", JSONB(), nullable=False),
)


def upgrade():
    connection = op.get_bind()
    questions = connection.execute(questions_during_migration.select())

    for question in questions:
        copied = deepcopy(question.rules)
        if not hasattr(copied["schedule"]["first"], "max_old_account_in_days"):
            copied["schedule"]["first"].update({"max_old_account_in_days": 30})

        if copied["schedule"]["repetitions"][
            "type"
        ] == "predefined" and not hasattr(
            copied["schedule"]["repetitions"], "min_days_since_previous_answer"
        ):
            copied["schedule"]["repetitions"].update(
                {"min_days_since_previous_answer": 14}
            )

        try:
            compare = {"rules": copied}
            FeedbackQuestionCreate.parse_obj(compare)
        except ValidationError as e:
            print(e)
            continue

        update_query = (
            questions_during_migration.update()
            .where(questions_during_migration.c.id == question.id)
            .values({"rules": copied})
        )

        connection.execute(update_query)


def downgrade():
    connection = op.get_bind()
    questions = connection.execute(questions_during_migration.select())

    for question in questions:
        copied = deepcopy(question.rules)
        copied["schedule"]["first"].pop("max_old_account_in_days", None)

        copied["schedule"]["repetitions"].pop(
            "min_days_since_previous_answer", None
        )

        update_query = (
            questions_during_migration.update()
            .where(questions_during_migration.c.id == question.id)
            .values({"rules": copied})
        )

        connection.execute(update_query)
