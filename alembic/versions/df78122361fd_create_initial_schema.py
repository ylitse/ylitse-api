"""create initial schema

Revision ID: df78122361fd
Revises:
Create Date: 2021-03-21 17:34:39.342880

"""
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM, JSONB

from alembic import op


# revision identifiers, used by Alembic.
revision = "df78122361fd"
down_revision = None
branch_labels = None
depends_on = None

ROLE_ENUM = ENUM("mentee", "mentor", "admin", name="role")
GENDER_ENUM = ENUM("male", "female", "other", name="gender")
PLATFORM_ENUM = ENUM("android", "ios", name="platform")
CONTACT_STATUS_ENUM = ENUM("ok", "banned", "deleted", name="contact_status")


def upgrade():
    op.create_table(
        "accounts",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "login_name", sa.Unicode(length=100), nullable=False, unique=True
        ),
        sa.Column("role", ROLE_ENUM, nullable=False),
        sa.Column("email", sa.Unicode(length=200), nullable=True),
        sa.Column("phone", sa.Unicode(length=20), nullable=True),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "users",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column("display_name", sa.Unicode(length=100), nullable=False),
        sa.Column("role", ROLE_ENUM, nullable=False),
        sa.Column(
            "account_id", sa.String(length=200), nullable=False, unique=True
        ),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "contacts",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "user_id", sa.String(length=200), nullable=False, index=True
        ),
        sa.Column(
            "contact_user_id",
            sa.String(length=200),
            nullable=False,
            index=True,
        ),
        sa.Column("status", CONTACT_STATUS_ENUM, nullable=False, default="ok"),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "mentors",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "account_id", sa.String(length=200), nullable=False, unique=True
        ),
        sa.Column(
            "user_id", sa.String(length=200), nullable=False, unique=True
        ),
        sa.Column("languages", JSONB(), nullable=False),
        sa.Column("birth_year", sa.Integer(), nullable=False),
        sa.Column("display_name", sa.Unicode(length=30), nullable=False),
        sa.Column("gender", GENDER_ENUM, nullable=False),
        sa.Column("region", sa.Unicode(length=100), nullable=False),
        sa.Column("story", sa.Unicode(length=2000), nullable=False),
        sa.Column("skills", JSONB(), nullable=False),
        sa.Column("communication_channels", JSONB(), nullable=False),
        sa.Column(
            "is_vacationing",
            sa.Boolean(),
            nullable=False,
            default=False,
            server_default=sa.sql.expression.literal(False),
        ),
        sa.Column(
            "status_message",
            sa.Unicode(length=2000),
            nullable=False,
            server_default="",
        ),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "identities",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "login_name", sa.Unicode(length=100), nullable=False, unique=True
        ),
        sa.Column("password", sa.Unicode(length=100), nullable=False),
        sa.Column("role", ROLE_ENUM, nullable=False),
        sa.Column(
            "scopes",
            JSONB(),
            nullable=False,
        ),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "sessions",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column("login_name", sa.Unicode(length=100), nullable=False),
        sa.Column("account_id", sa.String(length=200), nullable=False),
        sa.Column("start_time", sa.Integer(), nullable=False),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "skills",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column("name", sa.Unicode(length=40), nullable=False, unique=True),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "messages",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "sender_id", sa.String(length=200), nullable=False, index=True
        ),
        sa.Column(
            "recipient_id", sa.String(length=200), nullable=False, index=True
        ),
        sa.Column("content", sa.Unicode(length=5000), nullable=False),
        sa.Column(
            "opened",
            sa.Boolean(),
            nullable=False,
            server_default=sa.sql.expression.literal(False),
        ),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )
    op.create_index(
        "messages_sender_created_ix",
        "messages",
        ["sender_id", "created"],
        unique=False,
    )
    op.create_index(
        "messages_recipient_created_ix",
        "messages",
        ["recipient_id", "created"],
        unique=False,
    )

    op.create_table(
        "devices",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "user_id", sa.String(length=200), nullable=False, index=True
        ),
        sa.Column("platform", PLATFORM_ENUM, nullable=False),
        sa.Column("token", sa.Unicode(length=300), nullable=False),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "private_datas",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "user_id", sa.String(length=200), nullable=False, unique=True
        ),
        sa.Column("starred_users", JSONB(), nullable=False),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "stat_accounts",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column("role", ROLE_ENUM),
        sa.Column("updated", sa.Integer()),
        sa.Column("created", sa.Integer(), index=True),
        sa.Column("active", sa.Boolean()),
        sa.Column("stat_last_seen", sa.Integer(), index=True),
    )

    op.create_table(
        "stat_messages",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column("from_role", ROLE_ENUM),
        sa.Column("sender_id", sa.String(length=200)),
        sa.Column("recipient_id", sa.String(length=200)),
        sa.Column("opened", sa.Boolean()),
        sa.Column("updated", sa.Integer()),
        sa.Column("created", sa.Integer(), index=True),
        sa.Column("active", sa.Boolean()),
        sa.Column("time_of_day_fi", sa.Integer()),
        sa.Column("time_of_day_utc", sa.Integer()),
        sa.Column("stat_last_seen", sa.Integer()),
    )


def downgrade():
    op.drop_table("stat_messages")
    op.drop_table("stat_accounts")
    op.drop_table("private_datas")
    op.drop_table("devices")
    op.drop_index("messages_recipient_created_ix", table_name="messages")
    op.drop_index("messages_sender_created_ix", table_name="messages")
    op.drop_table("messages")
    op.drop_table("skills")
    op.drop_table("sessions")
    op.drop_table("identities")
    op.drop_table("mentors")
    op.drop_table("contacts")
    op.drop_table("users")
    op.drop_table("accounts")
    ROLE_ENUM.drop(op.get_bind())
    GENDER_ENUM.drop(op.get_bind())
    PLATFORM_ENUM.drop(op.get_bind())
    CONTACT_STATUS_ENUM.drop(op.get_bind())
