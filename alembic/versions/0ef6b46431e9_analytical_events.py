"""analytical events

Revision ID: 0ef6b46431e9
Revises: c0512c6c65d4
Create Date: 2021-12-31 09:31:07.637402+00:00

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

# revision identifiers, used by Alembic.
revision = "0ef6b46431e9"
down_revision = "c0512c6c65d4"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "stat_events",
        sa.Column("id", sa.BigInteger, sa.Identity(), primary_key=True),
        sa.Column("name", sa.String(length=200), nullable=False, index=True),
        sa.Column(
            "props",
            JSONB(),
            nullable=False,
            server_default=sa.text("'{}'::jsonb"),
        ),
        sa.Column("created", sa.Integer(), nullable=False, index=True),
    )
    # Default size limit for jsonb is 1GB.
    # Make it smaller.
    # This is used for other events too but skill filtering
    # was first to use this. Calculate limit so that it can fit
    # other events too.
    # Skill name max length = 40
    # Worst case for 1 char is 4 bytes
    # 20_000 should fit 125 skills full of emojis
    max_len_in_bytes = 20_000
    op.execute(
        f""" ALTER TABLE stat_events
        ADD CONSTRAINT check_props_length
        CHECK (octet_length(props::text) <= {max_len_in_bytes}); """
    )
    # Check that props is an object
    op.execute(
        f""" ALTER TABLE stat_events
        ADD CONSTRAINT props_is_object
        CHECK (jsonb_typeof(props) = 'object'); """
    )


def downgrade():
    op.drop_table("stat_events")
