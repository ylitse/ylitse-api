"""encrypted message

Revision ID: 15b57df3f1d7
Revises: df78122361fd
Create Date: 2021-11-08 19:30:36.357557+00:00

"""
import os
import sys

from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table

# Path hack, for sibling directory
sys.path.insert(0, os.path.abspath("."))

from ylitse.config import config  # noqa, pylint: disable=wrong-import-position
from ylitse.symmetric_crypto_functions import SymmetricCryptoFunctions  # noqa, pylint: disable=wrong-import-position

# revision identifiers, used by Alembic.
revision = "15b57df3f1d7"
down_revision = "df78122361fd"
branch_labels = None
depends_on = None

# Schema to use when in the middle of migrations.
# There are both content and content_enc
messages_during_migration = table(
    "messages",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("content", sa.Unicode(length=5000)),
    sa.Column("content_enc", sa.LargeBinary()),
)

crypto = SymmetricCryptoFunctions(config["encryption_key_file"])

# How many rows to read at one time
LIMIT = 100


def encrypt_all_messages(connection):
    """ Encrypts every message """
    # Idea is to paginate LIMIT amount of rows, encrypt content and update.
    # No offset here, we can just select rows where content_enc is still null.
    # Keep going until there are no nulls in content_enc anymore.
    done = False
    while not done:
        q = (
            messages_during_migration.select()
            .where(messages_during_migration.c.content_enc.is_(None))
            .limit(LIMIT)
        )
        rows = connection.execute(q)

        processed = 0

        for row in rows:
            content_enc = crypto.encrypt_string(row.content)
            update_q = (
                messages_during_migration.update()
                .where(messages_during_migration.c.id == row.id)
                .values({"content_enc": content_enc})
            )
            connection.execute(update_q)
            processed += 1

        if processed == 0:
            done = True


def decrypt_all_messages(connection):
    """ Does the opposite of encrypt_all_messages """

    done = False
    while not done:
        q = (
            messages_during_migration.select()
            .where(messages_during_migration.c.content.is_(None))
            .limit(LIMIT)
        )
        rows = connection.execute(q)

        processed = 0

        for row in rows:
            content = crypto.decrypt_to_string(row.content_enc)
            update_q = (
                messages_during_migration.update()
                .where(messages_during_migration.c.id == row.id)
                .values({"content": content})
            )
            connection.execute(update_q)
            processed += 1

        if processed == 0:
            done = True


def upgrade():
    # Create column to hold the encrypted values
    # but it needs to be null by default during the migration
    op.add_column("messages", sa.Column("content_enc", sa.LargeBinary()))

    # LargeBinary aka bytea doesn't support setting length in postgres.
    # Create a check to limit max size.
    # Fernet encrypted messages are base64 encoded with some
    # extra bytes for the crypto stuff.
    # Largest size I got when experimenting was almost 27_000
    max_len_in_bytes = 30_000  # Not exact maximum, nice round number
    op.execute(f""" ALTER TABLE messages
        ADD CONSTRAINT check_content_length
        CHECK (octet_length(content_enc) <= {max_len_in_bytes}); """)

    connection = op.get_bind()
    encrypt_all_messages(connection)

    # All nulls should be gone at this point, add "NOT NULL"
    op.alter_column("messages", "content_enc", nullable=False)

    # Dont need decrypted messages anymore
    op.drop_column("messages", "content")


def downgrade():
    # Add back the column to store plain text messages
    # but it needs to be null by default during the migration
    op.add_column("messages", sa.Column("content", sa.Unicode(length=5000)))

    connection = op.get_bind()
    decrypt_all_messages(connection)

    # All nulls should be gone at this point, add "NOT NULL"
    op.alter_column("messages", "content", nullable=False)

    # Dont need encrypted messages anymore
    op.drop_column("messages", "content_enc")
