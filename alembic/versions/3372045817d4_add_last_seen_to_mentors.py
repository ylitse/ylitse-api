"""add last_seen to mentors

Revision ID: 3372045817d4
Revises: 0ef6b46431e9
Create Date: 2022-01-04 08:56:09.290753+00:00

"""
from datetime import datetime
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table


# revision identifiers, used by Alembic.
revision = "3372045817d4"
down_revision = "0ef6b46431e9"
branch_labels = None
depends_on = None

sessions_during_migration = table(
    "sessions",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("account_id", sa.String(length=200), nullable=False),
    sa.Column("start_time", sa.Integer(), nullable=False),
)

mentors_during_migration = table(
    "mentors",
    sa.Column("id", sa.String(length=200), primary_key=True),
    sa.Column("account_id", sa.String(length=200), nullable=False),
    sa.Column("last_seen", sa.DateTime(timezone=False), nullable=True),
)


def upgrade():
    op.add_column(
        "mentors",
        sa.Column("last_seen", sa.DateTime(timezone=False), nullable=True),
    )

    connection = op.get_bind()
    populate_last_seen(connection)


def downgrade():
    op.drop_column("mentors", "last_seen")


def populate_last_seen(connection):
    """
    Populate last_seen from most recent session
    """
    q = mentors_during_migration.select()
    rows = connection.execute(q)
    for mentor in rows:
        session_q = (
            sessions_during_migration.select()
            .where(sessions_during_migration.c.account_id == mentor.account_id)
            .order_by(sa.desc(sessions_during_migration.c.start_time))
            .limit(1)
        )
        session = connection.execute(session_q).fetchone()
        if not session:
            continue

        last_seen = datetime.utcfromtimestamp(session.start_time)

        update_q = (
            mentors_during_migration.update()
            .where(mentors_during_migration.c.id == mentor.id)
            .values({"last_seen": last_seen})
        )
        connection.execute(update_q)
