"""add feedback tables

Revision ID: 033de5f6e261
Revises: 6cf8fd7a1ff8
Create Date: 2022-05-08 08:21:55.173579+00:00

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB


# revision identifiers, used by Alembic.
revision = "033de5f6e261"
down_revision = "6cf8fd7a1ff8"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "feedback_questions",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "rules",
            JSONB(),
            nullable=False,
            server_default=sa.text("'{}'::jsonb"),
        ),
        sa.Column(
            "active", sa.Boolean(), server_default="true", nullable=False
        ),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )

    op.create_table(
        "feedback_answers",
        sa.Column("id", sa.String(length=200), primary_key=True),
        sa.Column(
            "feedback_question_id", sa.String(length=200), nullable=False
        ),
        sa.Column("user_id", sa.String(length=200), nullable=False),
        sa.Column("asked_at", sa.DateTime(timezone=False), nullable=True),
        sa.Column("retries", sa.Integer(), server_default="0", nullable=False),
        sa.Column(
            "max_retries", sa.Integer(), server_default="0", nullable=False
        ),
        sa.Column("value", sa.Integer(), nullable=True),
        sa.Column("active", sa.Boolean(), nullable=False),
        sa.Column("updated", sa.DateTime(timezone=False), nullable=False),
        sa.Column("created", sa.DateTime(timezone=False), nullable=False),
    )


def downgrade():
    op.drop_table("feedback_answers")
    op.drop_table("feedback_questions")
