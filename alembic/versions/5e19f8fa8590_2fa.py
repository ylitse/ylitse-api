"""2fa

Revision ID: 5e19f8fa8590
Revises: b5024f14d4cc
Create Date: 2023-08-08 10:51:54.387422+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5e19f8fa8590'
down_revision = 'b5024f14d4cc'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "identities",
        sa.Column("failed_login_count",
                  sa.Integer(),
                  default=0,
                  nullable=True),
    )
    op.add_column(
        "identities",
        sa.Column("mfa_secret", sa.Unicode(), nullable=True),
    )


def downgrade():
    op.drop_column("mentors", "mfa_secret")
    op.drop_column("mentors", "failed_login_count")
