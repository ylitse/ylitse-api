"""limit display_name to 30 chars

Revision ID: a1ca1aa08571
Revises: 15b57df3f1d7
Create Date: 2021-12-20 15:12:22.629761+00:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a1ca1aa08571"
down_revision = "15b57df3f1d7"
branch_labels = None
depends_on = None


def upgrade():
    # limit to 30 chars and truncate old content
    op.execute(
        f""" ALTER TABLE users
                ALTER COLUMN display_name TYPE VARCHAR(30)
                USING SUBSTR(display_name, 1, 30) """
    )


def downgrade():
    # previous validation was max 100 chars
    op.execute(
        f""" ALTER TABLE users
            ALTER COLUMN display_name TYPE VARCHAR(100) """
    )
